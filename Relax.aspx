﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Relax.aspx.vb" Inherits="Relax" %>

<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>





<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li class="active">Relax</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->


    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">Relax</h1>
                <p>
                    <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText" HTMLID="37" />
                </p>
                <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="248" />
            </section>
            <!-- -- welcome-text ends here -- -->
        </div>

        <!-- -- title-bar starts here -- -->
        <div class="title-bar purplebg">
            <div class="container">
                <h2 class="title">Relax at sir bani yas island</h2>
                <span class="arrow"></span>
            </div>
        </div>
        <!-- -- title-bar ends here -- -->

        <!-- -- page-breadcrumb starts here -- -->
        <div class="container">
            <div class="page-breadcrumb">
                <ul class="list-unstyled">
                    <li>
                        <a data-scroll href="#spa">Spa</a>
                    </li>
                    <li>
                        <a data-scroll href="#beaches">Beaches & pools
                        </a>
                    </li>

                </ul>
            </div>
        </div>
        <!-- -- page-breadcrumb ends here -- -->

        <!-- -- parallax-section starts here -- -->
        <div class="parallax-section">
            <ul class="list-unstyled">
                <li class="bluebg" id="spa">
                    <div class="image-container">
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage"  ShowEdit="false" HTMLID="38" ImageType="Small"/>
                        
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-8 col-md-6">
                                <div class="white-wrapper">
                                    <h3 class="h2 title">
                                        <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="38" />
                                    </h3>
                                    <p>
                                        <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText1" HTMLID="38" ImageEdit="True" ImageType="Small" IMGSmallHeight="446" IMGSmallWidth="1600"  />

                                    </p>
                                    <a href='<%= domainName & "Spa"%>' class="readmore-btn">View more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="purplebg" id="beaches">
                    <div class="image-container">
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1"  ShowEdit="false" HTMLID="39" ImageType="Small"/>
                       
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-offset-6  col-sm-8 col-md-6 col-sm-4">
                                <div class="white-wrapper">
                                    <h3 class="h2 title">
                                        <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle1" HTMLID="39" />
                                    </h3>
                                    <p>
                                        <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText2" HTMLID="39" ImageEdit="True" ImageType="Small" IMGSmallHeight="446" IMGSmallWidth="1600" />

                                    </p>
                                    <a href='<%= domainName & "Beaches"%>' class="readmore-btn">View more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <!-- -- parallax-section ends here -- -->
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns">
        <div class="container">
            <ul class="list-unstyled">
                <li class="purplebg">
                    <a href='<%= domainName & "PhotoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/photo-gallery.png" %>' height="25" width="29" alt=""></span>
                        Photo gallery
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "VideoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/video-gallery.png" %>' height="20" width="25" alt=""></span>
                        Video gallery
                    </a>
                </li>
                <li class="bluebg">
                    <a class="bookNowIframe" href='<%= domainName & "BookNow-Pop" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/book-now.png" %>' height="27" width="27" alt=""></span>
                        Book now
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "Map-Pop" %>' class="mapFancyIframe">
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/location-map.png" %>' height="26" width="32" alt=""></span>
                        Location map
                    </a>
                </li>
                <li class="purplebg">
                    <a href='<%= domainName & "Travel-Air"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/flights.png" %>' height="14" width="38" alt=""></span>
                        Flights
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "Sir-Bani-Yas-History" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/history.png" %>' height="20" width="27" alt=""></span>
                        History
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "WhatsNew" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/whats-new.png" %>' height="17" width="18" alt=""></span>
                        What's New
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
</asp:Content>

