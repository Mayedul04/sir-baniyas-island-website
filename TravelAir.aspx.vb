﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class TravelAir
    Inherits System.Web.UI.Page
    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            ltImgRotating.Text = GetImage()
        End If
    End Sub
    Public Function getExternalLink(ByVal htmlid As String) As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        'Dim sql = "SELECT * FROM List_NexaClientCategory WHERE Status='1' order by SortIndex"

        Dim sql = "SELECT * FROM [HTML] where HtmlID=@HtmlID"

        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Add("HtmlID", SqlDbType.Int, 32).Value = htmlid
        Dim reader = cmd.ExecuteReader()
        If reader.HasRows = True Then

            While reader.Read()

                M += "<p>"
                M += "<a href=""" & reader("Link").ToString() & """ target=""_blank"" style=""color:white;"">" & reader("Title").ToString() & "</a></p>" & Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&File=0&SmallDetails=0&BigDetails=0&smallimage=0&BigImage=0&ImageAltText=0&VideoLink=0&VideoCode=0&Map=0&SecondImage=0&Title=1&Link=1")
               

            End While

        End If
        reader.Close()
        con.Close()



        Return M
    End Function
    Public Function GetImage() As String
        Dim M As String = ""
       

        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        'Dim sql = "SELECT * FROM List_NexaClientCategory WHERE Status='1' order by SortIndex"

        Dim sql = "SELECT * FROM [GalleryItem] where GalleryItemID in ('2089','2090') "

        Dim cmd = New SqlCommand(sql, con)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then

                While reader.Read()

                    M += "<li>"
                    M += "<img src=""" & domainName & "Admin/" & reader("BigImage") & """>"
                    M += Utility.showEditButton(Request, domainName & "Admin/A-Gallery/GalleryItemEdit.aspx?galleryItemId=" & reader("GalleryItemID").ToString())
                    M += "</li>"

                End While
            
            End If
            reader.Close()
            con.Close()



            Return M
        Catch ex As Exception
            Return M
        End Try
    End Function



End Class
