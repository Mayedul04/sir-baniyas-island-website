﻿Imports System.Data.SqlClient

Partial Class EventSummary
    Inherits System.Web.UI.Page
    Public domainName, title, footertitle, link, category, galurl As String
    Public pgalid, vgalid As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Function getGalleryID(ByVal htmlid As Integer, fieldname As String) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select " & fieldname & " from HTML where  HTMLID=@HTMLID and Lang='en'"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("HTMLID", Data.SqlDbType.Int, 32).Value = htmlid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If IsDBNull(reader(fieldname)) = False Then
                    retstr = reader(fieldname).ToString()
                End If



            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function Testimonials() As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select  * from List_Testimonial where Status=1 and Lang=@Lang "
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        ' cmd.Parameters.Add("MasterID", Data.SqlDbType.NVarChar, 50).Value = 1
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<li><div class=""roundedbox orange inner""><div class=""thumbnail-round"">"
                retstr += "<img src=""" & domainName & "ui/media/dist/round/orange.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                retstr += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """  class=""normal-thumb"" alt=""""></div>"

                retstr += "<div class=""box-content""><h5 class=""title-box"">Testimonials</h5>"
                retstr += "<h6 class=""sub-title"">" & reader("TestimonialBy").ToString() & "</h6>"
                retstr += "<p>" & reader("BigDetails").ToString() & "</p>"
                retstr += "</div></div>"
                retstr += Utility.showEditButton(Request, domainName & "Admin/A-Testimonial/TestimonialEdit.aspx?cgid=" & reader("ID") & "&SmallImageWidth=139&SmallImageHeight=138&lang=en") & "</li>"


            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Sub LoadGallery(ByVal galid As Integer)
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Event-Summary"">Photos<span class=""arrow""></span></a>"
                galurl = domainName & "Admin/" & reader("BigImage").ToString()

            End While
        End If
        cn.Close()
    End Sub
    Public Sub LoadContent(ByVal id As Integer)
        Dim sConn As String
        Dim selectString1 As String = "Select PGalleryEnabled, VGalleryEnabled from List_Event where EventID=@EventID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("EventID", Data.SqlDbType.Int, 32).Value = id
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
              
                If reader("PGalleryEnabled") = True Then
                    DivPhotoGal.Visible = True
                    pgalid = getGalleryID(205, "GalleryID")

                    If pgalid <> 0 Then
                        LoadGallery(pgalid)
                    End If
                  

                    LoadGallery(pgalid)
                End If
                If reader("VGalleryEnabled") = True Then
                    DivVideoGal.Visible = True
                    vgalid = getGalleryID(206, "VGalleryID")
                    If vgalid <> 0 Then
                        lblVGalleryUrl.Text = "<a href=""" & domainName & "Videos/Gallery/" & vgalid & "/Event-Summary" & """ class=""linkbtn"">Video Gallery<span class=""arrow""></span></a>"
                    End If
                End If

               

            End While
        End If
        cn.Close()
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            LoadContent(1)
           

        End If
    End Sub
End Class
