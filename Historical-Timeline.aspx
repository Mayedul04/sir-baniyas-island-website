﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Historical-Timeline.aspx.vb" Inherits="Historical_Timeline" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>







<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Sir-Bani-Yas-Island"%>'>Sir Bani yas Island</a></li>
            <li class="active">Historical Timeline</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    
    
    <!-- Historical TImeline Section -->
    <section class="historicalTimelineSection">

        <!-- Title Section -->
        <div class="title">
            <div class="container">
                <h1>Welcome to Historical Timeline</h1>
            </div>
        </div>
        <!-- Title Section -->

        <!-- Slide 1 -->
        <section class="slide1 trigger">

            <!-- Top Text Section -->
            <div class="topTextSection">
                <h2>
         <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="179" />
                </h2>
                <p>
                    <uc1:UserHTMLSmallText runat="server" HTMLID="179" ShowEdit="false" ID="UserHTMLSmallText" />
                    <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="179" TextType="Small" ID="UserControlEditButton" />
                </p>
                        <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="292" />
            </div>
            <!-- Top Text Section -->

            <!-- TIme Line Graph Section -->
            <div class="timeLineGraphSection">

                    <div class="graphLineHolder">
                        <img src='<%= domainName & "ui/media/dist/timeline/slide1-timeline.png"%>' alt="" class="wow bounceInUp">

                        <!-- Bullet Point Left -->
                        <div class="timeLineBullets wow fadeInLeft" id="animate1" style="top:20px;">
                            <span class="greenbg"></span>
                            <div class="contentSection">
                                <h4>
                                    <uc1:UserHTMLTitle runat="server" ID="da" HTMLID="180"  ShowEdit="false"/>
                                </h4>
                                <p class="white">
                                    <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="180"  ShowEdit="false"/>
                  
                                </p>
                                 
                            </div>
                            <div class="imgSection">
                                <uc1:UserControlEditButton ID="UserControlEditButton2" runat="server" TitleEdit="true" TextEdit="true" HTMLID="180" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth=""  />
                                 <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="180" ImageWidth="151" ImageHeight="157" ImageType="Small" />
                              </div>
                        </div>
                        <!-- Bullet Point Left -->

                        <!-- Bullet Point Left -->
                        <div class="timeLineBullets big  wow fadeInLeft" style="top:180px;">
                            <span class="greenbg"></span>
                            <div class="contentSection">
                             <h4>
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle1" HTMLID="181"  ShowEdit="false"/>
                                </h4>
                                <p class="white">
                                     <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt1" HTMLID="181"  ShowEdit="false"/>
                                </p>
                                <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="181" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton1" />
                            </div>
                            <div class="imgSection">
                                
                               <!--  <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" HTMLID="181" ImageWidth="151" ImageHeight="157" ImageType="Small" /> -->
                            </div>
                        </div>
                        <!-- Bullet Point Left -->

                                                <!-- Bullet Point Left -->
                        <div class="timeLineBullets wow fadeInLeft" style="top:375px;">
                            <span class="greenbg"></span>
                            <div class="contentSection">
                                <h4>
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle2" HTMLID="184"  ShowEdit="false"/>
                                </h4>
                                <p class="white">
                                    <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt2" HTMLID="184"  ShowEdit="false"/>
                                </p>
                                 <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="184" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton3" />
                            </div>
                            <div class="imgSection">
                               
                               <!-- <uc1:UserHTMLImage runat="server" ID="UserHTMLImage4" HTMLID="184" ImageWidth="151" ImageHeight="157" ImageType="Small" /> -->
                            </div>
                        </div>
                        <!-- Bullet Point Left -->

                         <!-- Bullet Point Left -->
                        <div class="timeLineBullets big  wow fadeInLeft" style="top:575px;">
                            <span class="greenbg"></span>
                            <div class="contentSection">
                                <h4>
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle4" HTMLID="185"  ShowEdit="false"/>
                                </h4>
                                <p class="white">
                                   <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt3" HTMLID="185"  ShowEdit="false"/>
                                </p> 
                                <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="185" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton4" />
                            </div>
                            <div class="imgSection">
                               
                                <!--<uc1:UserHTMLImage runat="server" ID="UserHTMLImage5" HTMLID="185" ImageWidth="151" ImageHeight="157" ImageType="Small" /> -->
                            </div>
                        </div>
                        <!-- Bullet Point Left -->


                        <!-- Bullet Point Left -->
                        <div class="timeLineBullets right  wow fadeInRight" style="top:150px;">
                            <span class="greenbg"></span>
                            <div class="contentSection">
                                <h4>
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle5" HTMLID="182"  ShowEdit="false"/>
                                </h4>
                                <p class="white">
                                     <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt4" HTMLID="182"  ShowEdit="false"/>
                                </p>
<uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="182" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton5" />
                            </div>
                            <div class="imgSection">
                                
                                <!-- <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="182" ImageWidth="151" ImageHeight="157" ImageType="Small" /> -->
                                
                            </div>
                        </div>
                        <!-- Bullet Point Left -->

                        <!-- Bullet Point Left -->
                        <div class="timeLineBullets right big  wow fadeInRight" style="top:330px;">
                            <span class="greenbg"></span>
                            <div class="contentSection">
                                <h4>
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle6" HTMLID="183"  ShowEdit="false"/>
                                </h4>
                                <p class="white">
                                     <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt5" HTMLID="183"  ShowEdit="false"/>
                                </p>

                            </div>
                            <div class="imgSection">
                            
                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage3" HTMLID="183" ImageWidth="151" ImageHeight="157" ImageType="Small" />
                                <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="183" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton6" />
                            </div>
                        </div>
                        <!-- Bullet Point Left -->

                        <!-- Bullet Point Right -->
                        <div class="timeLineBullets right wow fadeInRight" style="top:600px;">
                            <span class="greenbg"></span>
                            <div class="contentSection">
                                <h4>
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle7" HTMLID="186"  ShowEdit="false"/>
                                </h4>
                                <p class="white">
                                    <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt6" HTMLID="186"  ShowEdit="false"/>
                                </p>
                            </div>
                            <div class="imgSection">
                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage6" HTMLID="186" ImageWidth="151" ImageHeight="157" ImageType="Small" />
                                <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="186" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton7" />
                            </div>
                        </div>
                        <!-- Bullet Point Right -->

                        <!-- Bullet Point Right -->
                        <div class="timeLineBullets right big wow fadeInRight" style="top:800px;">
                            <span class="greenbg"></span>
                            <div class="contentSection">
                                <h4>
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle8" HTMLID="187"  ShowEdit="false"/>
                                </h4>
                                <p class="white">
                                     <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt7" HTMLID="187"  ShowEdit="false"/>
                                </p>
                            </div>
                            <div class="imgSection">
                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage7" HTMLID="187" ImageWidth="151" ImageHeight="157" ImageType="Small" />
                                <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="187" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton8" />
                            </div>
                        </div>
                        <!-- Bullet Point Right -->


                    </div>

            </div>
            <!-- TIme Line Graph Section -->
        
        </section>
        <!-- Slide 1 -->

        <!-- Slide 2 -->
        <section class="slide2">

            <!-- TIme Line Graph Section -->
            <div class="timeLineGraphSection">

                    <div class="graphLineHolder">
                        <img src='<%= domainName & "ui/media/dist/timeline/slide2-timeline.png"%>' alt="" class="wow bounceInUp">

                        <!-- Bullet Point Left -->
                        <div class="timeLineBullets wow fadeInLeft" style="top:165px;">
                            <span class="orangebg"></span>
                            <div class="contentSection">
                                <h4 class="black">
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle9" HTMLID="189"  ShowEdit="false"/>
                                </h4>
                                <p class="black">
                                     <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt8" HTMLID="189"  ShowEdit="false"/>
                                </p>
                            </div>
                            <div class="imgSection">
                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage9" HTMLID="189" ImageWidth="151" ImageHeight="157" ImageType="Small" />
                                <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="189" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton9" />
                            </div>
                        </div>
                        <!-- Bullet Point Left -->

                        <!-- Bullet Point Left -->
                        <div class="timeLineBullets big  wow fadeInLeft" style="top:380px;">
                            <span class="orangebg"></span>
                            <div class="contentSection">
                                <h4 class="black">
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle10" HTMLID="190"  ShowEdit="false"/>
                                </h4>
                                <p class="black">
                                     <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt9" HTMLID="190"  ShowEdit="false"/>
                                </p>
                            </div>
                            <div class="imgSection">
                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage10" HTMLID="190" ImageWidth="151" ImageHeight="157" ImageType="Small" />
                                <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="190" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton10" />
                            </div>
                        </div>
                        <!-- Bullet Point Left -->

                       


                        

                        


                        <!-- Bullet Point Right -->
                        <div class="timeLineBullets right wow fadeInRight" style="top:550px;">
                            <span class="orangebg"></span>
                            <div class="contentSection">
                                <h4 class="black">
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle11" HTMLID="188"  ShowEdit="false"/>
                                </h4>
                                <p class="black">
                                     <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt10" HTMLID="188"  ShowEdit="false"/>
                                </p>
                            </div>
                            <div class="imgSection">
                               <uc1:UserHTMLImage runat="server" ID="UserHTMLImage8" HTMLID="188" ImageWidth="151" ImageHeight="157" ImageType="Small" />
                                <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="188" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton11" />
                            </div>
                        </div>
                        <!-- Bullet Point Right -->

                        <!-- Bullet Point Right -->
                        <div class="timeLineBullets right wow fadeInRight" style="top:200px;">
                            <span class="orangebg"></span>
                            <div class="contentSection">
                                <h4 class="black">
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle12" HTMLID="192"  ShowEdit="false"/>
                                </h4>
                                <p class="black">
                                     <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt11" HTMLID="192"  ShowEdit="false"/>
                                </p>
                            </div>
                            <div class="imgSection">
                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage12" HTMLID="192" ImageWidth="151" ImageHeight="157" ImageType="Small" />
                                <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="192" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton12" />
                            </div>
                        </div>
                        <!-- Bullet Point Right -->


                    </div>

            </div>
            <!-- TIme Line Graph Section -->

        </section>
        <!-- Slide 2 -->

        <!-- Slide 3 -->
        <section class="slide3">

            <!-- TIme Line Graph Section -->
            <div class="timeLineGraphSection">

                    <div class="graphLineHolder">
                        <img src='<%= domainName & "ui/media/dist/timeline/slide3-timeline.png"%>' alt="" class="wow bounceInUp">




                        <!-- Bullet Point Left -->
                        <div class="timeLineBullets wow fadeInLeft" style="top:180px;">
                            <span class="yellowbg"></span>
                            <div class="contentSection">
                                <h4>
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle13" HTMLID="191"  ShowEdit="false"/>
                                </h4>
                                <p class="white">
                                     <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt12" HTMLID="191"  ShowEdit="false"/>
                                </p>
                            </div>
                            <div class="imgSection">
                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage11" HTMLID="191" ImageWidth="151" ImageHeight="157" ImageType="Small" />
                                <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="191" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton13" />
                            </div>
                        </div>
                        <!-- Bullet Point Left -->


                        

                        <!-- Bullet Point Left -->
                        <div class="timeLineBullets left big  wow fadeInLeft" style="top:450px;">
                            <span class="yellowbg"></span>
                            <div class="contentSection">
                             <h4>
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle14" HTMLID="193"  ShowEdit="false"/>
                                </h4>
                                <p class="white">
                                     <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt13" HTMLID="193"  ShowEdit="false"/>
                                </p>
                            </div>
                            <div class="imgSection">
                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage13" HTMLID="193" ImageWidth="151" ImageHeight="157" ImageType="Small" />
                                <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="193" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton14" />
                            </div>
                        </div>
                        <!-- Bullet Point Right -->


                        <!-- Bullet Point Right -->
                        <div class="timeLineBullets right  wow fadeInRight" style="top:240px;">
                            <span class="yellowbg"></span>
                            <div class="contentSection">
                            <h4>
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle15" HTMLID="194"  ShowEdit="false"/>
                                </h4>
                                <p class="white">
                                    <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt14" HTMLID="194"  ShowEdit="false"/>
                                </p>
                            </div>
                            <div class="imgSection">
                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage14" HTMLID="194" ImageWidth="151" ImageHeight="157" ImageType="Small" />
                                <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="194" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton15" />
                            </div>
                        </div>
                        <!-- Bullet Point Right -->


                    </div>

            </div>
            <!-- TIme Line Graph Section -->

        </section>
        <!-- Slide 3 -->

        <!-- Slide 4 -->
        <section class="slide4">

            <!-- TIme Line Graph Section -->
            <div class="timeLineGraphSection">

                    <div class="graphLineHolder">
                        <img class="marginLeft" src='<%= domainName & "ui/media/dist/timeline/slide4-timeline.png"%>' alt="" class="wow bounceInUp">




                        <!-- Bullet Point Right -->
                        <div class="timeLineBullets right  wow fadeInRight" style="top:200px;">
                            <span class="purplebg"></span>
                            <div class="contentSection">
                                <h4 class="black">
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle16" HTMLID="198"  ShowEdit="false"/>
                                </h4>
                                <p class="black">
                                     <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt15" HTMLID="198"  ShowEdit="false"/>
                                </p>
                            </div>
                            <div class="imgSection">
                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage18" HTMLID="198" ImageWidth="151" ImageHeight="157" ImageType="Small" />
                                <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="198" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton16" />
                            </div>
                        </div>
                        <!-- Bullet Point Right -->

                        <!-- Bullet Point Left -->
                        <div class="timeLineBullets left big  wow fadeInLeft" style="top:380px;">
                            <span class="purplebg"></span>
                            <div class="contentSection">
                            <h4 class="black">
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle17" HTMLID="199"  ShowEdit="false"/>
                                </h4>
                                <p class="black">
                                     <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt16" HTMLID="199"  ShowEdit="false"/>
                                </p>
                            </div>
                            <div class="imgSection">
                                                            <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="199" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton17" />

                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage19" HTMLID="199" ImageWidth="151" ImageHeight="157" ImageType="Small" />
                            </div>
                        </div>
                        <!-- Bullet Point Right -->


                        <!-- Bullet Point Right -->
                        <div class="timeLineBullets right  wow fadeInRight" style="top:550px;">
                            <span class="purplebg"></span>
                            <div class="contentSection">
                            <h4 class="black">
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle18" HTMLID="200"  ShowEdit="false"/>
                                </h4>
                                <p class="black">
                                    <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt17" HTMLID="200"  ShowEdit="false"/>
                                </p>
                            </div>
                            <div class="imgSection">
                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage20" HTMLID="200" ImageWidth="151" ImageHeight="157" ImageType="Small" />
                                <uc1:UserControlEditButton runat="server" TitleEdit="true" TextEdit="true" HTMLID="200" TextType="Big" ImageEdit="true" ImageHeight="" ImageType="Small" ImageWidth="" ID="UserControlEditButton18" />
                            </div>
                        </div>
                        <!-- Bullet Point Right -->


                    </div>

            </div>
            <!-- TIme Line Graph Section -->

        </section>
        <!-- Slide 4 -->

        <!-- Slide 5 -->
        <section class="slide5">

            <!-- TIme Line Graph Section -->
            <div class="timeLineGraphSection">

                    <div class="graphLineHolder">
                        <img class="" style="margin-left:-1px" src='<%= domainName & "ui/media/dist/timeline/slide5-timeline.png"%>' alt="" class="wow bounceInDown ">

                        <div class="footerTitleSection wow pulse" data-wow-iteration="5">
                            <h1>
                                <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle19" HTMLID="201"  ShowEdit="false"/>
                            </h1>
                            <h2>
                                <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText22" HTMLID="201"  TitleEdit="false" />
                            </h2>
                        </div>
                        
                    </div>

            </div>
            <!-- TIme Line Graph Section -->

        </section>
        <!-- Slide 5 -->


    </section>
    <!-- Historical TImeline Section -->
</asp:Content>

