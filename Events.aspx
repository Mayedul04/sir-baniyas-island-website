﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Events.aspx.vb" Inherits="Events" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>

            <li class="active">Events</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">

        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                     <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="125" ShowEdit="false" />
                </h1>
                <p>
                                    <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="125" />
                </p>
                <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="205" />
            </section>
            <!-- -- welcome-text ends here -- -->
        </div>


    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->


    <section class="whitebg">

        <section class="titleBar greenbg">

            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2>Sir Bani YAs Island Events & Memories</h2>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>

        </section>

        <!-- Sir Baniyas Events memmories -->
        <div class="container">

            <!-- Land & Water Activites -->
            <div class="activitesContainerSection eventsHome">

                <!-- Bottom FIx -->
                <div class="bottomFix">

                    <div class="container">

                        <div class="row">

                            <!-- Land Activities -->
                            <div class="col-md-6 col-sm-6">
                                <div class="landActivitesSection eventsSection">

                                   <%= getFeaturedEvent() %>
                                    <%= Utility.showAddButton(Request, domainName & "Admin/A-Event/EventsEdit.aspx?Small=1&Big=1&Footer=1&Checkbox=1")%>
                                </div>
                                
                            </div>
                            <!-- Land Activities -->

                            <!-- Water Activities -->
                            <div class="col-md-6 col-sm-6">
                                <div class="waterActivitesSection eventsSection">

                                    <!-- Water Activites Slider -->
                                    <div class="waterActivitiesSlider">

                                        <ul class="slides">

                                            <%= OtherEvents() %>

                                        </ul>
                                       
                                    </div>
                                    <!-- Water Activites Slider -->

                                </div>
                                 <%= Utility.showAddButton(Request, domainName & "Admin/A-Event/EventsEdit.aspx?Small=1&Big=1&Footer=1&Checkbox=1")%>
                            </div>
                            <!-- Water Activities -->

                        </div>

                    </div>

                </div>
                <!-- Bottom FIx -->

            </div>
            <!-- Land & Water Activites -->

        </div>
        <!-- Sir Baniyas Events memmories -->

        <!-- Seperator -->
        <div class="seperator greenbg"></div>
        <!-- Seperator -->

        <!-- Plan Your Event Section -->
        <section class="planYourEventsSection">
            <section class="container">

                <h2 class="orangefont">Plan your Events</h2>

                <div class="row">

                    <ul class="eventPlanListing">

                       <%= PlanningEvents() %>

                    </ul>

                </div>

            </section>
        </section>
        <!-- Plan Your Event Section -->

    </section>



    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns" class="nomargintop">
        <div class="container">
            <ul class="list-unstyled">
                <li class="purplebg">
                    <a href='<%= domainName & "PhotoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/photo-gallery.png" %>' height="25" width="29" alt=""></span>
                        Photo gallery
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "VideoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/video-gallery.png" %>' height="20" width="25" alt=""></span>
                        Video gallery
                    </a>
                </li>
                <li class="bluebg">
                    <a class="bookNowIframe" href='<%= domainName & "BookNow-Pop" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/book-now.png" %>' height="27" width="27" alt=""></span>
                        Book now
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "Map-Pop" %>' class="mapFancyIframe">
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/location-map.png" %>' height="26" width="32" alt=""></span>
                        Location map
                    </a>
                </li>
                <li class="purplebg">
                    <a href='<%= domainName & "Travel-Air" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/flights.png" %>' height="14" width="38" alt=""></span>
                        Flights
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "Sir-Bani-Yas-History" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/history.png" %>' height="20" width="27" alt=""></span>
                        History
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "WhatsNew" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/whats-new.png" %>' height="17" width="18" alt=""></span>
                        What's New
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
</asp:Content>

