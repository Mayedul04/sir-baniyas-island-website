﻿Imports System.Data.SqlClient

Partial Class EventDetails
    Inherits System.Web.UI.Page
    Public domainName, title, footertitle, link, category, galurl As String
    Public pgalid, vgalid As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            LoadContent(Page.RouteData.Values("id"))
            
        End If
    End Sub
    
    Public Sub LoadContent(ByVal id As Integer)
        Dim sConn As String
        Dim selectString1 As String = "Select * from List_Event where EventID=@EventID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("EventID", Data.SqlDbType.Int, 32).Value = id
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                title = reader("Title").ToString()
                category = reader("Category")
                hdnEventName.Value = title
                If reader("PGalleryEnabled") = True Then
                    DivPhotoGal.Visible = True
                    If IsDBNull(reader("GalleryID")) = False Then
                        pgalid = reader("GalleryID").ToString.ToString()
                    Else
                        pgalid = 0
                    End If
                    ImgPhotoGal.ImageUrl = domainName & "Admin/" & reader("PhotoGalleryImage").ToString()
                    LoadGallery(pgalid)
                End If
                If reader("VGalleryEnabled") = True Then
                    DivVideoGal.Visible = True
                    If IsDBNull(reader("VGalleryID")) = False Then
                        vgalid = reader("VGalleryID").ToString.ToString()
                    Else
                        vgalid = 0
                    End If
                    ImgVGallery.ImageUrl = domainName & "Admin/" & reader("VideoGalleryImage").ToString()
                End If
                
                ' lblFooterText.Text = reader("OtherText").ToString() & Utility.showEditButton(Request, domainName & "Admin/A-Event/EventsEdit.aspx?EventID=" & reader("EventID") & "&Small=0&Big=0&Footer=1")
                footertitle = reader("OtherTitle").ToString()
                link = reader("Link").ToString()
                hdnMasterID.Value = reader("MasterID").ToString()
                ' ImgFooter.ImageUrl = domainName & "Admin/" & reader("OtherImage").ToString()

                'If Convert.ToDateTime(reader("EndDate")) > Convert.ToDateTime(reader("StartDate")) Then
                '    lblDate.Text = Convert.ToDateTime(reader("StartDate")).ToString("MMM dd, yyyy") & " to " & Convert.ToDateTime(reader("StartDate")).ToString("MMM dd, yyyy")
                'Else
                '    lblDate.Text = Convert.ToDateTime(reader("StartDate")).ToString("MMM dd, yyyy")
                'End If
                ImgDetails.ImageUrl = domainName & "Admin/" & reader("BigImage").ToString()
                If reader("Location").ToString() <> "" Then
                    lblLocation.Text = "<h6 class=""dateTitle""><span><img src=""" & domainName & "ui/media/dist/plan-your-events/location-icon.jpg""  alt=""""></span>" & reader("Location").ToString() & " </h6>"
                End If
                lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, domainName & "Admin/A-Event/EventsEdit.aspx?EventID=" & reader("EventID") & "&Small=0&Big=1&Footer=0")
                DynamicSEO.PageID = id

            End While
        End If
        cn.Close()
    End Sub
    Public Sub LoadGallery(ByVal galid As Integer)
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If count = 1 Then
                    galurl = domainName & "Admin/" & reader("BigImage").ToString()
                Else
                    lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Spa"">Photo Gallery<span class=""arrow""></span></a>"
                End If
                count += 1
            End While
        End If
        cn.Close()
    End Sub
    Public Function Testimonials() As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select  * from List_Testimonial where Status=1 and Lang=@Lang "
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        ' cmd.Parameters.Add("MasterID", Data.SqlDbType.NVarChar, 50).Value = 1
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<li><div class=""roundedbox orange inner""><div class=""thumbnail-round"">"
                retstr += "<img src=""" & domainName & "ui/media/dist/round/orange.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                retstr += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """  class=""normal-thumb"" alt=""""></div>"

                retstr += "<div class=""box-content""><h5 class=""title-box"">Testimonials</h5>"
                retstr += "<h6 class=""sub-title"">" & reader("TestimonialBy").ToString() & "</h6>"
                retstr += "<p>" & reader("BigDetails").ToString() & "</p>"
                retstr += "</div></div>"
                retstr += Utility.showEditButton(Request, domainName & "Admin/A-Testimonial/TestimonialEdit.aspx?cgid=" & reader("ID") & "&SmallImageWidth=139&SmallImageHeight=138&lang=en") & "</li>"


            End While
        End If
        cn.Close()
        Return retstr
    End Function

    Protected Sub btnRegister_Click(sender As Object, e As EventArgs) Handles btnRegister.Click
        hdnDate.Value = Date.Now
        If sdsRegis.Insert > 0 Then
            hdnDate.Value = Date.Now()
            Dim msg As String = "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
            msg += "<tr><td style=""border:1px solid #ccc;""><table width=""740"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
            msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">First Name</td>"
            msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtName.Text & "</td></tr>"
            msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Email Address</td>"
            msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtEmail.Text & "</td></tr>"
            msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Mobile</td>"
            msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtMobile.Text & "</td></tr>"
            msg += "</table></td></tr></table>"
            Utility.SendMail(txtName.Text, txtEmail.Text, "mayedul.islam@wvss.net", "", "", "Event Registration", msg)
        End If
    End Sub

    
End Class
