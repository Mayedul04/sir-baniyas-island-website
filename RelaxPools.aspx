﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="RelaxPools.aspx.vb" Inherits="RelaxPools" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Relax" %>'>Relax</a></li>
            <li class="active">Beaches & Pools</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="39" />
                </h1>
                <p>

                    <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="39" />
                    <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="39" ImageClass="pull-left" ImageHeight="300" ImageWidth="550" ImageStyle="margin-right: 20px; margin-bottom: 10px;" />
                    <a href='<%= domainName & "BookNow-Pop" %>' class="pull-right bookNowIframe" style="margin-bottom: 20px">
                        <img src='<%= domainName & "ui/media/dist/relax/bookspa.png" %>' alt="">
                    </a>
                    <asp:Literal ID="lblPhotoGallery" runat="server"></asp:Literal>
                    <%= galurl %>
                    <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="216" ImageType="Small" ImageClass="boxthumbnail" ImageHeight="135" ImageWidth="270" ShowEdit="false" />
                    <span class="arrow"></span></a>
                    <% If pgalid <> 0 Then%>
                    <%= Utility.showAddButton(Request, domainName & "Admin/A-Gallery/AllGalleryItemEdit.aspx?galleryId=" & pgalid & "&Lang=en")%>
                    <% End If%>
                    <uc1:UserControlEditButton runat="server" ID="editPhotoGallery" HTMLID="216" PGalleryEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="135" ImageWidth="270" />
                    
                </p>



                <span class="clearAll"></span>
                <%--  <p>
                    <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt1" HTMLID="44" />
                       
                </p>--%>


                <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="252" />

            </section>
            <!-- -- welcome-text ends here -- -->
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">
        <div class="container">

            <div class="footer-nav">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <a href='<%= domainName & "Spa" %>' class="purplebg">
                            <img src='<%= domainName & "ui/media/dist/icons/spa-btn.png" %>' alt="" style="margin-top: -6px">
                            Spa</a>
                    </div>
                    <%--<div class="col-sm-4">
                        <a href='<%= domainName & "Beaches"%>' class="greenbg">
                            <img src='<%= domainName & "ui/media/dist/icons/beach-btn.png" %>' alt="" style="margin-top: -5px">
                            BEaches & Pools</a>
                    </div>--%>
                </div>
            </div>
        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->
</asp:Content>

