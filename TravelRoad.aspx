﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="TravelRoad.aspx.vb" Inherits="TravelRoad" %>

<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Travel" %>'>Travel</a></li>
            <li class="active">Travel By Road</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">Travel By Road</h1>
                <p>
                    <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="62" />
               
                </p>
                <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="256" />
            </section>
            <!-- -- welcome-text ends here -- -->

            <div class="row">
                <div class="col-sm-8">
                    <h2 class="orangefont">
                        <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="68" />

                    </h2>
                    <p>
                        <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt1" HTMLID="68" />
                       
                    </p>
                    <h2 class="orangefont">
                        <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle1" HTMLID="69" />
                    </h2>
                    <p>
                        <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt2" HTMLID="69" />
                       
                    </p>
                    
                    <a href='<%= domainName & "Map-Pop" %>' class="bookingmourning bluefont mapFancyIframe">View in Google Map</a>
                </div>
                <div class="col-sm-4">
                    <div class="purplebg border-box">
                        <img src='<%= domainName & "ui/media/dist/travel/location-thumb.png" %>' alt="" class="boxthumbnail">
                        <span class="arrow"></span>
                        <a href='<%= domainName & "Map-Pop" %>' class="linkbtn mapFancyIframe">Location Map</a>
                    </div>
                    <div class="distances-widget bluebg">
                        <span class="widget-title">
                            <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle2" HTMLID="70" />
                        </span>
                        <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt3" HTMLID="70" />

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">
        <div class="container">

            <div class="footer-nav">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-2">
                        <a href='<%= domainName & "Travel-Air" %>' class="purplebg">
                            <img src='<%= domainName &"ui/media/dist/icons/air-btn.png"%>' alt="" style="margin-top: -6px">
                            By Air</a>
                    </div>
                    <div class="col-sm-4">
                        <a href='<%= domainName & "Travel-Sea" %>' class="bluebg">
                            <img src='<%= domainName &"ui/media/dist/icons/sea-btn.png"%>' alt="" style="margin-top: -5px">
                            By sea</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->
</asp:Content>

