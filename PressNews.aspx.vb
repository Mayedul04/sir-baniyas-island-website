﻿Imports System.Data.SqlClient

Partial Class PressNews
    Inherits System.Web.UI.Page
    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Function News() As String
        Dim retstr As String = ""
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select  * from List_News where Status=1 and Lang=@Lang and Category='Press' order by SortIndex"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<section class=""row"">"
                retstr += "<div class=""col-md-3""><div class=""orangebg border-box small"">"
                retstr += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ width=""213"" alt="""" class=""boxthumbnail"" style=""width:100%;height:auto"">"
                retstr += "<span class=""arrow""></span></div></div>"
                'md-12
                retstr += "<div class=""col-md-9""><h2 class=""orangefont title"" style=""text-transform:none;"">" & reader("Title").ToString() & "</h2>"
                retstr += "<h6 class=""greenfont subtitle"">Date " & Convert.ToDateTime(reader("ReportDate")).ToString("dd/MM/yyyy") & "</h6>"
                retstr += "<p>" & reader("SmallDetails").ToString() & "</p><ul class=""bottomLinks"">"

                If IsDBNull(reader("FileUploaded")) = False Then
                    retstr += "<li><a target=""_blank"" href=""" & domainName & "Admin/" & reader("FileUploaded").ToString() & """>"
                    retstr += "<i><img src=""ui/media/dist/media-center/pdf-icon.jpg"" alt=""""></i>Read the whole story</a></li>"
                End If
                retstr += "<li>" & Utility.showEditButton(Request, domainName & "admin/A-News/NewsEdit.aspx?nid=" & reader("NewsID") & "&Small=1&Big=1&Footer=1") & "</li></ul>"
                retstr += "</div></section>"

             
            End While
        End If
        retstr += Utility.showAddButton(Request, domainName & "admin/A-News/NewsEdit.aspx?cat=2&Small=1&Big=1&Footer=1")
        cn.Close()
        Return retstr
    End Function
End Class
