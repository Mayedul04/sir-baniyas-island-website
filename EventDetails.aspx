﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="EventDetails.aspx.vb" Inherits="EventDetails" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>
<%@ Register Src="~/CustomControl/UCHTMLImage.ascx" TagPrefix="uc1" TagName="UCHTMLImage" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Events" %>'>Events</a></li>
            <li class="active"><a href="javascript:;"><%= title %></a></li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->


    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">

        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital"><%= title %></h1>
                <div class="row">
                    <div class="col-md-8">

                        <%--  <h6 class="dateTitle">
                            <asp:Literal ID="lblDate" runat="server"></asp:Literal>
                        </h6>--%>

                        <asp:Literal ID="lblLocation" Visible="false" runat="server"></asp:Literal>

                        <asp:HiddenField ID="hdnMasterID" runat="server" />
                        <asp:Literal ID="lblDetails" runat="server"></asp:Literal>
                        <uc1:DynamicSEO runat="server" ID="DynamicSEO" PageType="Events" />
                    </div>

                    <div class="col-md-4">
                        <div class="imgHold marginBtm15">
                            <asp:Image ID="ImgDetails" Width="360" runat="server" />

                        </div>

                        <div class="row">

                            <div class="col-md-12" id="DivPhotoGal" visible="false" runat="server">
                                <div class="purplebg border-box normal">
                                    <asp:Image ID="ImgPhotoGal" runat="server" class="boxthumbnail" />
                                    <%= Utility.showEditButton(Request, domainName & "Admin/A-Event/EventsEdit.aspx?EventID=" & Page.RouteData.Values("id") & "&Small=0&Big=0&Footer=0&pg=1&vg=0")%>
                                    <%--<img src='<%= domainName & "ui/media/dist/plan-your-events/event-thumb-img1.jpg" %>' alt="" class="boxthumbnail">--%>
                                    <a href="<%= galurl %>" class="linkbtn">Photos <span class="arrow"></span></a>
                                    <asp:Literal ID="lblPhotoGallery" runat="server"></asp:Literal>

                                </div>
                                <% If pgalid <> 0 Then%>
                                <%= Utility.showAddButton(Request, domainName & "Admin/A-Gallery/AllGalleryItemEdit.aspx?galleryId=" & pgalid & "&Lang=en")%>
                                <% End If%>
                            </div>

                            <div class="col-md-12" id="DivVideoGal" visible="false" runat="server">
                                <div class="greenbg border-box normal">
                                    <asp:Image ID="ImgVGallery" runat="server" class="boxthumbnail" />
                                    <%= Utility.showEditButton(Request, domainName & "Admin/A-Event/EventsEdit.aspx?EventID=" & Page.RouteData.Values("id") & "&Small=0&Big=0&Footer=0&pg=0&vg=1")%>
                                    <%-- <img src='<%= domainName &"ui/media/dist/plan-your-events/event-thumb-img1.jpg"%>' alt="" class="boxthumbnail">--%>
                                    <a href='<%= domainName & "Videos/Gallery/" & vgalid & "/" & Page.RouteData.Values("title") %>' class="linkbtn">Videos <span class="arrow"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- -- welcome-text ends here -- -->
        </div>


    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->


    <section class="container">

        <div class="row">
            <div class="col-sm-6">
                <!-- Testimonial Slider -->
                <div class="testimonialSlider">
                    <ul class="list-unstyled slides">
                        <%= Testimonials() %>
                    </ul>
                    <%= Utility.showAddButton(Request, domainName & "Admin/A-Event/TestimonialEdit.aspx?" & "SmallImageWidth=139&SmallImageHeight=138&lang=en")%>
                </div>
                <!-- Testimonial Slider -->
            </div>
            <div class="col-sm-6">
                <!-- -- roundedbox starts here -- -->
                <div class="roundedbox orange inner">
                    <div class="thumbnail-round">
                        <img src='<%= domainName & "ui/media/dist/round/orange.png" %>' height="113" width="122" alt="" class="roundedcontainer">
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage3" ShowEdit="false" HTMLID="6" imageType="Small" ImageClass="normal-thumb" ImageHeight="129" ImageWidth="151" />

                    </div>
                    <div class="box-content">
                        <h5 class="title-box"><a href='<%= domainName & "Adventure" %>'>Activities
                        </a>

                        </h5>
                        <p>
                            <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText1" HTMLID="6" ShowEdit="false" />
                        </p>
                        <a href='<%= domainName & "Adventure" %>' class="readmorebtn">Read more</a>
                    </div>
                    <uc1:UserControlEditButton runat="server" ID="EditBtnEventEnquiry" HTMLID="6" TitleEdit="true" TextEdit="true" TextType="Small" ImageEdit="true" ImageType="Small" ImageHeight="170" ImageWidth="170" />
                </div>

                <!-- -- roundedbox ends here -- -->
            </div>
        </div>
        <asp:Panel ID="pnlRegister" Visible="False" runat="server">
            <div class="register">
                <h2 class="capital h1" style="margin-top: -40px">Register</h2>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="formgroup">
                            <label for="" class="form-label">
                                Name
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="form5" ControlToValidate="txtName" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </label>
                            <asp:TextBox ID="txtName" class="form-control" runat="server"></asp:TextBox>

                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="formgroup">
                            <label for="" class="form-label">
                                Email
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="form5" ControlToValidate="txtEmail" ErrorMessage="*"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="form5" ControlToValidate="txtEmail" ErrorMessage="Invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </label>
                            <asp:TextBox ID="txtEmail" class="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="formgroup">
                            <label for="" class="form-label">
                                Mobile
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="form5" ControlToValidate="txtMobile" ErrorMessage="*"></asp:RequiredFieldValidator>

                            </label>

                            <asp:TextBox ID="txtMobile" class="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-sm-3 col-sm-offset-9">
                        <asp:Button ID="btnRegister" class="btnorange" ValidationGroup="form5" runat="server" Text="Register" />
                        <asp:HiddenField ID="hdnEventName" runat="server" />
                        <asp:HiddenField ID="hdnDate" runat="server" />
                        <asp:SqlDataSource ID="sdsRegis" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            InsertCommand="INSERT INTO [EventRegistration] ([Event], [Name], [Email], [Mobile], [Date1]) VALUES (@Event, @Name, @Email, @Mobile, @Date1)">

                            <InsertParameters>
                                <asp:ControlParameter ControlID="hdnEventName" Name="Event" PropertyName="Value" Type="String" />
                                <asp:ControlParameter ControlID="txtName" Name="Name" PropertyName="Text" Type="String" />
                                <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" Type="String" />
                                <asp:ControlParameter ControlID="txtMobile" Name="Mobile" PropertyName="Text" Type="String" />
                                <asp:ControlParameter ControlID="hdnDate" Name="Date1" PropertyName="Value" Type="DateTime" />
                            </InsertParameters>

                        </asp:SqlDataSource>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </section>


    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns" class="nomargintop">
        <div class="container">
            <ul class="list-unstyled">
                <li class="purplebg">
                    <a href='<%= domainName & "PhotoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/photo-gallery.png" %>' height="25" width="29" alt=""></span>
                        Photo gallery
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "VideoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/video-gallery.png" %>' height="20" width="25" alt=""></span>
                        Video gallery
                    </a>
                </li>
                <li class="bluebg">
                    <a class="bookNowIframe" href='<%= domainName & "BookNow-Pop" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/book-now.png" %>' height="27" width="27" alt=""></span>
                        Book now
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "Map-Pop" %>' class="mapFancyIframe">
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/location-map.png" %>' height="26" width="32" alt=""></span>
                        Location map
                    </a>
                </li>
                <li class="purplebg">
                    <a href='<%= domainName & "Travel-Air" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/flights.png" %>' height="14" width="38" alt=""></span>
                        Flights
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "Sir-Bani-Yas-History" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/history.png" %>' height="20" width="27" alt=""></span>
                        History
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "WhatsNew" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/whats-new.png" %>' height="17" width="18" alt=""></span>
                        What's New
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
</asp:Content>

