<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Search.aspx.vb" Inherits="Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li class="active"><a href="#">Search Result</a></li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->


    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">

        <div class="container">
            <asp:Panel CssClass="search-div" ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                <asp:TextBox ID="txtSearch" type="search" runat="server" placeholder="Search..." ValidationGroup="Search1" CssClass="form-control"></asp:TextBox>
                <asp:Button ID="btnSearch" runat="server" CssClass="searchButton" ValidationGroup="Search1" Text="Search"></asp:Button>
            </asp:Panel>
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">

                <h1 class="capital">search results</h1>

                <!-- Search Result Listings -->
                <asp:Literal ID="lblData" runat="server"></asp:Literal>
                <!-- Search Result Listings -->

                <!-- page-shifter starts here
               ---------------------------------------------- -->
                <nav class="pagi">
                    <div class="container">
                        <div class="page-shifter nomarginBtm">
                            <asp:Literal ID="lbPagingBottom" runat="server"></asp:Literal>
                        </div>
                    </div>
                </nav>

                <!-- page-shifter ends here
               ---------------------------------------------- -->

            </section>
            <!-- -- welcome-text ends here -- -->
        </div>


    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->


    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns">
        <div class="container">
             <ul class="list-unstyled">
                <li class="purplebg">
                    <a href='<%= domainName & "PhotoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/photo-gallery.png" %>' height="25" width="29" alt=""></span>
                        Photo gallery
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "VideoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/video-gallery.png" %>' height="20" width="25" alt=""></span>
                        Video gallery
                    </a>
                </li>
                <li class="bluebg">
                    <a class="bookNowIframe" href='<%= domainName & "BookNow-Pop" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/book-now.png" %>' height="27" width="27" alt=""></span>
                        Book now
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "Map-Pop" %>' class="mapFancyIframe">
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/location-map.png" %>' height="26" width="32" alt=""></span>
                        Location map
                    </a>
                </li>
                <li class="purplebg">
                    <a href='<%= domainName & "Travel-Air" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/flights.png" %>' height="14" width="38" alt=""></span>
                        Flights
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "Sir-Bani-Yas-History" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/history.png" %>' height="20" width="27" alt=""></span>
                        History
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "WhatsNew" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/whats-new.png" %>' height="17" width="18" alt=""></span>
                        What's New
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
</asp:Content>

