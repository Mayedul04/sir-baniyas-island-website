﻿Imports System.Data.SqlClient

Partial Class News
    Inherits System.Web.UI.Page
    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Function getFeaturedNews() As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select top 1.*  from List_News where Status=1 and Lang=@Lang and Featured=1 and Category='Island' order by SortIndex, LastUpdated desc"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<div class=""row""><div class=""col-md-6"">"
                retstr += "<p>" & reader("SmallDetails").ToString() & "</p>" & Utility.showEditButton(Request, domainName & "Admin/A-News/NewsEdit.aspx?nid=" & reader("NewsID") & "&Small=1&Big=1&Footer=1")
                retstr += Utility.showAddButton(Request, domainName & "Admin/A-News/NewsEdit.aspx?cat=1&Small=1&Big=1&Footer=1")
                retstr += "</div><div class=""col-md-6""><div class=""imgHold""><img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ width=""263"" alt=""""></div>"
                retstr += "<a class=""readmoreBttn purple"" href=""" & domainName & "Featured-News/" & reader("NewsID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """>Read more</a></div></div>"
            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function WhatNewImages() As String
        Dim retstr As String = ""
        Dim count As Integer = 0
        Dim sConn As String
        Dim selectString1 As String = "Select top 4 SmallImage from List_New where Status=1 and Lang=@Lang order by LastUpdated desc, SortIndex"
        ' Dim selectString1 As String = "Select top 4 SmallImage from List_News where Status=1 and Lang=@Lang and Featured=0 and Category='Island'"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If count = 0 Then
                    retstr += "<li class=""active"">"
                Else
                    retstr += "<li>"
                End If
                retstr += "<div class=""imgHold""><img src=""" & domainName & "Admin/" & reader("SmallImage") & """ width=""132"" alt=""""><span></span></div></li>"

                count += 1
            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function WhatNewContent() As String
        Dim retstr As String = ""
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select top 4. * from List_New where Status=1 and Lang=@Lang order by LastUpdated desc, SortIndex"
        ' Dim selectString1 As String = "Select top 4. * from List_News where Status=1 and Lang=@Lang and Featured=0 and Category='Island'"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<div class=""content-" & count & " contents"">"
                retstr += "<h2>" & reader("Title").ToString() & "</h2>"
                retstr += "<p>" & reader("SmallDetails").ToString() & "</p>"

                retstr += "<a href=""" & domainName & "Whats-New-Details/" & reader("ListID").ToString() & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """ class=""readmoreBttn orange"">Read more</a>" & Utility.showEditButton(Request, domainName & "Admin/A-What-New/WhatNewEdit.aspx?nid=" & reader("ListID") & "&Small=1&Big=0&Footer=1&smallImageWidth=132&smallImageHeight=129") & "</div>"
                ' retstr += "<a href=""" & domainName & "What-New-Details/" & reader("NewsID").ToString() & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """ class=""readmoreBttn orange"">Read more ></a>" & Utility.showEditButton(Request, domainName & "Admin/A-News/NewsEdit.aspx?nid=" & reader("NewsID") & "&Small=1&Big=0&Footer=0") & "</div>"
                count += 1
            End While
        End If
        cn.Close()
        retstr += Utility.showAddButton(Request, domainName & "Admin/A-What-New/WhatNewEdit.aspx?Small=1&Big=1&Footer=1")
        Return retstr
    End Function
End Class
