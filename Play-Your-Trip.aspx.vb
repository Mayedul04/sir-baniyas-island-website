﻿Imports System.Data.SqlClient
Partial Class Play_Your_Trip
    Inherits System.Web.UI.Page
    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Public Function HtmlText() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where HTMLID=173"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read

                M += " <h1 class=""capital"">Plan Your Trip - " & Page.RouteData.Values("t") & "</h1>"
                M += reader("BigDetails")
                M += Utility.showEditButton(Request, domainName & "Admin/A-OthersHTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")


            End While

        End If
        conn.Close()
        Return M
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ltHTML.Text = HtmlText()
        'ltRelax.Text = RelaxList()
        'ltDine.Text = DineList()
        'ltStay.Text = StayList()
        'ltTravel.Text = TravelList()
        GetValues()

    End Sub

    Public Function GetValues() As String

        If Page.RouteData.Values("t") = "Family-Fun" Then
            ltAdventure.Text = AdventureList("8,10,5,2")
            ltDine.Text = DineList("2,3,6")
            ltStay.Text = StayList("1")
        ElseIf Page.RouteData.Values("t") = "Rest-Relaxation" Then
            ltRelax.Text = RelaxList("38,39")
            ltDine.Text = DineList("7,8,10")
            ltStay.Text = StayList("18,19")
        ElseIf Page.RouteData.Values("t") = "Active-Adventure" Then
            ltAdventure.Text = AdventureList("1,2,5,6,7,8,9,10,11,12,14,15,16,17")
            ltDine.Text = DineList("1,3,7,8,10")
            ltStay.Text = StayList("4,17,18,19")
        ElseIf Page.RouteData.Values("t") = "Romantic-Getaway" Then
            ltAdventure.Text = AdventureList("5,6,12")
            ltRelax.Text = RelaxList("38,39")
            ltDine.Text = DineList("1,10")
            ltStay.Text = StayList("18,19")

        ElseIf Page.RouteData.Values("t") = "Cultural-Explorer" Then
            ltAdventure.Text = AdventureList("2,8,10")
            ltDine.Text = DineList("1,17")
            ltStay.Text = StayList("4,17,18,19")

        ElseIf Page.RouteData.Values("t") = "Foodie-Frenzy" Then

            ltDine.Text = DineList("1,2,3,5,6,7,8,9,10")
            ltStay.Text = StayList("18,19")



        End If


    End Function

    Public Function RelaxList(ByVal id As String) As String
        Dim M As String = ""
        Dim x As String() = id.Split(",")

        Dim i As String

        For Each i In x

        Next



        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "Select HtmlID,Title,BigDetails,LinkTitle,SmallImage,SmallDetails,ImageAltText,SecondImage from [dbo].[HTML] where Lang='en'  and HtmlID in (" + id + ")"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("HtmlID", Data.SqlDbType.NVarChar)
        'cmd.Parameters("HtmlID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then

            M += "<div class=""title-bar dbluebg"">"
            M += "<div class=""container"">"
            M += " <h2 class=""title"">Relax</h2>"
            M += "<span class=""arrow""></span>"
            M += "</div>"
            M += " </div>"
            M += " <div class=""container"">"
            M += "<ul class=""searchResultsListings newsLetter"">"
            While reader.Read


                M += "<li>"

                M += "<div class=""imgHold"">"
                If IsDBNull(reader("SecondImage")) Or reader("SecondImage").ToString = "" Then
                    M += "<img src=""" & domainName & "Admin/Content/noimagefound.jpg"" alt="""">"
                    M += "</img>"
                Else
                    M += "<img src=""" & domainName & "Admin/" & reader("SecondImage") & """ alt=""" & reader("ImageAltText") & """>"
                    M += "</img>"
                End If
                M += "</div>"


                M += "<div class=""content"">"
                M += "<h2 class=""dbluefont"">" & reader("Title") & " </h2>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a class=""viewButton dbluebg"" href=""" & domainName & reader("LinkTitle") & """>View</a>"
                M += "</div>"

                M += "</li>"

                M += Utility.showEditButton(Request, domainName & "Admin/A-OthersHTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")


            End While
            M += "</ul>"
            M += "</div>"
        End If
        conn.Close()
        Return M
    End Function

    Public Function TravelList(ByVal id As String) As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "Select HtmlID,Title,BigDetails,LinkTitle,SmallImage,SmallDetails,ImageAltText from [dbo].[HTML] where Lang='en' and Status=1 and  HtmlID in (" + id + ")"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        ' cmd.Parameters.Add("HtmlID", Data.SqlDbType.NVarChar)
        ' cmd.Parameters("HtmlID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then

            M += "<div class=""title-bar greenbg"">"
            M += "<div class=""container"">"
            M += " <h2 class=""title"">Travel</h2>"
            M += "<span class=""arrow""></span>"
            M += "</div>"
            M += " </div>"
            M += " <div class=""container"">"
            M += "<ul class=""searchResultsListings newsLetter"">"
            While reader.Read


                M += "<li>"

                M += "<div class=""imgHold"">"
                If IsDBNull(reader("SmallImage")) Or reader("SmallImage").ToString = "" Then
                    M += "<img src=""" & domainName & "Admin/Content/noimagefound.jpg"" alt="""">"
                    M += "</img>"
                Else
                    M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """>"
                    M += "</img>"
                End If
                M += "</div>"


                M += "<div class=""content"">"
                M += "<h2>" & reader("Title") & " </h2>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a class=""viewButton greenbg"" href=""" & domainName & reader("LinkTitle") & """>View</a>"
                M += "</div>"

                M += "</li>"

                M += Utility.showEditButton(Request, domainName & "Admin/A-OthersHTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")


            End While
            M += "</ul>"
            M += "</div>"
        End If
        conn.Close()
        Return M
    End Function

    Public Function DineList(ByVal id As String) As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "Select ListID,Title,BigDetails,SmallImage,BigImage,SmallDetails,ImageAltText from List_Dine where Lang='en' and Status=1 and ListID in (" + id + ")"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("ListID", Data.SqlDbType.NVarChar)
        'cmd.Parameters("ListID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then

            M += "<div class=""title-bar purplebg"">"
            M += "<div class=""container"">"
            M += " <h2 class=""title"">Dine</h2>"
            M += "<span class=""arrow""></span>"
            M += "</div>"
            M += " </div>"
            M += " <div class=""container"">"
            M += "<ul class=""searchResultsListings newsLetter"">"
            While reader.Read


                M += "<li>"

                M += "<div class=""imgHold"">"
                If IsDBNull(reader("SmallImage")) Or reader("SmallImage") = "" Then
                    M += "<img src=""" & domainName & "Admin/Content/noimagefound.jpg"" alt="""">"
                    M += "</img>"
                Else
                    M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """>"
                    M += "</img>"
                End If
                M += "</div>"


                M += "<div class=""content"">"
                M += "<h2 class=""purplefont"">" & reader("Title") & " </h2>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a class=""viewButton purplebg"" href=""" & domainName & "Dine"">View</a>"
                M += "</div>"

                M += "</li>"

                M += Utility.showEditButton(Request, domainName & "Admin/A-Dine/ListEdit.aspx?lid=" + reader("ListID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")


            End While
            M += "</ul>"
            M += "</div>"
        End If
        conn.Close()
        Return M
    End Function

    Public Function StayList(ByVal id As String) As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "Select RestaurantID,Title,BigDetails,SmallImage,SmallDetails,ImageAltText,SmallImage,LogoSmall from List_Restaurant where Lang='en' and Status=1 and RestaurantID in (" + id + ")"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("RestaurantID", Data.SqlDbType.NVarChar)
        'cmd.Parameters("RestaurantID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then

            M += "<div class=""title-bar yellowbg"">"
            M += "<div class=""container"">"
            M += " <h2 class=""title"">Stay</h2>"
            M += "<span class=""arrow""></span>"
            M += "</div>"
            M += " </div>"
            M += " <div class=""container"">"
            M += "<ul class=""searchResultsListings newsLetter"">"
            While reader.Read


                M += "<li>"

                M += "<div class=""imgHold"">"
                If IsDBNull(reader("LogoSmall")) Or reader("LogoSmall").ToString = "" Then
                    M += "<img src=""" & domainName & "Admin/Content/noimagefound.jpg"" alt="""">"
                    M += "</img>"
                Else
                    M += "<img src=""" & domainName & "Admin/" & reader("LogoSmall") & """ alt=""" & reader("ImageAltText") & """>"
                    M += "</img>"
                End If
                M += "</div>"


                M += "<div class=""content"">"
                M += "<h2 class=""yellowfont"">" & reader("Title") & " </h2>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a class=""viewButton yellowbg"" href=""" & domainName & "Stay-Details/" & reader("RestaurantID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>View</a>"
                M += "</div>"

                M += "</li>"

                M += Utility.showEditButton(Request, domainName & "Admin/A-Stay/StayEdit.aspx?lid=" + reader("RestaurantID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")


            End While
            M += "</ul>"
            M += "</div>"
        End If
        conn.Close()
        Return M
    End Function

    Public Function AdventureList(ByVal id As String) As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "Select * from List_Advanture_Activity where Lang = 'en' and Status=1 and ListID in ( " + id + " )"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("ListID", Data.SqlDbType.NVarChar)
        'cmd.Parameters("ListID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then

            M += "<div class=""title-bar orangebg"">"
            M += "<div class=""container"">"
            M += " <h2 class=""title"">Adventure</h2>"
            M += "<span class=""arrow""></span>"
            M += "</div>"
            M += " </div>"
            M += " <div class=""container"">"
            M += "<ul class=""searchResultsListings newsLetter"">"
            While reader.Read


                M += "<li>"

                M += "<div class=""imgHold"">"
                If IsDBNull(reader("SmallImage")) Or reader("SmallImage") = "" Then
                    M += "<img src=""" & domainName & "Admin/Content/noimagefound.jpg"" alt="""">"
                    M += "</img>"
                Else
                    M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """>"
                    M += "</img>"
                End If
                M += "</div>"


                M += "<div class=""content"">"
                M += "<h2 class=""orangebfont"">" & reader("Title") & " </h2>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a class=""viewButton orangebg"" href=""" & domainName & "Adventure-Activity/" & reader("ListID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>View</a>"
                M += "</div>"

                M += "</li>"

                M += Utility.showEditButton(Request, domainName & "Admin/A-AdvantureActivity/ListEdit.aspx?lid=" + reader("ListID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")


            End While
            M += "</ul>"
            M += "</div>"
        End If
        conn.Close()
        Return M
    End Function

End Class
