﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="philosophy.aspx.vb" Inherits="philosophy" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>

<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>

<%@ Register Src="~/CustomControl/UCStaySirBaniYas.ascx" TagPrefix="uc1" TagName="UCStaySirBaniYas" %>
<%@ Register Src="~/CustomControl/UCHistoricalTimeLine.ascx" TagPrefix="uc1" TagName="UCHistoricalTimeLine" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%=domainName%>'>Home</a></li>

            <li class="active">Philosophy</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">

                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="121" ShowEdit="false"  />
                </h1>

                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" ShowEdit ="false" HTMLID="121" ImageType ="Small" ImageClass="pull-left" ImageStyle="margin-right:20px;margin-bottom:20px;"  />
                <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="121" />
                <uc1:UserControlEditButton runat="server" ID="UserControlEditButton" HTMLID ="121" TitleEdit="true" ImageEdit="true" ImageType="Small" ImageHeight ="250" ImageWidth ="470" TextEdit ="true"  TextType="Big"       />

            </section>
            <uc1:StaticSEO runat="server" ID="StaticSEO"  SEOID="199"/>
            <!-- -- welcome-text ends here -- -->


        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->



    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">

        <div class="container">

            <div class="row">

                <div class="col-sm-6">
                    <!-- -- double-box starts here -- -->
                    <div class="double-box greenbg">
                       <div class="twopanel">
                            <h3 class="title">
                                History
                            </h3>
                            
                            <p>
                                <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText" HTMLID="123" ShowEdit="false"    />
                            </p>
                            <span class="arrow"></span>
                            <br/>
                            <a href='<%= domainName & "Sir-Bani-Yas-History"%>' class="readmorebtn">Read more</a>
                        </div>
                          
                        <!-- -- twopanel ends here -- -->
                        
                        <!-- -- twopanel imageholder starts here -- -->
                        <div class="twopanel imageholder">
                             <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" HTMLID="123"  ImageType="Small" ImageWidth="307" ShowEdit ="false" />
                            
                        </div>
                       
                    </div>
                     <uc1:UserControlEditButton runat="server" ID="UserControlEditButton1" HTMLID="123" TextEdit="true" TextType="Small" ImageEdit="true" ImageType="Small" ImageHeight="233" ImageWidth="478" />
                   
                   <!-- -- double-box ends here -- -->
                </div>
                <div class="col-sm-6">


                    <asp:Literal ID="ltLocationMap" runat="server"></asp:Literal>
                    <uc1:UserControlEditButton runat="server" ID="EditMap" HTMLID="75" TitleEdit="true" ImageEdit="true" ImageType="Big" ImageHeight="233" ImageWidth="555" TextEdit="true"  TextType="Small"/>



                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox blue inner">
                        
                        <uc1:UCStaySirBaniYas runat="server" ID="UCStaySirBaniYas" />

                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox orange inner">
                        

                        <uc1:UCHistoricalTimeLine runat="server" ID="UCHistoricalTimeLine" />
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
            </div>


        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->
</asp:Content>

