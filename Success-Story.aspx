﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Success-Story.aspx.vb" Inherits="Success_Story" %>

<%@ Register Src="~/CustomControl/UCHTMLTitle.ascx" TagPrefix="uc1" TagName="UCHTMLTitle" %>
<%@ Register Src="~/CustomControl/UCHTMLSmallImage.ascx" TagPrefix="uc1" TagName="UCHTMLSmallImage" %>
<%@ Register Src="~/CustomControl/UCHTMLDetails.ascx" TagPrefix="uc1" TagName="UCHTMLDetails" %>
<%@ Register Src="~/CustomControl/UCInterestingFactList.ascx" TagPrefix="uc1" TagName="UCInterestingFactList" %>
<%@ Register Src="~/CustomControl/UCPhotoAlbum.ascx" TagPrefix="uc1" TagName="UCPhotoAlbum" %>
<%@ Register Src="~/CustomControl/UCVideoAlbum.ascx" TagPrefix="uc1" TagName="UCVideoAlbum" %>
<%@ Register Src="~/CustomControl/UCTestimonial.ascx" TagPrefix="uc1" TagName="UCTestimonial" %>
<%@ Register Src="~/CustomControl/UCHTMLSmallDetails.ascx" TagPrefix="uc1" TagName="UCHTMLSmallDetails" %>
<%@ Register Src="~/CustomControl/UCIslandMap.ascx" TagPrefix="uc1" TagName="UCIslandMap" %>
<%@ Register Src="~/CustomControl/UserStaticPhotoAlbum.ascx" TagPrefix="uc1" TagName="UserStaticPhotoAlbum" %>

<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:HiddenField ID="hdnID" runat="server" />
    <asp:HiddenField ID="hdnMasterID" runat="server" />
    <asp:HiddenField ID="hdnTitle" runat="server" />

    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%=domainName%>'>Home</a></li>
            <li><a href='<%=domainName & "Wildlife"%>'>Wildelife</a></li>
            <li><a href='<%=domainName & "Arabian-Wildlife"%>'>Arabian Wildlife park</a></li>
            <li class="active">Sucess Stories</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <asp:Literal ID="ltSuccessStory" runat="server"></asp:Literal>

            </section>
            <uc1:DynamicSEO runat="server" ID="DynamicSEO"  PageType="List_SuccessStory"/>
            <!-- -- welcome-text ends here -- -->

        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->



    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <!-- -- double-box starts here -- -->
                    <uc1:UCInterestingFactList runat="server" ID="UCInterestingFactList" />
                    <!-- -- double-box ends here -- -->
                </div>
                <div class="col-sm-3">
                    <div class="bluebg single-box">
                        <asp:Literal ID="ltConservation" runat="server"></asp:Literal>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="bluebg border-box">

                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="232" ImageType="Small" ImageClass="boxthumbnail"  ImageWidth="374" ShowEdit="false" />
                            <a href='<%= galurl %>' class="linkbtn withthumbnail" rel="Success">Photo Gallery<span class="arrow"></span></a>
                            <uc1:UserControlEditButton runat="server" ID="editPhotoGallery" HTMLID="232" PGalleryEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="225" ImageWidth="374" />
                            <% If pgalid <> 0 Then%>
                            <%= Utility.showAddButton(Request, domainName & "Admin/A-Gallery/AllGalleryItemEdit.aspx?galleryId=" & pgalid & "&Lang=en")%>
                            <% End If%>
                        <asp:Literal ID="lblPhotoGallery" runat="server"></asp:Literal>
                    </div>
                </div>


            </div>

            <div class="row">
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox blue inner">
                        <uc1:UCIslandMap runat="server" ID="UCIslandMap" />
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-6">
                    <!-- Testimonial Slider -->
                    <div class="testimonialSlider">
                        <ul class="list-unstyled slides">
                            <uc1:UCTestimonial runat="server" ID="UCTestimonial" />

                        </ul>
                    </div>
                    <!-- Testimonial Slider -->
                </div>
            </div>

            <div class="footer-nav">
                <div class="row">
                    <div class="col-sm-4">
                        <a href='<%=domainName & "Animal"%>' class="orangebg">
                            <img src='<%=domainName & "ui/media/dist/icons/animal-btn.png" %>' height="29" width="29" alt="">
                            Animals</a>
                    </div>

                    <div class="col-sm-4">
                        <a href='<%=domainName & "Breeding"%>' class="bluebg">
                            <img src='<%=domainName & "ui/media/dist/icons/breeding-btn.png"%>' height="25" width="19" alt="">
                            Breeding & Relocation</a>
                    </div>
                    <div class="col-sm-4">
                        <a href='<%=domainName & "Arabian-Wildlife"%>' class="greenbg">
                            <img src='<%=domainName & "ui/media/dist/icons/wildlife-btn.png"%>' height="16" width="25" alt="">
                            Arabian Wildlife Park</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->
</asp:Content>

