﻿
Partial Class Advanture_Activity
    Inherits System.Web.UI.Page
    Public domainName As String

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub

    Public Function GetValueToHidden() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT List_Advanture_Activity.* FROM List_Advanture_Activity where List_Advanture_Activity.Status=1 and List_Advanture_Activity.ListID=@ListID order by List_Advanture_Activity.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try

            While reader.Read()

                hdnID.Value = Page.RouteData.Values("id")
                hdnTitle.Value = reader("Title")
                hdnMasterID.Value = reader("MasterID")
                hdnSection.Value = reader("Category")


            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function


    Public Function HTMLBody() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT List_Advanture_Activity.* FROM List_Advanture_Activity where List_Advanture_Activity.Status=1 and List_Advanture_Activity.ListID=@ListID order by List_Advanture_Activity.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try

            While reader.Read()

                M += "<div class=""col-md-8"">"

                M += reader("BigDetails")

                If reader("Category").ToString <> "Day Trip" Then
                    M += "<div class=""grayBox"">"
                    M += reader("BigDetails2")
                    M += "</div>"
                Else
                    M += reader("BigDetails2")

                End If
               
                M += "</div>"

                M += "<div class=""col-md-4"">"
                M += "<div class=""imgHold margin-bottom""><img src=""" & domainName & "Admin/" & reader("BigImage") & """ alt=""" & reader("ImageAltText") & """></div>"
                M += "<div class=""imgHold""><img src=""" & domainName & "Admin/" & reader("SecondImage") & """ alt=""" & reader("ImageAltText") & """></div>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-AdvantureActivity/ListEdit.aspx?lid=" + reader("ListID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

                M += "</div>"


            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function


    Public Function FooterAdvantureCategoryNavigation() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT List_Advanture_Activity.* FROM List_Advanture_Activity where List_Advanture_Activity.Status=1 and List_Advanture_Activity.ListID <> (@ListID) and Lang='en' and Category =@Category order by List_Advanture_Activity.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = Page.RouteData.Values("id")
        cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar)
        cmd.Parameters("Category").Value = hdnSection.Value
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try

            If hdnSection.Value = "WATER ACTIVITIES" Or hdnSection.Value = "Day Trip" Then
                M += "<ul class=""list-unstyled RedBull"">"
            Else
                M += "<ul class=""list-unstyled"">"

            End If

            While reader.Read()
                If i = 0 Then
                    M += "<li class=""purplebg"">"
                ElseIf i = 1 Then
                    M += "<li class=""orangebg"">"
                ElseIf i = 2 Then
                    M += "<li class=""bluebg"">"
                ElseIf i = 3 Then
                    M += "<li class=""greenbg"">"
                ElseIf i = 4 Then
                    M += "<li class=""yellowbg"">"
                ElseIf i = 5 Then
                    M += "<li class=""dbluebg"">"
                ElseIf i = 6 Then
                    M += "<li class=""brownbg"">"
                End If



                M += "<a href=""" & domainName & "Adventure-Activity/" & reader("ListID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>"
                M += "<span><img src=""" & domainName & "Admin/" & reader("Icon") & """ alt=""" & reader("ImageAltText") & """></span>"
                M += "<h3>" & reader("Title") & "</h3>"
                M += "</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-AdvantureActivity/ListEdit.aspx?lid=" + reader("ListID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

                M += "</li>"
                i += 1
            End While
            M += "</ul>"
            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function



    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        GetValueToHidden()
        ltBreadCrum.Text = hdnTitle.Value
        ltTitle.Text = hdnTitle.Value
        ltHtmlBody.Text = HTMLBody()
        DynamicSEO.PageID = Page.RouteData.Values("ID")
        ltFooterNavigation.Text = FooterAdvantureCategoryNavigation()
    End Sub
End Class
