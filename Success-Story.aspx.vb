﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class Success_Story
    Inherits System.Web.UI.Page
    Public domainName, galurl As String
    Public pgalid, vgalid As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString


    End Sub
    Public Sub LoadGallery()
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = 4
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()

                lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Spa"">Photo Gallery<span class=""arrow""></span></a>"

            End While
        End If
        cn.Close()
    End Sub
    Public Function getGalleryID(ByVal htmlid As Integer, fieldname As String) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select " & fieldname & " from HTML where  HTMLID=@HTMLID and Lang='en'"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("HTMLID", Data.SqlDbType.NVarChar, 50).Value = htmlid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If IsDBNull(reader(fieldname)) = False Then
                    retstr = reader(fieldname).ToString()
                End If



            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function PhotoGalleryImage() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM Gallery where Status='1' and GalleryID=@GalleryID order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        cmd.Parameters("GalleryID").Value = 4

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read

                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ style=""width:100% !important;max-height:360px;height:auto;"" alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"
                M += Utility.showEditButton(Request, domainName & "Admin/A-Gallery/GalleryEdit.aspx?galleryId=" + reader("GalleryID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

            End While

        End If
        conn.Close()
        Return M
    End Function

    Function ConservationHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 130)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()



                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""transparent"">"
                M += "<h3 class=""title"">" & reader("Title") & "</h3>"
                M += "<p>" & reader("SmallDetails") & "</p>"

                M += "<a href=""" & domainName & "Conservation"" class=""readmorebtn"">Read more</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Function PhotoHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 126)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()



                M += "<div class=""purplebg border-box bigheight"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"

                M += "<a href=""" & domainName & "Photos/Gallery/15/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""linkbtn"">" & reader("Title") & "<span class=""arrow""></span></a>"
                M += "</img>"
                M += "<p>" & reader("SmallDetails") & "</p>"

                M += "<a href=""" & domainName & "Photos/Gallery/15/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""readmoreBttn purple"">Read more</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")
                M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Public Function IslandMap() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where HTMLID=108"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read

                M += "<div class=""thumbnail-round"">"
                M += " <img src=""" & domainName & "ui/media/dist/round/blue.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""normal-thumb"" alt="""">"
                M += "</img>"
                M += "</div>"

                M += "<div class=""box-content"">"
                M += " <h5 class=""title-box""><a href=""" & domainName & "IslandMap-Pop"" class=""mapFancyIframe bluefont"">Island Map</a></h5>"
                M += " <h6 class=""sub-title"">" & reader("Title") & "</h6>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a href=""" & domainName & "IslandMap-Pop"" class=""readmorebtn mapFancyIframe"">View Map</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")
                M += "</div>"



            End While

        End If
        conn.Close()
        Return M
    End Function

    Public Function GetValueToHidden() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT List_SuccessStory.* FROM List_SuccessStory where List_SuccessStory.Status=1 and List_SuccessStory.ListID=@ListID order by List_SuccessStory.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try

            While reader.Read()

                hdnID.Value = Page.RouteData.Values("id")
                hdnTitle.Value = reader("Title")
                hdnMasterID.Value = reader("MasterID")


            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function

    Public Function SuccessStory() As String
        Dim M As String = ""


        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        'Dim sql = "SELECT * FROM List_NexaClientCategory WHERE Status='1' order by SortIndex"

        Dim sql = "select *  from List_SuccessStory WHERE Status='1' and ListID=@ListID"

        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = Page.RouteData.Values("id")
        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then

                While reader.Read()

                    M += "<h1 class=""capital"">" & reader("Title") & "</h1>"

                    M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ style=""margin-right:30px;margin-bottom:20px;"" alt="""" class=""pull-left"">"
                    M += reader("BigDetails")
                    M += Utility.showEditButton(Request, domainName & "Admin/A-SuccessStory/ListEdit.aspx?lid=" + reader("ListID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

                End While


            End If
            reader.Close()
            con.Close()



            Return M
        Catch ex As Exception
            Return M
        End Try
    End Function
    Public Sub LoadGallery(ByVal galid As Integer)
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If count = 1 Then
                    galurl = domainName & "Admin/" & reader("BigImage").ToString()
                Else
                    lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Flora"">Photo Gallery<span class=""arrow""></span></a>"
                End If
                count += 1
            End While
        End If
        cn.Close()
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        GetValueToHidden()
        ltSuccessStory.Text = SuccessStory()
        UCInterestingFactList.TableMasterID = hdnMasterID.Value
        UCTestimonial.Table_MasterID = hdnMasterID.Value
        ltConservation.Text = ConservationHTMLText()
        'ltPhoto.Text = PhotoHTMLText()
        'ltIslanMap.Text = IslandMap()
        LoadGallery()
        DynamicSEO.PageID = Page.RouteData.Values("id")
        pgalid = getGalleryID(232, "GalleryID")
        If pgalid <> 0 Then
            LoadGallery(pgalid)
        End If
        'ltPhotoImage.Text = PhotoGalleryImage()
        'ltAdd.Text = Utility.showAddButton(Request, domainName & "Admin/A-Gallery/AllGalleryItemEdit.aspx?galleryId=4")

    End Sub
End Class
