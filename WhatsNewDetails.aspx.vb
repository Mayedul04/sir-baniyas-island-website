﻿Imports System.Data.SqlClient

Partial Class WhatsNewDetails
    Inherits System.Web.UI.Page
    Public domainName As String
    Public title As String = ""
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            LoadContent(Page.RouteData.Values("id"))
        End If
    End Sub
    Private Sub LoadContent(ByVal listid As Integer)
        Dim sConn As String
        'Dim selectString1 As String = "Select * from List_New where ListID=@ListID"
        Dim selectString1 As String = "Select * from List_New where ListID=@ListID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int, 32).Value = listid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.Read Then
            title = reader("Title").ToString()
            ImgDetails.ImageUrl = domainName & "Admin/" & reader("BigImage").ToString()
            lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, domainName & "Admin/A-What-New/WhatNewEdit.aspx?nid=" + reader("ListID").ToString() + "&Small=1&Big=1&Footer=1")
            DynamicSEO.PageID = listid

        End If
        cn.Close()
    End Sub
End Class
