﻿Imports System.Data.SqlClient

Partial Class RelaxPools
    Inherits System.Web.UI.Page
    Public domainName, galurl As String
    Public pgalid, vgalid As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Sub LoadGallery(ByVal galid As Integer)
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If count = 1 Then
                    galurl = "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Pools"">"
                Else
                    lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Pools""><span class=""arrow""></span></a>"
                End If
                count += 1
            End While
        End If

        cn.Close()
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim sConn As String
            Dim selectString1 As String = "Select GalleryID, VGalleryID from HTML where  HTMLID=@HTMLID and Lang='en'"
            sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
            Dim cn As SqlConnection = New SqlConnection(sConn)
            cn.Open()
            Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
            cmd.Parameters.Add("HTMLID", Data.SqlDbType.NVarChar, 50).Value = 216
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            If reader.HasRows Then
                While reader.Read()
                    If IsDBNull(reader("GalleryID")) = False Then
                        pgalid = reader("GalleryID").ToString()
                    End If
                    If IsDBNull(reader("VGalleryID")) = False Then
                        vgalid = reader("VGalleryID").ToString()
                    End If


                End While
            End If
            cn.Close()
            LoadGallery(pgalid)
        End If
    End Sub
End Class
