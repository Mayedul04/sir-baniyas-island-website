
Partial Class Advanture
    Inherits System.Web.UI.Page
    Public domainName As String

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Function GetLandActivity() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT List_Advanture_Activity.* FROM List_Advanture_Activity where List_Advanture_Activity.Status=1 and Category=@Category and Lang= 'en' order by List_Advanture_Activity.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar)
        cmd.Parameters("Category").Value = "LAND ACTIVITIES"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try


            While reader.Read()

                M += "<li class="""">"
                M += "<a href=""" & domainName & "Adventure-Activity/" & reader("ListID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>"
                M += "<span class=""arrow""></span>"
                M += reader("Title")
                M += "</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-AdvantureActivity/ListEdit.aspx?lid=" + reader("ListID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

                M += "</li>"

              

            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function
    Public Function GetDayTrips() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT List_Advanture_Activity.* FROM List_Advanture_Activity where List_Advanture_Activity.Status=1 and Category=@Category and Lang= 'en' order by List_Advanture_Activity.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar)
        cmd.Parameters("Category").Value = "Day Trip"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try


            While reader.Read()

                M += "<a href=""" & domainName & "Adventure-Activity/" & reader("ListID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>"
                M += "<div class=""dtimg""><img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ alt=""""><h3>" & reader("Title").ToString() & "<span></span></h3>"
                M += "</div>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-AdvantureActivity/ListEdit.aspx?lid=" + reader("ListID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0") & "</a>"



            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function
    Public Function GetFeaturedLandActivity() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT Top 1 List_Advanture_Activity.* FROM List_Advanture_Activity where List_Advanture_Activity.Status=1 and Category=@Category and Lang= 'en' and Featured='1' order by List_Advanture_Activity.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar)
        cmd.Parameters("Category").Value = "LAND ACTIVITIES"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try


            While reader.Read()


                M += "<div class=""col-md-6 col-sm-6"">"
                M += "<h3 class=""title"" >" & reader("Title") & "</h3>"
                M += "<div class=""imgHold"">"
                M += Utility.showEditButton(Request, domainName & "Admin/A-AdvantureActivity/ListEdit.aspx?lid=" + reader("ListID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """>"
                M += "</img>"
                M += "</div>"
                M += "<a class=""readmoreBttn greenfont"" href=""" & domainName & "Adventure-Activity/" & reader("ListID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>Read more</a>"


                M += "</div>"
            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function


    Public Function GetWaterActivity() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT List_Advanture_Activity.* FROM List_Advanture_Activity where List_Advanture_Activity.Status=1 and Category=@Category and Lang='en' order by List_Advanture_Activity.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar)
        cmd.Parameters("Category").Value = "WATER ACTIVITIES"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try


            While reader.Read()

                M += "<li class=""bluebg"">"
                M += "<div class=""content"">"
                M += Utility.showEditButton(Request, domainName & "Admin/A-AdvantureActivity/ListEdit.aspx?lid=" + reader("ListID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

                M += "<div class=""imgHold"">"
                M += "<img src=""" & domainName & "Admin/" & reader("PortraitImage") & """ alt=""" & reader("ImageAltText") & """>"

                M += "<div class=""hoverContent"">"
                M += "<a href= """ & domainName & "Adventure-Activity/" & reader("ListID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>"
                M += "<div class=""content"">"
                M += " <p> " & reader("SmallDetails") & " </p>"
                M += "<span class=""readmoreBttn"">Read more</span>"
                M += "</a>"
                M += "</div>"
                M += "</div>"


                M += "</div>"
                M += "<h2>"
                M += "<a href= """ & domainName & "Adventure-Activity/" & reader("ListID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>"
                M += reader("Title")
                M += "<span></span></a></h2>"

                M += "</div>"
               
                M += "</li>"


            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ltLandActivity.Text = GetLandActivity()
        ltWaterActivity.Text = GetWaterActivity()
        ltFeaturedActivity.Text = GetFeaturedLandActivity()
        ltrDayTrip.Text = GetDayTrips()

    End Sub
End Class
