﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Map-Pop.aspx.vb" Inherits="Map_Pop" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sir Bani Yas</title>

    <!-- Bootstrap -->
    <link href="ui/style/css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ui/media/std/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ui/media/std/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ui/media/std/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="ui/media/std/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="ui/media/std/ico/apple-touch-icon-57-precomposed.png">
       <script type="text/javascript">
           function MM_openBrWindow(theURL, winName, features) { //v2.0
               window.open(theURL, winName, features);
           }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="popUpContainer">

            <div class="titleSection">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <h2>Location Map</h2>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <ul class="innerSocialIcons">
                           <%-- <li>
                                <a href="javascript:;">
                                    <img src="ui/media/dist/icons/mail-icon.png" alt=""></a>
                            </li>--%>
                            <li>
                                <a href="javascript:;" target="_blank" style="margin-right:60px;" onclick="window.print(); return false;">
                                    <img src="ui/media/dist/icons/print-icon.png" alt=""></a>
                            </li>
                           <%-- <li>
                                <a href="javascript:;">
                                    <img src="ui/media/dist/icons/pdf-icon.png" alt=""></a>
                            </li>--%>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="videoHold">

                <asp:Literal ID="ltHtml" runat="server"></asp:Literal>
            </div>

        </div>
        <!-- Pop Up Container -->


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src='<%= domainName &"ui/js/dist/bootstrap.min.js"%>'></script>
        <script src='<%= domainName &"ui/js/dist/jquery-ui-1.10.4.custom.min.js"%>'></script>
        <script src='<%= domainName &"ui/js/dist/uniform.min.js"%>'></script>
        <script src='<%= domainName &"ui/js/dist/jquery.fancybox.js?v=2.1.5"%>'></script>
        <script src='<%= domainName &"ui/js/dist/jquery.flexslider.js"%>'></script>
        <script src='<%= domainName &"ui/js/dist/smooth-scroll.min.js"%>'></script>
        <script src='<%= domainName &"ui/js/dist/custom.js"%>'></script>
    </form>
</body>
</html>
