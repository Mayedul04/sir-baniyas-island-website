﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="ActivityLand.aspx.vb" Inherits="ActivityLand" %>


<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Adventure" %>'>Adventure</a></li>
            <li class="active">Land</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->


    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="213" />
                </h1>
                <p>
                    <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText" HTMLID="213" />

                </p>
           
            </section>
            <uc1:StaticSEO runat="server" ID="StaticSEO1" SEOID="467" />
            <!-- -- welcome-text ends here -- -->
        </div>

        <!-- -- title-bar starts here -- -->
        <div class="title-bar greenbg">
            <div class="container">
                <h2 class="title">Land Activities</h2>
                <span class="arrow"></span>
            </div>
        </div>
        <!-- -- title-bar ends here -- -->

        <!-- -- page-breadcrumb starts here -- -->
        <div class="container">
            <div class="page-breadcrumb">
                <ul class="list-unstyled">
                    <%= ActivityNameList() %>
                </ul>
            </div>
        </div>
        <!-- -- page-breadcrumb ends here -- -->

        <!-- -- parallax-section starts here -- -->
        <div class="parallax-section">
            <ul class="list-unstyled adventurelisting">
                <%= ActivityList() %>
            </ul>
        </div>
        <!-- -- parallax-section ends here -- -->
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->
       <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="467" />
</asp:Content>

