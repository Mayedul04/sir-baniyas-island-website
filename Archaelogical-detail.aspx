﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Archaelogical-detail.aspx.vb" Inherits="Archaelogical_detail" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>
<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/CustomControl/UCFlora.ascx" TagPrefix="uc1" TagName="UCFlora" %>
<%@ Register Src="~/CustomControl/UCIslandMap.ascx" TagPrefix="uc1" TagName="UCIslandMap" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>









<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Sir-Bani-Yas-Island"%>'>Sir Bani YAs Island</a></li>
            <li class="active">Archaeological sites</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="208" />

                </h1>
                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="208" ShowEdit="false"  ImageType="Big" ImageClass="pull-left" ImageStyle="margin-bottom: 40px; margin-right: 30px;"/>
                <p>
                    
                    <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="208" />
               
               
                
                
                </p>

            </section>
            <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="376" />
            <!-- -- welcome-text ends here -- -->
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">

        <div class="container">

            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="greenbg border-box small">
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" HTMLID="210" ImageClass="boxthumbnail" ShowEdit="True" IMGSmallHeight="225" IMGSmallWidth="374" ImageType="Small" />
                        <a href='<%= domainName & "Videos/Gallery/8/Archaelogy" %>' class="linkbtn">Video Gallery <span class="arrow"></span></a>
                    </div>
                   <%-- <uc1:UserControlEditButton runat="server" ID="editVideoGallery" HTMLID="210" ImageEdit="true"  ImageType="Small" ImageWidth="374" ImageHeight="225"/>--%>
                </div>


                <div class="col-sm-6 col-md-3">
                    <div class="purplebg single-box sirbaniyasBox">
                        
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="128" ImageClass="transparent" ShowEdit="False"  ImageType="Small" />
                        <h3 class="title">Salt Domes</h3>
                        <p>
                            <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText" HTMLID="128"  ImageEdit="true" ImageType="Small" IMGSmallHeight="233" IMGSmallWidth="271"/>
                        </p>
                        <a href='<%= domainName & "Salt-Domes" %>' class="readmorebtn">Read more</a>
                    </div>
                </div>

                <div class="col-sm-6 col-md-5">
                    <div class="greenbg single-box sirbaniyasBox">
                        
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage3" HTMLID="130" ImageClass="transparent" ShowEdit="False"  ImageType="Big" ImageWidth="457px" />
                        <h3 class="title">Conservation</h3>
                        <p>
                            <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText1" HTMLID="130"  ImageEdit="true" ImageType="Big" IMGBigHeight="570" IMGBigWidth="302"/>
                        </p>
                        <a href='<%= domainName & "Conservation"%>' class="readmorebtn">Read more</a>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox blue inner">
                        <div class="thumbnail-round">
                            <img src="ui/media/dist/round/blue.png" height="113" width="122" alt="" class="roundedcontainer">
                            <uc1:UserHTMLImage runat="server" HTMLID="212" ID="UserHTMLImage5" ImageType="Small" ImageHeight="137" ImageWidth="151" ImageClass="normal-thumb" />
                            
                        </div>
                        <div class="box-content">
                            <h5 class="title-box"><a href='<%= domainName & "Sir-Bani-Yas-History" %>' class="bluefont">Sir bani yas history</a></h5>
                            
                            <p>
                                <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText3" HTMLID="212" ShowEdit="false" />
                            </p>
                            <a href='<%= domainName & "Sir-Bani-Yas-History" %>' class="readmorebtn">View More</a>
                        </div>
                        <uc1:UserControlEditButton runat="server" ID="UserControlEditButton"  HTMLID="212" ImageEdit="true" ImageHeight="137" ImageWidth="151" ImageType="Small" TextEdit="true" TextType="Small"/>
                    </div>
                    
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox orange inner">
                        <div class="thumbnail-round">
                            <img src='<%= domainName & "ui/media/dist/round/orange.png"%>' height="113" width="122" alt="" class="roundedcontainer">
                            <uc1:UserHTMLImage runat="server" ID="UserHTMLImage4" HTMLID="79" ShowEdit="false" ImageType="Small" ImageHeight="132" ImageWidth="152" ImageClass="normal-thumb" />
                           
                        </div>
                        <div class="box-content">
                            <h5 class="title-box"><a href='<%= domainName & "Historical-Timeline" %>' class="">Historical Timeline</a></h5>
                            <p><uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText2" ShowEdit="False" ImageEdit="True" HTMLID="79"  /></p>
                            <a href='<%= domainName & "Historical-Timeline" %>' class="readmorebtn">View More</a>
                             <uc1:UserControlEditButton runat="server" ID="editTimeline" HTMLID="79"  ImageEdit="true" ImageType="Small" ImageHeight="119" ImageWidth="139" TextEdit="true"  TextType="Small"/>
                        </div>
                    </div>
                    
                    <!-- -- roundedbox ends here -- -->
                </div>
            </div>


            <div class="row">
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox green inner">
                        <uc1:UCFlora runat="server" ID="UCFlora" />
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox purple inner">
                        <uc1:UCIslandMap runat="server" ID="UCIslandMap" />
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
            </div>




        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->


    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns" class="nomargintop">
        <div class="container">
           <ul class="list-unstyled">
                <li class="purplebg">
                    <a href='<%= domainName & "PhotoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/photo-gallery.png" %>' height="25" width="29" alt=""></span>
                        Photo gallery
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "VideoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/video-gallery.png" %>' height="20" width="25" alt=""></span>
                        Video gallery
                    </a>
                </li>
                <li class="bluebg">
                    <a class="bookNowIframe" href='<%= domainName & "BookNow-Pop" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/book-now.png" %>' height="27" width="27" alt=""></span>
                        Book now
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "Map-Pop" %>' class="mapFancyIframe">
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/location-map.png" %>' height="26" width="32" alt=""></span>
                        Location map
                    </a>
                </li>
                <li class="purplebg">
                    <a href='<%= domainName & "Travel-Air"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/flights.png" %>' height="14" width="38" alt=""></span>
                        Flights
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "Sir-Bani-Yas-History" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/history.png" %>' height="20" width="27" alt=""></span>
                        History
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "WhatsNew" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/whats-new.png" %>' height="17" width="18" alt=""></span>
                        What's New
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
</asp:Content>

