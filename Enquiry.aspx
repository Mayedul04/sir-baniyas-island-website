﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Enquiry.aspx.vb" Inherits="Enquiry" %>

<%@ Register Src="~/MyCaptcha.ascx" TagPrefix="uc1" TagName="MyCaptcha" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sir Bani Yas</title>

    <!-- Bootstrap -->
    <link href="ui/style/css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ui/media/std/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ui/media/std/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ui/media/std/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="ui/media/std/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="ui/media/std/ico/apple-touch-icon-57-precomposed.png">
</head>
<body>
    <form id="form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <!-- Pop Up Container -->
        <div class="popUpContainer">

            <div class="titleSection">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <h2 class="greenfont">Enquire Now</h2>
                    </div>

                </div>
            </div>


            <!-- Book Now Container -->
            <div class="bookNowContainer">

                <!-- Form Row -->
                <div class="row marginBtm20">

                    <div class="col-xs-12">
                        <!-- Normal Input -->
                        <div class="form-group">
                            <label>
                                Name
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="form1" ControlToValidate="txtName" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </label>
                            <asp:TextBox ID="txtName" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <!-- Normal Input -->
                    </div>

                    <div class="col-xs-12">
                        <!-- Normal Input -->
                        <div class="form-group">
                            <label>
                                Email
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="form1" ControlToValidate="txtEmail" ErrorMessage="*"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="form1" ControlToValidate="txtEmail" ErrorMessage="Invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </label>
                            <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <!-- Normal Input -->
                    </div>

                    <div class="col-xs-12">
                        <!-- Normal Input -->
                        <div class="form-group">
                            <label>
                                Mobile Number
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="form1" ControlToValidate="txtMobile" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </label>
                            <asp:TextBox ID="txtMobile" runat="server" class="form-control"></asp:TextBox>
                        </div>
                        <!-- Normal Input -->
                    </div>

                    <div class="col-xs-12">
                        <!-- Normal Input -->
                        <div class="form-group">
                            <label>Message</label>
                            <asp:TextBox ID="txtmsg" TextMode="MultiLine" Rows="3" runat="server" class="form-control"></asp:TextBox>

                        </div>
                        <!-- Normal Input -->
                    </div>
                    <div class="col-xs-12">
                        <!-- Normal Input -->
                        <div class="form-group">
                            <label></label>
                            <div class="row">
                                <uc1:MyCaptcha runat="server" ID="MyCaptcha" vgroup="form1"  />
                            <asp:Label ID="lbCaptchaError" ForeColor="Red" runat="server"></asp:Label>
                            </div>
                        </div>
                        <!-- Normal Input -->
                    </div>
                    <div class="col-xs-4 col-xs-offset-8">
                        <asp:Button ID="btnSend" runat="server" ValidationGroup="form1" class="checkAvailability greenbg send" Text="Send" />
                        <asp:HiddenField ID="hdnDate" runat="server" />
                    </div>

                </div>
                <!-- Form Row -->

            </div>
            <!-- Book Now Container -->
            <asp:SqlDataSource ID="sdsEnquiry" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                InsertCommand="INSERT INTO [Enquiry] ([Section], [Name], [EmailID], [Mobile], [Message], [Date1]) VALUES (@Section, @Name, @EmailID, @Mobile, @Message, @Date1)" >
                
                <InsertParameters>
                    <asp:RouteParameter Name="Section" RouteKey="section" Type="String" />
                    <asp:ControlParameter ControlID="txtName" Name="Name" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtEmail" Name="EmailID" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtMobile" Name="Mobile" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtmsg" Name="Message" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="hdnDate" Name="Date1" PropertyName="Value" Type="DateTime" />
                </InsertParameters>
                
            </asp:SqlDataSource>
        </div>
        <!-- Pop Up Container -->

         </form>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="ui/js/dist/bootstrap.min.js"></script>
        <script src="ui/js/dist/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="ui/js/dist/uniform.min.js"></script>
        <script src="ui/js/dist/jquery.fancybox.js?v=2.1.5"></script>
        <script src="ui/js/dist/jquery.flexslider.js"></script>
        <script src="ui/js/dist/smooth-scroll.min.js"></script>
        <script src="ui/js/dist/custom.js"></script>
   
</body>
</html>
