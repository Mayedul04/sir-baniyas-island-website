﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="RelaxSpa.aspx.vb" Inherits="RelaxSpa" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>
<%@ Register Src="~/CustomControl/UCInterestingFactList.ascx" TagPrefix="uc1" TagName="UCInterestingFactList" %>







<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Relax" %>'>Relax</a></li>
            <li class="active">Spa</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="38" />
                </h1>
                <p>
                    <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="38" />

                </p>
                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="38" ImageClass="pull-left" ImageHeight="300" ImageWidth="570" ImageStyle="margin-right:20px;margin-bottom:10px;" />
                <%--  <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" HTMLID="38"  ImageClass="pull-left" ImageHeight="300" ImageWidth="570"  />--%>

                <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt1" HTMLID="40" />
                <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="250" />
            </section>
            <!-- -- welcome-text ends here -- -->
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-5">
                    <!-- Intresting Facts Slider -->
                    <div class="intrestingFactsSlider">

                        <ul class="slides unstyled">
                            <%= HtmlContentTreatement() %>
                        </ul>
                        
                    </div>
                    <%= Utility.showAddButton(Request, domainName & "Admin/A-RelaxHTML/CommonFactsEdit.aspx?TableName=Treatments&TableMasterID=24&smallImageWidth=252&smallImageHeight=233&Lang=en")%>
                    <!-- Intresting Facts Slider -->
                </div>
                <div class="col-sm-3">
                    <div class="brownbg single-box">
                        <%= HtmlContentPDF(42) %>
                    </div>
                    <uc1:UserControlEditButton runat="server" ID="editDownloadPdf" FileEdit="true" HTMLID="42" TitleEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="233" ImageWidth="271" TextEdit="true" TextType="Small" LinkEdit="true" />
                </div>
                <div class="col-sm-2  col-xs-6">
                    <div class="purplebg border-box small">
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" HTMLID="202" ImageType="Small" ImageClass="boxthumbnail" ImageHeight="225" ImageWidth="170" ShowEdit="false" />


                        <a href='<%= domainName & "Enquiry/Spa" %>' class="linkbtn enquireiframe">
                            <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle1" HTMLID="202" ShowEdit="false" />
                            <span class="arrow"></span></a>
                    </div>
                    <uc1:UserControlEditButton runat="server" ID="editEnquereNow" LinkEdit="true" HTMLID="202" TitleEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="225" ImageWidth="170" />

                </div>
                <div class="col-sm-2 col-xs-6">
                    <div class="bluebg border-box small">

                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="203" ImageType="Small" ImageClass="boxthumbnail" ImageHeight="225" ImageWidth="170" ShowEdit="false" />

                        <%--<a href='<%= domainName & "Photos/Gallery/11/Spa-Gallery"%>' class="linkbtn">Photo Gallery<span class="arrow"></span></a>--%>
                        <asp:Literal ID="lblPhotoGallery" runat="server"></asp:Literal>
                    </div>
                    <uc1:UserControlEditButton runat="server" ID="editPhotoGallery" HTMLID="203" PGalleryEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="225" ImageWidth="170" />
                    <% If pgalid <> 0 Then%>
                    <%= Utility.showAddButton(Request, domainName & "Admin/A-Gallery/AllGalleryItemEdit.aspx?galleryId=" & pgalid & "&Lang=en")%>
                    <% End If%>
                </div>
            </div>



            <div class="footer-nav">
                <div class="row">
                    <%--<div class="col-sm-4 col-sm-offset-2">
                        <a href='<%= domainName & "Spa" %>' class="purplebg">
                            <img src='<%= domainName & "ui/media/dist/icons/spa-btn.png" %>' alt="" style="margin-top: -6px">
                            Spa</a>
                    </div>--%>
                    <div class="col-sm-4 col-sm-offset-4">
                        <a href='<%= domainName & "Beaches"%>' class="greenbg">
                            <img src='<%= domainName & "ui/media/dist/icons/beach-btn.png" %>' alt="" style="margin-top: -5px">
                            BEaches & Pools</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->
</asp:Content>

