﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Vet-Conservation-Team.aspx.vb" Inherits="Vet_Conservation_Team" %>
<%@ Register Src="~/CustomControl/UCHTMLTitle.ascx" TagPrefix="uc1" TagName="UCHTMLTitle" %>
<%@ Register Src="~/CustomControl/UCHTMLSmallImage.ascx" TagPrefix="uc1" TagName="UCHTMLSmallImage" %>
<%@ Register Src="~/CustomControl/UCHTMLDetails.ascx" TagPrefix="uc1" TagName="UCHTMLDetails" %>
<%@ Register Src="~/CustomControl/UCInterestingFactList.ascx" TagPrefix="uc1" TagName="UCInterestingFactList" %>
<%@ Register Src="~/CustomControl/UCPhotoAlbum.ascx" TagPrefix="uc1" TagName="UCPhotoAlbum" %>
<%@ Register Src="~/CustomControl/UCVideoAlbum.ascx" TagPrefix="uc1" TagName="UCVideoAlbum" %>
<%@ Register Src="~/CustomControl/UCTestimonial.ascx" TagPrefix="uc1" TagName="UCTestimonial" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserStaticPhotoAlbum.ascx" TagPrefix="uc1" TagName="UserStaticPhotoAlbum" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%=domainName%>'>Home</a></li>
            <li><a href='<%= domainName & "Wildelife"%>'>Wildelife</a></li>
            <li><a href='<%= domainName & "Arabian-Wildlife"%>'>Arabian Wildlife park</a></li>
            <li class="active">Vets & Conservation Team</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">

                  <h1 class="capital">
               
                      <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle1"  HTMLID="105" show_Edit="false" />
                </h1>
            
                <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt1" HTMLID="105" />


            </section>
            <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="179" />
            <!-- -- welcome-text ends here -- -->

            <ul class="list-unstyled">
                <asp:Literal ID="ltVetConservationList" runat="server"></asp:Literal>
            </ul>
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->
    <!-- page-shifter starts here
   ---------------------------------------------- -->
    <nav class="pagi">
        <div class="container">
            <div class="page-shifter">
                <ul class="pagination pagination-lg list-unstyled">
                    <asp:Literal ID="ltPage" runat="server"></asp:Literal>
                </ul>
            </div>
        </div>
    </nav>

    <!-- page-shifter ends here
   ---------------------------------------------- -->


    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <uc1:UCInterestingFactList runat="server" ID="UCInterestingFactList" />
                    <!-- -- double-box starts here -- -->
                  
                    <!-- -- double-box ends here -- -->
                </div>
                <%--<div class="col-sm-3">
                    <div class="bluebg single-box">
                   <h3 class="title " >
                         <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="105" />


                   </h3>
                      
                        <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="105"  />

                         <a href='<%= domainName & "Vet-Conservation-Team"%>' class="readmorebtn">Read more</a>
                    </div>
                </div>--%>

                  <div class="col-sm-3">
                    <div class="greenbg single-box sirbaniyasBox">
                        <asp:Literal ID="ltConservation" runat="server"></asp:Literal>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="bluebg border-box">
                        <asp:Literal ID="lblPhotoGallery" runat="server"></asp:Literal>
                       
                      <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="220" ImageType="Small" ImageClass="boxthumbnail" ImageHeight="225" ImageWidth="165" ShowEdit="false" />
                    <uc1:UserControlEditButton runat="server" ID="editPhotoGallery"  HTMLID="220"  PGalleryEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="225" ImageWidth="165"/>
                        
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="greenbg border-box">
                        <%= StaticVideoGallery(221) %>
                        <%--<img src="ui/media/dist/thumbnails/videogallery.jpg" alt="" class="boxthumbnail">

                        
                        <asp:Literal ID="ltVideoLink" runat="server"></asp:Literal>--%>
                        
                         </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox blue inner">
                        <asp:Literal ID="ltIslandMap" runat="server"></asp:Literal>
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-6">
                    <!-- Testimonial Slider -->
                    <div class="testimonialSlider">
                        <ul class="list-unstyled slides">
                            <uc1:UCTestimonial runat="server" ID="UCTestimonial" Table_Name="Html" Table_MasterID="66"  />
                        </ul>
                    </div>
                    <!-- Testimonial Slider -->
                </div>
            </div>

            <div class="footer-nav">
                <div class="row">
                    <div class="col-sm-4">
                        <a href='<%= domainName & "Animal"%>' class="orangebg">
                            <img src='<%= domainName & "ui/media/dist/icons/animal-btn.png"%>' height="29" width="29" alt="">
                            Animals</a>
                    </div>

                    <div class="col-sm-4">
                        <a href='<%= domainName & "Breeding"%>' class="bluebg">
                            <img src='<%= domainName &"ui/media/dist/icons/breeding-btn.png"%>' height="25" width="19" alt="">
                            Breeding & Relocation</a>
                    </div>
                    <div class="col-sm-4">
                        <a href='<%= domainName & "Arabian-Wildlife"%>' class="greenbg">
                            <img src='<%= domainName &"ui/media/dist/icons/wildlife-btn.png"%>' height="16" width="25" alt="">
                            Arabian Wildlife Park</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->

</asp:Content>

