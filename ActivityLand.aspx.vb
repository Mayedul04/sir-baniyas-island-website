﻿Imports System.Data.SqlClient

Partial Class ActivityLand
    Inherits System.Web.UI.Page
    Public domainName, title As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Function ActivityNameList() As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select ListID ,Title from List_Advanture_Activity where Status=1 and Lang=@Lang and Category='LAND ACTIVITIES'"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<li><a data-scroll href=""#archery" & reader("ListID") & """>" & reader("Title").ToString() & "</a></li>"
            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function ActivityList() As String
        Dim retstr As String = ""
        Dim counter As Integer = 0
        Dim sConn As String
        Dim selectString1 As String = "SELECT  dbo.List_Advanture_Activity.ListID, dbo.Advanture_Banner.BannerID, dbo.Advanture_Banner.BigImage, dbo.List_Advanture_Activity.Link, dbo.List_Advanture_Activity.SmallDetails, dbo.List_Advanture_Activity.Category, dbo.List_Advanture_Activity.Title FROM  dbo.List_Advanture_Activity INNER JOIN  dbo.Advanture_Banner ON dbo.List_Advanture_Activity.MasterID = dbo.Advanture_Banner.SectionName WHERE  (dbo.List_Advanture_Activity.Category = 'LAND ACTIVITIES') and (dbo.Advanture_Banner.Category = 1) and dbo.List_Advanture_Activity.Status=1 and dbo.List_Advanture_Activity.Lang=@Lang"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                counter += 1
                If IsDBNull(reader("Link")) Then
                    retstr += "<li class=""bluebg"" id=""archery" & reader("ListID").ToString() & """>"
                Else
                    retstr += "<li class=""" & reader("Link").ToString() & """ id=""archery" & reader("ListID").ToString() & """>"
                End If
                retstr += "<div class=""image-container"">" & Utility.showEditButton(Request, domainName & "Admin/A-AdvantureBanner/TopBannerEdit.aspx?bannerId=" + reader("BannerID").ToString() + "&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&Link=0") & "<img src=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ width=""1600"" alt="""">"
                retstr += "</div>"
                If (counter Mod 2 = 0) Then
                    retstr += "<div class=""container""><div class=""row""><div class=""col-sm-8 col-sm-offset-8 col-md-6 col-md-offset-6""><div class=""white-wrapper"">"
                Else
                    retstr += "<div class=""container""><div class=""row""><div class=""col-sm-8 col-md-6""><div class=""white-wrapper"">"
                End If

                retstr += "<h3 class=""h2 title"">" & reader("Title").ToString() & " </h3>"
                retstr += "<p>" & reader("SmallDetails").ToString() & "</p>"
                retstr += "<a href=""Adventure-Activity/" & reader("ListID").ToString() & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """  class=""readmore-btn"">View more</a>" & Utility.showEditButton(Request, domainName & "Admin/A-AdvantureActivity/ListEdit.aspx?lid=" & reader("ListID").ToString() & "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")
                retstr += "</div></div></div></div></li>"

            End While
        End If
        cn.Close()
        Return retstr
    End Function
End Class
