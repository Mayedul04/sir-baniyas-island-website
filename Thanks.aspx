﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Thanks.aspx.vb" Inherits="Thanks" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sir Bani Yas</title>

    <!-- Bootstrap -->
    <link href="ui/style/css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ui/media/std/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ui/media/std/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ui/media/std/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="ui/media/std/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="ui/media/std/ico/apple-touch-icon-57-precomposed.png">
</head>
<body>
    <form id="form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <!-- Pop Up Container -->
        <div class="popUpContainer">

            <div class="titleSection">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <h2 class="greenfont">Thank You</h2>
                    </div>

                </div>
            </div>


            <!-- Book Now Container -->
            <div class="bookNowContainer">
               
                <p>
                    Thank you for your enquiry. A representative will contact you shortly.
                </p>
            </div>
            <!-- Book Now Container -->

        </div>
        <!-- Pop Up Container -->

    </form>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="ui/js/dist/bootstrap.min.js"></script>
    <script src="ui/js/dist/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="ui/js/dist/uniform.min.js"></script>
    <script src="ui/js/dist/jquery.fancybox.js?v=2.1.5"></script>
    <script src="ui/js/dist/jquery.flexslider.js"></script>
    <script src="ui/js/dist/smooth-scroll.min.js"></script>
    <script src="ui/js/dist/custom.js"></script>

</body>
</html>
