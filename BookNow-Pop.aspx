﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BookNow-Pop.aspx.vb" Inherits="BookNow_Pop" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sir Bani Yas</title>

    <!-- Bootstrap -->
    <link href="ui/style/css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ui/media/std/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ui/media/std/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ui/media/std/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="ui/media/std/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="ui/media/std/ico/apple-touch-icon-57-precomposed.png">
    <script type="text/javascript">
        function fixform() {

            if (opener.document.getElementById("form1").target != "_blank") return;
            opener.document.getElementById("form1").target = "";
            opener.document.getElementById("form1").action = opener.location.href;
        }
    </script>
</head>
<body onload="fixform()">
    <form id="form1"   runat="server">
        <div class="popUpContainer">

            <div class="titleSection">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <h2>Book Now</h2>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <!-- <ul class="innerSocialIcons">
                        <li>
                            <a href="javascript:;"><img src="ui/media/dist/icons/mail-icon.png" alt=""></a>
                        </li>
                        <li>
                            <a href="javascript:;"><img src="ui/media/dist/icons/print-icon.png" alt=""></a>
                        </li>
                        <li>
                            <a href="javascript:;"><img src="ui/media/dist/icons/pdf-icon.png" alt=""></a>
                        </li>
                    </ul> -->
                    </div>
                </div>
            </div>


            <!-- Book Now Container -->
            <div class="bookNowContainer">

                <!-- Form Row -->
                <div class="row">

                    <div class="col-md-6 col-sm-6">
                        <!-- Normal Input -->
                        <div class="form-group">
                            <label>Select resort</label>


                            <asp:DropDownList ID="ddlStay" runat="server" CssClass="form-control"
                                DataSourceID="sdsLang" DataTextField="Title" DataValueField="Booking">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                SelectCommand="SELECT [Title], [Booking] FROM [List_Restaurant] ORDER BY [SortIndex]"></asp:SqlDataSource>


                        </div>
                        <!-- Normal Input -->
                    </div>

                    <div class="col-md-6 col-sm-6">
                        <!-- Normal Input -->
                        <div class="form-group">
                            <label>Check-in</label>
                         
                            <asp:TextBox ID="txtCheckin" CssClass="form-control datepicker" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtCheckin" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </div>
                        <!-- Normal Input -->
                    </div>



                </div>
                <!-- Form Row -->


                <!-- Form Row -->
                <div class="row marginBtm20">
                                        <div class="col-md-4 col-sm-4">
                        <!-- Normal Input -->
                        <div class="form-group">
                            <label>Nights</label>
                                 <asp:TextBox ID="txtNight" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtNight" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                       
                        </div>
                        <!-- Normal Input -->
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <!-- Normal Input -->
                        <div class="form-group">
                            <label>Promo Code</label>
                                <asp:TextBox ID="txtPromoCode" CssClass="form-control" runat="server"></asp:TextBox>
                       
                        </div>
                        <!-- Normal Input -->
                    </div>

                 
                    <div class="col-md-4 col-sm-4">

                        <asp:Button ID="btnCheckAvailability" Class="checkAvailability" runat="server" OnClientClick="form1.target ='_blank';"  Text="Check Availability"   />
                         </div>

                </div>
                <!-- Form Row -->

                <p>Disclaimer : By clicking “Check Availability” you will be directed to the Anantara Resorts Website</p>


            </div>
            <!-- Book Now Container -->

        </div>
    </form>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="ui/js/dist/bootstrap.min.js"></script>
    <script src="ui/js/dist/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="ui/js/dist/uniform.min.js"></script>
    <script src="ui/js/dist/jquery.fancybox.js?v=2.1.5"></script>
    <script src="ui/js/dist/jquery.flexslider.js"></script>
    <script src="ui/js/dist/smooth-scroll.min.js"></script>
    <script src="ui/js/dist/custom.js"></script>
</body>
</html>
