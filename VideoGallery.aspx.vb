﻿Imports System.Data.SqlClient

Partial Class VideoGallery
    Inherits System.Web.UI.Page
    Public domainName As String
    Public PageList As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Private Function getPageNumber() As Integer
        Dim totalRows As Integer = 0
        Dim secondvalue As Integer = 0
        Dim sConn As String
        Dim selectString1 As String = "Select COUNT(0) from Video where  Status=1 and ParentGalleryID=0"
        ' Dim selectString2 As String = "SELECT COUNT(0) FROM CommonVideo where  Status=1 GROUP BY TableName , TableMasterID "

        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        '   Dim cmd1 As SqlCommand = New SqlCommand(selectString2, cn)
        '' Dim reader As SqlDataReader = cmd1.ExecuteReader()
        'While reader.Read()
        '    secondvalue += 1
        'End While
        '  reader.Close()
        totalRows = Math.Ceiling((cmd.ExecuteScalar) / 8)
        cn.Close()
        Return totalRows
    End Function
    Public Function Gallery() As String
        PageList = ""
        Dim currentPage As Integer = 0
        Dim pageNumber As Integer = 0
        Dim searchCriteria As String = ""
        If Page.RouteData.Values("page") Is Nothing Then
            currentPage = 1
        Else
            currentPage = Page.RouteData.Values("page")
        End If

        searchCriteria = domainName & "VideoGallery/"
        pageNumber = getPageNumber()


        If pageNumber > 1 Then
            PageList = GetPager(currentPage, pageNumber, 5, searchCriteria)
        End If
        Dim NW As String = "<ul class=""list-unstyled row"">"
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString1 As String = ""
        'selectString1 += "((SELECT DISTINCT dbo.List_News.MasterID, dbo.List_News.Title, dbo.List_News.SmallImage,  'News' as TName FROM dbo.CommonVideo INNER JOIN dbo.List_News ON dbo.CommonVideo.TableMasterID = dbo.List_News.MasterID"
        'selectString1 += " WHERE  (dbo.CommonVideo.TableName = 'News' and dbo.List_News.Status=1) GROUP BY dbo.List_News.Title, dbo.List_News.SmallImage, dbo.List_News.MasterID)"

        'selectString1 += " union  (SELECT DISTINCT dbo.List1.MasterID, dbo.List1.Title, dbo.List1.SmallImage , 'Animal' as TName"
        'selectString1 += "  FROM  dbo.CommonVideo INNER JOIN dbo.List1 ON dbo.CommonVideo.TableMasterID = dbo.List1.MasterID WHERE  (dbo.CommonVideo.TableName = 'Animal' and dbo.List1.Status=1) GROUP BY dbo.List1.MasterID, dbo.List1.Title, dbo.List1.SmallImage)"

        'selectString1 += " union  (SELECT DISTINCT dbo.List_Event.MasterID, dbo.List_Event.Title, dbo.List_Event.SmallImage , 'Events' as TName"
        'selectString1 += "  FROM  dbo.CommonVideo INNER JOIN dbo.List_Event ON dbo.CommonVideo.TableMasterID = dbo.List_Event.MasterID WHERE  (dbo.CommonVideo.TableName = 'Events' and dbo.List_Event.Status=1) GROUP BY dbo.List_Event.MasterID, dbo.List_Event.Title, dbo.List_Event.SmallImage)"

        'selectString1 += " union (SELECT DISTINCT dbo.List_Restaurant.MasterID, dbo.List_Restaurant.Title, dbo.List_Restaurant.SmallImage, 'Stay' as TName"
        'selectString1 += "  FROM   dbo.CommonVideo INNER JOIN dbo.List_Restaurant ON dbo.CommonVideo.TableMasterID = dbo.List_Restaurant.MasterID WHERE  (dbo.CommonVideo.TableName = 'Stay' and dbo.List_Restaurant.Status=1) GROUP BY dbo.List_Restaurant.MasterID, dbo.List_Restaurant.Title, dbo.List_Restaurant.SmallImage)"

        selectString1 += "(Select Video.GalleryID as MasterID , Video.Title  , Video.SmallImage  ,  'Gallery' as TName from Video where Status=1 and ParentGalleryID=0)"



        Dim selectString As String = "DECLARE @PageNum AS INT; DECLARE @PageSize AS INT; SET @PageNum =" & currentPage & "; SET @PageSize = 8; Select * from ( select  ROW_NUMBER()  over(ORDER BY List1.Title ASC) AS RowNum  , MasterID, Title, SmallImage,  TName from" & selectString1
        selectString = selectString + "As List1) as MyTable WHERE RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 AND @PageNum * @PageSize"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        'cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()

        While reader.Read
            NW += "<li class=""col-sm-3""><a href=""" & domainName & "Videos/" & reader("TName") & "/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """><div class=""img-container"">"
            If IsDBNull(reader("SmallImage")) = False Then
                If reader("SmallImage").ToString().Contains("http") Then
                    NW += "<img src=""" & reader("SmallImage") & """ height=""190"" alt="""">"
                Else
                    NW += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ height=""190"" alt="""">"

                End If
            Else
                NW += "<h4>No Image</h4>"
            End If
            NW += "</div><h5 class=""title"">" & reader("Title").ToString() & "</h5>"
            'NW += "<div class=""extra""><span class=""pull-left""><img src=""" & domainName & "ui/media/dist/icons/worldicon.png"" height=""15"" width=""16"" alt="""">"
            'NW += "Videos: " & CountItem(reader("TName").ToString(), reader("MasterID")) & "</span></div>"
            NW += "</a>"
            If reader("TName").ToString() = "Gallery" Then
                NW += Utility.showEditButton(Request, domainName & "Admin/A-Video/GalleryEdit.aspx?galleryId=" & reader("MasterID").ToString() & "&SmallImageWidth=269&SmallImageHeight=190") & "</li>"
            End If
            ' NW += Utility.showEditButton(Request, domainName & "Admin/" & reader("MasterID").ToString()) & "</li>"
            NW += "</li>"

        End While
        cn.Close()

        If PageList <> "" Then
            pnlPages.Visible = True
        End If
        NW = NW + "</ul>"
        Return NW
    End Function
    Public Function CountItem(ByVal tablename As String, ByVal fieldvalue As Integer) As String
        Dim totalRows As Integer = 0
        Dim sConn As String
        Dim selectString1 As String
        If tablename = "Gallery" Then
            selectString1 = "Select COUNT(0) from  VideoItem where  Status=1 and GalleryID=" & fieldvalue
        Else
            selectString1 = "Select COUNT(0) from  CommonVideo where  Status=1 and TableName='" & tablename & "' and TableMasterID=" & fieldvalue
        End If


        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        totalRows = cmd.ExecuteScalar
        cn.Close()
        Return totalRows
    End Function
    Public Shared Function GetPager(ByVal presentPageNum As Integer, ByVal totalNumOfPage As Integer, ByVal totalPageNumToShow As Integer, ByVal urlToNavigateWithQStr As String) As String
        Dim i As Integer
        Dim loopStartNum, loopEndNum, presentNum, maxShownNum As Integer
        Dim pagerString As String = ""
        presentNum = presentPageNum
        maxShownNum = totalPageNumToShow
        Dim middleFactor As Integer = maxShownNum / 2
        pagerString = "<ul class=""pagination pagination-lg list-unstyled"">"
        If totalNumOfPage <= totalPageNumToShow Then
            loopStartNum = 1
            loopEndNum = totalNumOfPage
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum <= 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li>"
            ' pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        Else
            loopStartNum = If(presentNum <= (middleFactor + 1), 1, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage - (maxShownNum - 1), presentNum - middleFactor))
            loopEndNum = If(presentNum <= (middleFactor + 1), maxShownNum, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage, presentNum + middleFactor))
            loopEndNum = If(loopEndNum > totalNumOfPage, totalNumOfPage, loopEndNum)
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li> "
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        End If

        pagerString = pagerString & "</ul>"
        Return pagerString
    End Function
End Class
