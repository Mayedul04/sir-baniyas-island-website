﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class Vet_Conservation_Team
    Inherits System.Web.UI.Page
    Public domainName, galurl As String
    Public pgalid, vgalid As Integer

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString


    End Sub

    Public Sub LoadGallery(ByVal galid As Integer)
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()

                lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Spa"">Photo Gallery<span class=""arrow""></span></a>"

            End While
        End If
        cn.Close()
    End Sub

    Public Function ConservationTeam() As String
        Dim M As String = ""
        Dim currentPage As String = 1
        Dim pageSize As Integer = 6
        If Page.RouteData.Values("Page") Is Nothing Then
            currentPage = 1
        Else
            currentPage = Page.RouteData.Values("Page")
        End If

        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        'Dim sql = "SELECT * FROM List_NexaClientCategory WHERE Status='1' order by SortIndex"

        Dim sql = "DECLARE @PageNum AS INT;DECLARE @PageSize AS INT;SET @PageNum =" & currentPage & ";SET @PageSize = " & pageSize & ";select *  from( SELECT ROW_NUMBER() OVER(Order by SortIndex desc) AS RowNum ,List_VetConservationTeam.* from List_VetConservationTeam WHERE Status='1' and Lang='en' )as MyTable WHERE RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 AND @PageNum * @PageSize  order by SortIndex desc"

        Dim cmd = New SqlCommand(sql, con)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then

                While reader.Read()

                    M += "<li class=""col-sm-6"">"

                    M += "<div class=""roundedbox"">"
                    M += "<div class=""thumbnail-round"">"
                    M += "<img src=""" & domainName & "ui/media/dist/round/lightbrown.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                    M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""normal-thumb"" height=""112"" width=""122"" alt=""" & reader("ImageAltText") & """>"
                    M += "</img>"
                    M += "</div>"
                    M += "<div class=""box-content"">"
                    M += "<h5 class=""title-box""><a href=""" & domainName & "Vet-Conservation-Details/" & reader("ListID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""greenfont"">" & reader("Title") & "</a></h5>"
                    ' M += "<h6 class=""sub-title"">" & reader("Title") & "</h6>"
                    M += reader("SmallDetails")
                    M += "<a href=""" & domainName & "Vet-Conservation-Details/" & reader("ListID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""mainarrow""></a>"
                    M += "</div>"
                    M += "</img>"
                    M += Utility.showEditButton(Request, domainName & "Admin/A-VetConservation/ListEdit.aspx?lid=" + reader("ListID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

                    M += "</li>"


                End While


            End If
            reader.Close()
            con.Close()

            Dim page_ As String = If(String.IsNullOrEmpty(Page.RouteData.Values("Page")), "1", Page.RouteData.Values("Page"))
            Dim iPage As Int32 = 0
            If Not Integer.TryParse(page_, iPage) Then
                iPage = 1
            End If
            Dim paging As String = GetPager(iPage, CInt(Math.Floor(cntRow() / 6) + 1), 7, domainName & "Vet-Conservation-Team" & "/")

            ltPage.Text = paging

            Return M
        Catch ex As Exception
            Return M
        End Try
    End Function
    Public Shared Function GetPager(ByVal presentPageNum As Integer, ByVal totalNumOfPage As Integer, ByVal totalPageNumToShow As Integer, ByVal urlToNavigateWithQStr As String) As String
        Dim i As Integer
        Dim loopStartNum, loopEndNum, presentNum, maxShownNum As Integer
        Dim pagerString As String = ""
        presentNum = presentPageNum
        maxShownNum = totalPageNumToShow
        Dim middleFactor As Integer = maxShownNum / 2
        If totalNumOfPage <= totalPageNumToShow Then
            loopStartNum = 1
            loopEndNum = totalNumOfPage
            If presentNum >= loopEndNum Then
               pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & loopEndNum - 1 & """>Previous</a></li>"

            ElseIf presentNum <= 1 Then
                 pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & loopEndNum & """>Previous</a></li>"
            Else
               pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & (presentNum - 1) & """>Previous</a></li>"
            End If

            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                   pagerString = pagerString & "<li class=""active""><a href=""#"">" & i & "</a></li>"

                Else
                    If presentNum > loopEndNum Then
                        If i = loopEndNum Then
                            pagerString = pagerString & "<li class=""active""><a href=""#"">" & i & "</a></li>"
                        Else
                           pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"

                        End If
                    Else
                        pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                    End If

                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum >= totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li>"

        Else
            loopStartNum = If(presentNum <= (middleFactor + 1), 1, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage - (maxShownNum - 1), presentNum - middleFactor))
            loopEndNum = If(presentNum <= (middleFactor + 1), maxShownNum, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage, presentNum + middleFactor))
            loopEndNum = If(loopEndNum > totalNumOfPage, totalNumOfPage, loopEndNum)

            If presentNum >= loopEndNum Then
                pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & loopEndNum - 1 & """>Previous</a></li>"
            ElseIf presentNum <= 1 Then
                 pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & loopEndNum & """>Previous</a></li>"
            Else
                 pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & (presentNum - 1) & """>Previous</a></li>"
            End If


            For i = loopStartNum To loopEndNum

                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""#"">" & i & "</a></li>"
                Else
                    If presentNum > loopEndNum Then
                        If i = loopEndNum Then
                            pagerString = pagerString & "<li class=""active""><a href=""#"">" & i & "</a></li>"
                        Else
                            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                        End If
                    Else
                         pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                    End If

                End If

            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum >= totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li>"
        End If

        Return pagerString

    End Function

    Public Function cntRow() As Integer
        Dim cnt As Integer
        Dim conn As New SqlConnection
        Dim ds As New DataSet
        Dim da As SqlDataAdapter
        Dim selectString As String = ""

        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
        conn.Open()
        selectString = "SELECT * FROM List_VetConservationTeam WHERE Status='1' and Lang='en' order by SortIndex"
        da = New SqlDataAdapter(selectString, conn)
        da.Fill(ds, "cnt1")
        cnt = ds.Tables(0).Rows.Count
        Return cnt
    End Function

    Function PhotoHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 126)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()



                M += "<div class=""purplebg border-box bigheight"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"

                M += "<a href=""" & domainName & "Photos/Gallery/8/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""linkbtn"">" & reader("Title") & "<span class=""arrow""></span></a>"
                M += "</img>"
                M += "<p>" & reader("SmallDetails") & "</p>"

                M += "<a href=""" & domainName & "Photos/Gallery/8/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""readmoreBttn purple"">Read more</a>"""
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")
            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function
    Public Function IslandMap() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where HTMLID=108"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read
                Dim pdfurl As String = domainName & "Admin/" & reader("FileUploaded").ToString()
                M += "<div class=""thumbnail-round"">"
                M += " <img src=""" & domainName & "ui/media/dist/round/blue.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""normal-thumb"" alt="""">"
                M += "</img>"
                M += "</div>"

                M += "<div class=""box-content"">"
                M += " <h5 class=""title-box""><a href=""" & pdfurl & """ class="" bluefont"" target=""_blank"">Island Map</a></h5>"
                'M += " <h6 class=""sub-title"">" & reader("Title") & "</h6>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a href=""" & pdfurl & """ class=""readmorebtn"" target=""_blank"">View Map</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=139&SmallImageHeight=120&BigImageWidth=733&BigImageHeight=458")
                M += "</div>"



            End While

        End If
        conn.Close()
        Return M
    End Function


    Function ConservationHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 130)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()



                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""transparent"">"
                M += "<h3 class=""title"">" & reader("Title") & "</h3>"
                M += "<p>" & reader("SmallDetails") & Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=263&SmallImageHeight=232&BigImageWidth=733&BigImageHeight=458") & "</p>"

                M += "<a href=""" & domainName & "Conservation"" class=""readmorebtn"">Read more</a>"
                '   M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=478&SmallImageHeight=233&BigImageWidth=733&BigImageHeight=458")

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Public Function StaticVideoGallery(ByVal htmlid As Integer) As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT VGalleryID, SmallImage FROM HTML WHERE HTMLID=@HTMLID and Lang='en'"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HTMLID", htmlid)

        Dim reader = cmd.ExecuteReader()

        While reader.Read
            M += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ alt="""" class=""boxthumbnail"">"
            M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" & htmlid & "&VideoLink=0&VideoCode=0&Map=0&SecondImage=0&title=0&BigImage=0&ImageAltText=0&SmallImage=1&SmallImageWidth=165&SmallImageHeight=225&SmallDetails=0&BigDetails=0&link=0&file=0&pg=0&vg=1")
            '   M+= Utility.showEditButton(Request, domainName & "Admin/A-Video/GalleryItemEdit.aspx?"
            M += "<a href=""" & domainName & "Videos/Gallery/" & reader("VGalleryID") & "/Vet-Conservation-Team"" class=""linkbtn"">Video Gallery<span class=""arrow""></span></a>"
        End While
        con.Close()
        Return M

    End Function
    Public Function getGalleryID(ByVal htmlid As Integer, fieldname As String) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select " & fieldname & " from HTML where  HTMLID=@HTMLID and Lang='en'"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("HTMLID", Data.SqlDbType.NVarChar, 50).Value = htmlid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If IsDBNull(reader(fieldname)) = False Then
                    retstr = reader(fieldname).ToString()
                End If



            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ltVetConservationList.Text = ConservationTeam()
        pgalid = getGalleryID(217, "GalleryID")
        vgalid = getGalleryID(219, "VGalleryID")
        'ltVideoLink.Text =
        ltIslandMap.Text = IslandMap()
        ltConservation.Text = ConservationHTMLText()
       
        LoadGallery(pgalid)
        'LoadGallery(10)
        'ltPhotoLink.Text = "<a href=""" & galurl & """ class=""linkbtn withthumbnail"" rel=""Vet-Team"">Photo Gallery<span class=""arrow""></span></a>"
    End Sub
End Class
