﻿Imports System.Data.SqlClient

Partial Class MediaLogged
    Inherits System.Web.UI.Page
    Public domainName, heading As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Function GetClass(ByVal section As String) As String
        If Page.RouteData.Values("section").ToString().ToLower.Contains(section) Then
            Return "active"
        Else
            Return ""
        End If
    End Function
    Public Function LoadPermittedMedia() As String
        Dim retstr As String = ""
        Dim per As String = ""
        Dim sConn As String
        Dim selectString1 As String = ""
        If Page.RouteData.Values("section") = "Photo" Then
            selectString1 = "Select  PGalleryPermissions as Permissions from RegisteredUser where RegisterUserID=@UID"
            heading = "Photo Files"
        ElseIf Page.RouteData.Values("section") = "Video" Then
            selectString1 = "Select  VGalleryPermissions as Permissions from RegisteredUser where RegisterUserID=@UID"
            heading = "Video Files"
        ElseIf Page.RouteData.Values("section") = "News" Then
            selectString1 = "Select  NewsPermissions as Permissions from RegisteredUser where RegisterUserID=@UID"
            heading = "News"
        Else
            selectString1 = "Select  OthersPermissions as Permissions from RegisteredUser where RegisterUserID=@UID"
            heading = "Brochure Files"
        End If
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("UID", Data.SqlDbType.Int, 32).Value = Request.Cookies("RegUser").Value
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            per = reader("Permissions").ToString()
        
        End If
        cn.Close()
        If per <> "" Then
            retstr = "<h2>" & heading & "</h2>" + LoadMedia(per)
        Else
            retstr = "<h2>" & heading & "</h2><p>Sorry There is no files to download.</p>"
        End If


        Return retstr
    End Function
    Public Function LoadMedia(ByVal permissions As String) As String
        Dim retstr As String = ""
        Dim count As Integer = 1
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)

        Dim strArr() As String
        strArr = Split(permissions, ",")
        For count = 0 To strArr.Length - 1
            cn.Open()
            Dim selectString1 As String = "Select  * from MediaFiles where AlbumID=@AlbumID"
            Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
            cmd.Parameters.Add("AlbumID", Data.SqlDbType.Int, 32).Value = strArr(count)
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()
                If IsDBNull(reader("FileName")) = False Then
                    If reader("FileName").ToString().Contains(".zip") Or reader("FileName").ToString().Contains(".rar") Then
                        retstr += "<li><div class=""one"">" & reader("Title").ToString() & "</div>"
                        retstr += "<div class=""one"">" & reader("FileName").ToString() & "</div>"
                        retstr += "<div class=""one""><a href=""" & domainName & "MediaLibrary/" & reader("Filename").ToString() & """>Download</a></div>"
                        retstr += "</li>"
                    End If
                End If
            End If
            cn.Close()
        Next
       

        Return retstr
    End Function
    

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Not Request.Cookies("RegUser") Is Nothing Then
                If Request.Cookies("RegUser").Value = "" Then
                    Response.Redirect(domainName & "Media-Library-Request")
                End If
            Else
                Response.Redirect(domainName & "Media-Library-Request")
            End If
        End If
    End Sub
End Class
