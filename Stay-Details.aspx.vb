﻿Imports System.Data.SqlClient

Partial Class Stay_Details
    Inherits System.Web.UI.Page
    Public domainName, galurl, vgalurl As String
    Public pgalid, vgalid As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Sub LoadGallery(ByVal galid As Integer)
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If count = 1 Then
                    galurl = domainName & "Admin/" & reader("BigImage").ToString()
                Else
                    lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Stay"">Photo Gallery<span class=""arrow""></span></a>"
                End If
                count += 1
            End While
        End If
        cn.Close()
    End Sub
    Public Function GetHiddenValue() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM List_Restaurant where Status='1' and RestaurantID=@RestaurantID and Lang='en' order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("RestaurantID", Data.SqlDbType.Int)
        cmd.Parameters("RestaurantID").Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try


            While reader.Read()

                hdnCategoryID.Value = Page.RouteData.Values("id")
                hdnTitle.Value = reader("Title")
                hdnMasterID.Value = reader("MasterID")
                hdnBookingCode.Value = reader("Booking").ToString().Trim()
                ImgPGImage.ImageUrl = domainName & "Admin/" & reader("LogoSmall").ToString()
                ImgVideo.ImageUrl = domainName & "Admin/" & reader("SmallImage").ToString()
               
            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function


    Public Function Intro() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM List_Restaurant where Status='1' and RestaurantID=@RestaurantID and Lang='en' order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("RestaurantID", Data.SqlDbType.Int)
        cmd.Parameters("RestaurantID").Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try


            While reader.Read()
                If IsDBNull(reader("GalleryID")) = False Then
                    pgalid = reader("GalleryID").ToString()
                Else
                    pgalid = 0
                End If
                pgalid = reader("GalleryID").ToString()
                If IsDBNull(reader("VGalleryID")) = False Then
                    vgalid = reader("VGalleryID").ToString()
                Else
                    vgalid = 0
                End If


                M += "<h1 class=""capital"">" & reader("Title") & "</h1>"
                M += reader("BigDetails").ToString
                M += Utility.showEditButton(Request, domainName & "Admin/A-Stay/StayEdit.aspx?lid=" + reader("RestaurantID").ToString() + "&pg=0&vg=0")

                ltTripAdvisor.Text = reader("TripAdvisor")

                '  UserStayDynamicPhotoAlbum.List_ID = hdnCategoryID.Value
                ' UserStayDynamicPhotoAlbum.Master_ID = hdnMasterID.Value

            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function

    Public Function FeaturedVideo() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM CommonVideo where Status='1' and TableName='Stay' and TableMasterID=@TableMasterID order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("TableMasterID", Data.SqlDbType.Int)
        cmd.Parameters("TableMasterID").Value = hdnMasterID.Value
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try


            While reader.Read()
                'GetVideoAlbumImage()
                M += "<a href=""" & domainName & "VideoPop/Stay/" & reader("TableMasterID") & """ class=""playbtn videoFancyIframe"">"
                M += "<div class=""img-container""><img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ alt="""" class=""boxthumbnail""></div></a>"
                M += Utility.showEditButton(Request, domainName & "admin/A-Stay/CommonVideoEdit.aspx?cgid=" & reader("CommonGalleryID").ToString() & "&smallImageWidth=0&smallImageHeight=0&BigImageWidth=&BigImageHeight=&TableName=Stay&TableMasterID=" & hdnMasterID.Value & "")

                '  ltVideoLink.Text = M

            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function

    Function Activity() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 6) '108

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()
                M += "<div class=""brownbg single-box "" style=""height:301px;"">"
                M += "<img src=""" & domainName & "Admin/" & reader("BigImage") & """ alt="""" class=""transparent"" />"
                M += " <h3 class=""title"">"
                M += "<a href=""" & domainName & "Adventure"" > "
                M += "Activities"
                M += "</a>"
                M += "</h3>"

                M += "<p>" & reader("SmallDetails").ToString & "</p>"
                M += "<a href=""" & domainName & "Adventure"" class=""readmorebtn"">Read more</a>"

                M += "<p class=""pull-right""></p>" & Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SecondImage=0&File=0&SmallImage=0&SmallDetails=1&BigDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&Link=0&SmallImageWidth=151&SmallImageHeight=129&BigImageWidth=140&BigImageHeight=120")
                M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function
    Public Function FooterStayNavigation() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT List_Restaurant.* FROM List_Restaurant where List_Restaurant.Status=1 and RestaurantID <> (@RestaurantID) and Lang='en' order by List_Restaurant.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("RestaurantID", Data.SqlDbType.Int)
        cmd.Parameters("RestaurantID").Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try


            While reader.Read()
                i = i + 1

                If i = 1 Then
                    M += "<div class=""col-sm-5"">"
                    M += "<a href=""" & domainName & "Stay-Details/" & reader("RestaurantID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""greenbg"">"
                    M += "&nbsp;<img src=""" & domainName & "ui/media/dist/icons/sahel.png"" height=""19"" width=""17"" alt="""">"
                    M += reader("OpeningTime") & "</img></a>"
                    M += "</div>"
                Else
                    M += "<div class=""col-sm-5"">"
                    M += "<a href=""" & domainName & "Stay-Details/" & reader("RestaurantID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""orangebg"">"
                    M += "&nbsp;<img src=""" & domainName & "ui/media/dist/icons/sahel.png"" height=""19"" width=""17"" alt="""">"
                    M += reader("OpeningTime") & "</img></a>"
                    M += "</div>"

                End If


            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function
    Public Function getVideoID(ByVal id As Integer) As Integer
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 1.* FROM VideoItem where  GalleryID=@GalleryID and Status=1"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        cmd.Parameters("GalleryID").Value = id
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try


            While reader.Read()


                M += reader("GalleryItemID").ToString()



            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try
    End Function

    Public Function GetVideoAlbumImage() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM List_Restaurant where Status='1' and RestaurantID=@RestaurantID and Lang='en' order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("RestaurantID", Data.SqlDbType.Int)
        cmd.Parameters("RestaurantID").Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try


            While reader.Read()


                M += reader("SmallImage")



            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        GetHiddenValue()
        ltBredCrumb.Text = hdnTitle.Value
        ltIntro.Text = Intro()
       
        LoadGallery(pgalid)
        'ltPhotoLink.Text = "<a href=""" & domainName & "Photos/Stay/" & hdnMasterID.Value & "/" & Utility.EncodeTitle(hdnTitle.Value, "-") & """ class=""linkbtn"">Photo Gallery<span class=""arrow""></span></a> "

        'If isFeaturedVideo() = True Then
        If Page.RouteData.Values("ID") = 4 Then
            pnlVideoActivity.Visible = True
            pnlActivity.Visible = False

            FeaturedVideo()
        Else
            pnlVideoActivity.Visible = False
            pnlActivity.Visible = True

        End If
        If vgalid <> 0 Then
            vgalurl = domainName & "VideoPop/Gallery/" & getVideoID(vgalid) & "/" & Page.RouteData.Values("title")
        End If
        DynamicSEO.PageID = Page.RouteData.Values("ID")
        DynamicSEO.PageType = "Stay"
        UCInterestingFactList2.Table_MasterID = hdnMasterID.Value
        ltFooterNavigation.Text = FooterStayNavigation()
        

    End Sub

    Public Function isFeaturedVideo() As Boolean
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM CommonVideo where Status='1' and TableName='Stay' and TableMasterID=@TableMasterID order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("TableMasterID", Data.SqlDbType.Int)
        cmd.Parameters("TableMasterID").Value = hdnMasterID.Value
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try

            If reader.HasRows Then
                conn.Close()
                Return True
            Else
                conn.Close()
                Return False
            End If

        Catch ex As Exception
            'Return M
        End Try
    End Function



    'Protected Sub btnCheck_Click(sender As Object, e As EventArgs) Handles btnCheck.Click
    '    ' Response.Redirect(String.Format(ddlStay.SelectedValue & "&checkin=" & Convert.ToDateTime(txtCheckIn.Text).ToString("dd/MM/yyyy") & "&nights=" & txtNight.Text & "&promocode=" & txtPromoCode.Text).Trim)
    '    Dim url1 As String = String.Format(String.Format(hdnBookingCode.Value & "&checkin=" & Convert.ToDateTime(txtCheckIn.Text).ToString("dd/MM/yyyy") & "&nights=" & txtNights.Text & "&promocode=" & txtPromo.Text).Trim)
    '    'Dim s As String = "window.open('" & url1 + "', '_blank');"
    '    'ClientScript.RegisterStartupScript(Me.GetType(), "script", s, True)
    '    Page.ClientScript.RegisterStartupScript(Me.GetType(), "openwindow", "window.open('" & url1 & "','','height=600,width=800')", True)
    '    'Response.Redirect(url1)
    '    ' txtPromo.Text = url1
    '    ' System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, GetType(Page), "Script", "window.open(" & url1 & ", '_blank');", True)
    '    '  NewWindow.Opennewwindow(btnCheck, url1)
    'End Sub
End Class
