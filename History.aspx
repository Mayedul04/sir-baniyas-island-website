﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="History.aspx.vb" Inherits="History" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UCInterestingFactList.ascx" TagPrefix="uc1" TagName="UCInterestingFactList" %>
<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>






<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            
            <li class="active">History</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    
    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                         <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" ShowEdit="false"  HTMLID="164"/>
                </h1>
                <%--<p>
                     <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="164" />
                
               </p>--%>
                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" ImageHeight="227" ShowEdit="true" HTMLID="164" ImageType="Big"  ImageWidth="471" ImageStyle="float:left;margin-right:20px; margin-bottom:20px;"/>
               <uc1:UserControlEditButton runat="server" ID="UserControlEditButton2" HTMLID="164" ImageEdit="true"  ImageType="Big" ImageHeight="227" ImageWidth="471" />
                 <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt1" HTMLID="175" />
              
                
                  <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="277" />
            </section>
            <!-- -- welcome-text ends here -- -->

            
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    

    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">

        <div class="container">
        
            <div class="row">
                
                <div class="col-sm-6">
                    <!-- Intresting Facts Slider -->
                   
                             <uc1:UCInterestingFactList runat="server" ID="UCInterestingFactList1" />
                      

                  
                    <!-- Intresting Facts Slider -->
                </div>
                <div class="col-sm-6">
                    <div class="innerMapImgHold center">
                         <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" HTMLID="176" ShowEdit="true" ImageType="Small" ImageHeight="233" ImageWidth="555" />
                        
                        <div class="textContent ">
                            <h2><a href='<%= domainName & "Salt-Domes" %>'>Salt Domes</a></h2>
                           
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox blue inner">
                        <div class="thumbnail-round">
                            <img src='<%= domainName & "ui/media/dist/round/blue.png" %>' height="113" width="122" alt="" class="roundedcontainer">
                            
                       <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="208" ImageType="Small" ImageClass="normal-thumb" />
                        </div>
                        <div class="box-content">
                            <h5 class="title-box"><a href='<%= domainName & "Archaelogical-Details" %>' class="bluefont">Archaeological Sites</a></h5>
                            <%--<h6 class="sub-title">
                            <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle1" HTMLID="208"  />
                            </h6>--%>
                            <p>
                                <uc1:UserHTMLSmallText runat="server" id="UserHTMLSmallText" ShowEdit="False" ImageEdit="True" HTMLID="208"  />
                            </p>
                            <a href='<%= domainName & "Archaelogical-Details" %>' class="readmorebtn">Read More</a>
                            <uc1:UserControlEditButton runat="server" ID="UserControlEditButton1" HTMLID="208" ImageEdit="true" ImageType="Small" ImageHeight="137" ImageWidth="151" TextEdit="true"  TextType="Small"/>
                        </div>
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox orange inner">
                        <div class="thumbnail-round">
                            <img src='<%= domainName & "ui/media/dist/round/orange.png" %>' height="113" width="122" alt="" class="roundedcontainer">
                            
                            <uc1:UserHTMLImage runat="server" ID="UserHTMLImage3" HTMLID="79" ImageType="Small" ImageWidth="151" ImageHeight="137" ImageClass="normal-thumb" />
                        </div>
                        <div class="box-content">
                            <h5 class="title-box"><a href='<%= domainName & "Historical-Timeline" %>' class="">Historical Timeline</a></h5>
                            
                            <p><uc1:UserHTMLSmallText runat="server" id="UserHTMLSmallText1" ShowEdit="false" ImageEdit="True" HTMLID="79"  /></p>
                            <a href='<%= domainName & "Historical-Timeline" %>' class="readmorebtn">View More</a>
                            <uc1:UserControlEditButton runat="server" ID="UserControlEditButton" HTMLID="79" ImageEdit="true" ImageType="Small" ImageHeight="137" ImageWidth="151" TextEdit="true"  TextType="Small"/>
                        </div>
                      
                    </div>

                    <!-- -- roundedbox ends here -- -->
                </div>
            </div>

        
        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->

</asp:Content>

