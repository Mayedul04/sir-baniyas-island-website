﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="FeaturedNews.aspx.vb" Inherits="FeaturedNews" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Media-Centre" %>'>Media Centre</a></li>
            <li class="active">Featured News</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="103" ShowEdit="false" />
                </h1>
                <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="103" />
                <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="267" />
            </section>
            <!-- -- welcome-text ends here -- -->
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->


    <!-- Featured News Section -->
    <section class="container featuredNewsSection">

        <section class="row">
            <asp:HiddenField ID="hdnPageID" runat="server" />
            <%= getFeaturedNews() %>
            
            <uc1:DynamicSEO runat="server" ID="DynamicSEO"  PageType="News"/>
           
        </section>

    </section>
    <!-- Featured News Section -->


    <!-- Title Bar -->
    <section class="titleBar greenbg">

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Other News</h2>
                </div>
                <div class="col-md-6">
                   <%-- <a class="viewAllBtn right" href='<%= domainName & "WhatsNew" %>'>+ view all</a>--%>
                </div>
            </div>
        </div>

    </section>
    <!-- Title Bar -->


    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">
        <div class="container">


            <div class="row">
                <div class="col-sm-12">
                    <!-- -- roundedbox starts here -- -->
                    <%= OtherNews() %>
                </div>

            </div>


        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->


    <section class="container">
        <div class="footer-nav">
            <div class="row">


                

                <div class="col-sm-4 col-sm-offset-2">
                    <a href='<%= domainName & "Press-Release-News" %>' class="bluebg">
                        <img src='<%= domainName & "ui/media/dist/media-center/pressrelese-icon.png"%>' alt="">
                        Press Release</a>
                </div>

                <div class="col-sm-4">
                    <a href='<%= domainName & "Media-Library-Request" %>' class="purplebg">
                        <img src='<%= domainName & "ui/media/dist/media-center/image-library.png"%>' alt="">
                        Media Library Request</a>
                </div>


            </div>
        </div>
    </section>

</asp:Content>

