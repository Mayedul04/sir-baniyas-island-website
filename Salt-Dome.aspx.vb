﻿Imports System.Data.SqlClient
Partial Class Salt_Dome
    Inherits System.Web.UI.Page
    Public domainName, galurl As String
    Public pgalid, vgalid As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Sub LoadGallery(ByVal galid As Integer)
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If count = 1 Then
                    galurl = domainName & "Admin/" & reader("BigImage").ToString()
                Else
                    lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Flora"">Photo Gallery<span class=""arrow""></span></a>"
                End If
                count += 1
            End While
        End If
        cn.Close()
    End Sub
    Function ImageText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 128)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += "<img src=""" & domainName & "Admin/" & reader("BigImage") & """ style=""margin-right:30px; margin-bottom:20px;"" alt="""" class=""pull-left"">"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage01&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&BigDetails=0&Link=0&SmallImageWidth=478&SmallImageHeight=233&BigImageWidth=471&BigImageHeight=284")


            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


    Function Archaeological() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 143)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&Link=0&SmallImageWidth=478&SmallImageHeight=233")

                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""transparent"">"
                M += " <h3 class=""title""><a href=""" & domainName & "Archaelogical-Details"">" & reader("Title") & "</a></h3>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                ' M += "<a href=""javascript:;"" class=""readmorebtn"">Read more</a>"
                M += "<br/>"
                M += "<a href=""" & domainName & "Archaelogical-Details"" class=""readmorebtn"">Read more</a>"



            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Function Conservation() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 130)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SecondImage=0&File=0&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=1&Link=0&BigImageWidth=570&BigImageHeight=301")

                M += "<img src=""" & domainName & "Admin/" & reader("BigImage") & """ class=""transparent"" width=""440px"">"
                M += " <h3 class=""title""><a href=""" & domainName & "Conservation"">" & reader("Title") & "</a></h3>"
                M += "<p>" & reader("SmallDetails") & "</p>"
               
                M += "<a href=""" & domainName & "Conservation"" class=""readmorebtn"">Read more</a>"
               



            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Function ActivityHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 77)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += "<div class=""thumbnail-round"">"
                M += "<img src=""" & domainName & "ui/media/dist/round/blue.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += " <img src=""" & domainName & "Admin/" & reader("BigImage") & """ height=""137"" width=""132"" class=""normal-thumb"" alt=""" & reader("ImageAltText") & """>"
                M += "</img>"
                M += "</div>"
                M += "<div class=""box-content"">"
                M += "<h5 class=""title-box""><a href=""" & domainName & "Stay"" class=""bluefont"">" & reader("Title") & "</a></h5>"
                M += reader("BigDetails")
                M += "<a href=""" & domainName & "Stay"" class=""readmorebtn"">View More</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&BigDetails=1&Link=0&SmallImageWidth=139&SmallImageHeight=144&BigImageWidth=733&BigImageHeight=458")

                M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


    Function HistoricalTimelineHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 79)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += "<div class=""thumbnail-round"">"
                M += "<img src=""" & domainName & "ui/media/dist/round/orange.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += " <img src=""" & domainName & "Admin/" & reader("BigImage") & """ height=""137"" width=""132"" class=""normal-thumb"" alt=""" & reader("ImageAltText") & """>"
                M += "</img>"
                M += "</div>"
                M += "<div class=""box-content"">"
                M += "<h5 class=""title-box""><a href=""" & domainName & "Historical-Timeline"" class="""">" & reader("Title") & "</a></h5>"
                M += reader("BigDetails")
                M += "<a href=""" & domainName & "Historical-Timeline"" class=""readmorebtn"">View More</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&BigDetails=1&Link=0&SmallImageWidth=139&SmallImageHeight=120&BigImageWidth=733&BigImageHeight=458")
                M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Public Function IslandMap() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where HTMLID=108"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read
                Dim pdfurl As String = domainName & "Admin/" & reader("FileUploaded").ToString()
                M += "<div class=""thumbnail-round"">"
                M += " <img src=""" & domainName & "ui/media/dist/round/blue.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""normal-thumb"" alt="""">"
                M += "</img>"
                M += "</div>"

                M += "<div class=""box-content"">"
                M += " <h5 class=""title-box""><a href=""" & pdfurl & """ class="" bluefont"" target=""_blank"">Island Map</a></h5>"
                '  M += " <h6 class=""sub-title"">" & reader("Title") & "</h6>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a href=""" & pdfurl & """ class=""readmorebtn bluefont"" target=""_blank"">View Map</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=139&SmallImageHeight=120&BigImageWidth=733&BigImageHeight=458")

                M += "</div>"



            End While

        End If
        conn.Close()
        Return M
    End Function

    Function FloraHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 132)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += "<div class=""thumbnail-round"">"
                M += "<img src=""" & domainName & "ui/media/dist/round/green.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += " <img src=""" & domainName & "Admin/" & reader("SmallImage") & """ height=""137"" width=""132"" class=""normal-thumb"" alt=""" & reader("ImageAltText") & """>"
                M += "</img>"
                M += "</div>"
                M += "<div class=""box-content"">"
                M += "<h5 class=""title-box""><a href=""" & domainName & "Flora"" class="""">" & reader("Title") & "</a></h5>"
                M += reader("SmallDetails")
                M += "<a href=""" & domainName & "Flora"" class=""readmorebtn"">View More</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=139&SmallImageHeight=120&BigImageWidth=733&BigImageHeight=458")

                M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Public Function getGalleryID(ByVal htmlid As Integer, fieldname As String) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select " & fieldname & " from HTML where  HTMLID=@HTMLID and Lang='en'"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("HTMLID", Data.SqlDbType.NVarChar, 50).Value = htmlid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If IsDBNull(reader(fieldname)) = False Then
                    retstr = reader(fieldname).ToString()
                End If



            End While
        End If
        cn.Close()
        Return retstr
    End Function



    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ltImage.Text = ImageText()
        ltInterestingFacts.Text = Archaeological()
        ltConservation.Text = Conservation()
        ltActivity.Text = ActivityHTMLText()
        ltHistoricalTimeline.Text = HistoricalTimelineHTMLText()
        ltIslanMap.Text = IslandMap()
        ltFlora.Text = FloraHTMLText()
        pgalid = getGalleryID(234, "GalleryID")
        If pgalid <> 0 Then
            LoadGallery(pgalid)
        End If
    End Sub
End Class
