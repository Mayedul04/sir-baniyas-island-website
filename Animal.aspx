﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Animal.aspx.vb" Inherits="Animal" %>

<%@ Register Src="~/CustomControl/UCHTMLTitle.ascx" TagPrefix="uc1" TagName="UCHTMLTitle" %>
<%@ Register Src="~/CustomControl/UCHTMLSmallImage.ascx" TagPrefix="uc1" TagName="UCHTMLSmallImage" %>
<%@ Register Src="~/CustomControl/UCHTMLDetails.ascx" TagPrefix="uc1" TagName="UCHTMLDetails" %>
<%@ Register Src="~/CustomControl/UCInterestingFactList.ascx" TagPrefix="uc1" TagName="UCInterestingFactList" %>
<%@ Register Src="~/CustomControl/UCPhotoAlbum.ascx" TagPrefix="uc1" TagName="UCPhotoAlbum" %>
<%@ Register Src="~/CustomControl/UCVideoAlbum.ascx" TagPrefix="uc1" TagName="UCVideoAlbum" %>
<%@ Register Src="~/CustomControl/UCTestimonial.ascx" TagPrefix="uc1" TagName="UCTestimonial" %>
<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserVetConservationTeam.ascx" TagPrefix="uc1" TagName="UserVetConservationTeam" %>
<%@ Register Src="~/CustomControl/UserStaticPhotoAlbum.ascx" TagPrefix="uc1" TagName="UserStaticPhotoAlbum" %>
<%@ Register Src="~/CustomControl/UCIslandMap.ascx" TagPrefix="uc1" TagName="UCIslandMap" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>

<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>









<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName%>'>Home</a></li>
            <li><a href='<%= domainName & "Wildlife"%>'>Wildlife</a></li>
            <li class="active">Animals</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">

                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="4" ShowEdit="false" />
                </h1>
                <p>
                    
                    <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="4" />

                </p>


            </section>
            <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="17" />
            <!-- -- welcome-text ends here -- -->
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <section class="meet-the-animals">

        <div class="container">
            <h2 class="h1 maintitle">MEET THE ANIMALS</h2>
            <ul class="list-unstyled row">

                <asp:Literal ID="ltAnimalCategory" runat="server"></asp:Literal>

            </ul>
        </div>
    </section>

    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-5">
                    <!-- Intresting Facts Slider -->
                     <uc1:UCInterestingFactList runat="server" ID="UCInterestingFactList" />
                    <!-- Intresting Facts Slider -->
                </div>
                <div class="col-sm-4 col-md-3">
                    
                    
                    
                      <%--  <img src='<%=domainName & "ui/media/dist/thumbnails/vets.jpg"%>' alt="" class="transparent">

                        <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="105" />
                        <a href='<%= domainName & "Vet-Conservation-Team"%>' class="readmorebtn">Read more</a>
                   --%>

                       <%-- <asp:Literal ID="ltVetConservationTeam" runat="server"></asp:Literal>--%>

                        <uc1:UserVetConservationTeam runat="server" id="UserVetConservationTeam" HTMLID="105" />
                        



                </div>
                <div class="col-sm-4 col-md-2  col-xs-6">
                    <div class="greenbg border-box small">
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="224" ImageType="Small" ImageClass="boxthumbnail" ImageHeight="225" ImageWidth="165" ShowEdit="false" />
                        <asp:Literal ID="lblVGalleryUrl" runat="server"></asp:Literal>
                        <uc1:UserControlEditButton runat="server" ID="EditVideoGallery" HTMLID="224"  VGalleryEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="225" ImageWidth="165" />
                        
                    </div>
                </div>
                <div class="col-sm-4 col-md-2 col-xs-6">
                    <div class="bluebg border-box small">

                      <asp:Literal ID="lblPhotoGallery" runat="server"></asp:Literal>
                       
                      <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="223" ImageType="Small" ImageClass="boxthumbnail" ImageHeight="225" ImageWidth="165" ShowEdit="false" />
                   <a href='<%= galurl %>' class="linkbtn withthumbnail" rel="Spa">Photo Gallery<span class="arrow"></span></a>
                         <uc1:UserControlEditButton runat="server" ID="editPhotoGallery"  HTMLID="223"  PGalleryEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="225" ImageWidth="165"/>
                         <% If pgalid <> 0 then %>
                         <%= Utility.showAddButton(Request, domainName & "Admin/A-Gallery/AllGalleryItemEdit.aspx?galleryId=" & pgalid & "&Lang=en")%>
                        <% End if %>
                        

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox blue inner">
                        <uc1:UCIslandMap runat="server" id="UCIslandMap" />
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-6">
                    <!-- Testimonial Slider -->
                    <div class="testimonialSlider">
                        <ul class="list-unstyled slides">
                            <uc1:UCTestimonial runat="server" id="UCTestimonial" />
                        </ul>
                    </div>
                    <!-- Testimonial Slider -->
                </div>
            </div>


            <div class="footer-nav">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-2">
                        <a href='<%=domainName & "Arabian-Wildlife"%>' class="greenbg">
                            <img src="ui/media/dist/icons/wildlife-btn.png" alt="" style="margin-top: -5px">
                            Arabian Wildlife Park</a>
                    </div>
                    <div class="col-sm-4">
                        <a href='<%=domainName & "Breeding"%>' class="bluebg">
                            <img src="ui/media/dist/icons/breeding-btn.png" height="25" width="19" alt="">
                            Breeding & Relocation</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->

</asp:Content>

