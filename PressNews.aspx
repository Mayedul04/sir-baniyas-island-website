﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="PressNews.aspx.vb" Inherits="PressNews" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Media-Centre" %>'>Media Centre</a></li>
            <li class="active">Press Releases</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="104" ShowEdit="false" />
                </h1>

                <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="104" />

            </section>
            <uc1:StaticSEO runat="server" ID="StaticSEO"  SEOID="572"/>
            <!-- -- welcome-text ends here -- -->
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->


    <!-- Featured News Section -->
    <div class="featuredNewsSection">
        <section class="container ">

            <%= News() %>
        </section>
    </div>
    <!-- Featured News Section -->



    <section class="container">
        <div class="footer-nav">
            <div class="row">

                <div class="col-sm-4 col-sm-offset-2">
                    <a href='<%= domainName & "Island-News" %>' class="orangebg" >
                        <img src='<%= domainName & "ui/media/dist/media-center/island-news-icon.png"%>' alt="" style="margin-top: -5px">
                        Island News</a>
                </div>

               

                <div class="col-sm-4">
                    <a href='<%= domainName & "Media-Library-Request" %>' class="purplebg">
                        <img src='<%= domainName & "ui/media/dist/media-center/image-library.png"%>' alt="">
                        Media Library Request</a>
                </div>

            </div>
        </div>
    </section>
</asp:Content>

