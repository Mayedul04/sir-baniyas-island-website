﻿Imports System.Data.SqlClient

Partial Class TravelSea
    Inherits System.Web.UI.Page
    Public domainName, galurl As String
    Public pgalid, vgalid As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Sub LoadGallery(ByVal galid As Integer)
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If count = 1 Then
                    galurl = domainName & "Admin/" & reader("BigImage").ToString()
                Else
                    lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""TravelSea""><span class=""arrow""></span></a>"
                End If
                count += 1
            End While
        End If

        cn.Close()
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim sConn As String
            Dim selectString1 As String = "Select GalleryID, VGalleryID from HTML where  HTMLID=@HTMLID and Lang='en'"
            sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
            Dim cn As SqlConnection = New SqlConnection(sConn)
            cn.Open()
            Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
            cmd.Parameters.Add("HTMLID", Data.SqlDbType.NVarChar, 50).Value = 85
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            If reader.HasRows Then
                While reader.Read()
                    If IsDBNull(reader("GalleryID")) = False Then
                        pgalid = reader("GalleryID").ToString()
                    End If
                    If IsDBNull(reader("VGalleryID")) = False Then
                        vgalid = reader("VGalleryID").ToString()
                    End If


                End While
            End If
            cn.Close()
            If pgalid <> 0 Then
                LoadGallery(pgalid)
            End If
            ltLoactionMap.Text = LocationMap()
        End If
    End Sub
    Public Function LocationMap() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where HTMLID=75"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read



                M += "<img src=""" & domainName & "Admin/" & reader("BigImage") & """ alt=""" & reader("ImageAltText") & """>"
                M += "<div class=""textContent"">"
                M += "<h2><a  href=""" & domainName & "Map-Pop"" class=""mapFancyIframe"">Location Map</a></h2>"
                M += "<div class=""content"">"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=1&Link=0&SmallImageWidth=137&SmallImageHeight=108&BigImageWidth=555&BigImageHeight=233")

                M += "<p>" & If(reader("SmallDetails").ToString.Length > 100, reader("SmallDetails").ToString.Substring(0, 100), reader("SmallDetails").ToString) & "</p>"
                M += "<a class=""readmorebtn mapFancyIframe"" href=""" & domainName & "Map-Pop" & """>Click here</a><span class=""arrow""></span></div>"
                M += "</div>"


            End While

        End If
        conn.Close()
        Return M
    End Function
End Class
