﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="MediaCentre.aspx.vb" Inherits="MediaCentre" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>






<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>

            <li class="active">Media Centre</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="92" ShowEdit="false" />
                </h1>
                <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="92" />
                <uc1:StaticSEO runat="server" ID="StaticSEO"  SEOID="265"/>
            </section>
            <!-- -- welcome-text ends here -- -->
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <section class="meet-the-animals">

        <div class="container">
            <ul class="list-unstyled row">
                <li class="col-sm-4 aboutDesert">
                    <div class="orangebg border-box bigheight">
                        
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="93" ImageClass="boxthumbnail" ImageType="Small" ShowEdit="false" />
                        <a href='<%= domainName & "Island-News"%>' class="linkbtn">
                            <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle1" HTMLID="93" ShowEdit="False" ImageEdit="True" />
                            <span class="arrow"></span></a>
                    </div>
                    <uc1:UserControlEditButton runat="server" ID="UserControlEditButton" HTMLID="93" TitleEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="292" ImageWidth="365" />
                </li>
                <li class="col-sm-4 aboutDesert">
                    <div class="bluebg border-box bigheight">
                       
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" HTMLID="94" ImageClass="boxthumbnail" ImageType="Small" ShowEdit="false" />
                        <a href='<%= domainName & "Press-Release-News"%>' class="linkbtn">
                            <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle2" HTMLID="94" ShowEdit="false" />
                            <span class="arrow"></span></a>
                    </div>
                    <uc1:UserControlEditButton runat="server" ID="UserControlEditButton1" HTMLID="94" TitleEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="292" ImageWidth="365" />
                </li>
                <li class="col-sm-4 aboutDesert">
                    <div class="purplebg border-box bigheight">
                        
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="95" ImageClass="boxthumbnail" ImageType="Small" ShowEdit="false" />
                        <a href='<%= domainName & "Media-Library-Request"%>' class="linkbtn">
                            <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle3" HTMLID="95" ShowEdit="false" />
                            <span class="arrow"></span></a>
                    </div>
                    <uc1:UserControlEditButton runat="server" ID="UserControlEditButton2" HTMLID="95" TitleEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="292" ImageWidth="365" />
                </li>
            </ul>
        </div>
    </section>




    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns" class="nomargintop">
        <div class="container">
            <ul class="list-unstyled">
                <li class="purplebg">
                     <a href='<%= domainName & "PhotoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/photo-gallery.png" %>' height="25" width="29" alt=""></span>
                        Photo gallery
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "VideoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/video-gallery.png" %>' height="20" width="25" alt=""></span>
                        Video gallery
                    </a>
                </li>
                <li class="bluebg">
                    <a class="bookNowIframe" href='<%= domainName & "BookNow-Pop" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/book-now.png" %>' height="27" width="27" alt=""></span>
                        Book now
                    </a>
                </li>
                <li class="greenbg">
                     <a href='<%= domainName & "Map-Pop" %>' class="mapFancyIframe">
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/location-map.png"%>' height="26" width="32" alt=""></span>
                        Location map
                    </a>
                </li>
                <li class="purplebg">
                     <a href='<%= domainName & "Travel-Air" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/flights.png"%>' height="14" width="38" alt=""></span>
                        Flights
                    </a>
                </li>
                <li class="orangebg">
                   <a href='<%= domainName & "Sir-Bani-Yas-History" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/history.png"%>' height="20" width="27" alt=""></span>
                        History
                    </a>
                </li>
                <li class="greenbg">
                     <a href='<%= domainName & "WhatsNew" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/whats-new.png"%>' height="17" width="18" alt=""></span>
                        What's New
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
</asp:Content>

