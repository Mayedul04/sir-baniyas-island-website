﻿Imports System.Data.SqlClient

Partial Class PhotoPop
    Inherits System.Web.UI.Page
    Public domainName As String
    Public PageList As String
    Public section As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'If IsPostBack = False Then
        '    section = Page.RouteData.Values("section")
        '    LoadContent(Page.RouteData.Values("id"))
        'End If
        ltImage.Text = LoadContent()

    End Sub
    Public Function LoadContent() As String
        Dim sConn As String
        Dim selectString1 As String = ""
        Dim M As String = ""
        'If section = "Gallery" Then
        '    selectString1 = "Select Title, BigImage from GalleryItem where   GalleryItemID=" & id
        'Else
        selectString1 = "Select Title, BigImage from CommonGallery where TableName=@TableName and TableMasterID=@TableMasterID"
        'End If
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 500).Value = Page.RouteData.Values("section")
        cmd.Parameters.Add("TableMasterID", Data.SqlDbType.NVarChar, 500).Value = Page.RouteData.Values("id")

        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                M += "<img src=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ alt="""" class=""PopAnimal"" rel=""PopAnimal"">"
                'ImgBig.ImageUrl = domainName & "Admin/" & reader("BigImage").ToString()

                ' lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, domainName & "Admin/A-Event/EventsEdit.aspx?EventID=" & reader("EventID") & "&Small=0&Big=1&Footer=0")

            End While
        End If
        cn.Close()
        Return M
    End Function
End Class
