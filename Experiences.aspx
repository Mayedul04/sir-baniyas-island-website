﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Experiences.aspx.vb" Inherits="Experience" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Gallery" %>'>Gallery</a></li>
            <li class="active">Your Experiences</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">

        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="161" ShowEdit="false" />
                </h1>
                <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="161" />
            </section>
            <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="246" />
            <!-- -- welcome-text ends here -- -->
        </div>


        <!-- Land & Water Activites -->
        <div class="activitesContainerSection galleryExperiance">

            <!-- Bottom FIx -->
            <div class="bottomFix">

                <div class="container">

                    <div class="row">

                        <!-- Instagram Activities -->
                        <div class="col-md-6 col-sm-6">
                            <div class="landActivitesSection galleryExp">
                                <h2 class="orangebg">
                                    <span class="iconMar">
                                        <img src='<%= domainName & "ui/media/dist/galler-exp/insta-icon.png"%>' alt=""></span> Instagram
                                </h2>

                                <!-- Instagram Inner Slider -->
                                <div class="instagramInnerSlider">

                                    <ul class="slides">

                                        <%= InstgramFeed() %>
                                    </ul>

                                </div>
                                <!-- Instagram Inner Slider -->

                            </div>
                        </div>
                        <!-- Land Activities -->

                        <!-- Water Activities -->
                        <div class="col-md-6 col-sm-6">
                            <div class="waterActivitesSection galleryExp">
                                <h2 class="bluebg">
                                    <span class="iconMar">
                                        <img src='<%= domainName & "ui/media/dist/galler-exp/tweet-icon.png"%>' alt=""></span>Twitter
                                </h2>

                                <!-- Instagram Inner Slider -->
                                <div class="twitterInnerSlider">
                                    <%-- <a class="twitter-timeline"
 
  href="https://twitter.com/twitterdev"
  data-widget-id="YOUR-WIDGET-ID-HERE">
Tweets by @twitterdev
</a>--%>
                                    <a class="twitter-timeline" width="580" height="577" href="https://twitter.com/sirbaniyasae" data-widget-id="592576736241422336">Tweets by @sirbaniyasae</a>
<script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");</script>
                                    

                                </div>
                                <!-- Instagram Inner Slider -->

                            </div>
                        </div>
                        <!-- Water Activities -->

                    </div>

                </div>

            </div>
            <!-- Bottom FIx -->

        </div>
        <!-- Land & Water Activites -->



    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->



    <section class="container marginTop20">
        <div class="footer-nav">
            <div class="row">

                <div class="col-sm-4">
                    <a href='<%= domainName & "PhotoGallery" %>' class="purplebg">
                        <img src='<%= domainName & "ui/media/dist/galler-exp/photo-gallery-icon.png"%>' alt="" style="margin-top: -5px">
                        Photo Gallery</a>
                </div>

                <div class="col-sm-4">
                    <a href='<%= domainName & "VideoGallery" %>' class="orangebg">
                        <img src='<%= domainName & "ui/media/dist/galler-exp/video-gallery-icon.png"%>' alt="">
                        Video Galley</a>
                </div>

                <div class="col-sm-4">
                    <a href='<%= domainName & "Experiences" %>' class="greenbg">
                        <img src='<%= domainName & "ui/media/dist/galler-exp/your-exp-icon.png"%>' alt="">
                        yOUR eXperiences</a>
                </div>

            </div>
        </div>
    </section>
</asp:Content>

