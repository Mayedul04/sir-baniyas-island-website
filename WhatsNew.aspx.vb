﻿Imports System.Data.SqlClient

Partial Class WhatsNew
    Inherits System.Web.UI.Page
    Public domainName As String
    Public PageList As String = ""
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Private Function getPageNumber() As Integer
        Dim totalRows As Integer = 0
        Dim sConn As String
        Dim selectString1 As String = "Select COUNT(0) from List_New where  Status=1 and Lang=@Lang"
        ' Dim selectString1 As String = "Select COUNT(0) from List_News where  Status=1 and Lang=@Lang and Featured=0"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        totalRows = Math.Ceiling(cmd.ExecuteScalar / 6)
        cn.Close()
        Return totalRows
    End Function
    Public Function HtmlContentMap(ByVal htmlid As Integer) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select * from HTML where  Lang=@Lang and HtmlID=@HtmlID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        cmd.Parameters.Add("HtmlID", Data.SqlDbType.Int, 32).Value = htmlid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<div class=""innerMapImgHold""><img src=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ alt="""">"
                retstr += "<div class=""textContent""><h2><a href=""" & domainName & "Map-Pop" & """ class=""mapFancyIframe"">" & reader("Title").ToString() & "</a></h2>"
                'retstr += "<h5 class=""subtitle"">" & reader("SmallDetails").ToString() & "</h5>"
                retstr += "<div class=""content""><p>" & reader("SmallDetails").ToString() & "</p><br><a class=""readmorebtn mapFancyIframe"" href=""" & domainName & "Map-Pop" & """>Click here</a><span class=""arrow""></span></div>"
                '& Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0&SmallDetails=0") & "</div>"
                retstr += "</div></div>"
            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function NewList() As String
        PageList = ""
        Dim currentPage As Integer = 0
        Dim pageNumber As Integer = 0
        Dim searchCriteria As String = ""
        If Page.RouteData.Values("page") Is Nothing Then
            currentPage = 1
        Else
            currentPage = Page.RouteData.Values("page")
        End If

        searchCriteria = domainName & "WhatsNew/"
        pageNumber = getPageNumber()


        If pageNumber > 1 Then
            PageList = GetPager(currentPage, pageNumber, 3, searchCriteria)
        End If
        Dim NW As String = "<ul class=""list-unstyled"">"
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString As String = "DECLARE @PageNum AS INT; DECLARE @PageSize AS INT; SET @PageNum =" & currentPage & "; SET @PageSize = 6; Select * from ( select  ROW_NUMBER()   over(ORDER BY List_New.LastUpdated DESC, Sortindex) AS RowNum , ListID, MasterID, Title, SmallDetails, SmallImage, BigImage, ImageAltText, LastUpdated, SortIndex from List_New where Lang=@Lang and Status=1 "
        selectString = selectString + ") as MyTable WHERE RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 AND @PageNum * @PageSize"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read
                NW += "<li class=""col-sm-6""><div class=""roundedbox""><div class=""thumbnail-round"">"
                NW += "<img src=""" & domainName & "ui/media/dist/round/lightbrown.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                NW += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ class=""normal-thumb"" height=""112"" width=""122"" alt=""""></div>"

                NW += "<div class=""box-content""><h5 class=""title-box""><a href=""" & domainName & "Whats-New-Details/" & reader("ListID").ToString() & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """ class=""greenfont"">" & reader("Title").ToString() & "</a></h5>"
                NW += "<p>" & Mid(reader("SmallDetails").ToString(), 1, 65) & " . .</p>" & Utility.showEditButton(Request, domainName & "Admin/A-What-New/WhatNewEdit.aspx?nid=" & reader("ListID").ToString() & "&Small=1&Big=0&Footer=1")
                NW += "<a href=""" & domainName & "Whats-New-Details/" & reader("ListID").ToString() & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """ class=""mainarrow""></a>"

                NW += "</div></div></li>"


            End While
        
        End If
        cn.Close()
        NW = NW & Utility.showAddButton(Request, domainName & "Admin/A-What-New/WhatNewEdit.aspx?Small=1&Big=1&Footer=1") & "</ul>"
        If PageList = "" Then
            pnlPageination.Visible = False
        End If
        Return NW
    End Function
    Public Shared Function GetPager(ByVal presentPageNum As Integer, ByVal totalNumOfPage As Integer, ByVal totalPageNumToShow As Integer, ByVal urlToNavigateWithQStr As String) As String
        Dim i As Integer
        Dim loopStartNum, loopEndNum, presentNum, maxShownNum As Integer
        Dim pagerString As String = ""
        presentNum = presentPageNum
        maxShownNum = totalPageNumToShow
        Dim middleFactor As Integer = maxShownNum / 2
        pagerString = "<ul class=""pagination pagination-lg list-unstyled"">"
        If totalNumOfPage <= totalPageNumToShow Then
            loopStartNum = 1
            loopEndNum = totalNumOfPage
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum <= 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li>"
            ' pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        Else
            loopStartNum = If(presentNum <= (middleFactor + 1), 1, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage - (maxShownNum - 1), presentNum - middleFactor))
            loopEndNum = If(presentNum <= (middleFactor + 1), maxShownNum, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage, presentNum + middleFactor))
            loopEndNum = If(loopEndNum > totalNumOfPage, totalNumOfPage, loopEndNum)
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li> "
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        End If

        pagerString = pagerString & "</ul>"
        Return pagerString
    End Function
End Class
