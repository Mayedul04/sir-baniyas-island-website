﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="WhatsNew.aspx.vb" Inherits="WhatsNew" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>






<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName  %>'>Home</a></li>
            <li><a href='<%= domainName & "Media-Centre" %>'>Media Centre</a></li>
            <li class="active">What's New</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    
    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="107" ShowEdit="false" />
                </h1>
               <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="107" />
<uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="270" />
            </section>
            <!-- -- welcome-text ends here -- -->

            <%= NewList() %>
        </div>
        
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->
<!-- page-shifter starts here
   ---------------------------------------------- -->
     <asp:Panel ID="pnlPageination" runat="server">
    <nav class="pagi">
    <div class="container">

        <div class="page-shifter">
           <%= Utility.showAddButton(Request, domainName & "Admin/A-What-New/WhatNewEdit.aspx?Small=1&Big=1&Footer=1") %>
            <%= PageList %>

        </div>
         
    </div>
    </nav>
</asp:Panel>
   <!-- page-shifter ends here
   ---------------------------------------------- -->
    

    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">

        <div class="container">
        
            <div class="row">
                
                <div class="col-sm-6">
                    <!-- -- double-box starts here -- -->
                    <div class="double-box greenbg">
                        <!-- -- twopanel starts here -- -->
                        <div class="twopanel">
                            <h3 class="title">
                                 Our Philosphy
                            </h3>
                            
                            <p>
                                <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText" HTMLID="121"   />
                            </p>
                            <span class="arrow"></span>
                            <br/>
                            <a href='<%= domainName & "Philosophy"%>' class="readmorebtn">Read more</a>
                        </div>
                          
                        <!-- -- twopanel ends here -- -->
                        
                        <!-- -- twopanel imageholder starts here -- -->
                        <div class="twopanel imageholder">
                             <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="121"  ImageType="Small" ImageWidth="307" />
                           
                        </div>
                        <!-- -- twopanel imageholder ends here -- -->
                    </div>
                    <uc1:UserControlEditButton runat="server" ID="UserControlEditButton" HTMLID="121" TextEdit="true" TextType="Small" ImageEdit="true" ImageType="Small" ImageHeight="233" ImageWidth="478" />
                    <!-- -- double-box ends here -- -->
                </div>
                <div class="col-sm-6">
                    <%= HtmlContentMap(75)%>
                    <uc1:UserControlEditButton runat="server" ID="EditMap" HTMLID="75" TitleEdit="true" ImageEdit="true" ImageType="Big" ImageHeight="233" ImageWidth="555" TextEdit="true"  TextType="Small"/>
                   <%-- <div class="innerMapImgHold">
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" ShowEdit="false" ImageType="Small" HTMLID="108" ImageHeight="233" ImageClass="normal-thumb"  />
                        
                        <div class="textContent">
                            <h2><a href='<%= domainName & "IslandMap-Pop" %>' class="mapFancyIframe">Island Map</a></h2>
                            <div class="content">
                                <p>
                                    <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText1" ShowEdit="false" HTMLID="108"  />
                                </p>

                            </div>
                        </div>
                    </div>
                     <uc1:UserControlEditButton runat="server" ID="EditmapThumb" HTMLID="108" TextEdit="true" TextType="Small" ImageEdit="true" ImageType="Big" ImageHeight="233" ImageWidth="478" />--%>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox blue inner">
                        <div class="thumbnail-round">
                            <img src='<%= domainName & "ui/media/dist/round/blue.png" %>' height="113" width="122" alt="" class="roundedcontainer">
                            <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" ImageType="Small" HTMLID="6" ImageWidth="139" ImageHeight="139" ImageClass="normal-thumb" />
                            
                        </div>
                        <div class="box-content">
                            <h5 class="title-box"><a href='<%= domainName & "/Activity" %>' class="bluefont">Activities</a></h5>
                            
                            <p>
                                 <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText2" HTMLID="6"  ShowEdit="false" />
                            </p>
                            <a href='<%= domainName & "/Activity" %>' class="readmorebtn">View More</a>
                        </div>
                        <uc1:UserControlEditButton runat="server" ID="EditActivity" HTMLID="110"  TextEdit="true" TextType="Small" ImageEdit="true" ImageType="Small" ImageHeight="139" ImageWidth="139" />
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox orange inner">
                        <div class="thumbnail-round">
                            <img src='<%= domainName & "ui/media/dist/round/orange.png" %>' height="113" width="122" alt="" class="roundedcontainer">
                            <uc1:UserHTMLImage runat="server" ID="UserHTMLImage3" ImageType="Small" HTMLID="79" ImageWidth="139" ImageHeight="139" ImageClass="normal-thumb" />
                            
                        </div>
                        <div class="box-content">
                            <h5 class="title-box"><a href='<%= domainName & "historical-timeline"  %>' class="">Historical Timeline</a></h5>
                            
                            <p>
                                  <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText3" HTMLID="79" ShowEdit="false"/>
                            </p>
                            <a href='<%= domainName & "historical-timeline"  %>' class="readmorebtn">View More</a>
                        </div>
                        <uc1:UserControlEditButton runat="server" ID="EditTimelineThumb" HTMLID="79"  TextEdit="true" TextType="Small" ImageEdit="true" ImageType="Small" ImageHeight="139" ImageWidth="139" />
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
            </div>

        
        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->

        <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns" class="nomargintop">
        <div class="container">
             <ul class="list-unstyled">
                <li class="purplebg">
                    <a href='<%= domainName & "PhotoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/photo-gallery.png" %>' height="25" width="29" alt=""></span>
                        Photo gallery
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "VideoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/video-gallery.png" %>' height="20" width="25" alt=""></span>
                        Video gallery
                    </a>
                </li>
                <li class="bluebg">
                    <a class="bookNowIframe" href='<%= domainName & "BookNow-Pop"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/book-now.png" %>' height="27" width="27" alt=""></span>
                        Book now
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "Map-Pop" %>' class="mapFancyIframe">
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/location-map.png" %>' height="26" width="32" alt=""></span>
                        Location map
                    </a>
                </li>
                <li class="purplebg">
                    <a href='<%= domainName & "Travel-Air" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/flights.png" %>' height="14" width="38" alt=""></span>
                        Flights
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "Sir-Bani-Yas-History" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/history.png" %>' height="20" width="27" alt=""></span>
                        History
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "WhatsNew" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/whats-new.png" %>' height="17" width="18" alt=""></span>
                        What's New
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
</asp:Content>

