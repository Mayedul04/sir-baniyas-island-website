﻿Imports System.Data.SqlClient
Partial Class Conservation
    Inherits System.Web.UI.Page
    Public domainName As String

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub

    Function ImageText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 130)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += "<img src=""" & domainName & "Admin/" & reader("BigImage") & """ style=""margin-right:30px; margin-bottom:20px;"" alt="""" class=""pull-left"">"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&BigDetails=0&Link=0&SmallImageWidth=139&SmallImageHeight=120&BigImageWidth=570&BigImageHeight=300")


            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


    Function MangroveHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 141)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=370&SmallImageHeight=262&BigImageWidth=733&BigImageHeight=458")

                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"

                M += "<a href=""" & domainName & "Mangrove"" class=""linkbtn"">" & reader("Title") & "<span class=""arrow""></span></a>"""
                M += "</img>"



              
            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Function BreedingHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 5)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=370&SmallImageHeight=262&BigImageWidth=733&BigImageHeight=458")

                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"

                M += "<a href=""" & domainName & "Breeding"" class=""linkbtn"">" & reader("Title") & "<span class=""arrow""></span></a>"""
                M += "</img>"




            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Function ArchaeologicalSites() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 143)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()
               

                M += "<div class=""twopanel"">"
                M += "<h3 class=""title""><a href=""" & domainName & "Archaelogical-Details"">Archaeological Site</a></h3>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=1&Link=0&SmallImageWidth=478&SmallImageHeight=233&BigImageWidth=733&BigImageHeight=458")
                M += "<span class=""arrow""></span><br/>"
                M += "<a href=""" & domainName & "Archaelogical-Details"" class=""readmorebtn"">Read more</a>"

                M += "</div>"
                M += "<div class=""twopanel imageholder"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """>"
                M += "</img>"
                 M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


    Function SaltDomeHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 128)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()


                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=271&SmallImageHeight=233&BigImageWidth=733&BigImageHeight=458")

                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""transparent"">"
                M += "<h3 class=""title"">" & reader("Title") & "</h3>"
                M += "<p>" & reader("SmallDetails") & "</p>"

                M += "<a href=""" & domainName & "Salt-Domes"" class=""readmorebtn"">Read more</a>"
              
            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


    Function FloraHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 132)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()



                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ width=""359px"" alt=""" & reader("ImageAltText") & """ class=""transparent"">"
                M += "<h3 class=""title"">" & reader("Title") & "</h3>"
                M += "<p>" & reader("SmallDetails") & "</p>"

                M += "<a href=""" & domainName & "Flora"" class=""readmorebtn"">Read more</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SecondImage=0&File=0&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=359&SmallImageHeight=359&BigImageWidth=733&BigImageHeight=458")

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Function ActivityHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 77)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += "<div class=""thumbnail-round"">"
                M += "<img src=""" & domainName & "ui/media/dist/round/blue.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += " <img src=""" & domainName & "Admin/" & reader("BigImage") & """ height=""137"" width=""132"" class=""normal-thumb"" alt=""" & reader("ImageAltText") & """>"
                M += "</img>"
                M += "</div>"
                M += "<div class=""box-content"">"
                M += "<h5 class=""title-box""><a href=""" & domainName & "Stay"" class=""bluefont"">" & reader("Title") & "</a></h5>"
                M += reader("BigDetails")
                M += "<a href=""" & domainName & "Stay"" class=""readmorebtn"">View More</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&BigDetails=1&Link=0&SmallImageWidth=139&SmallImageHeight=144&BigImageWidth=733&BigImageHeight=458")
                M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


    Function HistoricalTimelineHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 79)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += "<div class=""thumbnail-round"">"
                M += "<img src=""" & domainName & "ui/media/dist/round/orange.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += " <img src=""" & domainName & "Admin/" & reader("BigImage") & """ height=""137"" width=""132"" class=""normal-thumb"" alt=""" & reader("ImageAltText") & """>"
                M += "</img>"
                M += "</div>"
                M += "<div class=""box-content"">"
                M += "<h5 class=""title-box""><a href=""" & domainName & "Historical-Timeline"" class="""">" & reader("Title") & "</a></h5>"
                M += reader("BigDetails")
                M += "<a href=""" & domainName & "Historical-Timeline"" class=""readmorebtn"">View More</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&BigDetails=1&Link=0&SmallImageWidth=139&SmallImageHeight=120&BigImageWidth=733&BigImageHeight=458")
                M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Public Function IslandMap() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where HTMLID=108"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read
                Dim pdfurl As String = domainName & "Admin/" & reader("FileUploaded").ToString()
                M += "<div class=""thumbnail-round"">"
                M += " <img src=""" & domainName & "ui/media/dist/round/blue.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""normal-thumb"" alt="""">"
                M += "</img>"
                M += "</div>"

                M += "<div class=""box-content"">"
                M += " <h5 class=""title-box""><a href=""" & pdfurl & """ class="" bluefont"" target=""_blank"">Island Map</a></h5>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a href=""" & pdfurl & """ class=""readmorebtn"" target=""_blank"">View Map</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=139&SmallImageHeight=120&BigImageWidth=733&BigImageHeight=458")
                M += "</div>"



            End While

        End If
        conn.Close()
        Return M
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ltImage.Text = ImageText()
        ltMangrove.Text = MangroveHTMLText()
        ltBreeding.Text = BreedingHTMLText()
        ltArchaeological.Text = ArchaeologicalSites()
        ltSaltDomain.Text = SaltDomeHTMLText()
        ltFlora.Text = FloraHTMLText()
        ltActivity.Text = ActivityHTMLText()
        ltHistoricalTimeLine.Text = HistoricalTimelineHTMLText()
        ltIsland.Text = IslandMap()

    End Sub
End Class
