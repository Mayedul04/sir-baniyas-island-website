﻿Imports System.Data.SqlClient

Partial Class Breeding
    Inherits System.Web.UI.Page
    Public domainName, galurl As String
    Public pgalid, vgalid As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Sub LoadGallery(ByVal galid As Integer)
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If count = 1 Then
                    galurl = domainName & "Admin/" & reader("BigImage").ToString()
                Else
                    lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Spa"">Photo Gallery<span class=""arrow""></span></a>"
                End If
                count += 1
            End While
        End If
        cn.Close()
    End Sub

    Public Function SuccessStory() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  * FROM List_SuccessStory where Status='1' and Lang='en' order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read



                M += "<li class="""">"

                M += "<div class=""roundedbox orange inner"">"
                M += "<div class=""thumbnail-round"">"
                M += " <img src=""" & domainName & "ui/media/dist/round/orange.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += " <img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""normal-thumb"" alt=""" & reader("ImageAltText") & """>"
                M += " </div>"
                M += " <div class=""box-content"">"
                M += " <h5 class=""title-box""><a href=""javascript:;"">" & reader("Title") & " </a></h5>"

                M += " <p>" & reader("SmallDetails") & "</p>"
                M += " <a href=""" & domainName & "Success-Story/" & reader("ListID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""readmorebtn"">Read more</a>"
                M += " </div>"
                M += " </div>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-SuccessStory/ListEdit.aspx?lid=" + reader("ListID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

                M += " </li>"

            End While

        End If
        conn.Close()
        Return M
    End Function
    Public Function getGalleryID(ByVal htmlid As Integer, fieldname As String) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select " & fieldname & " from HTML where  HTMLID=@HTMLID and Lang='en'"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("HTMLID", Data.SqlDbType.NVarChar, 50).Value = htmlid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If IsDBNull(reader(fieldname)) = False Then
                    retstr = reader(fieldname).ToString()
                End If
                


            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        pgalid = getGalleryID(218, "GalleryID")
        vgalid = getGalleryID(219, "VGalleryID")
        If pgalid <> 0 Then
            LoadGallery(pgalid)
        End If
        If vgalid <> 0 Then
            lblVGalleryUrl.Text = "<a href=""" & domainName & "Videos/Gallery/" & vgalid & "/Breeding" & """ class=""linkbtn"">Video Gallery<span class=""arrow""></span></a>"
        End If
        ltSuccessStory.Text = SuccessStory()
        ltSuccessAdd.Text = Utility.showAddButton(Request, domainName & "Admin/A-SuccessStory/ListEdit.aspx")

    End Sub
End Class
