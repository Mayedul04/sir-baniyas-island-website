﻿
Partial Class MasterPage
    Inherits System.Web.UI.MasterPage

    Public domainName As String

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Function GetInstagram() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT InstagramImage.* FROM InstagramImage where InstagramImage.Status=1 order by ImageID desc"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        'cmd.Parameters.Add("RestaurantID", Data.SqlDbType.Int)
        'cmd.Parameters("RestaurantID").Value = 0
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try

            M += "<li style=""width: 100%"">"
            M += "<ul class=""instagramImages"">"
            While reader.Read()
                i = i + 1
                M += "<li>"
                M += "<a class=""fancyMeiframe3"" href=""" & domainName & "Admin/" & reader("SmallImage") & """ height=""55"" width=""52"" rel=""instagallery"" >"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString & """ alt=""" & reader("Title").ToString.Replace("#", "-").Replace("@", "-").Replace("?", "-") & """ >"
                M += "</a>"
                M += "</li>"

                If i Mod 12 = 0 Then
                    M += "</ul>"
                    M += "</li>"
                    M += "<li style=""width: 100%"">"
                    M += "<ul class=""instagramImages"">"
                End If

            End While
            M += "</ul>"
            M += "</li>"
            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ltinstagram.Text = GetInstagram()
        BannerSection()



    End Sub



    Public Sub BannerSection()

        'If Request.Url.AbsoluteUri = domainName Then
        '    UserBanner.SectionName = "Home"
        '    UserBannerFooter.SectionName = "Home"
        'End If
        'If Request.Url.AbsoluteUri.ToLower.ToLower.Contains("default.aspx") Then
        '    UserBanner.SectionName = "Home"
        '    UserBannerFooter.SectionName = "Home"
        'End If
        'If Request.Url.AbsoluteUri.ToLower.ToLower.Contains(domainName) Then
        '    UserBanner.SectionName = "Home"
        '    UserBannerFooter.SectionName = "Home"
        'End If

        UserBanner.SectionName = "Home"
        UserBannerFooter.SectionName = "Home"



    End Sub


    Protected Sub btnNewsLetter_Click(sender As Object, e As System.EventArgs) Handles btnNewsLetter.Click
        hdnSubscriptionTime.Value = DateTime.Now.ToShortDateString
        Dim retVal As Integer = 0
        retVal = sdsNewsletterSubscription.Insert()
        If retVal > 0 Then
            Dim body As String = mailsndtoClient("Thank you for subscribing to our newsletter. We will be in touch with you soon.")
            Utility.SendMail("Sir Bani Yas Newsletter", "shohel@digitalnexa.com", txtNewsLetterEmail.Text, "", "shohel@wvss.net", "Newslettter Subscription for Sir Bani Yas", body)

            Dim body1 As String = mailsndtoLPD()
            Utility.SendMail("Client", txtNewsLetterEmail.Text, "shohel@digitalnexa.com", "shohel@digitalnexa.com", "shohel@wvss.net", "Request for Newsletter subscription", body1)

            Response.Redirect(domainName & "Thankyou")

        End If



    End Sub

    Public Function mailsndtoClient(ByVal body As String) As String
        Dim M As String = String.Empty

        M += "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
        M += "<html xmlns=""http://www.w3.org/1999/xhtml"">"
        M += "<head>"
        M += "<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />"
        M += "<title>Al Noon</title>"
        M += "</head>"

        M += "<body style=""margin:0; padding:0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#595959; text-align:justify;"">"
        M += "<table width=""700"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td style=""padding:50px 0; background:#f7f7f7;""><table width=""600"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td style=""background:#fff; padding:15px 0;""><table width=""570"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td>"

        M += "<p style=""font-family:Arial, Helvetica, sans-serif; font-size:14px; margin:0 0 15px 0; padding:0; color:#595959;"">This is an AUTOMATED MESSAGE. Please do not reply or respond back to this mail.</p>"

        M += "<p style=""font-family:Arial, Helvetica, sans-serif; font-size:14px; margin:0 0 15px 0; padding:0; color:#595959;"">" + body + "</p>"

        M += "<p style=""font-family:Arial, Helvetica, sans-serif; font-size:14px; margin:0 0 0 0; padding:0; color:#595959;""><strong>Best Regards,</strong><br />"
        M += "Sir Bani Yas</p>"
        M += "</td>"
        M += "</tr>"
        M += "</table></td>"
        M += "</tr>"
        M += "</table></td>"
        M += "</tr>"
        M += "</table>"
        M += "</body>"
        M += "</html>"

        Return M


    End Function

    Public Function mailsndtoLPD() As String
        Dim M As String = String.Empty

        M += "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
        M += "<html xmlns=""http://www.w3.org/1999/xhtml"">"
        M += "<head>"
        M += "<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />"
        M += "<title>Al Noon</title>"
        M += "</head>"

        M += "<body style=""margin:0; padding:0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#595959; text-align:justify;"">"
        M += "<table width=""700"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td style=""padding:50px 0; background:#f7f7f7;""><table width=""600"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td style=""background:#fff; padding:15px 0;""><table width=""570"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td>"

        M += "<p style=""font-family:Arial, Helvetica, sans-serif; font-size:14px; margin:0 0 15px 0; padding:0; color:#595959;"">Request for Newsletter subscription from email ID: " & txtNewsLetterEmail.Text & " </p>"
        M += "</td>"
        M += "</tr>"
        M += "</table></td>"
        M += "</tr>"
        M += "</table></td>"
        M += "</tr>"
        M += "</table>"
        M += "</body>"
        M += "</html>"

        Return M


    End Function


    Public Function getArabicUrl() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM Mappings where EnUrl=@url"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("url", Data.SqlDbType.NVarChar)
        cmd.Parameters("url").Value = Request.Url.ToString()
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            retstr = reader("ArUrl").ToString()
        Else
            If Request.Url.ToString.ToLower().Contains("innerphoto") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/InnerPhotoGallery/" & getUrlFrom("Gallery", "GalleryID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("innervideo") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/InnerVideoGallery/" & getUrlFrom("Video", "GalleryID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("photos") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/Photos/Gallery/" & getUrlFrom("Gallery", "GalleryID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("videos") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/Videos/Gallery/" & getUrlFrom("Video", "GalleryID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("event") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/Event/" & getUrlFrom("List_Event", "EventID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("featured-news") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/Featured-News/" & getUrlFrom("List_News", "NewsID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("whats-new-details") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/Whats-New-Details/" & getUrlFrom("List_New", "ListID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("meetanimal") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/MeetAnimal/" & getUrlFrom("List1", "ListID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("success-story") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/Success-Story/" & getUrlFrom("List_SuccessStory", "ListID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("interesting-facts") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/Interesting-Facts/" & Page.RouteData.Values("page")
            End If
        End If
        Return retstr
    End Function
    Public Function getUrlFrom(ByVal tablename As String, ByVal fieldname As String, ByVal fieldvalue As String) As String
        Dim retstr As String = ""
        Dim selectString As String
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        If tablename = "Gallery" Or tablename = "Video" Then
            selectString = "SELECT * FROM " & tablename & " where " & fieldname & "=@id"
        Else
            selectString = "SELECT " & tablename & ".Title, " & tablename & "." & fieldname & " FROM   " & tablename & " INNER JOIN " & tablename & " AS List_Event_1 ON " & tablename & ".MasterID = List_Event_1.MasterID WHERE  (List_Event_1." & fieldname & " =@id) AND (" & tablename & ".Lang = 'ar')"
        End If

        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("id", Data.SqlDbType.Int)
        cmd.Parameters("id").Value = fieldvalue
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            If tablename = "Gallery" Or tablename = "Video" Then
                retstr = reader(fieldname).ToString() & "/" & Utility.EncodeTitle(reader("ArTitle").ToString, "-")
            Else
                retstr = reader(fieldname).ToString() & "/" & Utility.EncodeTitle(reader("Title").ToString, "-")
            End If
        End If
        Return retstr
    End Function
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Response.Redirect(domainName & "Search/" & txtSearch.Text)
    End Sub



    Protected Sub lnkbtnGo_Click(sender As Object, e As EventArgs) Handles lnkbtnGo.Click
        Response.Redirect(domainName & "Play-Your-Trip/" & txtplanyourtrip.SelectedValue)
    End Sub
End Class

