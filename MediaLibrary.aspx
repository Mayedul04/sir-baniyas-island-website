﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="MediaLibrary.aspx.vb" Inherits="MediaLibrary" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/MyCaptcha.ascx" TagPrefix="uc1" TagName="MyCaptcha" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Media-Centre" %>'>Media Centre</a></li>
            <li class="active">Media Library Request</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">

        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="95" ShowEdit="false" />
                </h1>
                <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="95" />
                <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="271" />
            </section>
            <!-- -- welcome-text ends here -- -->
        </div>


        <!-- Land & Water Activites -->
        <div class="activitesContainerSection mediaCenterNews">

            <!-- Bottom FIx -->
            <div class="bottomFix">

                <div class="container">

                    <div class="row">

                        <!-- Land Activities -->
                        <div class="col-md-6 col-sm-6">
                            <div class="landActivitesSection featuredNews">
                                <h2 class="purplebg">
                                    <a>Register</a>
                                </h2>
                                <!-- Book Now Container -->
                                <div class="mediaFormContainer">

                                    <!-- Form Row -->
                                    <div class="row marginBtm40">

                                        <div class="col-md-6 col-sm-6">
                                            <!-- Normal Input -->
                                            <div class="form-group">
                                                <label>
                                                    fIRST nAME
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="form3" ControlToValidate="txtFName" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                </label>
                                                <asp:TextBox ID="txtFName" runat="server" class="form-control"></asp:TextBox>

                                            </div>
                                            <!-- Normal Input -->
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <!-- Normal Input -->
                                            <div class="form-group">
                                                <label>
                                                    Last nAME
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="form3" ControlToValidate="txtLName" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                </label>
                                                <asp:TextBox ID="txtLName" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                            <!-- Normal Input -->
                                        </div>

                                        <div class="col-md-12 col-sm-12">
                                            <!-- Normal Input -->
                                            <div class="form-group">
                                                <label>
                                                    Email
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="form3" ControlToValidate="txtEmail" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="form3" ControlToValidate="txtEmail" ErrorMessage="Invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                </label>
                                                <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                            <!-- Normal Input -->
                                        </div>
                                        <br />
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <label>
                                                    <br />
                                                </label>
                                                <div class="row">
                                                <uc1:MyCaptcha runat="server" ID="MyCaptcha" vgroup="form3" />
                                                <asp:Label ID="lbCaptchaError" ForeColor="Red" runat="server"></asp:Label>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Row -->

                                    <p>By clicking Sign Up, you agree to our <a href='<%= domainName & "Terms-Condition"%>' class="purplefont" target="_blank">Terms</a> and that you have read our <a href='<%= domainName & "Privacy-Policy" %>' target="_blank" class="purplefont">Data Use Policy</a>, including our <a href='<%= domainName & "Disclaimer" %>' target="_blank" class="purplefont">Cookie Use</a>.</p>

                                    <div class="row marginBtm20">
                                        <div class="col-md-4">
                                            <asp:Button ID="btnSend" runat="server" ValidationGroup="form3" class="SignUpBttn purplebg" Text="Sign Up" />
                                            <asp:HiddenField ID="hdnDate" runat="server" />
                                            <asp:HiddenField ID="hdnPass" runat="server" />
                                        </div>

                                    </div>
                                    <asp:SqlDataSource ID="sdsRegister" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                        InsertCommand="INSERT INTO [RegisteredUser] ([FName], [LName], [Email], [Pass1], [User1], [RegisteredDate],[Approve]) VALUES (@FName, @LName, @Email, @Pass1, @User1, @RegisteredDate,@Approve)">

                                        <InsertParameters>
                                            <asp:ControlParameter ControlID="txtFName" Name="FName" PropertyName="Text" Type="String" />
                                            <asp:ControlParameter ControlID="txtLName" Name="LName" PropertyName="Text" Type="String" />
                                            <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" Type="String" />
                                            <asp:ControlParameter ControlID="hdnPass" Name="Pass1" PropertyName="Value" Type="String" />
                                            <asp:ControlParameter ControlID="txtFName" Name="User1" PropertyName="Text" Type="String" />
                                            <asp:ControlParameter ControlID="hdnDate" Name="RegisteredDate" PropertyName="Value" Type="DateTime" />
                                            <asp:Parameter DefaultValue="False" Name="Approve" />
                                        </InsertParameters>

                                    </asp:SqlDataSource>

                                </div>
                                <!-- Book Now Container -->


                            </div>
                        </div>
                        <!-- Land Activities -->

                        <!-- Water Activities -->
                        <div class="col-md-6 col-sm-6">
                            <div class="waterActivitesSection whatsNewSection">
                                <h2 class="orangebg">
                                    <a>Login</a>
                                </h2>
                                <div class="mediaFormContainer">

                                    <!-- Form Row -->
                                    <div class="row marginBtm40">



                                        <div class="col-md-12 col-sm-12">
                                            <!-- Normal Input -->
                                            <div class="form-group">
                                                <label>
                                                    Email
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="form5" ControlToValidate="txtEmail1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationGroup="form5" ControlToValidate="txtEmail1" ErrorMessage="Invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="form4" ControlToValidate="txtEmail1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="form4" ControlToValidate="txtEmail1" ErrorMessage="Invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                </label>
                                                <asp:TextBox ID="txtEmail1" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                            <!-- Normal Input -->
                                        </div>

                                        <div class="col-md-12 col-sm-12">
                                            <!-- Normal Input -->
                                            <div class="form-group">
                                                <label>
                                                    Password
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="form4" ControlToValidate="txtPass" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                </label>
                                                <asp:TextBox ID="txtPass" runat="server" TextMode="Password" class="form-control"></asp:TextBox>
                                            </div>
                                            <!-- Normal Input -->
                                        </div>

                                    </div>
                                    <!-- Form Row -->



                                    <div class="row marginBtm20">
                                        <div class="col-md-4">
                                            <asp:Button ID="btnSubmit" class="SignUpBttn orangebg" ValidationGroup="form4" runat="server" Text="Sign In" />

                                        </div>
                                        <asp:LinkButton ID="btnForgotPass" ValidationGroup="form5" runat="server">Forgot Password?</asp:LinkButton>
                                    </div>


                                </div>
                                <!-- Book Now Container -->
                            </div>
                        </div>
                        <!-- Water Activities -->

                    </div>

                </div>

            </div>
            <!-- Bottom FIx -->

        </div>
        <!-- Land & Water Activites -->



    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->



    <section class="container marginTop20">
        <div class="footer-nav">
            <div class="row">

                <div class="col-sm-4 col-sm-offset-2">
                    <a href='<%= domainName & "Island-News" %>' class="orangebg" >
                        <img src='<%= domainName & "ui/media/dist/media-center/island-news-icon.png"%>' alt="" style="margin-top: -5px">
                        Island News</a>
                </div>

                <div class="col-sm-4">
                    <a href='<%= domainName & "Press-Release-News" %>' class="bluebg">
                        <img src='<%= domainName & "ui/media/dist/media-center/pressrelese-icon.png"%>' alt="">
                        Press Release</a>
                </div>

               

            </div>
        </div>
    </section>
</asp:Content>

