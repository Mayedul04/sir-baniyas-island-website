﻿Imports System.Data.SqlClient
Partial Class Flora
    Inherits System.Web.UI.Page
    Public domainName, galurl As String
    Public pgalid, vgalid As Integer



    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub




    Public Function FloraHTML() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where HTMLID=132"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read

                M += "<h1 class=""capital"">Welcome to Flora</h1>"""
                  M += "<p>" & reader("SmallDetails") & "</p>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-Flora/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")


            End While

        End If
        conn.Close()
        Return M
    End Function

    Public Function FloraList() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  List_Flora.* FROM  List_Flora where List_Flora.Status='1' and Lang='en' order by List_Flora.Sortindex"
        ''    Dim selectString = "SELECT * FROM List_Restaurant where Status='1' and Category='Bar' and RestaurantID <> 0 order by Sortindex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then

           

            While reader.Read


                M += "<li>"

                M += "<div class=""row"">"
                M += "<div class=""col-sm-3"">"

                M += "<div class=""greenbg border-box bigheight"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail "">"
                M += "<span class=""arrow""></span>"
                M += "</img>"
                M += "</div>"
                M += "</div>"

                M += "<div class=""col-sm-9"">"
                M += " <h4 class=""title"">" & reader("Title") & "</h4>"
                M += reader("BigDetails")
                M += "</div>"

                M += "</div>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-Flora/ListEdit.aspx?lid=" & reader("ListID"))

                M += "</li>"



            End While

            
            reader.Read()

        End If
        conn.Close()
        Return M
    End Function



    Function PhotoHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 126)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()


                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=366&SmallImageHeight=292&BigImageWidth=733&BigImageHeight=458")

                M += "<div class=""purplebg border-box"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"

                M += "<a href=""" & domainName & "Photos/Gallery/14/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""linkbtn"">" & reader("Title") & "<span class=""arrow""></span></a>"
                M += "</img>"
                M += "<p>" & reader("SmallDetails") & "</p>"

                M += "<a href=""" & domainName & "Photos/Gallery/14/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""readmoreBttn purple"">Read more</a>"
               M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function
    Public Sub LoadGallery()
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = 14
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()

                lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Spa"">Photo Gallery<span class=""arrow""></span></a>"

            End While
        End If
        cn.Close()
    End Sub

    Public Function PhotoGalleryImage() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM Gallery where Status='1' and GalleryID=@GalleryID order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        cmd.Parameters("GalleryID").Value = 14

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read

                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ style=""width:100% !important;max-height:360px;height:auto;"" alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"
                M += Utility.showEditButton(Request, domainName & "Admin/A-Gallery/GalleryEdit.aspx?galleryId=" + reader("GalleryID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

            End While

        End If
        conn.Close()
        Return M
    End Function



    Function SaltDomeHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 128)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()



                M += "<img src=""" & domainName & "Admin/" & reader("BigImage") & """ width=""457px"" alt=""" & reader("ImageAltText") & """ class=""transparent"">"
                M += "<h3 class=""title"">" & reader("Title") & "</h3>"
                M += "<p>" & reader("SmallDetails") & "</p>"

                M += "<a href=""" & domainName & "Salt-Domes"" class=""readmorebtn"">Read more</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=1s&BigDetails=0&Link=0&SmallImageWidth=100&SmallImageHeight=86&BigImageWidth=733&BigImageHeight=458")

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Function ConservationHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 130)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()



                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""transparent"">"
                M += "<h3 class=""title"">" & reader("Title") & "</h3>"
                M += "<p>" & reader("SmallDetails") & "</p>"

                M += "<a href=""" & domainName & "Conservation"" class=""readmorebtn"">Read more</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=100&SmallImageHeight=86&BigImageWidth=733&BigImageHeight=458")

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


    Function ActivityHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 77)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += "<div class=""thumbnail-round"">"
                M += "<img src=""" & domainName & "ui/media/dist/round/blue.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += " <img src=""" & domainName & "Admin/" & reader("BigImage") & """ height=""137"" width=""132"" class=""normal-thumb"" alt=""" & reader("ImageAltText") & """>"
                M += "</img>"
                M += "</div>"
                M += "<div class=""box-content"">"
                M += "<h5 class=""title-box""><a href=""" & domainName & "Stay"" class=""bluefont"">" & reader("Title") & "</a></h5>"
                M += reader("BigDetails")
                M += "<a href=""" & domainName & "Stay"" class=""readmorebtn"">View More</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&BigDetails=1&Link=0&SmallImageWidth=139&SmallImageHeight=144&BigImageWidth=733&BigImageHeight=458")
                M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


    Function HistoricalTimelineHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 79)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += "<div class=""thumbnail-round"">"
                M += "<img src=""" & domainName & "ui/media/dist/round/orange.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += " <img src=""" & domainName & "Admin/" & reader("BigImage") & """ height=""137"" width=""132"" class=""normal-thumb"" alt=""" & reader("ImageAltText") & """>"
                M += "</img>"
                M += "</div>"
                M += "<div class=""box-content"">"
                M += "<h5 class=""title-box""><a href=""" & domainName & "Historical-Timeline"" class="""">" & reader("Title") & "</a></h5>"
                M += reader("BigDetails")
                M += "<a href=""" & domainName & "Historical-Timeline"" class=""readmorebtn"">View More</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&BigDetails=1&Link=0&SmallImageWidth=139&SmallImageHeight=120&BigImageWidth=733&BigImageHeight=458")
                M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Public Function IslandMap() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where HTMLID=108"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read

                M += "<div class=""thumbnail-round"">"
                M += " <img src=""" & domainName & "ui/media/dist/round/blue.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""normal-thumb"" alt="""">"
                M += "</img>"
                M += "</div>"

                M += "<div class=""box-content"">"
                M += " <h5 class=""title-box""><a href=""" & domainName & "IslandMap-Pop"" class=""mapFancyIframe bluefont"">Island Map</a></h5>"
                'M += " <h6 class=""sub-title"">" & reader("Title") & "</h6>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a href=""" & domainName & "IslandMap-Pop"" class=""readmorebtn mapFancyIframe"">View Map</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=139&SmallImageHeight=120&BigImageWidth=733&BigImageHeight=458")
                M += "</div>"



            End While

        End If
        conn.Close()
        Return M
    End Function

    Function FloraHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 132)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += "<div class=""thumbnail-round"">"
                M += "<img src=""" & domainName & "ui/media/dist/round/green.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += " <img src=""" & domainName & "Admin/" & reader("SmallImage") & """ height=""137"" width=""132"" class=""normal-thumb"" alt=""" & reader("ImageAltText") & """>"
                M += "</img>"
                M += "</div>"
                M += "<div class=""box-content"">"
                M += "<h5 class=""title-box""><a href=""" & domainName & "Flora"" class="""">" & reader("Title") & "</a></h5>"
                M += reader("BigDetails")
                M += "<a href=""" & domainName & "Flora"" class=""readmorebtn"">View More</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=1&Link=0&SmallImageWidth=139&SmallImageHeight=120&BigImageWidth=733&BigImageHeight=458")
                M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


    Public Function getGalleryID(ByVal htmlid As Integer, fieldname As String) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select " & fieldname & " from HTML where  HTMLID=@HTMLID and Lang='en'"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("HTMLID", Data.SqlDbType.NVarChar, 50).Value = htmlid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If IsDBNull(reader(fieldname)) = False Then
                    retstr = reader(fieldname).ToString()
                End If



            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Sub LoadGallery(ByVal galid As Integer)
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If count = 1 Then
                    galurl = domainName & "Admin/" & reader("BigImage").ToString()
                Else
                    lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Flora"">Photo Gallery<span class=""arrow""></span></a>"
                End If
                count += 1
            End While
        End If
        cn.Close()
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ltFloraList.Text = FloraList()
        'ltPhoto.Text = PhotoHTMLText()
        ltSaltDome.Text = SaltDomeHTMLText()
        ltConservation.Text = ConservationHTMLText()
        ltActivity.Text = ActivityHTMLText()
        ltHistoricalTimeline.Text = HistoricalTimelineHTMLText()
        ltIslanMap.Text = IslandMap()
        ' ltFlora.Text = FloraHTMLText()
        LoadGallery()
        pgalid = getGalleryID(227, "GalleryID")
        If pgalid <> 0 Then
            LoadGallery(pgalid)
        End If
        
    End Sub
End Class
