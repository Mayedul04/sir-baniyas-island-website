﻿Imports System.Data.SqlClient
Partial Class Animal
    Inherits System.Web.UI.Page
    Public domainName, galurl As String
    Public pgalid, vgalid As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Function getGalleryID(ByVal htmlid As Integer, fieldname As String) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select " & fieldname & " from HTML where  HTMLID=@HTMLID and Lang='en'"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("HTMLID", Data.SqlDbType.NVarChar, 50).Value = htmlid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If IsDBNull(reader(fieldname)) = False Then
                    retstr = reader(fieldname).ToString()
                End If



            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Sub LoadGallery(ByVal galid As Integer)
        Dim count As Integer = 1
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString

        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString As String = "Select GalleryID from Gallery where  ParentGalleryID=@PGalleryID"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        cmd.Parameters.Add("PGalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                IntermediateGallery(reader("GalleryID").ToString())
            End While
        Else
            LoadChildGallery(galid)
        End If
        cn.Close()
    End Sub
    Public Sub IntermediateGallery(ByVal galid As String)
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString

        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString As String = "Select GalleryID from Gallery where  ParentGalleryID=@PGalleryID"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        cmd.Parameters.Add("PGalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                LoadChildGallery(reader("GalleryID").ToString())
            End While
        Else
            LoadChildGallery(galid)
        End If
        cn.Close()
    End Sub
    Public Sub LoadChildGallery(ByVal galid As String)
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If galid = "" Then
                    galurl = domainName & "Admin/" & reader("BigImage").ToString()
                Else
                    lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Spa"">معرض الصور<span class=""arrow""></span></a>"
                End If
                'count += 1
            End While
        End If
        cn.Close()
    End Sub
    Public Function AnimalCategory() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM List1 where Status='1' and Lang='en' order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read




                M += "<li class=""col-sm-3  col-xs-6"">"
                M += "<div class=""orangebg border-box meetanim"">"
                M += "<a href=""" & domainName & "MeetAnimal/" & reader("ListID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>"
                M += "<img src=""" & domainName & "Admin/" & reader("BigImage") & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"
                M += " <span class=""arrow""></span>"
                M += " </a>"
                M += "</div>"
                M += "<h4 class=""orangefont title""><a href=""" & domainName & "MeetAnimal/" & reader("ListID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>" & reader("Title") & "</a></h4>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-WildLifeAnimal/List1Edit.aspx?l1id=" + reader("ListID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

                M += "</li>"



            End While

        End If
        conn.Close()
        Return M
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        pgalid = getGalleryID(223, "GalleryID")
        vgalid = getGalleryID(224, "VGalleryID")
        lblVGalleryUrl.Text = "<a href=""" & domainName & "Videos/Gallery/" & vgalid & "/Breeding" & """ class=""linkbtn"">Video Gallery<span class=""arrow""></span></a>"
        LoadGallery(pgalid)
        ltAnimalCategory.Text = AnimalCategory()

     
    End Sub


End Class
