﻿Imports System.Data.SqlClient

Partial Class MasterPageInner
    Inherits System.Web.UI.MasterPage

    Public domainName, sectionname As String

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Function getArabicUrl() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM Mappings where EnUrl=@url"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("url", Data.SqlDbType.NVarChar)
        cmd.Parameters("url").Value = Request.Url.ToString()
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            retstr = reader("ArUrl").ToString()
        Else
            If Request.Url.ToString.ToLower().Contains("innerphoto") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/InnerPhotoGallery/" & getUrlFrom("Gallery", "GalleryID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("innervideo") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/InnerVideoGallery/" & getUrlFrom("Video", "GalleryID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("photos") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/Photos/Gallery/" & getUrlFrom("Gallery", "GalleryID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("videos") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/Videos/Gallery/" & getUrlFrom("Video", "GalleryID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("event") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/Event/" & getUrlFrom("List_Event", "EventID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("featured-news") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/Featured-News/" & getUrlFrom("List_News", "NewsID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("whats-new-details") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/Whats-New-Details/" & getUrlFrom("List_New", "ListID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("meetanimal") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/MeetAnimal/" & getUrlFrom("List1", "ListID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("success-story") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/Success-Story/" & getUrlFrom("List_SuccessStory", "ListID", Page.RouteData.Values("id"))
            End If
            If Request.Url.ToString.ToLower().Contains("interesting-facts") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/Interesting-Facts/" & Page.RouteData.Values("page")
            End If
        End If
        Return retstr
    End Function
    Public Function getUrlFrom(ByVal tablename As String, ByVal fieldname As String, ByVal fieldvalue As String) As String
        Dim retstr As String = ""
        Dim selectString As String
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        If tablename = "Gallery" Or tablename = "Video" Then
            selectString = "SELECT * FROM " & tablename & " where " & fieldname & "=@id"
        Else
            selectString = "SELECT " & tablename & ".Title, " & tablename & "." & fieldname & " FROM   " & tablename & " INNER JOIN " & tablename & " AS List_Event_1 ON " & tablename & ".MasterID = List_Event_1.MasterID WHERE  (List_Event_1." & fieldname & " =@id) AND (" & tablename & ".Lang = 'ar')"
        End If

        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("id", Data.SqlDbType.Int)
        cmd.Parameters("id").Value = fieldvalue
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            If tablename = "Gallery" Or tablename = "Video" Then
                retstr = reader(fieldname).ToString() & "/" & Utility.EncodeTitle(reader("ArTitle").ToString, "-")
            Else
                retstr = reader(fieldname).ToString() & "/" & Utility.EncodeTitle(reader("Title").ToString, "-")
            End If
        End If
        Return retstr
    End Function
    Protected Function GetCurrentClass(pageName As String) As String
        Dim virtualPath As String = Page.AppRelativeVirtualPath.Replace("~/", "").ToLower()
        If virtualPath.Contains(pageName) Then
            Return "active"

        Else
            Return ""
        End If

    End Function

    Protected Function GetCurrentClass1(ParamArray pageName() As String) As String
        Dim virtualPath As String = Page.AppRelativeVirtualPath.ToLower()


        For Each pageName1 In pageName
            If virtualPath.ToLower().Contains(pageName1.ToLower()) Then
                Return "active"
                'Else
                'Return ""
            End If
        Next





    End Function
   
    Public Function GetInstagram() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT InstagramImage.* FROM InstagramImage where InstagramImage.Status=1 order by InstagramImage.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        'cmd.Parameters.Add("RestaurantID", Data.SqlDbType.Int)
        'cmd.Parameters("RestaurantID").Value = 0
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try

            M += "<li style=""width: 100%"">"
            M += "<ul class=""instagramImages"">"
            While reader.Read()
                i = i + 1
                M += "<li>"

                M += "<a title=""" & "@" & reader("Instauser") & "<br/>" & reader("Title").ToString() & """ class=""fancyMeiframe3"" href=""" & domainName & "Admin/" & reader("SmallImage") & """ height=""55"" width=""52"" rel=""instagallery"" >"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString & """ width=""80%"" alt=""" & reader("Title").ToString.Replace("#", "-").Replace("@", "-").Replace("?", "-") & """ >"
                M += "</a>"
                M += "</li>"

                If i Mod 12 = 0 Then
                    M += "</ul>"
                    M += "</li>"
                    M += "<li style=""width: 100%"">"
                    M += "<ul class=""instagramImages"">"
                End If

            End While
            M += "</ul>"
            M += "</li>"
            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ltinstagram.Text = GetInstagram()
        BannerSection()



    End Sub

    'Public Function GetBanner() As String
    '    Dim M As String = ""
    '    Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
    '    con.Open()
    '    Dim sql = "SELECT * FROM Banner WHERE SectionName=@SectionName and Status='1' order by SortIndex"
    '    Dim cmd = New SqlCommand(sql, con)
    '    cmd.Parameters.Clear()
    '    cmd.Parameters.Add("SectionName", Data.SqlDbType.NVarChar, 100).Value = ""

    '    Try
    '        Dim reader = cmd.ExecuteReader()
    '        If reader.HasRows = True Then
    '            M += "<ul class=""list-unstyled slides"">"
    '            While reader.Read()
    '                M += "<li>"
    '                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString & """ alt=""" & reader("ImageAltText").ToString & """ height=""633"" width=""1600"">"
    '                M += Utility.showEditButton(Request, domainName & "Admin/A-Banner/TopBannerEdit.aspx?bannerId=" + reader("BannerID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")
    '                M += "</li>"
    '            End While
    '            M += "</ul>"
    '        End If
    '        reader.Close()
    '        con.Close()
    '        Return M
    '    Catch ex As Exception
    '        Return M
    '    End Try
    'End Function

    Public Sub BannerSection()
        '  If Request.Url.AbsoluteUri.ToLower = (domainName & "Travel-Air").ToLower Then
        ' TRAVEL SECTION START
        If Request.Url.AbsoluteUri.ToLower().ToString() = domainName & "travel-air" Then
            UserBanner.SectionName = "Travel_Air"
            UserBannerFooter.SectionName = "Travel_Air"
        End If
        If Request.Url.AbsoluteUri.ToLower().ToString() = domainName & "flights" Then
            UserBanner.SectionName = "Travel_Air"
            UserBannerFooter.SectionName = "Travel_Air"
        End If
        If Request.Url.AbsoluteUri.ToLower().ToString() = domainName & "travel-road" Then
            UserBanner.SectionName = "Travel_Road"
            UserBannerFooter.SectionName = "Travel_Road"
        End If

        If Request.Url.AbsoluteUri.ToLower().ToString() = domainName & "travel-sea" Then
            UserBanner.SectionName = "Travel_Sea"
            UserBannerFooter.SectionName = "Travel_Sea"
        End If
        If Request.Url.AbsoluteUri.ToLower = domainName & "travel" Then
            UserBanner.SectionName = "Travel"
            UserBannerFooter.SectionName = "Travel"
        End If
        ' TRAVEL SECTION START



        'Others START
        If Request.Url.AbsoluteUri.ToLower = domainName & "archaelogical-details" Then
            UserBanner.SectionName = "archaelogy"
            UserBannerFooter.SectionName = "archaelogy"
        End If

        If Request.Url.AbsoluteUri.ToLower.ToString().Contains("interesting-facts") Then
            UserBanner.SectionName = "Interesting_Fact"
            UserBannerFooter.SectionName = "Interesting_Fact"
        End If

        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("play-your-trip") Then
            UserBanner.SectionName = "Search"
            UserBannerFooter.SectionName = "Search"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("search") Then
            UserBanner.SectionName = "Search"
            UserBannerFooter.SectionName = "Search"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("contact-us") Then
            UserBanner.SectionName = "Contact_Us"
            UserBannerFooter.SectionName = "Contact_Us"
        End If
        If Request.Url.AbsoluteUri = domainName Then
            UserBanner.SectionName = "Home"
            UserBannerFooter.SectionName = "Home"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "thank-you" Or Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "thanks-news" Then
            UserBanner.SectionName = "Home"
            UserBannerFooter.SectionName = "Home"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "disclaimer" Then
            UserBanner.SectionName = "Disclaimer"
            UserBannerFooter.SectionName = "Disclaimer"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "historical-timeline" Then
            UserBanner.SectionName = "Historical_Timeline"
            UserBannerFooter.SectionName = "Historical_Timeline"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "philosophy" Then
            UserBanner.SectionName = "Philosophy"
            UserBannerFooter.SectionName = "Philosophy"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "about-tdic" Then
            UserBanner.SectionName = "About_TDIC"
            UserBannerFooter.SectionName = "About_TDIC"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("privacy") Then
            UserBanner.SectionName = "privacy"
            UserBannerFooter.SectionName = "privacy"
        End If

        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("terms") Then
            UserBanner.SectionName = "Terms_Condition"
            UserBannerFooter.SectionName = "Terms_Condition"
        End If
        'Others END


        'WildLife Start
        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("wildlife") Then
            UserBanner.SectionName = "Wildlife"
            UserBannerFooter.SectionName = "Wildlife"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("arabian-wildlife") Then
            UserBanner.SectionName = "Arabian_WildLife"
            UserBannerFooter.SectionName = "Arabian_WildLife"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "animal" Then
            UserBanner.SectionName = "Animal"
            UserBannerFooter.SectionName = "Animal"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("meetanimal") Then
            UserBanner.SectionName = "MeetAnimal"
            UserBannerFooter.SectionName = "MeetAnimal"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("vet") Then
            UserBanner.SectionName = "Vet_Conservation"
            UserBannerFooter.SectionName = "Vet_Conservation"
        End If
        'WildLife Start


        'Adventure Start

        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("adventure") Or Request.Url.AbsoluteUri.ToLower.ToString.Contains("advanture-activity") Then
            UserBanner.SectionName = "Adventure"
            UserBannerFooter.SectionName = "Adventure"
        End If

        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("breeding") Then
            UserBanner.SectionName = "Breeding"
            UserBannerFooter.SectionName = "Breeding"
        End If

        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("conservation") Then
            UserBanner.SectionName = "Conservation"
            UserBannerFooter.SectionName = "Conservation"
        End If



        'Deresrt Island START
        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "dalma-island" Then
            UserBanner.SectionName = "Delma_Island"
            UserBannerFooter.SectionName = "Delma_Island"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "desert-islands" Then
            UserBanner.SectionName = "Desert_Island"
            UserBannerFooter.SectionName = "Desert_Island"
        End If

        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "discovery-islands" Then
            UserBanner.SectionName = "Discovery_Island"
            UserBannerFooter.SectionName = "Discovery_Island"
        End If
        
        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("flora") Then
            UserBanner.SectionName = "Flora"
            UserBannerFooter.SectionName = "Flora"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("mangrove") Then
            UserBanner.SectionName = "Mangrove"
            UserBannerFooter.SectionName = "Mangrove"
        End If
        'Deresrt Island END


        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "events" Or Request.Url.AbsoluteUri.ToLower.ToString().Contains("event") Or Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "events-summary" Then
            UserBanner.SectionName = "Events"
            UserBannerFooter.SectionName = "Events"
        End If


       

        'Media Centre  Start
        If Request.Url.AbsoluteUri.ToLower.ToString().Contains("featured-news") Then
            UserBanner.SectionName = "Island_News"
            UserBannerFooter.SectionName = "Island_News"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "island-news" Then
            UserBanner.SectionName = "Island_News"
            UserBannerFooter.SectionName = "Island_News"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "press-release-news" Then
            UserBanner.SectionName = "Press_Release"
            UserBannerFooter.SectionName = "Press_Release"
        End If

        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "media-centre" Then
            UserBanner.SectionName = "Media_Centre"
            UserBannerFooter.SectionName = "Media_Centre"
        End If

        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("media") Then
            UserBanner.SectionName = "Media_Centre"
            UserBannerFooter.SectionName = "Media_Centre"
        End If

        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "whatsnew" Then
            UserBanner.SectionName = "Whats_New"
            UserBannerFooter.SectionName = "Whats_New"
        End If

        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("Whats") Then
            UserBanner.SectionName = "Whats_New"
            UserBannerFooter.SectionName = "Whats_New"
        End If
        'Media Centre  Start


      

        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("gallery") Then
            UserBanner.SectionName = "Gallery"
            UserBannerFooter.SectionName = "Gallery"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("success-story") Then
            UserBanner.SectionName = "Success_Story"
            UserBannerFooter.SectionName = "Success_Story"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "experiences" Then
            UserBanner.SectionName = "Experience_Gallery"
            UserBannerFooter.SectionName = "Experience_Gallery"
        End If



        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("sir-bani-yas") Then
            UserBanner.SectionName = "Sir_Bani_Yas"
            UserBannerFooter.SectionName = "Sir_Bani_Yas"
        End If

        'Relax Start
        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("dine") Then
            UserBanner.SectionName = "Dine"
            UserBannerFooter.SectionName = "Dine"
        End If

        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("spa") Then
            UserBanner.SectionName = "Spa"
            UserBannerFooter.SectionName = "Spa"
        End If

        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("relax") Then
            UserBanner.SectionName = "Relax"
            UserBannerFooter.SectionName = "Relax"
        End If

        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("beaches") Then
            UserBanner.SectionName = "Beach_Pool"
            UserBannerFooter.SectionName = "Beach_Pool"
        End If
        'Relax END

        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("sitemap") Then
            UserBanner.SectionName = "Sitemap"
            UserBannerFooter.SectionName = "Sitemap"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString() = domainName & "salt-domes" Then
            UserBanner.SectionName = "Saltdomes"
            UserBannerFooter.SectionName = "Saltdomes"
        End If
        If Request.Url.AbsoluteUri.ToLower.ToString.Contains("stay") Then
            UserBanner.SectionName = "Stay"
            UserBannerFooter.SectionName = "Stay"
        End If
        



    End Sub


    Protected Sub btnNewsLetter_Click(sender As Object, e As System.EventArgs) Handles btnNewsLetter.Click
        hdnSubscriptionTime.Value = DateTime.Now.ToShortDateString
        Dim retVal As Integer = 0
        retVal = sdsNewsletterSubscription.Insert()
        If retVal > 0 Then
            Dim body As String = mailsndtoClient()
            Utility.SendMail("Sir Bani Yas Newsletter", "noreply@sirbaniyasisland.com", txtNewsLetterEmail.Text, "", "jatin@digitalnexa.com", "Latest News for Sir Bani Yas", body)

            Dim body1 As String = mailsndtoLPD()
            Utility.SendMail("Client", txtNewsLetterEmail.Text, "info@tdic.ae", "", "jatin@digitalnexa.com", "Subscription for Latest News ", body1)

            Response.Redirect(domainName & "Thanks-news")

        End If



    End Sub

    Public Function mailsndtoClient() As String

        Dim emailtemplate As String = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
        emailtemplate += "<html xmlns=""http://www.w3.org/1999/xhtml"">"
        emailtemplate += "<head><meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />"
        emailtemplate += "<title>Sir Bani Yas</title></head>"
        emailtemplate += "<body style=""margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; font-size: 12px; background-color: #f1f1f1;"">"
        emailtemplate += "<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
        emailtemplate += "<tr><td style=""background-color: #f1f1f1;"">"
        emailtemplate += "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        emailtemplate += "<tr><td>&nbsp;</td></tr>"
        emailtemplate += "<tr><td><a href=""http://www.sirbaniyasisland.com/"" target=""_blank""><img src=""" & domainName & "images/banner-img.jpg"" width=""800"" /></a>"
        emailtemplate += "</td></tr><tr><td style=""background-color: #fff;"">"
        emailtemplate += "<table width=""712"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
        emailtemplate += "<tr><td><h1 style=""color: #f58023; text-align: center; text-transform: uppercase; font-weight: normal"">Thank you for signing up for our latest news</h1>"
        emailtemplate += "<h2 style=""color: #333; text-align: center;"">You will receive our latest news in due course</h2></td>"
        emailtemplate += "</tr><tr><td></td></tr>"
        emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
        emailtemplate += "<tr><td style=""height: 52px; text-align: center;""><a href=""mailto:info@tdic.ae"" style=""color: #f58023"">info@tdic.ae</a> or call us"
        emailtemplate += "800-TDIC (8342)<td></tr></table>"
        emailtemplate += "</td></tr><tr><td>&nbsp;</td></tr></table></td></tr><tr>"
        emailtemplate += "<td style="""">&nbsp;</td></tr></table></body></html>"



        Return emailtemplate


    End Function

    Public Function mailsndtoLPD() As String
        Dim M As String = String.Empty

        M += "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
        M += "<html xmlns=""http://www.w3.org/1999/xhtml"">"
        M += "<head>"
        M += "<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />"
        M += "<title>Al Noon</title>"
        M += "</head>"

        M += "<body style=""margin:0; padding:0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#595959; text-align:justify;"">"
        M += "<table width=""700"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td style=""padding:50px 0; background:#f7f7f7;""><table width=""600"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td style=""background:#fff; padding:15px 0;""><table width=""570"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td>"

        M += "<p style=""font-family:Arial, Helvetica, sans-serif; font-size:14px; margin:0 0 15px 0; padding:0; color:#595959;"">Request for News subscription from email ID: " & txtNewsLetterEmail.Text & " </p>"
        M += "</td>"
        M += "</tr>"
        M += "</table></td>"
        M += "</tr>"
        M += "</table></td>"
        M += "</tr>"
        M += "</table>"
        M += "</body>"
        M += "</html>"

        Return M


    End Function

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Response.Redirect(domainName & "Search/" & txtSearch.Text)
    End Sub

    Protected Sub lnkbtnGo_Click(sender As Object, e As EventArgs) Handles lnkbtnGo.Click
        Response.Redirect(domainName & "Play-Your-Trip/" & txtplanyourtrip.SelectedValue)
    End Sub
End Class

