﻿
Partial Class MasterPageInner
    Inherits System.Web.UI.MasterPage

    Public domainName As String

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub

    Public Function getArabicUrl() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM Mappings where EnUrl=@url"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("url", Data.SqlDbType.NVarChar)
        cmd.Parameters("url").Value = Request.Url.ToString()
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            retstr = reader("ArUrl").ToString()
        Else
            If Request.Url.ToString.ToLower().Contains("adventure-activity") Then
                retstr = "http://www.sirbaniyasisland.com/Ar/Adventure-Activity/" & getUrlFrom("List_Advanture_Activity", "ListID", Page.RouteData.Values("id"))
            End If
            
        End If
        Return retstr
    End Function
    Public Function getUrlFrom(ByVal tablename As String, ByVal fieldname As String, ByVal fieldvalue As String) As String
        Dim retstr As String = ""
        Dim selectString As String
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
       
            selectString = "SELECT " & tablename & ".Title, " & tablename & "." & fieldname & " FROM   " & tablename & " INNER JOIN " & tablename & " AS List_Event_1 ON " & tablename & ".MasterID = List_Event_1.MasterID WHERE  (List_Event_1." & fieldname & " =@id) AND (" & tablename & ".Lang = 'ar')"

        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("id", Data.SqlDbType.Int)
        cmd.Parameters("id").Value = fieldvalue
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
          
                retstr = reader(fieldname).ToString() & "/" & Utility.EncodeTitle(reader("Title").ToString, "-")

        End If
        Return retstr
    End Function
    Public Function GetInstagram() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT InstagramImage.* FROM InstagramImage where InstagramImage.Status=1 order by InstagramImage.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        'cmd.Parameters.Add("RestaurantID", Data.SqlDbType.Int)
        'cmd.Parameters("RestaurantID").Value = 0
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try

            M += "<li style=""width: 100%"">"
            M += "<ul class=""instagramImages"">"
            While reader.Read()
                i = i + 1
                M += "<li>"
                M += "<a title=""" & "@" & reader("InstaUser") & "<br/>" & reader("Title").ToString() & """ class=""fancyMeiframe3"" href=""" & domainName & "Admin/" & reader("SmallImage") & """ height=""55"" width=""52"" rel=""instagallery"" >"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString & """ width=""80%"" alt=""" & reader("Title").ToString.Replace("#", "-").Replace("@", "-").Replace("?", "-") & """ >"
                M += "</a>"
                M += "</li>"

                If i Mod 12 = 0 Then
                    M += "</ul>"
                    M += "</li>"
                    M += "<li style=""width: 100%"">"
                    M += "<ul class=""instagramImages"">"
                End If

            End While
            M += "</ul>"
            M += "</li>"
            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ltinstagram.Text = GetInstagram()
        'GetBanner()

        UserAdvantureBanner.SectionName = getMasterID()
        UserBannerFooter.SectionName = "Adventure"

    End Sub
    Public Function getMasterID() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT MasterID FROM List_Advanture_Activity where ListID=@ListID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'Dim M As String = ""
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        While reader.Read()
            M = reader("MasterID").ToString()
        End While
        conn.Close()
        Return M
    End Function
    Protected Sub btnNewsLetter_Click(sender As Object, e As System.EventArgs) Handles btnNewsLetter.Click
        hdnSubscriptionTime.Value = DateTime.Now.ToShortDateString
        Dim retVal As Integer = 0
        retVal = sdsNewsletterSubscription.Insert()
        If retVal > 0 Then
            Dim body As String = mailsndtoClient()
            Utility.SendMail("Sir Bani Yas Newsletter", "noreply@sirbaniyasisland.com", txtNewsLetterEmail.Text, "", "jatin@digitalnexa.com", "Latest News for Sir Bani Yas", body)

            Dim body1 As String = mailsndtoLPD()
            Utility.SendMail("Client", txtNewsLetterEmail.Text, "info@tdic.ae", "", "jatin@digitalnexa.com", "Subscription for Latest News ", body1)

            Response.Redirect(domainName & "Thanks-news")

        End If



    End Sub

    Public Function mailsndtoClient() As String

        Dim emailtemplate As String = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
        emailtemplate += "<html xmlns=""http://www.w3.org/1999/xhtml"">"
        emailtemplate += "<head><meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />"
        emailtemplate += "<title>Sir Bani Yas</title></head>"
        emailtemplate += "<body style=""margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; font-size: 12px; background-color: #f1f1f1;"">"
        emailtemplate += "<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
        emailtemplate += "<tr><td style=""background-color: #f1f1f1;"">"
        emailtemplate += "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        emailtemplate += "<tr><td>&nbsp;</td></tr>"
        emailtemplate += "<tr><td><a href=""http://www.sirbaniyasisland.com/"" target=""_blank""><img src=""" & domainName & "images/banner-img.jpg"" width=""800"" /></a>"
        emailtemplate += "</td></tr><tr><td style=""background-color: #fff;"">"
        emailtemplate += "<table width=""712"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
        emailtemplate += "<tr><td><h1 style=""color: #f58023; text-align: center; text-transform: uppercase; font-weight: normal"">Thank you for signing up for our latest news</h1>"
        emailtemplate += "<h2 style=""color: #333; text-align: center;"">You will receive our latest news in due course</h2></td>"
        emailtemplate += "</tr><tr><td></td></tr>"
        emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
        emailtemplate += "<tr><td style=""height: 52px; text-align: center;""><a href=""mailto:info@tdic.ae"" style=""color: #f58023"">info@tdic.ae</a> or call us"
        emailtemplate += "800-TDIC (8342)<td></tr></table>"
        emailtemplate += "</td></tr><tr><td>&nbsp;</td></tr></table></td></tr><tr>"
        emailtemplate += "<td style="""">&nbsp;</td></tr></table></body></html>"



        Return emailtemplate


    End Function

    Public Function mailsndtoLPD() As String
        Dim M As String = String.Empty

        M += "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
        M += "<html xmlns=""http://www.w3.org/1999/xhtml"">"
        M += "<head>"
        M += "<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />"
        M += "<title>Al Noon</title>"
        M += "</head>"

        M += "<body style=""margin:0; padding:0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#595959; text-align:justify;"">"
        M += "<table width=""700"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td style=""padding:50px 0; background:#f7f7f7;""><table width=""600"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td style=""background:#fff; padding:15px 0;""><table width=""570"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td>"

        M += "<p style=""font-family:Arial, Helvetica, sans-serif; font-size:14px; margin:0 0 15px 0; padding:0; color:#595959;"">Request for News subscription from email ID: " & txtNewsLetterEmail.Text & " </p>"
        M += "</td>"
        M += "</tr>"
        M += "</table></td>"
        M += "</tr>"
        M += "</table></td>"
        M += "</tr>"
        M += "</table>"
        M += "</body>"
        M += "</html>"

        Return M


    End Function



    Public Function GetBanner() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT Advanture_Banner.* FROM Advanture_Banner where Advanture_Banner.Status=1 and Category=1 order by Advanture_Banner.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'Dim M As String = ""
        'cmd.Parameters.Add("RestaurantID", Data.SqlDbType.Int)
        'cmd.Parameters("RestaurantID").Value = 0
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try

            While reader.Read()
                M = Utility.EncodeTitle(reader("SectionName"), "-")
                If Page.RouteData.Values("t").ToString.ToLower.Contains(Utility.EncodeTitle(reader("SectionName"), "-").ToLower) Then
                    UserAdvantureBanner.SectionName = reader("SectionName").ToString
                    UserBannerFooter.SectionName = "Adventure"
                End If
            End While
            
            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Response.Redirect(domainName & "Search/" & txtSearch.Text)
    End Sub


  Protected Sub lnkbtnGo_Click(sender As Object, e As EventArgs) Handles lnkbtnGo.Click
        Response.Redirect(domainName & "Play-Your-Trip/" & txtplanyourtrip.SelectedValue)
    End Sub
End Class

