﻿Imports System.Data.SqlClient

Partial Class FeaturedNews
    Inherits System.Web.UI.Page
    Public domainName As String
    Public pageid As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Function getFeaturedNews() As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String
        Dim cmd As SqlCommand
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        If Not Page.RouteData.Values("id") Is Nothing Then

            selectString1 = "Select *  from List_News where NewsID=" & Page.RouteData.Values("id")
            cmd = New SqlCommand(selectString1, cn)
            ' cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Else
            selectString1 = "Select top 1.*  from List_News where Featured=1 and Lang=@Lang and Status=1"
            cmd = New SqlCommand(selectString1, cn)
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        End If


       
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If Not Page.RouteData.Values("id") Is Nothing Then
                    DynamicSEO.PageID = Page.RouteData.Values("id")
                Else
                    DynamicSEO.PageID = hdnPageID.Value
                End If
                hdnPageID.Value = reader("NewsID")
                retstr += "<div class=""col-md-4""><div class=""orangebg border-box noheight"">"
                If IsDBNull(reader("BigImage")) = False Then
                    retstr += "<img src=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"
                Else
                    retstr += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"
                End If

                retstr += "<span class=""arrow""></span></div></div>"

                retstr += "<div class=""col-md-8""><h2 class=""orangefont"">" & reader("Title").ToString() & "</h2>"
                If IsDBNull(reader("Publisher")) = False Then
                    retstr += "<h6 class=""greenfont"">" & reader("Publisher").ToString() & "</h6>"
                End If

                retstr += reader("BigDetails").ToString() & Utility.showEditButton(Request, domainName & "Admin/A-News/NewsEdit.aspx?nid=" & reader("NewsID") & "&Small=1&Big=1&Footer=1")
                retstr += Utility.showAddButton(Request, domainName & "Admin/A-News/NewsEdit.aspx?cat=1&Small=1&Big=1&Footer=1")
                retstr += "<ul class=""bottomLinks"">"

                If IsDBNull(reader("FileUploaded")) = False Then
                    retstr += "<li><a target=""_blank"" href=""" & domainName & "Admin/" & reader("FileUploaded").ToString() & """>"
                    retstr += "<i><img src=""" & domainName & "ui/media/dist/media-center/pdf-icon.jpg"" alt=""""></i>Read the whole story</a></li>"
                End If
                'If getVideoId() <> "" Then
                '    retstr += "<li><a class=""photopopup"" target=""_blank"" href=""" & domainName & "VideoPop/News/" & getVideoId() & """>"
                '    retstr += "<i><img src=""" & domainName & "ui/media/dist/media-center/video-icon.jpg"" alt=""""></i>Play Video</a></li>"
                'End If
                retstr += "</ul></div>"

            End While
        End If

        cn.Close()
        Return retstr
    End Function
    Public Function OtherNews() As String
        Dim retstr As String = ""
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select  * from List_News where Status=1 and Lang=@Lang and Featured=1 and NewsID not in (" & hdnPageID.Value & ") order by LastUpdated desc, SortIndex"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<div class=""roundedbox green inner oneBlock""><div class=""thumbnail-round"">"
                retstr += "<img src=""" & domainName & "ui/media/dist/round/green.png" & """ height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                retstr += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ class=""normal-thumb"" alt=""""></div>"
                retstr += "<div class=""box-content""><h5 class=""title-box""><a href=""" & domainName & "Featured-News/" & reader("NewsID").ToString() & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """ class=""greenfont"">" & reader("Title").ToString() & "</a></h5>"
                If IsDBNull(reader("Publisher")) = False Then
                    retstr += "<h6 class=""orangefont"">" & reader("Publisher").ToString() & "</h6>"
                End If
                retstr += "<p>" & reader("SmallDetails").ToString() & "</p>"
                retstr += "<a href=""" & domainName & "Featured-News/" & reader("NewsID").ToString() & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """ class=""readmoreBttn orange"">Read more</a>" & Utility.showEditButton(Request, domainName & "Admin/A-News/NewsEdit.aspx?nid=" & reader("NewsID") & "&Small=1&Big=1&Footer=0") & "</div>"
                retstr += " </div>"
            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function getVideoId() As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select  top 1.* from CommonVideo where TableName='News' and TableMasterID=" & Page.RouteData.Values("id")
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr = reader("CommonGalleryID").ToString()
            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
           
            DynamicSEO.PageID = Page.RouteData.Values("ID")
        End If

    End Sub
End Class
