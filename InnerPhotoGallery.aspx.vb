﻿Imports System.Data.SqlClient

Partial Class InnerPhotoGallery
    Inherits System.Web.UI.Page
    Public domainName, title, parent As String
    Public PageList As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Function GetParentGalleryNav() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT Title  FROM   Gallery Where GalleryID=@GalleryID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = hdnParentID.Value

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            retstr = "<li><a href=""" & domainName & "InnerPhotoGallery/" & hdnParentID.Value & "/" & reader("Title").ToString() & """>" & reader("Title").ToString() & "</a></li>"

        End If
        conn.Close()
        Return retstr
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT Title, ParentGalleryID  FROM   Gallery Where GalleryID=@GalleryID"
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("id")

            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()
                title = reader("Title").ToString()
                hdnParentID.Value = reader("ParentGalleryID").ToString()
            End If
            conn.Close()
            If hdnParentID.Value <> 0 Then
                parent = GetParentGalleryNav()
            Else
                parent = ""
            End If
            DynamicSEO.PageID = Page.RouteData.Values("id")
        End If
    End Sub
    Private Function getPageNumber() As Integer
        Dim totalRows As Integer = 0
        Dim secondvalue As Integer = 0
        Dim sConn As String
        Dim selectString1 As String = "Select COUNT(0) from Gallery where  Status=1 and Lang=@Lang and ParentGalleryID=@PGalleryID"
        '  Dim selectString2 As String = "SELECT COUNT(0)  FROM [dbo].[CommonGallery] where Status=1 GROUP BY TableName,TableMasterID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        If Not Page.RouteData.Values("id") Is Nothing Then
            cmd.Parameters.Add("PGalleryID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("id")
        Else
            cmd.Parameters.Add("PGalleryID", Data.SqlDbType.Int, 32).Value = 0
        End If

       
        totalRows = Math.Ceiling((cmd.ExecuteScalar) / 8)
        cn.Close()
        Return totalRows
    End Function
    Public Function Gallery() As String
        PageList = ""
        Dim currentPage As Integer = 0
        Dim pageNumber As Integer = 0
        Dim searchCriteria As String = ""
        If Page.RouteData.Values("page") Is Nothing Then
            currentPage = 1
        Else
            currentPage = Page.RouteData.Values("page")
        End If

        searchCriteria = domainName & "InnerPhotoGallery/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title") & "/"
        pageNumber = getPageNumber()


        If pageNumber > 1 Then
            PageList = GetPager(currentPage, pageNumber, 5, searchCriteria)
        End If
        Dim NW As String = "<ul class=""list-unstyled row"">"
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString1 As String = ""




        selectString1 += "  (Select Gallery.GalleryId as MasterID, Gallery.Title, Gallery.ArTitle  , Gallery.SmallImage , Gallery.LastUpdated , 'Gallery' as TName, ParentGalleryID from Gallery where Status=1 and ParentGalleryID=@PGalleryID)"
        Dim selectString As String = "DECLARE @PageNum AS INT; DECLARE @PageSize AS INT; SET @PageNum =" & currentPage & "; SET @PageSize = 8; Select * from ( select  ROW_NUMBER()  over(ORDER BY List1.Title ASC) AS RowNum  , MasterID, Title ,ArTitle, SmallImage, LastUpdated, TName from" & selectString1


        selectString = selectString + "As List1) as MyTable WHERE RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 AND @PageNum * @PageSize"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        If Not Page.RouteData.Values("id") Is Nothing Then
            cmd.Parameters.Add("PGalleryID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("id")
        Else
            cmd.Parameters.Add("PGalleryID", Data.SqlDbType.Int, 32).Value = 0
        End If
        Dim reader As SqlDataReader = cmd.ExecuteReader()

        While reader.Read
            NW += "<li class=""col-sm-3 col-xs-6""><a href=""" & domainName & "Photos/" & reader("TName") & "/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """><div class=""img-container"">"
            If IsDBNull(reader("SmallImage")) = False Then
                NW += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ height=""190"" alt="""">"
            Else
                NW += "<h4>No Image</h4>"
            End If

            NW += "</div><h5 class=""title"">" & reader("Title").ToString() & "</h5>"
            ' NW += "<span class=""pull-left"">" & Convert.ToDateTime(reader("LastUpdated")).ToString("MMM dd, yyyy") & "</span>"
            'NW += "<div class=""extra""><span class=""pull-right""><img src=""" & domainName & "ui/media/dist/icons/worldicon.png"" height=""15"" width=""16"" alt="""">"
            'NW += "Photos: " & CountItem(reader("TName").ToString(), reader("MasterID")) & "</span></div>"
            NW += "</a>"
            If reader("TName").ToString() = "Gallery" Then
                NW += Utility.showEditButton(Request, domainName & "Admin/A-Gallery/GalleryEdit.aspx?galleryId=" & reader("MasterID").ToString())
            End If
            NW += "</li>"


        End While
        cn.Close()
        NW = NW + "</ul>"
        Return NW
    End Function
    Public Function CountItem(ByVal tablename As String, ByVal fieldvalue As Integer) As String
        Dim totalRows As Integer = 0
        Dim sConn As String
        Dim selectString1 As String
        If tablename = "Gallery" Then
            selectString1 = "Select COUNT(0) from  GalleryItem where  Status=1 and GalleryID=" & fieldvalue
        Else
            selectString1 = "Select COUNT(0) from  CommonGallery where  Status=1 and TableName='" & tablename & "' and TableMasterID=" & fieldvalue
        End If

        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        totalRows = cmd.ExecuteScalar
        cn.Close()
        Return totalRows
    End Function
    Public Shared Function GetPager(ByVal presentPageNum As Integer, ByVal totalNumOfPage As Integer, ByVal totalPageNumToShow As Integer, ByVal urlToNavigateWithQStr As String) As String
        Dim i As Integer
        Dim loopStartNum, loopEndNum, presentNum, maxShownNum As Integer
        Dim pagerString As String = ""
        presentNum = presentPageNum
        maxShownNum = totalPageNumToShow
        Dim middleFactor As Integer = maxShownNum / 2
        pagerString = "<ul class=""pagination pagination-lg list-unstyled"">"
        If totalNumOfPage <= totalPageNumToShow Then
            loopStartNum = 1
            loopEndNum = totalNumOfPage
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum <= 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li>"
            ' pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        Else
            loopStartNum = If(presentNum <= (middleFactor + 1), 1, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage - (maxShownNum - 1), presentNum - middleFactor))
            loopEndNum = If(presentNum <= (middleFactor + 1), maxShownNum, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage, presentNum + middleFactor))
            loopEndNum = If(loopEndNum > totalNumOfPage, totalNumOfPage, loopEndNum)
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li> "
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        End If

        pagerString = pagerString & "</ul>"
        Return pagerString
    End Function
End Class
