﻿Imports System.Data.SqlClient
Partial Class Map_Pop
    Inherits System.Web.UI.Page
    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Function HtmlText() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where HTMLID=108"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then

            While reader.Read
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&file=1&SecondImage=1&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=1&BigDetails=0&Link=0&SmallImageWidth=139&SmallImageHeight=120&BigImageWidth=733&BigImageHeight=458")


                M += "<img src=""" & domainName & "Admin/" & reader("BigImage") & """ alt="""">"
                M += "<a href=""" & domainName & "Admin/" & reader("BigImage") & """>Click here to view larger image</a>"
                ltPDF.Text = "<a href=""" & domainName & "Admin/" & reader("FileUploaded") & """ target=""_blank""><img src=""" & domainName & "ui/media/dist/icons/pdf-icon.png"" alt=""""></a> "

            End While

        End If
        conn.Close()
        Return M
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ltHtml.Text = HtmlText()
    End Sub
End Class
