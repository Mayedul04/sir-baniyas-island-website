﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Wildlife.aspx.vb" Inherits="Wildlife" %>

<%@ Register Src="~/CustomControl/UCHTMLTitleDetails.ascx" TagPrefix="uc1" TagName="UCHTMLTitleDetails" %>
<%@ Register Src="~/CustomControl/UCHTMLTitle.ascx" TagPrefix="uc1" TagName="UCHTMLTitle" %>
<%@ Register Src="~/CustomControl/UCHTMLImage.ascx" TagPrefix="uc1" TagName="UCHTMLImage" %>
<%@ Register Src="~/CustomControl/UCHTMLTitleDetailsWildLife.ascx" TagPrefix="uc1" TagName="UCHTMLTitleDetailsWildLife" %>
<%@ Register Src="~/CustomControl/UCHTMLSmallDetails.ascx" TagPrefix="uc1" TagName="UCHTMLSmallDetails" %>
<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>








<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName%>'>Home</a></li>
            <li class="active">Wildlife</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->


    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <uc1:UCHTMLTitleDetails runat="server" ID="UCHTMLTitleDetails" HTMLID="101" />
                
            </section>
            <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="176" />
            <!-- -- welcome-text ends here -- -->
        </div>

        <!-- -- title-bar starts here -- -->
        <div class="title-bar orangebg">
            <div class="container">
                <h2 class="title">Wildlife Experience</h2>
                <span class="arrow"></span>
            </div>
        </div>
        <!-- -- title-bar ends here -- -->

        <!-- -- page-breadcrumb starts here -- -->
        <div class="container">
            <div class="page-breadcrumb">
                <ul class="list-unstyled">
                    <li>
                        <a data-scroll href="#arabian">

                    
                            <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle3" HTMLID="3" ShowEdit="false" />
                        </a>
                    </li>
                    <li>
                        <a data-scroll href="#animals">

                                  <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle4" HTMLID="4" ShowEdit="false" />
                      
                        </a>
                    </li>
                    <li>
                        <a data-scroll href="#breeding">
                                  <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle5" HTMLID="5" ShowEdit="false" />
                      
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- -- page-breadcrumb ends here -- -->

        <!-- -- parallax-section starts here -- -->
        <div class="parallax-section">
            <ul class="list-unstyled">
                <li class="greenbg" id="arabian">
                    <div class="image-container">
                        <%--<uc1:UCHTMLImage runat="server" ID="UCHTMLImage" HTMLID="3" ImageHeight="446" ImageWidth="1600" />--%>
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="3" ImageType="Big" />

                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-offset-6 col-sm-offset-4 col-sm-8 col-md-6">
                                <div class="white-wrapper">
                                  
                                    <h3 class="h2 title">
                                        <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="3" ShowEdit="false" />
                                    </h3>
                                    <p>
                                        <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText" HTMLID="3" ImageEdit="True" ImageType="Big" IMGBigWidth="1600" IMGBigHeight="446" IMGSmallWidth="370" IMGSmallHeight="262"  />
                                    </p>



                                    <a href='<%= domainName & "Arabian-Wildlife"%>' class="readmore-btn">View more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="orangebg" id="animals">
                    <div class="image-container">
                       <%-- <uc1:UCHTMLImage runat="server" ID="UCHTMLImage1" HTMLID="4" ImageHeight="446" ImageWidth="1600" />--%>
                                           <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" HTMLID="4" ImageType="Big"  />

                         </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-8 col-md-6">
                                <div class="white-wrapper">
                                    <h3 class="h2 title">
                                        <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle1" HTMLID="4" ShowEdit="false" />
                                    </h3>
                                    <p>
                                         <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText1" HTMLID="4" ImageEdit="True" ImageType="Big" IMGBigWidth="1600" IMGBigHeight="446" IMGSmallWidth="370" IMGSmallHeight="262"  />
                                    </p>
                                    <a href='<%= domainName & "Animal"%>' class="readmore-btn">View more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="bluebg" id="breeding">
                    <div class="image-container">
                        <%--<uc1:UCHTMLImage runat="server" ID="UCHTMLImage2" HTMLID="5" ImageHeight="446" ImageWidth="1600" />--%>
                                           <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="5" ImageType="Big"   />

                         </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-offset-6 col-sm-offset-4 col-sm-8 col-md-6">
                                <div class="white-wrapper">
                                     <h3 class="h2 title">
                                        <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle2" HTMLID="5" ShowEdit="false" />
                                    </h3>
                                    <p>
                                              <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText2" HTMLID="5" ImageEdit="True" ImageType="Big" IMGBigWidth="1600" IMGBigHeight="446" IMGSmallWidth="370" IMGSmallHeight="262"  />
                                   
                                    </p>

                                    <a href='<%= domainName & "Breeding"%>' class="readmore-btn">View more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <!-- -- parallax-section ends here -- -->
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns">
        <div class="container">
             <ul class="list-unstyled">
                <li class="purplebg">
                    <a href='<%= domainName & "PhotoGallery"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/photo-gallery.png"%>' height="25" width="29" alt=""></span>
                        Photo gallery
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "VideoGallery"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/video-gallery.png"%>' height="20" width="25" alt=""></span>
                        Video gallery
                    </a>
                </li>
                <li class="bluebg">
                    <a class="bookNowIframe" href='<%= domainName & "Booknow-Pop"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/book-now.png"%>' height="27" width="27" alt=""></span>
                        Book now
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "Map-Pop"%>' class="mapFancyIframe">
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/location-map.png"%>' height="26" width="32" alt=""></span>
                        Location map
                    </a>
                </li>
                <li class="purplebg">
                    <a href='<%= domainName & "Travel-Air"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/flights.png"%>' height="14" width="38" alt=""></span>
                        Flights
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "Sir-Bani-Yas-History"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/history.png"%>' height="20" width="27" alt=""></span>
                        History
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "WhatsNew"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/whats-new.png"%>' height="17" width="18" alt=""></span>
                        What's New
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->

</asp:Content>

