﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="TravelAir.aspx.vb" Inherits="TravelAir" %>

<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>






<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Travel" %>'>Travel</a></li>
            <li class="active">Travel By Air</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">Travel By Air</h1>
                   
                <%--<p>
                    
                </p>--%>
                
                <%--<div class="row">
                    <div class="col-sm-6">
                        <h2 class="subtitlepage">
                            <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="65" />
                        </h2>
                        <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt2" HTMLID="65" />
                    </div>
                    <div class="col-sm-6">
                        <h2 class="subtitlepage">
                            
                            <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle1" HTMLID="66" />
                        </h2>
                        <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt3" HTMLID="66" />
                    </div>
                </div>--%>

                <div class="row">
                    <div class="col-sm-8">
                        <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt"  HTMLID="61"/>
                        <%--<uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt1" HTMLID="64" />--%>
                    </div>
                   
                    <div class="col-sm-4">
                        <%--<div class="imagethumb">
                            <a href="http://rotanajet.com/" target="_blank">
                                <img src='<%= domainName & "ui/media/dist/travel/moreinforotana.jpg" %>' alt="">
                            </a>
                        </div>--%>
                        <div class="greenBlock">
                             <%=getExternalLink("315") %>
                        </div>

                     

                          <div class="imagethumb slidertravel">
                           <ul class="list-unstyled slides ">
                              <asp:Literal ID="ltImgRotating" runat="server"></asp:Literal>
                           </ul>
                            
                        </div>
                        <div class="greenBlock">
                            <%=getExternalLink("317") %>
                        </div>
                        <%-- <div class="imagethumb">
                            <a href="http://www.seawingslifestyle.ae/private-charter-to-sir-baniyas/" target="_blank">
                                <img src='<%= domainName & "ui/media/dist/travel/seawings ENG.jpg"%>' alt=""/>
                            </a>
                        </div>--%>


                    </div>
                </div>
                 <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="255" />
            </section>
            <!-- -- welcome-text ends here -- -->
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">
        <div class="container">

            <div class="footer-nav">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-2">
                        <a href='<%= domainName & "Travel-Road"%>' class="orangebg">
                            <img src='<%= domainName &"ui/media/dist/icons/road-btn.png"%>' alt="" style="margin-top: -6px">&nbsp  By Road</a>
                    </div>
                    <div class="col-sm-4">
                        <a href='<%= domainName & "Travel-Sea" %>' class="bluebg">
                            <img src='<%= domainName &"ui/media/dist/icons/sea-btn.png"%>' alt="" style="margin-top: -5px">&nbsp  By Sea</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->
</asp:Content>

