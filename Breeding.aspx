﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Breeding.aspx.vb" Inherits="Breeding" %>

<%@ Register Src="~/CustomControl/UCHTMLTitle.ascx" TagPrefix="uc1" TagName="UCHTMLTitle" %>
<%@ Register Src="~/CustomControl/UCHTMLSmallImage.ascx" TagPrefix="uc1" TagName="UCHTMLSmallImage" %>
<%@ Register Src="~/CustomControl/UCHTMLDetails.ascx" TagPrefix="uc1" TagName="UCHTMLDetails" %>
<%@ Register Src="~/CustomControl/UCInterestingFactList.ascx" TagPrefix="uc1" TagName="UCInterestingFactList" %>
<%@ Register Src="~/CustomControl/UCPhotoAlbum.ascx" TagPrefix="uc1" TagName="UCPhotoAlbum" %>
<%@ Register Src="~/CustomControl/UCVideoAlbum.ascx" TagPrefix="uc1" TagName="UCVideoAlbum" %>
<%@ Register Src="~/CustomControl/UCTestimonial.ascx" TagPrefix="uc1" TagName="UCTestimonial" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserVetConservationTeam.ascx" TagPrefix="uc1" TagName="UserVetConservationTeam" %>
<%@ Register Src="~/CustomControl/UserStaticPhotoAlbum.ascx" TagPrefix="uc1" TagName="UserStaticPhotoAlbum" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>

<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName%>'>Home</a></li>
            <li><a href='<%= domainName & "Wildlife"%>'>Wildlife</a></li>
            <li class="active">Breeding & Relocation</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                    
                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="5" />
                </h1>
             
                <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" ShowEdit ="" HTMLID="5"/>

            </section>
            <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="18" />
            <!-- -- welcome-text ends here -- -->

            <div class="row">
                <div class="col-sm-8 col-md-12">
                    <!-- -- success-stories starts here -- -->
               
                    <div class="success-stories">
                              <asp:Literal ID="ltSuccessAdd" runat="server"></asp:Literal>
                        <h2 class="title orangefont">Success Stories</h2>
                        <ul class="list-unstyled slides">
                            <asp:Literal ID="ltSuccessStory" runat="server"></asp:Literal>
                        </ul>
                    </div>
                    <!-- -- success-stories starts here -- -->
                </div>
              
            </div>

        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <!-- -- double-box starts here -- -->
                   
                            <uc1:UCInterestingFactList runat="server" ID="UCInterestingFactList" />

                  
                    <!-- -- double-box ends here -- -->
                </div>

                  <div class="col-sm-4 col-md-2 smallervet">
                    <%--<div class="bluebg single-box smallvets">
                        <img src='<%=domainName &"ui/media/dist/thumbnails/vets.jpg"%>' alt="" class="transparent">
                       
                        <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt1" HTMLID="105" />
                        <a href='<%= domainName & "Vet-Conservation-Team"%>' class="readmorebtn">Read more</a>
                    </div>--%>
                      <uc1:UserVetConservationTeam runat="server" id="UserVetConservationTeam" HTMLID="105" />
                </div>

                <div class="col-sm-3 col-md-2 col-xs-6">
                    <div class="greenbg border-box small">
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="219" ImageType="Small" ImageClass="boxthumbnail" ImageHeight="225" ImageWidth="165" ShowEdit="false" />
                        <asp:Literal ID="lblVGalleryUrl" runat="server"></asp:Literal>
                        <uc1:UserControlEditButton runat="server" ID="EditVideoGallery" HTMLID="219"  VGalleryEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="225" ImageWidth="165" />
                    </div>
                </div>
                <div class="col-sm-3 col-md-2 col-xs-6">
                    <div class="bluebg border-box small">
                      <asp:Literal ID="lblPhotoGallery" runat="server"></asp:Literal>
                       
                      <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="218" ImageType="Small" ImageClass="boxthumbnail" ImageHeight="225" ImageWidth="165" ShowEdit="false" />
                   <a href='<%= galurl %>' class="linkbtn withthumbnail" rel="Spa">Photo Gallery<span class="arrow"></span></a>
                         <uc1:UserControlEditButton runat="server" ID="editPhotoGallery"  HTMLID="218"  PGalleryEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="225" ImageWidth="165"/>
                       <% If pgalid <> 0 then %>
                         <%= Utility.showAddButton(Request, domainName & "Admin/A-Gallery/AllGalleryItemEdit.aspx?galleryId=" & pgalid & "&Lang=en")%>
                        <% End if %>
                    </div>
                </div>
            </div>



            <div class="footer-nav">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-2">
                        <a href='<%=domainName & "Arabian-Wildlife"%>' class="greenbg">
                            <img src="ui/media/dist/icons/wildlife-btn.png" alt="" style="margin-top: -5px">
                            Arabian Wildlife Park</a>
                    </div>
                    <div class="col-sm-4">
                          <a href='<%=domainName & "Animal"%>' class="orangebg">
                            <img src="ui/media/dist/icons/animal-btn.png" height="29" width="29" alt="">
                            Animals</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->
</asp:Content>

