﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Play-Your-Trip - Copy.aspx.vb" Inherits="Play_Your_Trip" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Plan your trip</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->


    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">Plan Your Trip - Romance</h1>
                <p>Discover the secrets of the world beneath the surface of the water. No trip to Sir Bani Yas Island is complete without exploring some of the beautiful reefs on a guided snorkelling.
                </p>
            </section>
            <!-- -- welcome-text ends here -- -->
        </div>
        
        <!-- -- title-bar starts here -- -->
        <div class="title-bar dbluebg">
            <div class="container">
                <h2 class="title">DINE</h2>
                <span class="arrow"></span>
            </div>
        </div>
        <!-- -- title-bar ends here -- -->

        <!-- Content Listing Setion -->
        <div class="container">

            <!-- Search Result Listings -->
            <ul class="searchResultsListings newsLetter">

                <li>
                        
                    <div class="imgHold">
                        <img src="ui/media/dist/plan-your-events/samak.jpg" alt="">
                    </div>

                    <div class="content">
                        <h2 class="dbluefont">Samak </h2>
                        <p>Lorem ipsum dolor sit amet, in suscipit finibus. Nulla et leo non erat porta auctor eget eu velit. Suspendisse et tempus orci. Duis pulvinar est arcu, at aliquet sem aliquam a. Nunc laoreet venenatis semper. </p>
                        <a class="viewButton dbluebg" href="javascript:;">View</a>
                    </div>

                </li>

                <li>
                        
                    <div class="imgHold">
                        <img src="ui/media/dist/plan-your-events/palm.jpg" alt="">
                    </div>

                    <div class="content">
                        <h2 class="dbluefont">The Palm </h2>
                        <p>Lorem ipsum dolor sit amet, in suscipit finibus. Nulla et leo non erat porta auctor eget eu velit. Suspendisse et tempus orci. Duis pulvinar est arcu, at aliquet sem aliquam a. Nunc laoreet venenatis semper. </p>
                        <a class="viewButton dbluebg" href="javascript:;">View</a>
                    </div>

                </li>

            </ul>
            <!-- Search Result Listings -->

        </div>
        <!-- Content Listing Setion -->


        <!-- -- title-bar starts here -- -->
        <div class="title-bar purplebg">
            <div class="container">
                <h2 class="title">Relax</h2>
                <span class="arrow"></span>
            </div>
        </div>
        <!-- -- title-bar ends here -- -->

        <!-- Content Listing Setion -->
        <div class="container">

            <!-- Search Result Listings -->
            <ul class="searchResultsListings newsLetter">

                <li>
                        
                    <div class="imgHold">
                        <img src="ui/media/dist/plan-your-events/spa.jpg" alt="">
                    </div>

                    <div class="content">
                        <h2 class="purplefont">Spa</h2>
                        <p>Lorem ipsum dolor sit amet, in suscipit finibus. Nulla et leo non erat porta auctor eget eu velit. Suspendisse et tempus orci. Duis pulvinar est arcu, at aliquet sem aliquam a. Nunc laoreet venenatis semper. </p>
                        <a class="viewButton purplebg" href="relax-spa.html">View</a>
                    </div>

                </li>

                <li>
                        
                    <div class="imgHold">
                        <img src="ui/media/dist/plan-your-events/beach.jpg" alt="">
                    </div>

                    <div class="content">
                        <h2 class="purplefont">Beaches & Pools </h2>
                        <p>Lorem ipsum dolor sit amet, in suscipit finibus. Nulla et leo non erat porta auctor eget eu velit. Suspendisse et tempus orci. Duis pulvinar est arcu, at aliquet sem aliquam a. Nunc laoreet venenatis semper. </p>
                        <a class="viewButton purplebg" href="relax-beach.html">View</a>
                    </div>

                </li>

            </ul>
            <!-- Search Result Listings -->

        </div>
        <!-- Content Listing Setion -->


        <!-- -- title-bar starts here -- -->
        <div class="title-bar yellowbg">
            <div class="container">
                <h2 class="title">Stay</h2>
                <span class="arrow"></span>
            </div>
        </div>
        <!-- -- title-bar ends here -- -->

        <!-- Content Listing Setion -->
        <div class="container">

            <!-- Search Result Listings -->
            <ul class="searchResultsListings newsLetter">

                <li>
                        
                    <div class="imgHold">
                        <img src="ui/media/dist/plan-your-events/anantara-1.jpg" alt="">
                    </div>

                    <div class="content">
                        <h2 class="yellowfont">Desert Islands Resort & Spa by Anantara</h2>
                        <p>Lorem ipsum dolor sit amet, in suscipit finibus. Nulla et leo non erat porta auctor eget eu velit. Suspendisse et tempus orci. Duis pulvinar est arcu, at aliquet sem aliquam a. Nunc laoreet venenatis semper. </p>
                        <a class="viewButton yellowbg" href="stay-detailed.html">View</a>
                    </div>

                </li>

                <li>
                        
                    <div class="imgHold">
                        <img src="ui/media/dist/plan-your-events/anantara-2.jpg" alt="">
                    </div>

                    <div class="content">
                        <h2 class="yellowfont">Anantara Al Yamm Villa Resort </h2>
                        <p>Lorem ipsum dolor sit amet, in suscipit finibus. Nulla et leo non erat porta auctor eget eu velit. Suspendisse et tempus orci. Duis pulvinar est arcu, at aliquet sem aliquam a. Nunc laoreet venenatis semper. </p>
                        <a class="viewButton yellowbg" href="stay-detailed.html">View</a>
                    </div>

                </li>

                <li>
                        
                    <div class="imgHold">
                        <img src="ui/media/dist/plan-your-events/anantara-3.jpg" alt="">
                    </div>

                    <div class="content">
                        <h2 class="yellowfont">Anantara Al Sahel Villa Resort</h2>
                        <p>Lorem ipsum dolor sit amet, in suscipit finibus. Nulla et leo non erat porta auctor eget eu velit. Suspendisse et tempus orci. Duis pulvinar est arcu, at aliquet sem aliquam a. Nunc laoreet venenatis semper. </p>
                        <a class="viewButton yellowbg" href="stay-detailed.html">View</a>
                    </div>

                </li>

            </ul>
            <!-- Search Result Listings -->

        </div>
        <!-- Content Listing Setion -->


        <!-- -- title-bar starts here -- -->
        <div class="title-bar greenbg">
            <div class="container">
                <h2 class="title">Travel</h2>
                <span class="arrow"></span>
            </div>
        </div>
        <!-- -- title-bar ends here -- -->

        <!-- Content Listing Setion -->
        <div class="container">

            <!-- Search Result Listings -->
            <ul class="searchResultsListings newsLetter">

                <li>
                        
                    <div class="imgHold">
                        <img src="ui/media/dist/plan-your-events/air.jpg" alt="">
                    </div>

                    <div class="content">
                        <h2>By Air </h2>
                        <p>Lorem ipsum dolor sit amet, in suscipit finibus. Nulla et leo non erat porta auctor eget eu velit. Suspendisse et tempus orci. Duis pulvinar est arcu, at aliquet sem aliquam a. Nunc laoreet venenatis semper. </p>
                        <a class="viewButton greenbg" href="travel-air.html">View</a>
                    </div>

                </li>

                <li>
                        
                    <div class="imgHold">
                        <img src="ui/media/dist/plan-your-events/road.jpg" alt="">
                    </div>

                    <div class="content">
                        <h2>By Road </h2>
                        <p>Lorem ipsum dolor sit amet, in suscipit finibus. Nulla et leo non erat porta auctor eget eu velit. Suspendisse et tempus orci. Duis pulvinar est arcu, at aliquet sem aliquam a. Nunc laoreet venenatis semper. </p>
                        <a class="viewButton greenbg" href="travel-road.html">View</a>
                    </div>

                </li>

                <li>
                        
                    <div class="imgHold">
                        <img src="ui/media/dist/plan-your-events/sea.jpg" alt="">
                    </div>

                    <div class="content">
                        <h2>By Sea </h2>
                        <p>Lorem ipsum dolor sit amet, in suscipit finibus. Nulla et leo non erat porta auctor eget eu velit. Suspendisse et tempus orci. Duis pulvinar est arcu, at aliquet sem aliquam a. Nunc laoreet venenatis semper. </p>
                        <a class="viewButton greenbg" href="travel-sea.html">View</a>
                    </div>

                </li>

            </ul>
            <!-- Search Result Listings -->

        </div>
        <!-- Content Listing Setion -->

        


    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns">
        <div class="container">
            <ul class="list-unstyled">
                <li class="purplebg">
                    <a href="photo-gallery.html">
                        <span><img src="ui/media/dist/icons/photo-gallery.png" height="25" width="29" alt=""></span>
                        Photo gallery
                    </a>
                </li>
                <li class="orangebg">
                    <a href="video-gallery.html">
                        <span><img src="ui/media/dist/icons/video-gallery.png" height="20" width="25" alt=""></span>
                        Video gallery
                    </a>
                </li>
                <li class="bluebg">
                    <a href="book-now.html">
                        <span><img src="ui/media/dist/icons/book-now.png" height="27" width="27" alt=""></span>
                        Book now
                    </a>
                </li>
                <li class="greenbg">
                    <a href="location-map.html">
                        <span><img src="ui/media/dist/icons/location-map.png" height="26" width="32" alt=""></span>
                        Location map
                    </a>
                </li>
                <li class="purplebg">
                    <a href="flights.html">
                        <span><img src="ui/media/dist/icons/flights.png" height="14" width="38" alt=""></span>
                        Flights
                    </a>
                </li>
                <li class="orangebg">
                    <a href="history.html">
                        <span><img src="ui/media/dist/icons/history.png" height="20" width="27" alt=""></span>
                        History
                    </a>
                </li>
                <li class="greenbg">
                    <a href="whats-new.html">
                        <span><img src="ui/media/dist/icons/whats-new.png" height="17" width="18" alt=""></span>
                        What's New
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
</asp:Content>

