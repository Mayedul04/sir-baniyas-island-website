﻿Imports System.Data.SqlClient

Partial Class PhotoPop
    Inherits System.Web.UI.Page
    Public domainName As String
    Public PageList As String
    Public section As String
    Public shareurl As String = ""
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            section = Page.RouteData.Values("section")
            LoadContent(Page.RouteData.Values("id"))

        End If
    End Sub
    Public Sub LoadContent(ByVal id As Integer)
        Dim sConn As String
        Dim selectString1 As String = ""
        If section = "Gallery" Then
            selectString1 = "Select Title, BigImage, GalleryID from GalleryItem where   GalleryItemID=" & id
        Else
            selectString1 = "Select Title, BigImage from CommonGallery where  CommonGalleryID=" & id
        End If
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                Title = reader("Title").ToString()
                ImgBig.ImageUrl = domainName & "Admin/" & reader("BigImage").ToString()
                shareurl = domainName & "Photos/Gallery/" & reader("GalleryID") & "/" & Title
                'lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, domainName & "Admin/A-Event/EventsEdit.aspx?EventID=" & reader("EventID") & "&Small=0&Big=1&Footer=0")

            End While
        End If
        cn.Close()
    End Sub
End Class
