﻿Imports System.Data.SqlClient
Partial Class philosophy
    Inherits System.Web.UI.Page
    Public domainName As String

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub


    Public Function HtmlContentMap(ByVal htmlid As Integer) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select * from HTML where  Lang=@Lang and HtmlID=@HtmlID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        cmd.Parameters.Add("HtmlID", Data.SqlDbType.Int, 32).Value = htmlid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<div class=""innerMapImgHold""><img src=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ alt="""">"
                retstr += "<div class=""textContent""><h2><a href=""" & domainName & "Map-Pop" & """ class=""mapFancyIframe"">" & reader("Title").ToString() & "</a></h2>"
                'retstr += "<h5 class=""subtitle"">" & reader("SmallDetails").ToString() & "</h5>"
                retstr += "<div class=""content""><p>" & reader("SmallDetails").ToString() & "</p><br><a class=""readmorebtn mapFancyIframe"" href=""" & domainName & "Map-Pop" & """>Click here</a><span class=""arrow""></span></div>"
                '& Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0&SmallDetails=0") & "</div>"
                retstr += "</div></div>"
            End While
        End If
        cn.Close()
        Return retstr
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ltLocationMap.Text = HtmlContentMap(75)
    End Sub
End Class
