﻿Imports System.Data.SqlClient

Partial Class Photos
    Inherits System.Web.UI.Page
    Public domainName, title, parent As String
    Public PageList As String
    Public section As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Function GetParentGalleryNav() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT Title  FROM   Gallery Where GalleryID=@GalleryID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = hdnParentID.Value

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            retstr = "<li><a href=""" & domainName & "InnerPhotoGallery/" & hdnParentID.Value & "/" & reader("Title").ToString() & """>" & reader("Title").ToString() & "</a></li>"

        End If
        conn.Close()
        Return retstr
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If IsDipestGallery() = False Then
                Response.Redirect(domainName & "InnerPhotoGallery/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title"))
            End If
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT Title, ParentGalleryID  FROM   Gallery Where GalleryID=@GalleryID"
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("id")

            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()
                title = reader("Title").ToString()
                hdnParentID.Value = reader("ParentGalleryID").ToString()
            End If
            conn.Close()
            If hdnParentID.Value <> 0 Then
                parent = GetParentGalleryNav()
            Else
                parent = ""
            End If
            section = Page.RouteData.Values("section")
            DynamicSEO.PageID = Page.RouteData.Values("id")
        End If
    End Sub
    Private Function getPageNumber() As Integer
        Dim totalRows As Integer = 0
        Dim selectString1 As String = ""
        Dim sConn As String
        If section = "Gallery" Then
            selectString1 = "Select COUNT(0) from GalleryItem where  Status=1 and GalleryID=" & Page.RouteData.Values("id")
        Else
            selectString1 = "Select COUNT(0) from CommonGallery where  Status=1 and  TableName='" & section & "' and TableMasterID=" & Page.RouteData.Values("id")
        End If
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)


        totalRows = Math.Ceiling((cmd.ExecuteScalar / 8))
        cn.Close()
        Return totalRows
    End Function
    Public Function IsDipestGallery() As Boolean
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT GalleryID  FROM   Gallery Where ParentGalleryID=@PGalleryID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("PGalleryID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("id")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            conn.Close()
            Return False
        Else
            conn.Close()
            Return True
        End If
    End Function
    Public Function Photos() As String
        PageList = ""
        Dim currentPage As Integer = 0
        Dim pageNumber As Integer = 0
        Dim searchCriteria As String = ""
        If Page.RouteData.Values("page") Is Nothing Then
            currentPage = 1
        Else
            currentPage = Page.RouteData.Values("page")
        End If

        searchCriteria = domainName & "Photos/" & section & "/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title") & "/"
        pageNumber = getPageNumber()


        If pageNumber > 1 Then
            PageList = GetPager(currentPage, pageNumber, 5, searchCriteria)
        End If
        Dim NW As String = "<ul class=""list-unstyled row"">"
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString As String
        If section = "Gallery" Then
            selectString = "DECLARE @PageNum AS INT; DECLARE @PageSize AS INT; SET @PageNum =" & currentPage & "; SET @PageSize = 8; Select * from ( select  ROW_NUMBER()  over(ORDER BY GalleryItem.GalleryItemID DESC) AS RowNum  , GalleryItemID as ID, Title, SmallImage from GalleryItem where  Status=1 and GalleryID=" & Page.RouteData.Values("id")

        Else
            selectString = "DECLARE @PageNum AS INT; DECLARE @PageSize AS INT; SET @PageNum =" & currentPage & "; SET @PageSize = 8; Select * from ( select  ROW_NUMBER()  over(ORDER BY CommonGallery.CommonGalleryID DESC) AS RowNum  , CommonGalleryID as ID, Title, SmallImage from CommonGallery where  Status=1 and  TableName='" & section & "' and TableMasterID=" & Page.RouteData.Values("id")

        End If
        selectString = selectString + ") as MyTable WHERE RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 AND @PageNum * @PageSize"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        'cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows = True Then
        
        While reader.Read
            NW += "<li class=""col-sm-3 col-xs-6""><a href=""" & domainName & "PhotoPop/" & section & "/" & reader("ID") & """ rel=""album"" class=""photopopup""><div class=""img-container"">"
            If IsDBNull(reader("SmallImage")) = False Then
                NW += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """  height=""190"" alt="""">"
            Else
                NW += "<h4>No Photo(s)</h4>"
            End If

                NW += "</div>"
                ' & reader("Title").ToString() & 
                NW += "<h5 class=""title""></h5>"
            NW += "<div class=""hoverbox""><span class=""plus""></span></div></a>"
            If section <> "Gallery" Then
                NW += Utility.showEditButton(Request, domainName & "Admin/A-CommonGallery/CommonGalleryEdit.aspx?cgid=" & reader("ID").ToString() & "&smallImageWidth=269&smallImageHeight=190")
            Else
                NW += Utility.showEditButton(Request, domainName & "Admin/A-Gallery/GalleryItemEdit.aspx?galleryItemId=" & reader("ID").ToString())
            End If
            NW += "</li>"


            End While
        Else
            NW = "Sorry there is no photo(s) to show."
            If section <> "Gallery" Then
                NW += Utility.showAddButton(Request, domainName & "Admin/A-CommonGallery/AllCommonGalleryEdit.aspx?TableName=" & section & "&TableMasterID=" & Page.RouteData.Values("ID") & "&BigImageWidth=360&BigImageHeight=268&SmallImageWidth=263&SmallImageHeight=215")
            Else
                NW += Utility.showAddButton(Request, domainName & "Admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & Page.RouteData.Values("id"))
            End If

        End If
        cn.Close()
        If PageList = "" Then
            pnlPageination.Visible = False
        End If
        NW = NW + "</ul>"
        Return NW
    End Function
    Public Shared Function GetPager(ByVal presentPageNum As Integer, ByVal totalNumOfPage As Integer, ByVal totalPageNumToShow As Integer, ByVal urlToNavigateWithQStr As String) As String
        Dim i As Integer
        Dim loopStartNum, loopEndNum, presentNum, maxShownNum As Integer
        Dim pagerString As String = ""
        presentNum = presentPageNum
        maxShownNum = totalPageNumToShow
        Dim middleFactor As Integer = maxShownNum / 2
        pagerString = "<ul class=""pagination pagination-lg list-unstyled"">"
        If totalNumOfPage <= totalPageNumToShow Then
            loopStartNum = 1
            loopEndNum = totalNumOfPage
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum <= 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li>"
            ' pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        Else
            loopStartNum = If(presentNum <= (middleFactor + 1), 1, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage - (maxShownNum - 1), presentNum - middleFactor))
            loopEndNum = If(presentNum <= (middleFactor + 1), maxShownNum, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage, presentNum + middleFactor))
            loopEndNum = If(loopEndNum > totalNumOfPage, totalNumOfPage, loopEndNum)
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li> "
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        End If

        pagerString = pagerString & "</ul>"
        Return pagerString
    End Function

    
End Class
