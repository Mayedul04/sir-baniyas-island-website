﻿Imports System.Data.SqlClient
Partial Class _Default
    Inherits System.Web.UI.Page
    Public domainName, brochure As String

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub


    Public Function getFeaturedNews() As String
        ltIslandNews.Text = String.Empty
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString As String = "Select top 1.*  from List_News where Status=1 and Lang=@Lang and Featured=1 and Category='Island' and AtHome=1 order by SortIndex"
        Dim selectString1 As String = "Select top 1.*  from List_New where Status=1 and Lang=@Lang and AtHome=1 order by SortIndex"
        Dim selectString2 As String = "Select top 1.*  from HTML where  Lang=@Lang and HTMLID=313 "
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString2, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()

            retstr += "<div class=""thumbnail-round"">"
            retstr += "<img src=""ui/media/dist/round/lightbrown.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
            retstr += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ class=""normal-thumb"" height=""112"" width=""122"" alt="""">"
            retstr += "</img>"
            retstr += "</div>"

            retstr += "<div class=""box-content"">"
            retstr += "<h5 class=""title-box""><a href=""" & reader("Link").ToString() & """ class=""greenfont"">" & reader("Title").ToString() & "</a></h5>"

            'retstr += "<h6 class=""sub-title"">"
            'retstr += reader("Title").ToString()
            'retstr += "</h6>"

            retstr += "<p>" & reader("SmallDetails").ToString() & "</p>" & Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=313&BigImage=0&BigDetails=0&Link=1&File=0&SecondImage=0&SmallImageWidth=122&SmallImageHeight=122")
            'retstr += Utility.showEditButton(Request, domainName & "Admin/A-News/NewsEdit.aspx?nid=" & reader("NewsID") & "&Small=1&Big=0&Footer=1&smallImageWidth=132&smallImageHeight=129")
            retstr += "<a href=""" & reader("Link").ToString() & """ class=""mainarrow""></a>"
            retstr += "</div>"

        Else
            reader.Close()
            Dim cmd1 As SqlCommand = New SqlCommand(selectString1, cn)
            cmd1.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
            Dim reader1 As SqlDataReader = cmd1.ExecuteReader()
            If reader1.HasRows Then
                reader1.Read()
                retstr += "<div class=""thumbnail-round"">"
                retstr += "<img src=""ui/media/dist/round/lightbrown.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                retstr += "<img src=""" & domainName & "Admin/" & reader1("SmallImage") & """ class=""normal-thumb"" height=""112"" width=""122"" alt="""">"
                retstr += "</img>"
                retstr += "</div>"

                retstr += "<div class=""box-content"">"
                retstr += "<h5 class=""title-box""><a href=""" & domainName & "Island-News" & """ class=""greenfont"">Island News</a></h5>"

                'retstr += "<h6 class=""sub-title"">"
                'retstr += reader("Title").ToString()
                'retstr += "</h6>"

                retstr += "<p>" & reader1("SmallDetails").ToString() & "</p>" & Utility.showEditButton(Request, domainName & "Admin/A-What-New/WhatNewEdit.aspx?nid=" & reader1("ListID") & "&Small=1&Big=0&Footer=1&smallImageWidth=132&smallImageHeight=129")
                retstr += "<a href=""" & domainName & "Whats-New-Details" & "/" & reader1("ListID") & "/" & Utility.EncodeTitle(reader1("Title").ToString(), "-") & """ class=""mainarrow""></a>"
                retstr += "</div>"
            End If
        End If
        cn.Close()
        Return retstr
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        ltIslandNews.Text = getFeaturedNews()
        ltrFile.Text = HTMLFile()
        ltphoto.Text = HTMLText11()
    End Sub
    Function HTMLFile() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 321)

        Dim reader = cmd.ExecuteReader()
        If reader.HasRows = True Then
            reader.Read()
            brochure = domainName & "Admin/" & reader("FileUploaded").ToString()
            M += "<a href=""" & domainName & "Admin/" & reader("FileUploaded").ToString() & """ target=""_blank""><img src=""" & domainName & "Admin/" & reader("SmallImage") & """ height=""284"" width=""402"" alt=""brochure""/></a>"

            M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SecondImage=0&SmallImage=1&SmallDetails=0&Details=0&File=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0&SmallImageWidth=402&SmallImageHeight=284&BigImageWidth=&BigImageHeight=")

        End If
        reader.Close()
        con.Close()
        Return M


    End Function



         

    Function HTMLText11() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 209)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """>"

                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SecondImage=0&File=0&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0&SmallImageWidth=308&SmallImageHeight=288&BigImageWidth=&BigImageHeight=")

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


End Class
