﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Sir-Bani-Yas-History.aspx.vb" Inherits="Sir_Bani_Yas_History" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%=domainName%>'>Home</a></li>
            <li><a href='<%=domainName & "Sir-Bani-Yas-Island"%>'>Sir Bani Yas Island</a></li>
            <li class="active"><a href="#">History</a></li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->


    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">

        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                    <asp:Literal ID="ltTitle" runat="server"></asp:Literal>

                </h1>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Literal ID="ltHtm1" runat="server"></asp:Literal>

                        <asp:Literal ID="ltHtml2" Visible="false"  runat="server"></asp:Literal>

                    </div>
                </div>
            </section>
            <!-- -- welcome-text ends here -- -->
        </div>


    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->




    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">
        <div class="container">

            <div class="footer-nav">
                <div class="row">

                    <div class="col-sm-4 col-sm-offset-2">
                        <a href='<%=domainName & "Arabian-Wildlife"%>' class="greenbg">Arabian Wildlife Park</a>
                    </div>

                    <div class="col-sm-4">
                        <a href='<%=domainName & "PhotoGallery"%>'  class="purplebg">Photo Gallery</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->
</asp:Content>

