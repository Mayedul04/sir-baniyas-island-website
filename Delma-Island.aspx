﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Delma-Island.aspx.vb" Inherits="Delma_Island" %>

<%@ Register Src="~/CustomControl/UCHTMLTitle.ascx" TagPrefix="uc1" TagName="UCHTMLTitle" %>
<%@ Register Src="~/CustomControl/UCHTMLSmallImage.ascx" TagPrefix="uc1" TagName="UCHTMLSmallImage" %>
<%@ Register Src="~/CustomControl/UCHTMLDetails.ascx" TagPrefix="uc1" TagName="UCHTMLDetails" %>
<%@ Register Src="~/CustomControl/UCInterestingFactList.ascx" TagPrefix="uc1" TagName="UCInterestingFactList" %>
<%@ Register Src="~/CustomControl/UCPhotoAlbum.ascx" TagPrefix="uc1" TagName="UCPhotoAlbum" %>
<%@ Register Src="~/CustomControl/UCVideoAlbum.ascx" TagPrefix="uc1" TagName="UCVideoAlbum" %>
<%@ Register Src="~/CustomControl/UCTestimonial.ascx" TagPrefix="uc1" TagName="UCTestimonial" %>
<%@ Register Src="~/CustomControl/UCHTMLImage.ascx" TagPrefix="uc1" TagName="UCHTMLImage" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLBigText.ascx" TagPrefix="uc1" TagName="UserHTMLBigText" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserStaticPhotoAlbum.ascx" TagPrefix="uc1" TagName="UserStaticPhotoAlbum" %>


<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%=domainName%>'>Home</a></li>
            <li><a href='<%=domainName & "Desert-Island"%>'>About Desert Island</a></li>
            <li class="active">Dalma Island</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">Dalma Island</h1>

                <div class="row">

                    <!-- Content Section -->
                    <div class="col-md-8 col-sm-6">

                        <uc1:UserHTMLBigText runat="server" ID="UserHTMLBigText" HTMLID="117" TitleEdit="true" IMGBigHeight="262" IMGBigWidth="370" IMGSmallHeight="262" IMGSmallWidth="370" />

                    </div>
                    <!-- Content Section -->

                    <!-- Right Panel -->
                    <div class="col-md-4 col-sm-6">

                        <div class="imgHold sideImg">

                            <uc1:UserHTMLImage runat="server" HTMLID="117" ID="UserHTMLImage" ShowEdit="false" />
                        </div>
                        <%--
                        <div class="purplebg border-box bigheight">
                         <uc1:UCPhotoAlbum runat="server" ID="UCPhotoAlbum" idfield_name="6" />
                        </div>--%>

                        <div class="purplebg border-box bigheight">
                            <asp:Literal ID="lblPhotoGallery" runat="server"></asp:Literal>

                            <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="226" ImageType="Small" ImageClass="boxthumbnail" ImageHeight="292" ImageWidth="412" ShowEdit="false" />
                            <a href='<%= galurl %>' class="linkbtn withthumbnail" rel="Dalma">Photo Gallery<span class="arrow"></span></a>
                            <uc1:UserControlEditButton runat="server" ID="editPhotoGallery" HTMLID="226" PGalleryEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="292" ImageWidth="412" />
                            <% If pgalid <> 0 Then%>
                            <%= Utility.showAddButton(Request, domainName & "Admin/A-Gallery/AllGalleryItemEdit.aspx?galleryId=" & pgalid & "&Lang=en")%>
                            <% End If%>
                        </div>
                    </div>
                    <!-- Right Panel -->

                </div>

            </section>
            <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="195" />
            <!-- -- welcome-text ends here -- -->
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->



    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-6">
                    <!-- Intresting Facts Slider -->


                    <uc1:UCInterestingFactList runat="server" ID="UCInterestingFactList" />



                    <!-- Intresting Facts Slider -->
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="innerMapImgHold">
                        <asp:Literal ID="ltLocationMap" runat="server"></asp:Literal>
                    </div>
                </div>

            </div>

        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->


    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns" class="nomargintop">
        <div class="container">
            <ul class="list-unstyled">
                <li class="purplebg">
                    <a href='<%= domainName & "PhotoGallery"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/photo-gallery.png"%>' height="25" width="29" alt=""></span>
                        Photo gallery
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "VideoGallery"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/video-gallery.png"%>' height="20" width="25" alt=""></span>
                        Video gallery
                    </a>
                </li>
                <li class="bluebg">
                    <a class="bookNowIframe" href='<%= domainName & "Booknow-Pop"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/book-now.png"%>' height="27" width="27" alt=""></span>
                        Book now
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "Map-Pop"%>' class="mapFancyIframe">
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/location-map.png"%>' height="26" width="32" alt=""></span>
                        Location map
                    </a>
                </li>
                <li class="purplebg">
                    <a href='<%= domainName & "Travel-Air"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/flights.png"%>' height="14" width="38" alt=""></span>
                        Flights
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "Sir-Bani-Yas-History"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/history.png"%>' height="20" width="27" alt=""></span>
                        History
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "WhatsNew"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/whats-new.png"%>' height="17" width="18" alt=""></span>
                        What's New
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->

</asp:Content>

