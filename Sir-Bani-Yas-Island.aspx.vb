﻿Imports System.Data.SqlClient
Partial Class Sir_Bani_Yas_Island
    Inherits System.Web.UI.Page

    Public domainName, galurl As String
    Public pgalid, vgalid As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString


    End Sub
    Public Function getGalleryID(ByVal htmlid As Integer, fieldname As String) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select " & fieldname & " from HTML where  HTMLID=@HTMLID and Lang='en'"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("HTMLID", Data.SqlDbType.NVarChar, 50).Value = htmlid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If IsDBNull(reader(fieldname)) = False Then
                    retstr = reader(fieldname).ToString()
                End If



            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Function HistoryText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 123)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()



                M += "<div class=""dbluebg border-box bigheight"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"

                M += "<a href=""" & domainName & "Sir-Bani-Yas-History"" class=""linkbtn"">" & reader("Title") & "<span class=""arrow""></span></a>"
                M += "</img>"
                'M += "<p>" & reader("SmallDetails") & "</p>"

                M += "<a href=""" & domainName & "Sir-Bani-Yas-History"" class=""readmoreBttn blue"">Read more</a>"""
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")
            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Function ArabianWildlifeHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 3)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()



                M += "<div class=""greenbg border-box bigheight"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"

                M += "<a href=""" & domainName & "Arabian-Wildlife"" class=""linkbtn"">" & reader("Title") & "<span class=""arrow""></span></a>"
                M += "</img>"
                'M += "<p>" & reader("SmallDetails") & "</p>"

                M += "<a href=""" & domainName & "Arabian-Wildlife"" class=""readmoreBttn green"">Read more</a>"""
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")
            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


    Function PhotoHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 126)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()



                M += "<div class=""purplebg border-box bigheight"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"

                M += "<a href=""" & domainName & "Photos/Gallery/8/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""linkbtn"">" & reader("Title") & "<span class=""arrow""></span></a>"
                M += "</img>"
                M += "<p>" & reader("SmallDetails") & "</p>"

                M += "<a href=""" & domainName & "Photos/Gallery/8/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""readmoreBttn purple"">Read more</a>"""
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")
                M += "</div>"
            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function



    Function SaltDomeHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 128)

        Try
            Dim reader = cmd.ExecuteReader()

            If reader.HasRows = True Then

                reader.Read()



                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""transparent"">"
                M += "<h3 class=""title"">" & reader("Title") & "</h3>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=478&SmallImageHeight=233&BigImageWidth=733&BigImageHeight=458")

                M += "<p>" & reader("SmallDetails") & "</p>"

                M += "<a href=""" & domainName & "Salt-Domes"" class=""readmorebtn"">Read more</a>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Function ConservationHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 130)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()



                M += "<img src=""" & domainName & "Admin/" & reader("BigImage") & """ alt=""" & reader("ImageAltText") & """ width=""440px"" class=""transparent"">"
                M += "<h3 class=""title"">" & reader("Title") & "</h3>"
                M += "<p>" & reader("SmallDetails") & "</p>"

                M += "<a href=""" & domainName & "Conservation"" class=""readmorebtn"">Read more</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=1&BigDetails=0&Link=0&SmallImageWidth=478&SmallImageHeight=233&BigImageWidth=733&BigImageHeight=458")

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


    Function ActivityHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 77)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += "<div class=""thumbnail-round"">"
                M += "<img src=""" & domainName & "ui/media/dist/round/blue.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += " <img src=""" & domainName & "Admin/" & reader("BigImage") & """ height=""137"" width=""132"" class=""normal-thumb"" alt=""" & reader("ImageAltText") & """>"
                M += "</img>"
                M += "</div>"
                M += "<div class=""box-content"">"
                M += "<h5 class=""title-box""><a href=""" & domainName & "Stay"" class=""bluefont"">" & reader("Title") & "</a></h5>"
                M += reader("BigDetails")
                M += "<a href=""" & domainName & "Stay"" class=""readmorebtn"">View More</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&BigDetails=1&Link=0&SmallImageWidth=478&SmallImageHeight=233&BigImageWidth=139&BigImageHeight=144")

                M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

  
    Function HistoricalTimelineHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 79)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += "<div class=""thumbnail-round"">"
                M += "<img src=""" & domainName & "ui/media/dist/round/orange.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += " <img src=""" & domainName & "Admin/" & reader("BigImage") & """ height=""137"" width=""132"" class=""normal-thumb"" alt=""" & reader("ImageAltText") & """>"
                M += "</img>"
                M += "</div>"
                M += "<div class=""box-content"">"
                M += "<h5 class=""title-box""><a href=""" & domainName & "Historical-Timeline"" class="""">" & reader("Title") & "</a></h5>"
                M += reader("BigDetails")
                M += "<a href=""" & domainName & "Historical-Timeline"" class=""readmorebtn"">View More</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&BigDetails=1&Link=0&SmallImageWidth=478&SmallImageHeight=233&BigImageWidth=139&BigImageHeight=120")

                M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Public Function IslandMap() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where HTMLID=108"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read
                Dim pdfurl As String = domainName & "Admin/" & reader("FileUploaded").ToString()
                M += "<div class=""thumbnail-round"">"
                M += " <img src=""" & domainName & "ui/media/dist/round/blue.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""normal-thumb"" alt="""">"
                M += "</img>"
                M += "</div>"

                M += "<div class=""box-content"">"
                M += " <h5 class=""title-box""><a href=""" & pdfurl & """ class="" bluefont"" target=""_blank"">Island Map</a></h5>"
                'M += " <h6 class=""sub-title"">" & reader("Title") & "</h6>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a href=""" & pdfurl & """ class=""readmorebtn"" target=""_blank"">View Map</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=151&SmallImageHeight=129&BigImageWidth=733&BigImageHeight=458")

                M += "</div>"



            End While

        End If
        conn.Close()
        Return M
    End Function

    Function FloraHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 132)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += "<div class=""thumbnail-round"">"
                M += "<img src=""" & domainName & "ui/media/dist/round/green.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += " <img src=""" & domainName & "Admin/" & reader("SmallImage") & """ height=""137"" width=""132"" class=""normal-thumb"" alt=""" & reader("ImageAltText") & """>"
                M += "</img>"
                M += "</div>"
                M += "<div class=""box-content"">"
                M += "<h5 class=""title-box""><a href=""" & domainName & "Flora"" class="""">" & reader("Title") & "</a></h5>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a href=""" & domainName & "Flora"" class=""readmorebtn"">View More</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=1&Link=0&SmallImageWidth=139&SmallImageHeight=117&BigImageWidth=733&BigImageHeight=458")

                M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


    Public Function Archological() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where HTMLID=143" ''Dim selectString = "SELECT * FROM List_Restaurant where Status='1' and Category='Restaurant' and RestaurantID <> 0 order by Sortindex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.AddWithValue("TableName", TableName)
        'cmd.Parameters.AddWithValue("TableMasterID", TableMasterID)

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then



            While reader.Read



                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=1&Link=0&SmallImageWidth=478&SmallImageHeight=233&BigImageWidth=733&BigImageHeight=458")

                M += "<div class=""double-box greenbg"">"

                M += "<div class=""twopanel"">"
                M += "<h3 class=""title""><a href=""" & domainName & "Archaelogical-Details"">" & reader("Title") & "</a></h3>"

                M += reader("BigDetails")
               
                M += "<span class=""arrow""></span><br/>"

                M += "<a href=""" & domainName & "Archaelogical-Details"" class=""readmorebtn"">Read more</a>"
                M += "</div>"

                M += "<div class=""twopanel imageholder"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """>"
                M += " </img>"
                M += "</div>"

                M += "<h5 class=""subtitle""><a href=""" & domainName & "Archaelogical-Details"" >More</a></h5>"

                M += "</div>"


            End While


            reader.Read()

        End If
        conn.Close()
        Return M
    End Function
    Public Sub LoadGallery(ByVal galid As Integer)
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If count = 1 Then
                    galurl = domainName & "Admin/" & reader("BigImage").ToString()
                Else
                    lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Flora"">Photo Gallery<span class=""arrow""></span></a>"
                End If
                count += 1
            End While
        End If
        cn.Close()
    End Sub


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ltHistory.Text = HistoryText()
        ltArabianWildlife.Text = ArabianWildlifeHTMLText()
        'ltPhoto.Text = PhotoHTMLText()
        ltSaltDome.Text = SaltDomeHTMLText()
        ltConservation.Text = ConservationHTMLText()
        ltActivity.Text = ActivityHTMLText()
        ltHistoricalTimeline.Text = HistoricalTimelineHTMLText()
        ltIslanMap.Text = IslandMap()
        ltFlora.Text = FloraHTMLText()
        ltArchological.Text = Archological()
        pgalid = getGalleryID(233, "GalleryID")
        If pgalid <> 0 Then
            LoadGallery(pgalid)
        End If
    End Sub
End Class
