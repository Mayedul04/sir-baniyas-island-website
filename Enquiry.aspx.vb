﻿
Partial Class Enquiry
    Inherits System.Web.UI.Page
    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Protected Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        If MyCaptcha.IsValid Then
            hdnDate.Value = Date.Now()
            Dim emailtemplate As String = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
            emailtemplate += "<html xmlns=""http://www.w3.org/1999/xhtml"">"
            emailtemplate += "<head><meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />"
            emailtemplate += "<title>Sir Bani Yas</title></head>"
            emailtemplate += "<body style=""margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; font-size: 12px; background-color: #f1f1f1;"">"
            emailtemplate += "<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
            emailtemplate += "<tr><td style=""background-color: #f1f1f1;"">"
            emailtemplate += "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
            emailtemplate += "<tr><td>&nbsp;</td></tr>"
            emailtemplate += "<tr><td><a href=""http://www.sirbaniyasisland.com/"" target=""_blank""><img src=""" & domainName & "images/banner-img.jpg"" width=""800"" /></a>"
            emailtemplate += "</td></tr><tr><td style=""background-color: #fff;"">"
            emailtemplate += "<table width=""712"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
            emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
            emailtemplate += "<tr><td><h1 style=""color: #f58023; text-align: center; text-transform: uppercase; font-weight: normal"">Thank you for SignUp for Media Library</h1>"
            emailtemplate += "<h2 style=""color: #333; text-align: center;"">One Email will be sent to you with Password shortly</h2></td>"
            emailtemplate += "</tr><tr><td></td></tr>"
            emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
            emailtemplate += "<tr><td style=""height: 52px; text-align: center;""><a href=""mailto:info@tdic.ae"" style=""color: #f58023"">info@tdic.ae</a> or call us"
            emailtemplate += "800-TDIC (8342)<td></tr></table>"
            emailtemplate += "</td></tr><tr><td>&nbsp;</td></tr></table></td></tr><tr>"
            emailtemplate += "<td style="""">&nbsp;</td></tr></table></body></html>"
            
            Dim msg As String = "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
            msg += "<tr><td style=""border:1px solid #ccc;""><table width=""740"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
            msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">First Name</td>"
            msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtName.Text & "</td></tr>"
            msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Email Address</td>"
            msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtEmail.Text & "</td></tr>"
            msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Mobile</td>"
            msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtMobile.Text & "</td></tr>"
            msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Message</td>"
            msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtmsg.Text & "</td></tr>"
            msg += "</table></td></tr></table>"
            If sdsEnquiry.Insert > 0 Then
                Utility.SendMail(txtName.Text, txtEmail.Text, "anantaraspa.dirs@anantara.com", "", "jatin@digitalnexa.com", Page.RouteData.Values("section") & " Enquiry", msg)
                Utility.SendMail(txtName.Text, "noreply@sirbaniyasisland.com", txtEmail.Text, "", "jatin@digitalnexa.com", Page.RouteData.Values("section") & " Enquiry", emailtemplate)
                Response.Redirect(domainName & "Thanks/" & Page.RouteData.Values("section"))
            End If
        End If
    End Sub
End Class
