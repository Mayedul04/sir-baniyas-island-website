﻿Imports System.Data.SqlClient

Partial Class Dine
    Inherits System.Web.UI.Page
    Public domainName, title As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Function ResNameList() As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select ListID ,Title from List_Dine where Status=1 and Lang=@Lang"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<li><a data-scroll href=""#res" & reader("ListID") & """>" & reader("Title").ToString() & "</a></li>"
            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function RestaurantList() As String
        Dim retstr As String = ""
        Dim counter As Integer = 0
        Dim sConn As String
        Dim selectString1 As String = "Select * from List_Dine where Status=1 and Lang=@Lang"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                counter += 1
                If IsDBNull(reader("BG")) Then
                    retstr += "<li class=""bluebg"" id=""res" & reader("ListID").ToString() & """>"
                Else
                    retstr += "<li class=""" & reader("BG").ToString() & """ id=""res" & reader("ListID").ToString() & """>"
                End If
                retstr += "<div class=""image-container""><img src=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ width=""1600"" alt=""""></div>"
                If (counter Mod 2 = 0) Then
                    retstr += "<div class=""container""><div class=""row""><div class=""col-sm-8 col-sm-offset-8 col-md-6 col-md-offset-6""><div class=""white-wrapper"">"
                Else
                    retstr += "<div class=""container""><div class=""row""><div class=""col-sm-8 col-md-6""><div class=""white-wrapper"">"
                End If

                retstr += "<h3 class=""h2 title"">" & reader("Title").ToString() & " </h3>"
                retstr += "<p>" & reader("SmallDetails").ToString() & "</p>"
                If IsDBNull(reader("Link")) = False Then
                    retstr += "<a href=""" & reader("Link").ToString() & """ target=""_blank"" class=""readmore-btn"">View more</a>"
                End If
                retstr += Utility.showEditButton(Request, domainName & "Admin/A-Dine/ListEdit.aspx?lid=" & reader("ListID").ToString())
                retstr += "</div></div></div></div></li>"
            End While
        End If
        cn.Close()
        Return retstr
    End Function
End Class
