﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="InterestingFacts.aspx.vb" Inherits="InterestingFacts" %>


<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>
<%@ Register Src="~/CustomControl/UCStaySirBaniYas.ascx" TagPrefix="uc1" TagName="UCStaySirBaniYas" %>





<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName  %>'>Home</a></li>

            <li class="active">Interesting Facts</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                     <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="211" ShowEdit="false" />
                </h1>
                <p>
                     <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="211" />
                         <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="461" />
               
                </p>
            </section>
            <!-- -- welcome-text ends here -- -->

            <%= FactList() %>
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->
    <!-- page-shifter starts here
   ---------------------------------------------- -->
     <asp:Panel ID="pnlPageination" runat="server">
    <nav class="pagi">
        <div class="container">
            <div class="page-shifter">
               <%= PageList %>
            </div>
        </div>
    </nav>
         </asp:Panel>
    <!-- page-shifter ends here
   ---------------------------------------------- -->


    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">

        <div class="container">

            <div class="row">

                <div class="col-sm-6">
                    <!-- -- double-box starts here -- -->
                   <div class="double-box greenbg">
                        <!-- -- twopanel starts here -- -->
                        <div class="twopanel">
                            <h3 class="title">
                                 Our Philosphy
                            </h3>
                            
                            <p>
                                <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText" HTMLID="121"   />
                            </p>
                            <span class="arrow"></span>
                            <br/>
                            <a href='<%= domainName & "Philosophy"%>' class="readmorebtn">Read more</a>
                        </div>
                          
                        <!-- -- twopanel ends here -- -->
                        
                        <!-- -- twopanel imageholder starts here -- -->
                        <div class="twopanel imageholder">
                             <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="121"  ImageType="Small" ImageWidth="307" />
                           
                        </div>
                        <!-- -- twopanel imageholder ends here -- -->
                    </div>
                    <uc1:UserControlEditButton runat="server" ID="UserControlEditButton" HTMLID="121" TextEdit="true" TextType="Small" ImageEdit="true" ImageType="Small" ImageHeight="233" ImageWidth="478" />
                    <!-- -- double-box ends here -- -->
                </div>
                <div class="col-sm-6">
                   <%= HtmlContentMap(75)%>
                    <uc1:UserControlEditButton runat="server" ID="EditMap" HTMLID="75" TitleEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="233" ImageWidth="555" TextEdit="true"  TextType="Big"/>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                   
                    <div class="roundedbox blue inner">
                         <uc1:UCStaySirBaniYas runat="server" ID="UCStaySirBaniYas"  />
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox orange inner">
                        <div class="thumbnail-round">
                            <img src='<%= domainName & "ui/media/dist/round/orange.png" %>' height="113" width="122" alt="" class="roundedcontainer">
                            <uc1:UserHTMLImage runat="server" ID="UserHTMLImage3" ImageType="Small" HTMLID="79" ImageWidth="139" ImageHeight="139" ImageClass="normal-thumb" />
                            
                        </div>
                        <div class="box-content">
                            <h5 class="title-box"><a href='<%= domainName & "historical-timeline"  %>' class="">Historical Timeline</a></h5>
                            
                            <p>
                                  <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText3" HTMLID="79" ShowEdit="false"/>
                            </p>
                            <a href='<%= domainName & "historical-timeline"  %>' class="readmorebtn">View More</a>
                        </div>
                        <uc1:UserControlEditButton runat="server" ID="EditTimelineThumb" HTMLID="79" TitleEdit="true" TextEdit="true" TextType="Small" ImageEdit="true" ImageType="Small" ImageHeight="139" ImageWidth="139" />
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
            </div>


        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->
</asp:Content>

