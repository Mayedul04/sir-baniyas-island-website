﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Mangrove.aspx.vb" Inherits="Conservation" %>

<%@ Register Src="~/CustomControl/UCHTMLTitle.ascx" TagPrefix="uc1" TagName="UCHTMLTitle" %>
<%@ Register Src="~/CustomControl/UCHTMLSmallImage.ascx" TagPrefix="uc1" TagName="UCHTMLSmallImage" %>
<%@ Register Src="~/CustomControl/UCHTMLDetails.ascx" TagPrefix="uc1" TagName="UCHTMLDetails" %>
<%@ Register Src="~/CustomControl/UCInterestingFactList.ascx" TagPrefix="uc1" TagName="UCInterestingFactList" %>
<%@ Register Src="~/CustomControl/UCPhotoAlbum.ascx" TagPrefix="uc1" TagName="UCPhotoAlbum" %>
<%@ Register Src="~/CustomControl/UCVideoAlbum.ascx" TagPrefix="uc1" TagName="UCVideoAlbum" %>
<%@ Register Src="~/CustomControl/UCTestimonial.ascx" TagPrefix="uc1" TagName="UCTestimonial" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>






<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%=domainName%>'>Home</a></li>
            <li><a href='<%=domainName & "Sir-Bani-Yas-Island"%>'>Sir bani yas Island</a></li>
              <li><a href='<%= domainName & "Conservation"%>'>Conservation</a></li>
             <li class="active">Mangroves</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                   
                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="141" ShowEdit="false" />
                </h1>
                <asp:Literal ID="ltImage" runat="server"></asp:Literal>
              
                <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="141" />

            </section>
            <uc1:StaticSEO runat="server" ID="StaticSEO1" SEOID="224" />
            <!-- -- welcome-text ends here -- -->
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->



    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">

        <div class="container marginTop20">
            <ul class="list-unstyled row">
                <li class="col-sm-6 aboutDesert">
                    <div class="orangebg border-box normal">
                        <asp:Literal ID="ltConservation" runat="server"></asp:Literal>
                    </div>
                </li>
                <li class="col-sm-6 aboutDesert">
                    <div class="purplebg border-box normal">
                        <asp:Literal ID="ltBreeding" runat="server"></asp:Literal>

                    </div>
                </li>
            </ul>
            <div class="row">

                <div class="col-sm-5">
                    <!-- -- double-box starts here -- -->
                    <div class="double-box greenbg">
                        <!-- -- twopanel starts here -- -->
                        <asp:Literal ID="ltArchaeological" runat="server"></asp:Literal>
                        <!-- -- twopanel imageholder ends here -- -->
                    </div>
                    <!-- -- double-box ends here -- -->
                </div>

                <div class="col-sm-3">
                    <div class="purplebg single-box sirbaniyasBox">
                        <asp:Literal ID="ltSaltDomain" runat="server"></asp:Literal> 
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="greenbg single-box sirbaniyasBox">
                        <asp:Literal ID="ltFlora" runat="server"></asp:Literal>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-4">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox blue inner">
                        <asp:Literal ID="ltActivity" runat="server"></asp:Literal>
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-4">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox orange inner">
                        <asp:Literal ID="ltHistoricalTimeLine" runat="server"></asp:Literal>
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-4">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox purple inner">
                        <asp:Literal ID="ltIsland" runat="server"></asp:Literal>
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
            </div>

        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->


    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns" class="nomargintop">
        <div class="container">
               <ul class="list-unstyled">
                <li class="purplebg">
                    <a href='<%= domainName & "PhotoGallery"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/photo-gallery.png"%>' height="25" width="29" alt=""></span>
                        Photo gallery
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "VideoGallery"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/video-gallery.png"%>' height="20" width="25" alt=""></span>
                        Video gallery
                    </a>
                </li>
                <li class="bluebg">
                    <a class="bookNowIframe" href='<%= domainName & "Booknow-Pop"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/book-now.png"%>' height="27" width="27" alt=""></span>
                        Book now
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "Map-Pop"%>' class="mapFancyIframe">
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/location-map.png"%>' height="26" width="32" alt=""></span>
                        Location map
                    </a>
                </li>
                <li class="purplebg">
                    <a href='<%= domainName & "Travel-Air"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/flights.png"%>' height="14" width="38" alt=""></span>
                        Flights
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "Sir-Bani-Yas-History"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/history.png"%>' height="20" width="27" alt=""></span>
                        History
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "WhatsNew"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/whats-new.png"%>' height="17" width="18" alt=""></span>
                        What's New
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="224" />
</asp:Content>

