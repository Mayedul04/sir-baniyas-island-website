﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="News.aspx.vb" Inherits="News" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Media-Centre" %>'>Media Centre</a></li>
            <li class="active">Island News</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    
    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="99" ShowEdit="false" />
                </h1>
                 <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="99" />
                  <uc1:StaticSEO runat="server" ID="StaticSEO"  SEOID="266"/>
            </section>
            <!-- -- welcome-text ends here -- -->
        </div>


        <!-- Land & Water Activites -->
        <div class="activitesContainerSection mediaCenterNews">

            <!-- Bottom FIx -->
            <div class="bottomFix">

                <div class="container">

                    <div class="row">
                    
                        <!-- Land Activities -->
                        <div class="col-md-6 col-sm-6">
                            <div class="landActivitesSection featuredNews">
                                <h2 class="purplebg">
                                    Featured News
                                   <%--<a href='<%= domainName & "Featured-News"%>'> Featured News</a>--%>
                                </h2>

                               <%= getFeaturedNews() %>

                            </div>
                        </div>
                        <!-- Land Activities -->

                        <!-- Water Activities -->
                        <div class="col-md-6 col-sm-6">
                            <div class="waterActivitesSection whatsNewSection">
                                <h2 class="orangebg">
                                    <a href='<%= domainName & "WhatsNew" %>'>Whats New</a>
                                </h2>

                                <!-- whats new tab navigation -->
                                <ul class="whatsNewTabNavigation">
                                    <%= WhatNewImages() %>
                                  
                                </ul>
                                <!-- whats new tab navigation -->

                                <!-- Tab Content Container -->
                                <div class="tabContentContainer">

                                   <%= WhatNewContent() %>

                                    

                                </div>
                                <!-- Tab Content Container -->

                            </div>
                        </div>
                        <!-- Water Activities -->

                    </div>

                </div>

            </div>
            <!-- Bottom FIx -->

        </div>
        <!-- Land & Water Activites -->

        
        
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    

    <section class="container marginTop20">
        <div class="footer-nav">
            <div class="row">
                
               
                
                <div class="col-sm-4 col-sm-offset-2">
                    <a href='<%= domainName & "Press-Release-News" %>' class="bluebg"><img src='<%= domainName & "ui/media/dist/media-center/pressrelese-icon.png"%>' alt=""> Press Release</a>
                </div>

                <div class="col-sm-4">
                    <a href='<%= domainName & "Media-Library-Request" %>' class="purplebg"><img src='<%= domainName & "ui/media/dist/media-center/image-library.png"%>' alt=""> Media Library Request</a>
                </div>

               

            </div>
        </div>
    </section>
</asp:Content>

