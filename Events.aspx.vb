﻿Imports System.Data.SqlClient

Partial Class Events
    Inherits System.Web.UI.Page
    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Function getFeaturedEvent() As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select top 1.*  from List_Event where Status=1 and Lang=@Lang and Featured=1 and Category='Our-Event'"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<div class=""row""><div class=""col-md-6"">"
                retstr += "<p>" & reader("SmallDetails").ToString() & "</p>" & Utility.showEditButton(Request, domainName & "Admin/A-Event/EventsEdit.aspx?EventID=" & reader("EventID") & "&Small=1&Big=0&Footer=0")
                retstr += "</div><div class=""col-md-6""><div class=""imgHold""><img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ width=""263"" alt=""""></div>"
                'If IsDBNull(reader("BigDetails")) = False Then
                retstr += "<a class=""readmoreBttn purple"" href=""" & domainName & "Event/" & reader("EventID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>Read more</a></div></div>"
                '  Else
                '   retstr += "</div></div>"
                ' End If

            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function OtherEvents() As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select top 4. * from List_Event where Lang=@Lang and  Category='Our-Event' and  Featured=0 order by SortIndex"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<li class=""orangebg""><div class=""content"">"
                retstr += "<div class=""imgHold""><img src=""" & domainName & "Admin/" & reader("OtherImage") & """ width=""128"" alt="""">"
                retstr += "<div class=""hoverContent""><div class=""content""><p>" & reader("SmallDetails").ToString() & "</p>"
                retstr += "<a href=""" & domainName & "Event/" & reader("EventID").ToString() & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """ class=""readmoreBttn"">Read more ></a>"
                retstr += "</div></div></div>"
                retstr += "<h2><a href=""" & domainName & "Event/" & reader("EventID").ToString() & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """ class=""ggka orangefont"">" & reader("Title").ToString() & "</a><span></span></h2>" & Utility.showEditButton(Request, domainName & "Admin/A-Event/EventsEdit.aspx?EventID=" & reader("EventID") & "&Small=1&Big=0&Footer=1")
                retstr += "</div></li>"
            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function PlanningEvents() As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select top 4. * from List_Event where Status=1 and Lang=@Lang and Category='Plan-Your-Event' order by SortIndex"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<li class=""col-md-3 col-sm-3"">"
                retstr += "<div class=""imgHold""><img src=""" & domainName & "Admin/" & reader("SmallImage") & """ width=""263"" alt=""""></div>"
                retstr += "<h2 class=""orangefont"">" & reader("Title").ToString() & "</h2><p>" & reader("SmallDetails").ToString() & "</p>"
                retstr += "<a href=""" & domainName & "Events-Summary"" class=""readmoreBttn orange"">Read more</a>"
                retstr += Utility.showEditButton(Request, domainName & "Admin/A-Event/EventsEdit.aspx?EventID=" & reader("EventID") & "&Small=1&Big=0&Footer=1")
                retstr += "</li>"
            End While
        End If
        cn.Close()
        Return retstr
    End Function
End Class
