﻿Imports System.Data.SqlClient

Partial Class Video
    Inherits System.Web.UI.Page
    Public domainName, title, parent As String
    Public PageList As String
    Public section As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Function GetParentGalleryNav() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT Title  FROM   Video Where GalleryID=@GalleryID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = hdnParentID.Value

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            retstr = "<li><a href=""" & domainName & "InnerVideoGallery/" & hdnParentID.Value & "/" & reader("Title").ToString() & """>" & reader("Title").ToString() & "</a></li>"

        End If
        conn.Close()
        Return retstr
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If IsDipestGallery() = False Then
                Response.Redirect(domainName & "InnerVideoGallery/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title"))
            End If
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT Title, ParentGalleryID  FROM   Video Where GalleryID=@GalleryID"
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("id")

            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()
                title = reader("Title").ToString()
                hdnParentID.Value = reader("ParentGalleryID").ToString()
            End If
            conn.Close()
            If hdnParentID.Value <> 0 Then
                parent = GetParentGalleryNav()
            Else
                parent = ""
            End If
            section = Page.RouteData.Values("section")
            DynamicSEO.PageID = Page.RouteData.Values("id")
        End If
    End Sub
    Public Function IsDipestGallery() As Boolean
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT GalleryID  FROM   Video Where ParentGalleryID=@PGalleryID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("PGalleryID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("id")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            conn.Close()
            Return False
        Else
            conn.Close()
            Return True
        End If
    End Function
    
    Private Function getPageNumber() As Integer
        Dim totalRows As Integer = 0
        Dim selectString1 As String = ""
        Dim sConn As String
        If section = "Gallery" Then
            selectString1 = "Select COUNT(0) from VideoItem where  Status=1 and GalleryID=" & Page.RouteData.Values("id")
        Else
            selectString1 = "Select COUNT(0) from CommonVideo where   TableName='" & section & "' and TableMasterID=" & Page.RouteData.Values("id")
        End If


        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)


        totalRows = Math.Ceiling((cmd.ExecuteScalar / 8))
        cn.Close()
        Return totalRows
    End Function
    Public Function Videos() As String
        PageList = ""
        Dim currentPage As Integer = 0
        Dim pageNumber As Integer = 0
        Dim searchCriteria As String = ""
        If Page.RouteData.Values("page") Is Nothing Then
            currentPage = 1
        Else
            currentPage = Page.RouteData.Values("page")
        End If

        searchCriteria = domainName & "Videos/" & section & "/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title") & "/"
        pageNumber = getPageNumber()


        If pageNumber > 1 Then
            PageList = GetPager(currentPage, pageNumber, 5, searchCriteria)
        End If
        Dim NW As String = "<ul class=""list-unstyled row"">"
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString As String
        If section = "Gallery" Then
            selectString = "DECLARE @PageNum AS INT; DECLARE @PageSize AS INT; SET @PageNum =" & currentPage & "; SET @PageSize = 8; Select * from ( select  ROW_NUMBER()  over(ORDER BY VideoItem.GalleryID DESC) AS RowNum  , GalleryItemID as ID, Title, VideoImageURL from VideoItem where  Status=1 and GalleryID=" & Page.RouteData.Values("id")
        Else
            selectString = "DECLARE @PageNum AS INT; DECLARE @PageSize AS INT; SET @PageNum =" & currentPage & "; SET @PageSize = 8; Select * from ( select  ROW_NUMBER()  over(ORDER BY CommonVideo.CommonGalleryID DESC) AS RowNum  , CommonGalleryID as ID, Title, VideoImageURL from CommonVideo where  Status=1 and  TableName='" & section & "' and TableMasterID=" & Page.RouteData.Values("id")
        End If


        selectString = selectString + ") as MyTable WHERE RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 AND @PageNum * @PageSize"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        'cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows = True Then


            While reader.Read
                NW += "<li class=""col-sm-3""><a href=""" & domainName & "VideoPop/" & section & "/" & reader("ID") & """ rel=""album"" class=""photopopup""><div class=""img-container"">"
                If IsDBNull(reader("VideoImageURL")) = False Then
                    NW += "<img src=""" & reader("VideoImageURL") & """  height=""190"" alt="""">"
                Else
                    NW += "<h4>No Image</h4>"
                End If

                NW += "</div><h5 class=""title"">" & reader("Title").ToString() & "</h5>"
                NW += "<span class=""videoplay""><img src=""" & domainName & "ui/media/dist/icons/play-icon.png"" height=""33"" width=""34"" alt=""""></span></a>"
                If section = "Gallery" Then
                    NW += Utility.showEditButton(Request, domainName & "Admin/A-Video/GalleryItemEdit.aspx?galleryItemId=" & reader("ID").ToString() & "&SmallImageWidth=269&SmallImageHeight=190") & "</li>"
                Else
                    NW += Utility.showEditButton(Request, domainName & "Admin/A-CommonVideo/CommonVideoEdit.aspx?cgid=" & reader("ID").ToString() & "&SmallImageWidth=269&SmallImageHeight=190") & "</li>"
                End If



            End While

        Else
            If section = "Gallery" Then
                NW = "Sorry there is no video(s) to show." & Utility.showAddButton(Request, domainName & "Admin/A-Video/GalleryItemEdit.aspx?galleryId=" & Page.RouteData.Values("id") & "&SmallImageWidth=269&SmallImageHeight=190")
            Else
                NW = "Sorry there is no video(s) to show." & Utility.showAddButton(Request, domainName & "Admin/A-CommonVideo/CommonVideoEdit.aspx?TableName=" & section & "&TableMasterID=" & Page.RouteData.Values("id") & "SmallImageWidth=269&SmallImageHeight=190")
            End If

        End If
        cn.Close()
        If PageList = "" Then
            pnlPageination.Visible = False
        End If
        NW = NW + "</ul>"
        Return NW
    End Function
    Public Shared Function GetPager(ByVal presentPageNum As Integer, ByVal totalNumOfPage As Integer, ByVal totalPageNumToShow As Integer, ByVal urlToNavigateWithQStr As String) As String
        Dim i As Integer
        Dim loopStartNum, loopEndNum, presentNum, maxShownNum As Integer
        Dim pagerString As String = ""
        presentNum = presentPageNum
        maxShownNum = totalPageNumToShow
        Dim middleFactor As Integer = maxShownNum / 2
        pagerString = "<ul class=""pagination pagination-lg list-unstyled"">"
        If totalNumOfPage <= totalPageNumToShow Then
            loopStartNum = 1
            loopEndNum = totalNumOfPage
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum <= 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li>"
            ' pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        Else
            loopStartNum = If(presentNum <= (middleFactor + 1), 1, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage - (maxShownNum - 1), presentNum - middleFactor))
            loopEndNum = If(presentNum <= (middleFactor + 1), maxShownNum, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage, presentNum + middleFactor))
            loopEndNum = If(loopEndNum > totalNumOfPage, totalNumOfPage, loopEndNum)
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li> "
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        End If

        pagerString = pagerString & "</ul>"
        Return pagerString
    End Function
End Class
