﻿Imports System.Data.SqlClient
Partial Class MeetAnimal
    Inherits System.Web.UI.Page
    Public domainName, galurl As String
    Public pgalid, vgalid As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub


    Public Function GetAnimalCategory() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT List1.* FROM List1 where List1.Status=1 and ListID=@ListID order by List1.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try

        
            While reader.Read()
              
                hdnCategoryID.Value = Page.RouteData.Values("id")
                hdnTitle.Value = reader("Title")
                hdnMasterID.Value = reader("MasterID")


            End While
          
            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function


    Public Function Intro() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT List1.* FROM List1 where List1.Status=1 and ListID=@ListID order by List1.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try


            While reader.Read()

                hdnCategoryID.Value = Page.RouteData.Values("id")
                hdnTitle.Value = reader("Title").ToString()
                If IsDBNull(reader("GalleryID")) = False Then
                    pgalid = reader("GalleryID").ToString()
                Else
                    pgalid = 0
                End If
                If IsDBNull(reader("VGalleryID")) = False Then
                    vgalid = reader("VGalleryID").ToString()
                Else
                    vgalid = 0
                End If

                ImgPGImage.ImageUrl = domainName & "Admin/" & reader("PhotoAlbumImage").ToString()
                M += "<h1 class=""capital"">" & reader("Title") & "</h1>"
                M += reader("BigDetails")
                M += Utility.showEditButton(Request, domainName & "Admin/A-WildLifeAnimal/List1Edit.aspx?l1id=" + reader("ListID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

                ltAddAnimal.Text = Utility.showAddButton(Request, domainName & "Admin/A-WildLifeAnimal/List1ChildEdit.aspx?l1id=" & reader("ListID") & "&Lang=en")
                'ltAddInterestingFacts.Text = Utility.showAddButton(Request, domainName & "Admin/A-InterestingFacts/CommonFactsEdit.aspx?smallImageWidth=478&smallImageHeight=233&TableName=Animal&TableMasterID=" & reader("MasterID") & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&Lang=en")

                ltAddTestimonial.Text = Utility.showAddButton(Request, domainName & "Admin/A-Testimonial/TestimonialEdit.aspx?smallImageWidth=478&smallImageHeight=233&TableName=Animal&TableMasterID=" & reader("MasterID") & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&Lang=en")


                ltVideoAlbum.Text = "<img src=""" & domainName & "Admin/" & reader("VideoAlbumImage") & """ alt="""" class=""boxthumbnail"">" & Utility.showEditButton(Request, domainName & "Admin/A-WildLifeAnimal/List1Edit.aspx?l1id=" & Page.RouteData.Values("id") & "&Lang=en&Small=0&Big=0&Link=0&pg=0&vg=1")
                ' ltPhotoAlbum.Text = "<img src=""" & domainName & "Admin/" & reader("PhotoAlbumImage") & """ alt="""" class=""boxthumbnail"">"

            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function
    Public Sub LoadGallery(ByVal galid As Integer)
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If count = 1 Then
                    galurl = domainName & "Admin/" & reader("BigImage").ToString()
                Else
                    lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Spa"">Photo Gallery<span class=""arrow""></span></a>"
                End If
                count += 1
            End While
        End If
        cn.Close()
    End Sub
    Public Function AnimalList() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT List1_Child.* FROM List1_Child where List1_Child.Status=1 and ListID=@ListID order by List1_Child.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try


            While reader.Read()


                
                



                M += "<li id=""" & reader("Title") & """>"
                M += "<div class=""row"">"
                M += " <div class=""col-sm-3"">"
                M += "<div class=""orangebg border-box meetanim"">"
                If IsDBNull(reader("BigImage")) = False Then
                    M += "<a href=""" & domainName & "Admin/" & reader("BigImage") & """ class=""withthumbnail"" rel=""" & Utility.EncodeTitle(reader("Title"), "-") & """>"
                Else
                    M += "<a href=""javascript:;"" >"
                End If
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """  alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"
                M += " <span class=""magnifier""></span><span class=""arrow""></span>"
                M += " </a>"
                M += " </div>"
                M += " </div>"
                M += " <div class=""col-sm-9"">"
                M += " <h2 class=""title orangefont"" rel=""fancybox-thumb"">" & reader("Title") & "</h2>"

                If IsDBNull(reader("Sound")) = False Then
                    M += " <a href=""javascript:void(0)""  class=""animalsound-btn"" >Click here and hear how this animal sounds</a>"
                    M += "<audio id=""audio"">"
                    M += " <source src=""" & domainName & "Admin/" & reader("Sound") & """ type=""audio/mpeg"">"
                    M += "</audio>"
                End If
              

                M += reader("Bigdetails")
                M += "<div class=""quickfactsbox"">"
                M += "<h3 class=""subtitle orangefont"">Quick Facts</h3>"
                M += reader("SmallDetails")
                M += " </div>"
                M += " </div>"
                M += " </div>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-WildLifeAnimal/List1ChildEdit.aspx?l1cid=" + reader("ChildID").ToString() + "&l1id=" & reader("ListID") & "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0&pg=1&vg=0")
                If IsDBNull(reader("GalleryID")) = False Then
                    M += Utility.showAddButton(Request, domainName & "Admin/A-Gallery/AllGalleryItemEdit.aspx?galleryId=" & reader("GalleryID") & "&Lang=en")
                    M += LoadGalleryContent(Utility.EncodeTitle(reader("Title"), "-"), reader("GalleryID"))
                    ' M += LoadContent(Utility.EncodeTitle(reader("Title"), "-"), reader("MasterID"))
                End If


                M += " </li>"


             
            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function
    Public Function LoadGalleryContent(ByVal tname As String, ByVal galid As Integer) As String
        Dim M As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                
                M += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""withthumbnail displaynoneimage"" rel=""" & tname & """></a>"
               
            End While
        End If
        cn.Close()
        Return M
    End Function

    'Public Function LoadContent(ByVal TNAM As String, ByVal TID As String) As String
    '    Dim sConn As String
    '    Dim selectString1 As String = ""
    '    Dim M As String = ""
    '    'If section = "Gallery" Then
    '    '    selectString1 = "Select Title, BigImage from GalleryItem where   GalleryItemID=" & id
    '    'Else
    '    selectString1 = "Select Title, BigImage from CommonGallery where TableName=@TableName and TableMasterID=@TableMasterID"
    '    'End If
    '    sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
    '    Dim cn As SqlConnection = New SqlConnection(sConn)
    '    cn.Open()
    '    Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
    '    cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 500).Value = TNAM
    '    cmd.Parameters.Add("TableMasterID", Data.SqlDbType.NVarChar, 500).Value = TID

    '    Dim reader As SqlDataReader = cmd.ExecuteReader()
    '    If reader.HasRows Then
    '        While reader.Read()
    '            M += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""withthumbnail displaynoneimage"" rel=""" & TNAM & """ ></a>"
    '            'ImgBig.ImageUrl = domainName & "Admin/" & reader("BigImage").ToString()

    '            ' lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, domainName & "Admin/A-Event/EventsEdit.aspx?EventID=" & reader("EventID") & "&Small=0&Big=1&Footer=0")

    '        End While
    '    End If
    '    cn.Close()
    '    Return M
    'End Function

    Public Function LoadContentAnimal(ByVal TNAM As String, ByVal TID As String) As String
        Dim sConn As String
        Dim selectString1 As String = ""
        Dim M As String = ""
        'If section = "Gallery" Then
        '    selectString1 = "Select Title, BigImage from GalleryItem where   GalleryItemID=" & id
        'Else
        selectString1 = "Select Title, BigImage from CommonGallery where TableName=@TableName and TableMasterID=@TableMasterID"
        'End If
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 500).Value = TNAM
        cmd.Parameters.Add("TableMasterID", Data.SqlDbType.NVarChar, 500).Value = TID

        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                M += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""withthumbnail displaynoneimage"" rel=""" & TNAM & """ ></a>"
                'ImgBig.ImageUrl = domainName & "Admin/" & reader("BigImage").ToString()

                ' lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, domainName & "Admin/A-Event/EventsEdit.aspx?EventID=" & reader("EventID") & "&Small=0&Big=1&Footer=0")

            End While
        End If
        cn.Close()
        Return M
    End Function


    'Public Function AnimalGallery(ByVal MID As String, ByVal tblnm As String) As String
    '    Dim M As String = String.Empty

    '    Dim i As Integer = 0
    '    Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
    '    conn.Open()
    '    Dim selectString = "SELECT top 1 CommonGallery.* FROM CommonGallery where CommonGallery.Status=1 and TableMasterID=@TableMasterID and TableName='" & tblnm & "' order by SortIndex"
    '    Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

    '    cmd.Parameters.Add("TableMasterID", Data.SqlDbType.Int)
    '    cmd.Parameters("TableMasterID").Value = MID
    '    Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
    '    Try


    '        While reader.Read()

    '            M = reader("CommonGalleryID")

    '        End While

    '        conn.Close()

    '        Return M
    '    Catch ex As Exception
    '        Return M
    '    End Try



    'End Function


    Public Function AnimalListName() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT List1_Child.* FROM List1_Child where List1_Child.Status=1 and ListID=@ListID order by List1_Child.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try
            While reader.Read()


                M += "<li>"
                M += "<a data-scroll href=""#" & reader("Title") & """>" & reader("Title") & "</a>"
                M += "</li>"

            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function



    Public Function IslandMap() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where  HTMLID=108"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read
                Dim pdfurl As String = domainName & "Admin/" & reader("FileUploaded").ToString()
                M += "<div class=""thumbnail-round"">"
                M += " <img src=""" & domainName & "ui/media/dist/round/blue.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""normal-thumb"" alt="""">"
                M += "</img>"
                M += "</div>"

                M += "<div class=""box-content"">"
                M += " <h5 class=""title-box""><a href=""" & pdfurl & """ class="" bluefont"" target=""_blank"">Island Map</a></h5>"
                'M += " <h6 class=""sub-title"">" & reader("Title") & "</h6>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a href=""" & pdfurl & """ class=""readmorebtn"" target=""_blank"">View Map</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=1&BigDetails=0&Link=0&SmallImageWidth=151&SmallImageHeight=129&BigImageWidth=733&BigImageHeight=458")
                M += "</div>"



            End While

        End If
        conn.Close()
        Return M
    End Function
    Public Function FooterAnimalCategoryNavigation() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT List1.* FROM List1 where List1.Status=1 and ListID <> (@ListID) and Lang='en' order by List1.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try


            While reader.Read()
                If i = 0 Then
                    M += "<li class=""purplebg Single"">"
                ElseIf i = 1 Then
                    M += "<li class=""orangebg Single"">"
                ElseIf i = 2 Then
                    M += "<li class=""bluebg Single"">"
                ElseIf i = 3 Then
                    M += "<li class=""greenbg Single"">"
                ElseIf i = 4 Then
                    M += "<li class=""yellowbg Single"">"
                ElseIf i = 5 Then
                    M += "<li class=""dbluebg Single"">"
                ElseIf i = 6 Then
                    M += "<li class=""brownbg Single"">"
                End If



                M += "<a href=""" & domainName & "MeetAnimal/" & reader("ListID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """ >" & reader("Title") & ""
                M += " </a>"
                M += " </li>"
                i += 1
            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function



    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        GetAnimalCategory()
        ltAnimalList.Text = AnimalList()
        ltAnimalName.Text = AnimalListName()
        ltCategoryName.Text = hdnTitle.Value
        ltBreadcrumb.Text = hdnTitle.Value
        ltIntro.Text = Intro()
        If pgalid <> 0 Then
            LoadGallery(pgalid)
        End If

        UCInterestingFactList.TableMasterID = hdnMasterID.Value
        UCTestimonial.Table_MasterID = hdnMasterID.Value
        ltVideoLink.Text = "<a href=""" & domainName & "Videos/Gallery/" & vgalid & "/" & Utility.EncodeTitle(hdnTitle.Value, "-") & """ class=""linkbtn"">Video Gallery<span class=""arrow""></span></a>"
        ' ltVideoLink.Text = "<a href=""" & domainName & "Videos/Animal/" & hdnMasterID.Value & "/" & Utility.EncodeTitle(hdnTitle.Value, "-") & """ class=""linkbtn"">Video Gallery<span class=""arrow""></span></a>"
        'ltPhotoLink.Text = "<a href=""" & domainName & "Photos/Animal/" & hdnMasterID.Value & "/" & Utility.EncodeTitle(hdnTitle.Value, "-") & """ class=""linkbtn"">Photo Gallery<span class=""arrow""></span></a>"
        ltIslandMap.Text = IslandMap()
        ltFooterNavigation.Text = FooterAnimalCategoryNavigation()
        DynamicSEO.pageID = Page.RouteData.Values("id")
        '  UserDynamicPhotoAlbum.Master_ID = hdnMasterID.Value

        '  UserDynamicPhotoAlbum.List_ID = hdnCategoryID.Value


    End Sub
End Class
