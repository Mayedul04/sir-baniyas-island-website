﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="EventSummary.aspx.vb" Inherits="EventSummary" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>







<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName%>'>Home</a></li>
            <li><a href='<%= domainName & "Events" %>'>Events</a></li>
            <li class="active"><a href="#">Plan your Event</a></li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->


    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">

        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="204" ShowEdit="False" />
                </h1>
                <div class="row">
                    <div class="col-md-8">
                        <%--<uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText" HTMLID="204" ShowEdit="False" />--%>


                        <p>
                            <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="204" />
                        </p>

                        <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="366" />
                    </div>

                    <div class="col-md-4">
                        <div class="imgHold marginBtm15">
                            <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" ShowEdit="true" HTMLID="204" IMGBigHeight="268" IMGBigWidth="360" ImageType="Big" />

                        </div>

                        <div class="row">
                            <div class="col-md-12" id="DivPhotoGal" visible="false" runat="server">
                                <div class="purplebg border-box normal">
                                    <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" HTMLID="205" ShowEdit="False" ImageType="Small" ImageClass="boxthumbnail" />
                                    <asp:Literal ID="lblPhotoGallery" runat="server"></asp:Literal>
                                    <a href="<%= galurl%>" class="linkbtn withthumbnail" rel="Event-Summary">Photos <span class="arrow"></span></a>
                                </div>
                                <uc1:UserControlEditButton runat="server" ID="EditPhotoGallery" HTMLID="205" PGalleryEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="228" ImageWidth="360" />
                                <% If pgalid <> 0 Then%>
                                <%= Utility.showAddButton(Request, domainName & "Admin/A-Gallery/AllGalleryItemEdit.aspx?galleryId=" & pgalid & "&Lang=en")%>
                                <% End If%>
                            </div>

                            <div class="col-md-12" id="DivVideoGal" visible="false" runat="server">
                                <div class="greenbg border-box normal">
                                    <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="206" ShowEdit="False" ImageType="Small" ImageClass="boxthumbnail" />
                                    <asp:Literal ID="lblVGalleryUrl" runat="server"></asp:Literal>
                                    

                                </div>
                                <uc1:UserControlEditButton runat="server" ID="UserControlEditButton1" HTMLID="206" VGalleryEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="228" ImageWidth="360" />
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- -- welcome-text ends here -- -->
        </div>


    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->


    <section class="container">

        <div class="row">
            <div class="col-sm-6">
                <!-- Testimonial Slider -->
                <div class="testimonialSlider">
                    <ul class="list-unstyled slides">
                        <%= Testimonials() %>
                    </ul>
                    <%= Utility.showAddButton(Request, domainName & "Admin/A-Event/TestimonialEdit.aspx?SmallImageWidth=139&SmallImageHeight=138&lang=en")%>
                </div>
                <!-- Testimonial Slider -->
            </div>
            <div class="col-sm-6">
                <!-- -- roundedbox starts here -- -->
                <div class="roundedbox orange inner">
                    <div class="thumbnail-round">
                        <img src='<%= domainName & "ui/media/dist/round/orange.png" %>' height="113" width="122" alt="" class="roundedcontainer">
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage3" ShowEdit="false" HTMLID="6" ImageType="Small" ImageClass="normal-thumb" ImageHeight="129" ImageWidth="151" />

                    </div>
                    <div class="box-content">
                        <h5 class="title-box"><a href='<%= domainName & "Adventure" %>'>Activities
                        </a>

                        </h5>
                        <p>
                            <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText1" HTMLID="6" ShowEdit="false" />
                        </p>
                        <a href='<%= domainName & "Adventure" %>' class="readmorebtn">Read more</a>
                    </div>
                    <uc1:UserControlEditButton runat="server" ID="EditBtnEventEnquiry" HTMLID="6" TitleEdit="true" TextEdit="true" TextType="Small" ImageEdit="true" ImageType="Small" ImageHeight="170" ImageWidth="170" />
                </div>

                <!-- -- roundedbox ends here -- -->
            </div>
        </div>



    </section>
</asp:Content>

