﻿
Partial Class Stay
    Inherits System.Web.UI.Page
    Public domainName As String

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub





    Public Function StayListName() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM List_Restaurant where Status='1' and Lang='en' order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read

                M += "<li>"
                M += "<a data-scroll href=""#" & reader("RestaurantID") & """>" & reader("OpeningTime").ToString & "</a>"
                M += "</li>"
                'M += Utility.showEditButton(Request, domainName & "Admin/A-WildLifeAnimal/List1Edit.aspx?l1id=" + reader("ListID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")
            End While

        End If
        conn.Close()
        Return M
    End Function

    Public Function StayListDetails() As String
        Dim M As String = ""
        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM List_Restaurant where Status='1' and Lang='en' order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read

                i = i + 1
                    M += "<li class=""" & reader("Link") & """ id=""" & reader("RestaurantID") & """>"
                    M += "<div class=""image-container"">"
                    M += "<img src=""" & domainName & "Admin/" & reader("BigImage") & """ alt=""" & reader("ImageAltText") & """>"
                    M += "</img>"
                    M += " </div>"
                    M += "<div class=""container"">"
                    M += "<div class=""row"">"

                    If i Mod 2 = 0 Then
                        M += "<div class=""col-sm-offset-4 col-md-offset-6  col-sm-8 col-md-6"">"

                    Else
                        M += "<div class=""col-sm-8 col-md-6"">"
                    End If

                    M += "<div class=""white-wrapper"">"
                    M += "<h3 class=""h2 title"">"
                    M += reader("Title")
                    M += "</h3>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-Stay/StayEdit.aspx?lid=" + reader("RestaurantID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

                    M += " <a href=""" & domainName & "Stay-Details/" & reader("RestaurantID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""readmore-btn"">View more</a>"
                    M += " </div>"
                    M += " </div>"
                    M += " </div>"
                    M += " </div>"
                   
                    M += "</li>"





            End While

        End If
        conn.Close()
        Return M
    End Function



    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ltStayList.Text = StayListName()
        ltStayDetails.Text = StayListDetails()
    End Sub
End Class
