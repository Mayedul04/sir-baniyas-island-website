﻿Imports System.Data.SqlClient
Imports System.Data
Partial Class Vet_Conservation_Details
    Inherits System.Web.UI.Page
    Public domainName, galurl As String

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString


    End Sub


    Function PhotoHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 126)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()



                M += "<div class=""purplebg border-box bigheight"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"

                M += "<a href=""" & domainName & "Photos/Gallery/8/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""linkbtn"">" & reader("Title") & "<span class=""arrow""></span></a>"
                M += "</img>"
                M += "<p>" & reader("SmallDetails") & "</p>"

                M += "<a href=""" & domainName & "Photos/Gallery/8/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""readmoreBttn purple"">Read more</a>"""
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function
    'Public Function IslandMap() As String
    '    Dim M As String = ""
    '    Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
    '    conn.Open()
    '    Dim selectString = "SELECT * FROM HTML where Status='1' and HTMLID=108"
    '    Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
    '    'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
    '    'cmd.Parameters("Gallery").Value = IDFieldName

    '    Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
    '    If reader.HasRows Then


    '        While reader.Read

    '            M += "<div class=""thumbnail-round"">"
    '            M += " <img src=""" & domainName & "ui/media/dist/round/blue.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
    '            M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""normal-thumb"" alt="""">"
    '            M += "</img>"
    '            M += "</div>"

    '            M += "<div class=""box-content"">"
    '            M += " <h5 class=""title-box""><a href=""" & domainName & "Island-Map"" class=""mapFancyIframe bluefont"">Island Map</a></h5>"""
    '            M += " <h6 class=""sub-title"">" & reader("Title") & "</h6>"""
    '            M += "<p>" & reader("SmallDetails") & "</p>"
    '            M += "<a href=""" & domainName & "Island-Map"" class=""readmorebtn mapFancyIframe"">View Map</a>"""
    '            M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")
    '            M += "</div>"



    '        End While

    '    End If
    '    conn.Close()
    '    Return M
    'End Function

    Public Function GetValueToHidden() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT List_VetConservationTeam.* FROM List_VetConservationTeam where List_VetConservationTeam.Status=1 and List_VetConservationTeam.ListID=@ListID order by List_VetConservationTeam.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try

            While reader.Read()

                hdnID.Value = Page.RouteData.Values("id")
                hdnTitle.Value = reader("Title")
                hdnMasterID.Value = reader("MasterID")
                DynamicSEO.PageType = "List_VetConservationTeam"
                DynamicSEO.PageID = reader("ListID")

            End While

            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function

    Public Function IslandMap() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where HTMLID=108"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read

                M += "<div class=""thumbnail-round"">"
                M += " <img src=""" & domainName & "ui/media/dist/round/blue.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""normal-thumb"" alt="""">"
                M += "</img>"
                M += "</div>"

                M += "<div class=""box-content"">"
                M += " <h5 class=""title-box""><a href=""" & domainName & "IslandMap-Pop"" class=""mapFancyIframe bluefont"">Island Map</a></h5>"
                'M += " <h6 class=""sub-title"">" & reader("Title") & "</h6>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a href=""" & domainName & "IslandMap-Pop"" class=""readmorebtn mapFancyIframe"">View Map</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")
                M += "</div>"



            End While

        End If
        conn.Close()
        Return M
    End Function


    Public Function ConservationTeam() As String
        Dim M As String = ""


        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        'Dim sql = "SELECT * FROM List_NexaClientCategory WHERE Status='1' order by SortIndex"

        Dim sql = "select *  from List_VetConservationTeam WHERE Status='1' and ListID=@ListID"

        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = Page.RouteData.Values("id")
        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then

                While reader.Read()

                    M += "<h1 class=""capital"">" & reader("Title") & "</h1>"

                    M += "<img src=""" & domainName & "Admin/" & reader("BigImage") & """ style=""margin-right:30px;margin-bottom:20px;"" alt="""" class=""pull-left"">"
                    M += reader("BigDetails")
                    M += Utility.showEditButton(Request, domainName & "Admin/A-VetConservation/ListEdit.aspx?lid=" + reader("ListID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

                End While


            End If
            reader.Close()
            con.Close()

           

            Return M
        Catch ex As Exception
            Return M
        End Try
    End Function

    Function ConservationHTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", 4)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()



                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""transparent"">"
                M += "<h3 class=""title""><a href=""" & domainName & "Animal"">" & reader("Title") & "</a></h3>"
                M += "<p>" & reader("SmallDetails") & Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=478&SmallImageHeight=233&BigImageWidth=733&BigImageHeight=458") & "</p>"

                M += "<a href=""" & domainName & "Animal"" class=""readmorebtn"">Read more</a>"
                '   M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=478&SmallImageHeight=233&BigImageWidth=733&BigImageHeight=458")

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function
    Public Function StaticVideoGallery(ByVal galid As Integer) As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT SmallImage FROM Video WHERE GalleryID=@GalleryID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("GalleryID", galid)

        Dim reader = cmd.ExecuteReader()

        While reader.Read
            M += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ alt="""" class=""boxthumbnail"">"
            M += Utility.showEditButton(Request, domainName & "Admin/A-Video/GalleryEdit.aspx?galleryId=" & galid)
            '   M+= Utility.showEditButton(Request, domainName & "Admin/A-Video/GalleryItemEdit.aspx?"
            M += "<a href=""" & domainName & "Videos/Gallery/" & galid & "/Vet-Conservation-Team"" class=""linkbtn"">Video Gallery<span class=""arrow""></span></a>"
        End While
        con.Close()
        Return M

    End Function
    
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        GetValueToHidden()
        UCInterestingFactList.TableMasterID = hdnMasterID.Value
        UCTestimonial.Table_MasterID = hdnMasterID.Value

        '  ltVideoLink.Text = "<a href=""" & domainName & "Video-Gallery/6/Vet-Conservation-Team"" class=""linkbtn"">Video Gallery<span class=""arrow""></span></a>"
        'ltIslandMap.Text = IslandMap()
        lblMeetTheAnimal.Text = ConservationHTMLText()
        ltDetails.Text = ConservationTeam()
      
    End Sub
End Class
