﻿Imports System.Data.SqlClient
Partial Class Arabian_Wildlife
    Inherits System.Web.UI.Page
    Public domainName As String
    Public pgalid, vgalid As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub


    Public Function IslandMap() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where  HTMLID=108"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read
                Dim pdfurl As String = domainName & "Admin/" & reader("FileUploaded").ToString()
                M += "<div class=""thumbnail-round"">"
                M += " <img src=""" & domainName & "ui/media/dist/round/blue.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""normal-thumb"" alt="""">"
                M += "</img>"
                M += "</div>"

                M += "<div class=""box-content"">"
                '  M += " <h5 class=""title-box""><a href=""" & domainName & "IslandMap-Pop"" class=""mapFancyIframe2 bluefont"">Island Map</a></h5>"
                M += " <h5 class=""title-box""><a href=""" & pdfurl & """ class="" bluefont"" target=""_blank"">Island Map</a></h5>"
                'M += " <h6 class=""sub-title"">" & reader("Title") & "</h6>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a href=""" & pdfurl & """ class=""readmorebtn"" target=""_blank"">View Map</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=1&BigDetails=0&Link=0&SmallImageWidth=151&SmallImageHeight=129&BigImageWidth=733&BigImageHeight=458")
                M += "</div>"



            End While

        End If
        conn.Close()
        Return M
    End Function

    Public Function Activity() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where  HTMLID=6"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read

                M += "<div class=""thumbnail-round"">"
                M += " <img src=""" & domainName & "ui/media/dist/round/green.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""normal-thumb"" alt="""">"
                M += "</img>"
                M += "</div>"

                M += "<div class=""box-content"">"
                M += "<h5 class=""title-box""><a href=""" & domainName & "Adventure"" class=""greenfont"">Activities</a></h5>"
                'M += "<h6 class=""sub-title"">" & reader("Title") & " </h6>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a href=""" & domainName & "Adventure"" class=""readmorebtn"">View All</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SecondImage=0&File=0&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&BigDetails=0&Link=0&SmallImageWidth=139&SmallImageHeight=139&BigImageWidth=0&BigImageHeight=0")
                M += "</div>"


            End While

        End If
        conn.Close()
        Return M
    End Function


 

    Public Sub LoadGallery(ByVal galid As Integer)
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()

                lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Spa"">Photo Gallery<span class=""arrow""></span></a>"

            End While
        End If
        cn.Close()
    End Sub

    'Public Function PhotoGalleryImage() As String
    '    Dim M As String = ""
    '    Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
    '    conn.Open()
    '    Dim selectString = "SELECT * FROM Gallery where Status='1' and GalleryID=@GalleryID order by SortIndex"
    '    Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
    '    cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
    '    cmd.Parameters("GalleryID").Value = 3

    '    Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
    '    If reader.HasRows Then


    '        While reader.Read

    '            M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"
    '            M += Utility.showEditButton(Request, domainName & "Admin/A-Gallery/GalleryEdit.aspx?galleryId=" + reader("GalleryID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

    '        End While

    '    End If
    '    conn.Close()
    '    Return M
    'End Function

    'Function VetConservationTeam() As String
    '    Dim M As String = ""
    '    Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
    '    con.Open()
    '    Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
    '    Dim cmd = New SqlCommand(sql, con)
    '    cmd.Parameters.Clear()
    '    cmd.Parameters.AddWithValue("HtmlID", 105)

    '    Try
    '        Dim reader = cmd.ExecuteReader()
    '        If reader.HasRows = True Then
    '            reader.Read()
    '            M += "<div class=""bluebg single-box"">"
    '            M += "<img src=""" & domainName & "Admin/" & reader("BigImage") & """ alt="""" class=""transparent"" />"
    '            M += " <h3 class=""title"">"
    '            M += "<a href=""" & domainName & "Vet-Conservation-Team"" > "
    '            M += reader("Title")
    '            M += "</a>"
    '            M += "</h3>"

    '            M += reader("BigDetails").ToString
    '            M += "<a href=""" & domainName & "Vet-Conservation-Team"" class=""readmorebtn"">Read more</a>"

    '            M += "<p class=""pull-right"">" & Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0") & "</p>"
    '            M += "</div>"

    '        End If
    '        reader.Close()
    '        con.Close()
    '        Return M
    '    Catch ex As Exception
    '        Return M
    '    End Try

    'End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim sConn As String
        Dim selectString1 As String = "Select GalleryID, VGalleryID from HTML where  HTMLID=@HTMLID and Lang='en'"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("HTMLID", Data.SqlDbType.NVarChar, 50).Value = 217
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                If IsDBNull(reader("GalleryID")) = False Then
                    pgalid = reader("GalleryID").ToString()
                End If
                If IsDBNull(reader("VGalleryID")) = False Then
                    vgalid = reader("VGalleryID").ToString()
                End If


            End While
        End If
        cn.Close()
        LoadGallery(pgalid)
        'ltPhotoImage.Text = PhotoGalleryImage()
        ltIslandMap.Text = IslandMap()
        ltAdvanture.Text = Activity()


    End Sub
End Class
