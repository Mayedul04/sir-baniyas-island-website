<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<%@ Register Src="~/CustomControl/UCHTMLTitleDetails.ascx" TagPrefix="uc1" TagName="UCHTMLTitleDetails" %>
<%@ Register Src="~/CustomControl/UCFeaturedVideoFromAlbum.ascx" TagPrefix="uc1" TagName="UCFeaturedVideoFromAlbum" %>
<%@ Register Src="~/CustomControl/UCHTMLTitle.ascx" TagPrefix="uc1" TagName="UCHTMLTitle" %>
<%@ Register Src="~/CustomControl/UCHTMLDetails.ascx" TagPrefix="uc1" TagName="UCHTMLDetails" %>
<%@ Register Src="~/CustomControl/UCAdvantureActivityHome.ascx" TagPrefix="uc1" TagName="UCAdvantureActivityHome" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserHTMLSecondImage.ascx" TagPrefix="uc1" TagName="UserHTMLSecondImage" %>
<%@ Register Src="~/CustomControl/UCHTMLSmallImage.ascx" TagPrefix="uc1" TagName="UCHTMLSmallImage" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>
<%@ Register Src="~/CustomControl/UCHTMLImage.ascx" TagPrefix="uc1" TagName="UCHTMLImage" %>
<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>















<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <uc1:UCHTMLTitleDetails runat="server" ID="UCHTMLTitleDetails" HTMLID="90" />

            </section>
            <!-- -- welcome-text ends here -- -->

            <div class="row">
                <div class="col-md-6">
                    <!-- -- shortcut-box starts here -->
                    <div class="shortcut-box">
                        <!-- -- quicklinks starts here -- -->
                        <div class="quicklinks">
                            <a href='<%= domainName & "VideoGallery" %>' class="videogallery orangebg">Video Gallery
                               
                                <span class="arrow"></span>
                                <span class="linkarrow"></span>
                            </a>
                            <a href='<%= domainName & "PhotoGallery"%>' class="photogallery purplebg">Photo Gallery
                                <span class="arrow"></span>
                                <span class="linkarrow"></span>
                            </a>
                            <a href='<%= domainName & "Admin/Content/The-Island-Map-fileupload22112014124423.pdf"%>' target="_blank" class=" islandmap greenbg">Island Map
                                <span class="arrow"></span>
                                <span class="linkarrow"></span>
                            </a>
                            <a href='<%= brochure %>' target="_blank" class="dpdfs bluebg">
                                Download Brochure
                                <span class="arrow"></span>
                                <span class="linkarrow"></span>
                            </a>
                        </div>
                        <!-- -- quicklinks ends here -->

                        <!-- -- video-box starts here -- -->
                        <div class="video-box">
                            <%-- <img src="ui/media/dist/elements/video.jpg" height="284" width="402" alt="">--%>
                            <uc1:UCFeaturedVideoFromAlbum runat="server" ID="UCFeaturedVideoFromAlbum" ImageHeight="284" ImageWidth="402" idfield_name="3" />
                           
                        </div>


                           <div class="photo-box">
                            <a href='<%= domainName & "PhotoGallery"%>'>
        
                                <asp:Literal ID="ltphoto" runat="server"></asp:Literal>    
                            </a>
                        </div>
                        <div class="map-box">
                             <a href='<%= domainName & "Admin/Content/The-Island-Map-fileupload22112014124423.pdf"%>' target="_blank" >
                                 <uc1:UserHTMLSecondImage runat="server" id="UserHTMLSecondImage" HTMLID="108" ImageType="Second"  />
                             </a>
                        </div>
                        <div class="dpdfs-box">
                         <asp:Literal ID="ltrFile" runat="server"></asp:Literal>
                           
                        </div>
                        <!-- -- video-box ends here -->
                    </div>
                    <!-- -- shortcut-box ends here -->
                </div>

                <div class="col-md-6">
                    <!-- -- shortcut-links-home -- starts here -->
                    <ul class="list-unstyled shortcut-links-home">
                        <li>
                            <a class="bookNowIframe" href='<%= domainName & "Booknow-Pop"%>'>
                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="227" ImageType="Small"  ImageWidth="139"/>
                                
                                <span class="bar bluebg">Book now<span class="arrow"></span></span>
                                <uc1:UserControlEditButton runat="server" ID="UserControlEditButton"  HTMLID="227" ImageEdit="true" ImageType="Small" ImageHeight="108" ImageWidth="139"/>
                            </a>
                        </li>
                        <li>
                            <a href='<%= domainName & "Map-Pop"%>' class="mapFancyIframe">
                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" HTMLID="228" ImageType="Small"  ImageWidth="137" />
                                
                                <span class="bar greenbg">Location map<span class="arrow"></span></span>
                                <uc1:UserControlEditButton runat="server" ID="UserControlEditButton1"  HTMLID="228" ImageEdit="true" ImageType="Small" ImageHeight="108" ImageWidth="137"/>
                            </a>
                        </li>
                        <li>
                            <a href='<%= domainName & "Travel-Air"%>'>
                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="229" ImageType="Small"  ImageWidth="137" />
                                
                                <span class="bar purplebg">
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="229" />
                                    <span class="arrow"></span></span>
                                <uc1:UserControlEditButton runat="server" ID="UserControlEditButton2"  HTMLID="229" TitleEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="108" ImageWidth="137"/>
                            </a>
                        </li>
                        <li>
                            <a href='<%= domainName & "Sir-Bani-Yas-History"%>'>
                                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage3" HTMLID="230" ImageType="Small"  ImageWidth="137" />
                                
                                <span class="bar orangebg">
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle2" HTMLID="230" />
                                    <span class="arrow"></span></span>
                                <uc1:UserControlEditButton runat="server" ID="UserControlEditButton3"   HTMLID="230" TitleEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="108" ImageWidth="137"/>
                            </a>
                        </li>
                    </ul>
                    <!-- -- shortcut-links-home ends here -- -->

                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox homepageboxer">
                      
                            <asp:Literal ID="ltIslandNews" runat="server"></asp:Literal>
                         
                    <!-- -- roundedbox ends here -- -->
                </div>
            </div>
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->
    <section class="daytrip">
        <div class="container">
             <div class="roundedbox orange extrahomer">
                <div class="square-image">
                    <uc1:UserHTMLImage runat="server" ID="UserHTMLImage4" HTMLID="319" ImageType="Small"  ImageWidth="285" ImageHeight="135" />
                    
                </div>
                <div class="box-content">
                    <h3 class="title-box"><a href='<%= domainName & "Adventure-Activity-Day-Trip"%>'><uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle3" HTMLID="319" show_Edit="False" /></a></h3>
                    <p>
                         <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText" HTMLID="319" ShowEdit="true"  TitleEdit="true" ImageEdit="true" ImageHeight="135" ImageWidth="285" ImageType="Small"/>
                    </p> <a href='<%= domainName & "Adventure-Activity-Day-Trip"%>' class="readmorebtn">Read more</a> 
                </div>
            </div>
        </div>
    </section>

    <!-- Activities-area starts here 
    -------------------------------------------- -->
    <section id="Home-activites-area">
        <div class="container">
            <h2 class="h1 centeralized panel-heading capital">
           
                <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle1" HTMLID="98" show_Edit="True" />
            </h2>
            <div class="row">
                <uc1:UCAdvantureActivityHome runat="server" ID="UCAdvantureActivityHome" p_class="en" />
            </div>
        </div>
    </section>
    <!-- Activities-area ends here 
    -------------------------------------------- -->
    <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="169" />
</asp:Content>

