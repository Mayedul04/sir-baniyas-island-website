﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Arabian-Wildlife.aspx.vb" Inherits="Arabian_Wildlife" %>

<%@ Register Src="~/CustomControl/UCHTMLTitle.ascx" TagPrefix="uc1" TagName="UCHTMLTitle" %>
<%@ Register Src="~/CustomControl/UCHTMLSmallImage.ascx" TagPrefix="uc1" TagName="UCHTMLSmallImage" %>
<%@ Register Src="~/CustomControl/UCHTMLDetails.ascx" TagPrefix="uc1" TagName="UCHTMLDetails" %>
<%@ Register Src="~/CustomControl/UCInterestingFactList.ascx" TagPrefix="uc1" TagName="UCInterestingFactList" %>
<%@ Register Src="~/CustomControl/UCPhotoAlbum.ascx" TagPrefix="uc1" TagName="UCPhotoAlbum" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserVetConservationTeam.ascx" TagPrefix="uc1" TagName="UserVetConservationTeam" %>
<%@ Register Src="~/CustomControl/UserStaticPhotoAlbum.ascx" TagPrefix="uc1" TagName="UserStaticPhotoAlbum" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>











<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName%>'>Home</a></li>
            <li><a href='<%= domainName & "Wildlife"%>'>Wildlife</a></li>
            <li class="active">Arabian Wildlife Park</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">

                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="3" />
                </h1>

                <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" show_Edit="True" IMG_Class="pull-left" IMG_Style="margin-right:20px;margin-bottom:20px;" ImageType="Small" HTMLID="3" ImageWidth="370" ImageHeight="262" IMGBigWidth="1600" IMGBigHeight="446" IMGSmallWidth="370" IMGSmallHeight="262" />


                <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="3" />

            </section>
            <!-- -- welcome-text ends here -- -->
            <uc1:StaticSEO runat="server" ID="StaticSEO"  SEOID="16" />
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">
        <div class="container">
            <div class="row">

                <div class="col-sm-12 col-md-5">
                    <!-- Intresting Facts Slider -->
                    <uc1:UCInterestingFactList runat="server" ID="UCInterestingFactList" />
                    <!-- Intresting Facts Slider -->
                </div>


                <div class="col-sm-6 col-md-3">
                    <!-- Vet Conservation -->
              
                    <uc1:UserVetConservationTeam runat="server" ID="UserVetConservationTeam" HTMLID="105" />

                    <!-- Vet Conservation -->
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="bluebg border-box">

                        <!-- Photo Gallery -->

                        <asp:Literal ID="lblPhotoGallery" runat="server"></asp:Literal>
                       
                      <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="217" ImageType="Small" ImageClass="boxthumbnail" ImageHeight="225" ImageWidth="374" ShowEdit="false" />
                    <uc1:UserControlEditButton runat="server" ID="editPhotoGallery"  HTMLID="217"  PGalleryEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="225" ImageWidth="374"/>
                        
                        <!-- Photo Gallery -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox blue inner">
                        <asp:Literal ID="ltIslandMap" runat="server"></asp:Literal>
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox green inner">
                        <asp:Literal ID="ltAdvanture" runat="server"></asp:Literal>
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
            </div>

            <div class="footer-nav">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-2">
                        <a href='<%=domainName & "Animal"%>' class="orangebg">
                            <img src="ui/media/dist/icons/animal-btn.png" height="29" width="29" alt="">
                            Animals</a>
                    </div>
                    <div class="col-sm-4">
                        <a href='<%=domainName & "Breeding"%>' class="bluebg">
                            <img src="ui/media/dist/icons/breeding-btn.png" height="25" width="19" alt="">
                            Breeding & Relocation</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->

</asp:Content>

