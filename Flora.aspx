<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Flora.aspx.vb" Inherits="Flora" %>

<%@ Register Src="~/CustomControl/UCHTMLTitle.ascx" TagPrefix="uc1" TagName="UCHTMLTitle" %>
<%@ Register Src="~/CustomControl/UCHTMLSmallImage.ascx" TagPrefix="uc1" TagName="UCHTMLSmallImage" %>
<%@ Register Src="~/CustomControl/UCHTMLDetails.ascx" TagPrefix="uc1" TagName="UCHTMLDetails" %>
<%@ Register Src="~/CustomControl/UCInterestingFactList.ascx" TagPrefix="uc1" TagName="UCInterestingFactList" %>
<%@ Register Src="~/CustomControl/UCPhotoAlbum.ascx" TagPrefix="uc1" TagName="UCPhotoAlbum" %>
<%@ Register Src="~/CustomControl/UCVideoAlbum.ascx" TagPrefix="uc1" TagName="UCVideoAlbum" %>
<%@ Register Src="~/CustomControl/UCTestimonial.ascx" TagPrefix="uc1" TagName="UCTestimonial" %>
<%@ Register Src="~/CustomControl/UserStaticPhotoAlbum.ascx" TagPrefix="uc1" TagName="UserStaticPhotoAlbum" %>

<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Sir-Bani-Yas-Island"%>'>Sir bani yas</a></li>
            <li class="active">Flora</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">

                <asp:Literal ID="ltFloraText" runat="server"></asp:Literal>
            </section>
            <!-- -- welcome-text ends here -- -->

            <ul class="list-unstyled floralist">
                <asp:Literal ID="ltFloraList" runat="server"></asp:Literal>

            </ul>
        </div>
    </section>
    <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="214" />
    <!-- Content-area ends here 
    -------------------------------------------- -->



    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">

        <div class="container">

            <div class="row">

                <div class="col-sm-4">

                   <div class="purplebg border-box">
                      

                            <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="227" ImageType="Small" ImageClass="boxthumbnail"  ImageWidth="374" ShowEdit="false" />
                            <a href='<%= galurl %>' class="linkbtn withthumbnail" rel="Flora">Photo Gallery<span class="arrow"></span></a>
                            <uc1:UserControlEditButton runat="server" ID="editPhotoGallery" HTMLID="227" PGalleryEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="225" ImageWidth="374" />
                            <% If pgalid <> 0 Then%>
                            <%= Utility.showAddButton(Request, domainName & "Admin/A-Gallery/AllGalleryItemEdit.aspx?galleryId=" & pgalid & "&Lang=en")%>
                            <% End If%>
                       
                        <asp:Literal ID="lblPhotoGallery" runat="server"></asp:Literal>
                   <%-- <uc1:UserStaticPhotoAlbum runat="server" ID="UserStaticPhotoAlbum" Gallery_ID="14" />--%>
                       </div> 
                </div>

                <div class="col-sm-3">
                    <div class="greenbg single-box sirbaniyasBox">
                        <asp:Literal ID="ltConservation" runat="server"></asp:Literal>
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="purplebg single-box sirbaniyasBox">
                        <asp:Literal ID="ltSaltDome" runat="server"></asp:Literal>
                    </div>

                </div>

            </div>

            <div class="row">
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox blue inner">
                        <asp:Literal ID="ltActivity" runat="server"></asp:Literal>
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox orange inner">
                        <asp:Literal ID="ltHistoricalTimeline" runat="server"></asp:Literal>
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
            </div>


            <div class="row">
                <div class="col-sm-6">
                    <div class="roundedbox orange inner">
                    <div class="thumbnail-round">
                        <img src='<%= domainName & "ui/media/dist/round/orange.png" %>' height="113" width="122" alt="" class="roundedcontainer">
                       <uc1:UserHTMLImage runat="server" ID="UserHTMLImage3" ShowEdit="false" HTMLID="6" imageType="Small" ImageClass="normal-thumb" ImageHeight="129" ImageWidth="151" />
                       
                    </div>
                    <div class="box-content">
                        <h5 class="title-box"><a href='<%= domainName & "Adventure" %>'>
                            Activities
                                              </a>

                        </h5>
                        <p>
                            <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText1" HTMLID="6" ShowEdit="false" />
                        </p>
                        <a href='<%= domainName & "Adventure" %>' class="readmorebtn">Read more</a>
                    </div>
                    <uc1:UserControlEditButton runat="server" ID="EditBtnEventEnquiry" HTMLID="6" TitleEdit="true" TextEdit="true" TextType="Small" ImageEdit="true" ImageType="Small" ImageHeight="170" ImageWidth="170" />
                </div>
                    <!-- -- roundedbox starts here -- -->
                    <%--<div class="roundedbox green inner">
                        <asp:Literal ID="ltFlora" runat="server"></asp:Literal>
                    </div>--%>
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox blue inner">
                        <asp:Literal ID="ltIslanMap" runat="server"></asp:Literal>
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
            </div>

        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->

    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns" class="nomargintop">
        <div class="container">
            <ul class="list-unstyled">
                <li class="purplebg">
                    <a href='<%= domainName & "PhotoGallery"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/photo-gallery.png"%>' height="25" width="29" alt=""></span>
                        Photo gallery
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "VideoGallery"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/video-gallery.png"%>' height="20" width="25" alt=""></span>
                        Video gallery
                    </a>
                </li>
                <li class="bluebg">
                    <a class="bookNowIframe" href='<%= domainName & "Booknow-Pop"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/book-now.png"%>' height="27" width="27" alt=""></span>
                        Book now
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "Map-Pop"%>' class="mapFancyIframe">
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/location-map.png"%>' height="26" width="32" alt=""></span>
                        Location map
                    </a>
                </li>
                <li class="purplebg">
                    <a href='<%= domainName & "Travel-Air"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/flights.png"%>' height="14" width="38" alt=""></span>
                        Flights
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "Sir-Bani-Yas-History"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/history.png"%>' height="20" width="27" alt=""></span>
                        History
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "WhatsNew"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/whats-new.png"%>' height="17" width="18" alt=""></span>
                        What's New
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->

</asp:Content>

