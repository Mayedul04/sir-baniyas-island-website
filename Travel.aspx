﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Travel.aspx.vb" Inherits="Travel" %>

<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName%>'>Home</a></li>
            <li class="active">Travel</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->


    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">

                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle3" HTMLID="60" ShowEdit="false" />
                </h1>
                <p>
                    <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText" HTMLID="60" />

                </p>
                <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="254" />
            </section>
            <!-- -- welcome-text ends here -- -->
        </div>

        <!-- -- title-bar starts here -- -->
        <div class="title-bar greenbg">
            <div class="container">
                <h2 class="title">How To get To Sir Bani Yas Island</h2>
                <span class="arrow"></span>
            </div>
        </div>
        <!-- -- title-bar ends here -- -->

        <!-- -- page-breadcrumb starts here -- -->
        <div class="container">
            <div class="page-breadcrumb">
                <ul class="list-unstyled">
                    <li>
                        <a data-scroll href="#air">By Air</a>
                    </li>
                    <li>
                        <a data-scroll href="#road">By Road
                        </a>
                    </li>
                    <li>
                        <a data-scroll href="#sea">By Sea
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- -- page-breadcrumb ends here -- -->

        <!-- -- parallax-section starts here -- -->
        <div class="parallax-section">
            <ul class="list-unstyled">
                <li class="purplebg" id="air">
                    <div class="image-container">
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage"  ShowEdit="false" HTMLID="61" ImageType="Big"/>
                      
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-8 col-md-6">
                                <div class="white-wrapper">
                                    <h3 class="h2 title">
                                        <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="61" ShowEdit="false" />
                                    </h3>
                                    <p>
                                        
                                        <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText1" HTMLID="61" ImageEdit="True" ImageType="Big" IMGBigHeight="446" IMGBigWidth="1600" />
                                    </p>
                                    <a href='<%= domainName & "Travel-Air"%>' class="readmore-btn">View more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="orangebg" id="road">
                    <div class="image-container">
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1"  ShowEdit="false" HTMLID="62" ImageType="Big"/>
                       
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-4 col-md-6 col-md-offset-6 ">
                                <div class="white-wrapper">
                                    <h3 class="h2 title">
                                        <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle1" HTMLID="62" ShowEdit="false" />
                                    </h3>
                                    <p>
                                        <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText2" HTMLID="62" ImageEdit="True" ImageType="Big" IMGBigHeight="446" IMGBigWidth="1600" />

                                    </p>
                                    <a href='<%= domainName & "Travel-Road" %>' class="readmore-btn">View more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="bluebg" id="sea">
                    <div class="image-container">
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2"  ShowEdit="false" HTMLID="63" ImageType="Big"/>
                       
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-8 col-md-6">
                                <div class="white-wrapper">
                                    <h3 class="h2 title">
                                        <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle2" HTMLID="63" ShowEdit="false" />
                                    </h3>
                                    <p>
                                        <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText3" HTMLID="63" ImageEdit="true" ImageType="Big" IMGBigHeight="446" IMGBigWidth="1600" />

                                    </p>
                                    <a href='<%= domainName & "Travel-Sea" %>' class="readmore-btn">View more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <!-- -- parallax-section ends here -- -->
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns">
        <div class="container">
            <ul class="list-unstyled">
                <li class="purplebg">
                    <a href='<%= domainName & "PhotoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/photo-gallery.png"%>' height="25" width="29" alt=""></span>
                        Photo gallery
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "VideoGallery" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/video-gallery.png"%>' height="20" width="25" alt=""></span>
                        Video gallery
                    </a>
                </li>
                <li class="bluebg">
                    <a class="bookNowIframe" href='<%= domainName & "BookNow-Pop"%>'>
                        <span>
                            <img src='<%= domainName &"ui/media/dist/icons/book-now.png"%>' height="27" width="27" alt=""></span>
                        Book now
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "Map-Pop"%>' class="mapFancyIframe">
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/location-map.png"%>' height="26" width="32" alt=""></span>
                        Location map
                    </a>
                </li>
                <li class="purplebg">
                    <a href='<%= domainName & "Travel-Air"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/flights.png"%>' height="14" width="38" alt=""></span>
                        Flights
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "Sir-Bani-Yas-History" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/history.png"%>' height="20" width="27" alt=""></span>
                        History
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "WhatsNew" %>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/whats-new.png"%>' height="17" width="18" alt=""></span>
                        What's New
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
</asp:Content>

