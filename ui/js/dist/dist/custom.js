// Playground for the site begin 
// // ========================================

$('#gallery ul li').hover(function(){
    $(this).addClass('hover');
},function(){
    $(this).removeClass('hover');
})

// Homepage slider using flexslider
// ------------------------------------
$('#Header-slider').flexslider({
    animation: "slide",
    controlNav: false,
    directionNav: false,
    useCSS: false,
    itemWidth: 0,
    minItems: 1,
    move: 1
});



//For New Arrival Slider
//===================================================
var dynwidth = $(window).width();
if ($(window).width() <= 1023 ) {
    $('.waterActivitiesSlider').flexslider({
      animation: "slide",
      animationLoop: true,
      itemWidth: 130,
      maxItems: 3,
      minItems: 3, 
      directionNav: true,
      controlNav: false,
      slideshow: true,
      move: 1,
      useCSS: false
    });
} else if ($(window).width() <= 480){
    $('.waterActivitiesSlider').flexslider({
      animation: "slide",
      animationLoop: true,
      itemWidth: 130,
      maxItems: 2,
      minItems: 2, 
      directionNav: true,
      controlNav: false,
      slideshow: true,
      move: 1,
      useCSS: false
    });
} else if ($(window).width() >= 1024){
  $('.waterActivitiesSlider').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 130,
    maxItems: 4,
    minItems: 4, 
    directionNav: true,
    controlNav: false,
    slideshow: true,
    move: 1,
    useCSS: false
  });
}



//For New Arrival Slider
//===================================================


// For Instagram
// ==================================================

$('.instagramInnerSlider').flexslider({
    animation: "slide",
    controlNav: false,
    directionNav: true,
    useCSS: false,
    itemWidth: 0,
    minItems: 1,
    move: 1
});



// For Twitter
// ==================================================

$('.twitterInnerSlider').flexslider({
    animation: "slide",
    controlNav: false,
    directionNav: true,
    useCSS: false,
    itemWidth: 0,
    minItems: 1,
    move: 1
});


// For Intresting Facts
// ===================================================

$('.intrestingFactsSlider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: false,
    useCSS: false,
    itemWidth: 0,
    minItems: 1,
    move: 1
});


$('.success-stories').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: false,
    useCSS: false,
    itemMargin : 0,
    itemWidth: 40,
    minItems: 2,
    maxItems : 2,
    move: 1
});

// For Testimonials
// ===================================================

$('.testimonialSlider').flexslider({
    animation: "slide",
    controlNav: true,
    directionNav: false,
    useCSS: false,
    itemWidth: 0,
    slideshowSpeed: 4000,
    minItems: 1,
    move: 1
});


// Date Picker
// =================================================== 

$( ".datepicker" ).datepicker({
showOn: "both",
buttonImage: "http://sby.nexadesigns.com/ui/media/dist/icons/calender-icon.jpg",
buttonImageOnly: true
});

// Date Picker
// ===================================================


// Custom Styling For Select Box
// ===================================================
$("select").uniform({
  selectAutoWidth : false
});


// scrolltotop appearing when scroll down
// ====================================================

$(window).scroll(function(){
  var scrollposition = $(window).scrollTop();
  if (scrollposition >= 300) {
     $('.scrolltotop').removeClass('animated fadeOutDown');
     $('.scrolltotop').addClass('animated fadeInUp');
  } else {
    $('.scrolltotop').removeClass('animated fadeInUp');
    $('.scrolltotop').addClass('animated fadeOutDown');
  }
});

// animation hover effect to shortcut-links-home
// -----------------------------------------------
// $('.shortcut-links-home li a').hover(function() {
//     $(this).find('.bar').addClass('animated bounce');
// }, function() {
//     $(this).find('.bar').removeClass('animated bounce');
// });

// animation hover effect to roundedbox
// -----------------------------------------------
$('.roundedbox').hover(function() {
   
    
}, function() {
    $(this).find('.thumbnail-round').removeClass('animated pulse');
   
});


// giving opacity to the header on scroll 
// ------------------------------------
var divs = $('#Main-head');
$(window).on('scroll', function() {
    var st = $(this).scrollTop();
    divs.css({
        'opacity': (1 - st / 500)
    });
});

// animation hover effect to Home-activites-area
// -----------------------------------------------
// $('#Home-activites-area li a').hover(function() {
//     $(this).find('.title').addClass('animated bounce');
// }, function() {
//     $(this).find('.title').removeClass('animated bounce');
// });



// social slider Slider Starts here
// -----------------------------------------------------------------------------------------------
$(".aak_slider_list").each(function() {

    $(this).find("li:first").fadeIn().addClass("current");

})

$(".aak_right").click(function() {
    $this = $(this);
    $index = $this.closest(".aak_slider").find(".aak_slider_list > li.current").index();
    $index++;
    $size = $this.closest(".aak_slider").find(".aak_slider_list > li").size();

    $this.closest(".aak_slider").find(".aak_slider_list > li").addClass("asad")
        //alert($size +" ; "+ $index );
    if ($index != $size) {
        $this.closest(".aak_slider").find(".aak_slider_list >  li.current").removeClass("current").hide().next("li").addClass("current").fadeIn();
        $index = $this.closest(".aak_slider").find(".aak_slider_list > li.current").index();
        $index++;
        $size = $this.closest(".aak_slider").find(".aak_slider_list > li").size();
        //counter($index, $size);


        $start = $(this).closest(".aak_slider").find(".aak_slider_list > li.current").index();
        $start++; //alert($start);
        $ends = $(this).closest(".aak_slider").find(".aak_slider_list > li").size();

        $(this).closest(".aak_slider").find(".paging-start").html($start);
        $(this).closest(".aak_slider").find(".paging-ends").html($ends);
    }
})

$(".aak_left").click(function() {
    $this = $(this);
    $index = $this.closest(".aak_slider").find(".aak_slider_list > li.current").index();
    $index++;
    $size = $this.closest(".aak_slider").find(".aak_slider_list > li").size();
    //alert($size +" ; "+ $index );
    if ($index != 1) {
        $this.closest(".aak_slider").find(".aak_slider_list > li.current").removeClass("current").hide().prev("li").addClass("current").fadeIn();
        $index = $this.closest(".aak_slider").find(".aak_slider_list > li.current").index();
        $index++;
        $size = $this.closest(".aak_slider").find(".aak_slider_list > li").size();
        //counter($index, $size);

        $start = $(this).closest(".aak_slider").find(".aak_slider_list >  li.current").index();
        $start++; //alert($start);
        $ends = $(this).closest(".aak_slider").find(".aak_slider_list > li").size();

        $(this).closest(".aak_slider").find(".paging-start").html($start);
        $(this).closest(".aak_slider").find(".paging-ends").html($ends);

    }
})

//===========Slider ENDs Here======//
//==================================//

// social box Toggle

var socialWrapper = $('div.socialToggleBox'),
    boxWidth = $(".socialToggleBox").outerWidth();

if ($(window).width() > 480) {
    socialWrapper.animate({
        left: -boxWidth
    }, 1);
    socialWrapper.fadeIn();
}

//social button toggle
$('.socialClose').on('click', function() {
    var buttonimg = $(this);
    $(this).toggleClass('socialUp');


    if (buttonimg.hasClass('socialUp')) {
        socialWrapper.animate({
            left: 0
        });
    } else {
        socialWrapper.animate({
            left: -boxWidth
        });
    }
});



//socialSlider popup start
var windowHeightRV = $(window).height() / 2,
    windowWidthRV = $(window).width() / 2,
    socialSliderWidth = $('.socialSlider').outerWidth() / 2,
    socialSliderHeight = $('.socialSlider').outerHeight() / 2,
    leftPosition = windowWidthRV - socialSliderWidth;
topPosition = windowHeightRV - socialSliderHeight;

$(window).resize(function() {
    var windowHeightRV = $(window).height() / 2,
        windowWidthRV = $(window).width() / 2,
        socialSliderWidth = $('.socialSlider').outerWidth() / 2,
        socialSliderHeight = $('.socialSlider').outerHeight() / 2,
        leftPosition = windowWidthRV - socialSliderWidth;
    topPosition = windowHeightRV - socialSliderHeight;
    if ($(window).width() > 767) {
        $('.socialSlider').css('position', 'fixed').css('top', topPosition);
        $('.social-overlay').css('position', 'fixed').css('top', 0);
    };
});
//socialSlider center function    
if ($(window).width() > 767) {
    $('.socialSlider').css('position', 'fixed').css('top', topPosition);
    $('.social-overlay').css('position', 'fixed').css('top', 0);
};

//socialSlider open function
$('.socialbtn').on('click', function() {
    $('.socialBox').fadeIn('slow');
    $('.social-overlay').fadeIn('slow');
});

//socialSlider close function
$('.closeBtn').on('click', function() {
    $('.socialBox').fadeOut('slow');
     $('.social-overlay').fadeOut('slow');
});

//socialSlider popup end
//social button toggle ENDS here



// smoothscroll initialize 
// ----------------------------------------
smoothScroll.init();



// For Fancybox
//=====================================================


$('.fancybox').fancybox();

$('.videoFancyIframe').fancybox({

      'autoScale'            : false,
      'transitionIn'          : 'none',
      'transitionOut'        : 'none',
      'type'                 : 'iframe',
      'width'                : 850,
      'height'               : 600,
      'scrolling'            : 'no'

});

$('.mapFancyIframe').fancybox({

      'autoScale'            : false,
      'transitionIn'          : 'none',
      'transitionOut'        : 'none',
      'type'                 : 'iframe',
      'width'                : 850,
      'height'               : 570,
      'scrolling'            : 'no'

});

$('.mapFancyIframe2').fancybox({

      'autoScale'            : true,
      'transitionIn'          : 'none',
      'transitionOut'        : 'none',
      'type'                 : 'iframe',
      'width'                : '100%',
      'height'               : '100%',
      'scrolling'            : 'yes'

});
$('.fancyMeiframe3').fancybox({

      'autoScale'            : false,
      'transitionIn'          : 'none',
      'transitionOut'        : 'none',
      'type'                 : 'iframe',
      'width'                : '600',
      'height'               : '600',
      'scrolling'            : 'yes'

});

$('.bookNowIframe').fancybox({

      'autoScale'            : false,
      'transitionIn'          : 'none',
      'transitionOut'        : 'none',
      'type'                 : 'iframe',
      'width'                : 850,
      'height'               : 310,
      'scrolling'            : 'no'

});

$('.enquireiframe').fancybox({

      'autoScale'            : false,
      'transitionIn'          : 'none',
      'transitionOut'        : 'none',
      'type'                 : 'iframe',
      'width'                : 540,
      'height'               : 440,
      'scrolling'            : 'no'

});

$('.photopopup').fancybox({

      'autoScale'            : false,
      'transitionIn'          : 'none',
      'transitionOut'        : 'none',
      'type'                 : 'iframe',
      'width'                : 850,
      'height'               : 600,
      'scrolling'            : 'no'

});

$('.newsletterpopup').fancybox({

      'autoScale'            : false,
      'transitionIn'          : 'none',
      'transitionOut'        : 'none',
      'type'                 : 'iframe',
      'width'                : 850,
      'height'               : 570,
      'scrolling'            : 'no'

});


$(".withthumbnail").fancybox({
  
    prevEffect  : 'none',
    nextEffect  : 'none',
    helpers : {
      title : {
        type: 'inside'
      },
      thumbs  : {
        width : 50,
        height  : 50
      }
    }
  });
// For Fancybox

//=====================================================


// For Media Center Tabbing
// ===================================================


$(".tabContentContainer > div").hide();
$(".tabContentContainer div:first-child").show('0');

$("ul.whatsNewTabNavigation li").click(function(){

    var $this = $(this)

    if($(this).attr("class") != "active")

    {

      var $index = $this.index(); $index++;
      $this.addClass("active").siblings().removeClass("active");

      $(".tabContentContainer > div").hide();
      $(".tabContentContainer > div.content-"+$index).show();

    } 


});

// For Media Center Tabbing
// ===================================================



// tabs for plan your trip 
// ===================================================
$('.tab-link li a').click(function(){
    var gettingid = $(this).attr('id');
    $('body').find('.tabcontent').fadeOut('500').removeClass('.active');
    $('body').find('.'+gettingid + '.tabcontent').fadeIn('1000');
})




// For Newsletter Tabbing
// ===================================================


$(".newsLetterTabContentContainer > div").hide();
$(".newsLetterTabContentContainer div:first-child").show('0');

$("ul.newsTabnavigation li").click(function(){

    var $this = $(this)

    if($(this).attr("class") != "active")

    {

      var $index = $this.index(); $index++;
      $this.addClass("active").siblings().removeClass("active");

      $(".newsLetterTabContentContainer > div").hide();
      $(".newsLetterTabContentContainer > div.content-"+$index).show();

    } 


});

// For Newsletter Tabbing
// ===================================================



$('.animalsound-btn').click(function(){
  console.log('click');
  $(this).siblings().closest('#audio')[0].play();
  $(this).parents().siblings('li').each(function(){
    $(this).find('#audio')[0].pause();
  })
});





$('.quicklinks .photogallery').hover(function(){
  $('.shortcut-box').find('.video-box').hide();
  $('.shortcut-box').find('.map-box').hide();
  $('.shortcut-box').find('.photo-box').show();
  $(this).siblings().removeClass('active');
  $(this).addClass('active');
});

$('.quicklinks .islandmap').hover(function(){
  $('.shortcut-box').find('.video-box').hide();
  $('.shortcut-box').find('.map-box').show();
  $('.shortcut-box').find('.photo-box').hide();
   $(this).siblings().removeClass('active');
  $(this).addClass('active');
});


$('.quicklinks .videogallery').hover(function(){
  $('.shortcut-box').find('.video-box').show();
  $('.shortcut-box').find('.map-box').hide();
  $('.shortcut-box').find('.photo-box').hide();
   $(this).siblings().removeClass('active');
  $(this).addClass('active');
});

$('.detailPage ul li').each(function(){
  if ($(this).find('a h3').text().length <= 12) {
   
    $(this).find('a').addClass('oneline');
  }else {
  
  }
});

$('.meetanimalsbtn ul li').each(function(){
  if ($(this).find('a').text().length <= 17) {
   
    $(this).addClass('single');
  }else {
  
  }
});


  var texter = $('.homepageboxer .box-content p').text();
  var hometexter = $('.homepageboxer .box-content p');
  if ($('.homepageboxer .box-content p').text().length >= 60) {
    hometexter.text(hometexter.text().substring(0,62))
  }else {
  
  }



