﻿<%@ Application Language="VB" %>
<%@ Import Namespace="System.Globalization" %>
<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
        RegisterRoute(Routing.RouteTable.Routes)
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
    
    Sub RegisterRoute(ByVal routes As Routing.RouteCollection)
       
        routes.MapPageRoute("Default-Home", "", "~/Default.aspx")
        'routes.MapPageRoute("Default-About-Fun-Dining", "About-Fun-Dining", "~/About-Fun-Dining.aspx")
        'routes.MapPageRoute("Default-Video-Pop", "Video-Pop/{id}/{t}", "~/VideoPop.aspx")
        'routes.MapPageRoute("Default-Restaurant-Bar", "Restaurant-Bar/{id}/{t}", "~/Restaurant-Bar.aspx")
        'routes.MapPageRoute("Default-Restaurant-Bar-1", "Restaurant-Bar/{id}/{t}/{tab}", "~/Restaurant-Bar.aspx")
        'routes.MapPageRoute("Default-Video-Pop-Restaurant", "Video-Pop-Restaurant/{id}/{t}", "~/VideoPopRestaurant.aspx")
        'routes.MapPageRoute("Default-Booking", "Booking/{id}", "~/booking.aspx")
        'routes.MapPageRoute("Default-Monthly-Promotion", "Monthly-Promotions", "~/Monthly-Promotion.aspx")
        'routes.MapPageRoute("Default-Monthly-Promotion-Listing", "Monthly-Promotion-Listing/{id}/{t}", "~/Monthly-Promotion-Listing.aspx")
        'routes.MapPageRoute("Default-Monthly-Promotion-Listing-1", "Monthly-Promotion-Listing/{id}/{t}/{Page}", "~/Monthly-Promotion-Listing.aspx")
        
        'routes.MapPageRoute("Default-Monthly-Promotion-Details", "Monthly-Promotion-Details/{id}/{t}", "~/Monthly-Promotion-Details.aspx")
        'routes.MapPageRoute("Default-Gallery", "Gallery", "~/Gallery.aspx")
        
        'routes.MapPageRoute("Default-NewYear-Promotion-Listing", "NewYear-Promotion-Listing/{Page}", "~/NewYear-Promotion-Listing.aspx")
        'routes.MapPageRoute("Default-NewYear-Promotion-Listing-1", "NewYear-Promotion-Listing", "~/NewYear-Promotion-Listing.aspx")
        'routes.MapPageRoute("Default-NewYear-Promotion-Details", "NewYear-Promotion-Details/{id}/{t}", "~/NewYear-Promotion-Details.aspx")
              
        'routes.MapPageRoute("Default-Carlos-Kitchen", "Carlos-Kitchen/{Tab}/{Page}", "~/carlos-kitchen.aspx")
        'routes.MapPageRoute("Default-Carlos-Kitchen-2", "Carlos-Kitchen/{Tab}", "~/carlos-kitchen.aspx")
        'routes.MapPageRoute("Default-Carlos-Kitchen-1", "Carlos-Kitchen", "~/carlos-kitchen.aspx")
        'routes.MapPageRoute("Default-Carlos-Kitchen-Details", "Carlos-Kitchen-Details/{id}/{t}", "~/Carlos-Kitchen-Details.aspx")
        
        ''routes.MapPageRoute("Default-Tips-and-Tricks", "Tips-and-Tricks/{Page}", "~/Tips-and-Tricks.aspx")
        ''routes.MapPageRoute("Default-Tips-and-Tricks-1", "Tips-and-Tricks", "~/Tips-and-Tricks.aspx")
        'routes.MapPageRoute("Default-Tips-and-Tricks-Details", "Tips-and-Tricks-Details/{id}/{t}", "~/Tips-and-Tricks-Details.aspx")
        
        ''routes.MapPageRoute("Default-Cooking-Classes", "Cooking-Classes/{Page}", "~/Cooking-Classes.aspx")
        ''routes.MapPageRoute("Default-Cooking-Classes-1", "Cooking-Classes", "~/Cooking-Classes.aspx")
        'routes.MapPageRoute("Default-Cooking-Classes-Details", "Cooking-Classes-Details/{id}/{t}", "~/Cooking-Classes-Details.aspx")
        
        'routes.MapPageRoute("Default-Meet-Celebrate1", "Meet-Celebrate/{tab}", "~/meet-celebrate.aspx")
        'routes.MapPageRoute("Default-Meet-Celebrate", "Meet-Celebrate/{tab}/{page}", "~/meet-celebrate.aspx")
        'routes.MapPageRoute("Default-Meeting-Details", "Meeting-Details/{lid}/{t}", "~/meeting-details.aspx")
        'routes.MapPageRoute("Default-Contact-Us", "Contact-Us", "~/contact-us.aspx")
        'routes.MapPageRoute("Default-Fun-Rewards1", "Fun-Rewards", "~/fun-rewards.aspx")
        'routes.MapPageRoute("Default-Fun-Rewards", "Fun-Rewards/{id}/{t}", "~/fun-rewards-details.aspx")
        
        'routes.MapPageRoute("Default-VideoPopDeshboard", "VideoPopDeshboard/{id}/{t}", "~/VideoPopDeshboard.aspx")
        
    End Sub
       
    Protected Sub Application_BeginRequest(sender As [Object], e As EventArgs)
      
        Dim domainName As String
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
        Dim newCulture As CultureInfo = DirectCast(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
        newCulture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy"
        newCulture.DateTimeFormat.DateSeparator = "/"
        Threading.Thread.CurrentThread.CurrentCulture = newCulture
         
        
    End Sub
    
    
    
</script>