<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageStay.master" AutoEventWireup="false" CodeFile="Stay-Details.aspx.vb" Inherits="Stay_Details" %>

<%@ Register Src="~/CustomControl/UCHTMLTitle.ascx" TagPrefix="uc1" TagName="UCHTMLTitle" %>
<%@ Register Src="~/CustomControl/UCHTMLSmallImage.ascx" TagPrefix="uc1" TagName="UCHTMLSmallImage" %>
<%@ Register Src="~/CustomControl/UCHTMLDetails.ascx" TagPrefix="uc1" TagName="UCHTMLDetails" %>
<%@ Register Src="~/CustomControl/UCInterestingFactList.ascx" TagPrefix="uc1" TagName="UCInterestingFactList" %>
<%@ Register Src="~/CustomControl/UCPhotoAlbum.ascx" TagPrefix="uc1" TagName="UCPhotoAlbum" %>
<%@ Register Src="~/CustomControl/UCVideoAlbum.ascx" TagPrefix="uc1" TagName="UCVideoAlbum" %>
<%@ Register Src="~/CustomControl/UCTestimonial.ascx" TagPrefix="uc1" TagName="UCTestimonial" %>
<%@ Register Src="~/CustomControl/UCInterestingFactList2.ascx" TagPrefix="uc1" TagName="UCInterestingFactList2" %>
<%@ Register Src="~/CustomControl/UserStayDynamicPhotoAlbum.ascx" TagPrefix="uc1" TagName="UserStayDynamicPhotoAlbum" %>
<%@ Register Src="~/CustomControl/UserActivity.ascx" TagPrefix="uc1" TagName="UserActivity" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <asp:HiddenField ID="hdnCategoryID" runat="server" />
    <asp:HiddenField ID="hdnTitle" runat="server" />
    <asp:HiddenField ID="hdnMasterID" runat="server" />
    <asp:HiddenField ID="hdnBookingCode" runat="server" />

    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%=domainName%>'>Home</a></li>
            <li><a href='<%=domainName & "Stay"%>'>Stay</a></li>
            <li class="active">
                <asp:Literal ID="ltBredCrumb" runat="server"></asp:Literal>

            </li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <asp:Literal ID="ltIntro" runat="server"></asp:Literal>
            </section>
            <uc1:DynamicSEO runat="server" ID="DynamicSEO"  />
            <!-- -- welcome-text ends here -- -->
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->



    <!-- -- section-footer starts here -->
    <%---------------------------------------------- -->--%>
    <footer id="section-footer">
        <div class="container">

            <div class="row">
                <div class="col-sm-4">
                    <div class="bluebg border-box bigheight">
                       
                       
                         <asp:Literal ID="lblPhotoGallery" runat="server"></asp:Literal>
                        <asp:Image ID="ImgPGImage" style="max-height:292px" runat="server" />
                     
                   <a href='<%= galurl %>' class="linkbtn withthumbnail" rel="Stay">Photo Gallery<span class="arrow"></span></a>
                         <%= Utility.showEditButton(Request, domainName & "Admin/A-Stay/StayEdit.aspx?lid=" + Page.RouteData.Values("id") + "&Small=1&VideoLink=0&VideoCode=0&Map=0&Big=0&Link=0&Booking=0&trip=0&pg=1&vg=0")%>
                    <% If pgalid <> 0 Then%>
                    <%= Utility.showAddButton(Request, domainName & "Admin/A-Gallery/AllGalleryItemEdit.aspx?galleryId=" & pgalid & "&Lang=en")%>
                    <% End If%>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="reservationbox purplebg bookNowContainer">
                        <h4 class="title">Make A Reservation</h4>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Check-In</label>
                                    <%--<input type="text" class="form-control datepicker">--%>
                                    <asp:TextBox ID="txtCheckIn" CssClass="form-control datepicker" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator cssClass="eerror" ID="RequiredFieldValidator1" ControlToValidate="txtCheckIn" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>

                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group ">
                                    <label for="exampleInputEmail1">Nights</label>
                                    <asp:TextBox ID="txtNights" CssClass="form-control" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator cssClass="eerror" ID="RequiredFieldValidator2" ControlToValidate="txtNights" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>

                                </div>
                            </div>

                            <div class="col-sm-8">
                                <div class="form-group ">
                                    <label for="exampleInputEmail1">Promo</label>
                                    <asp:TextBox ID="txtPromo" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    
                                  <span id="bookingerror" style="font-size: 11px;float: left;color: orange;width: 40%;margin: 15px 0 0;line-height: 13px; display:none;">Please select the date and nights</span>
                                  <%--  <asp:Button ID="btnCheck" CssClass="pull-right submitbtn" placeholder="Check availability"  OnClick="btnCheck_Click" runat="server" Text="Check availability" />--%>
                                    <a class="pull-right submitbtn" href="javascript:;"  id="checkSubmit" target="_blank">Check availability</a>
                                </div>
                            </div>
                        </div>
                        <p style="margin-top: 5px;font-size:10px;">Disclaimer : By clicking "Check Availability" you will be directed to the Anantara Resorts Website</p>

                    </div>
                </div>


                <asp:Panel ID="pnlVideoActivity" runat="server">

                    <div class="col-sm-4">
                        <div class="greenbg border-box bigheight">
                           <%-- <asp:Literal ID="ltVideoLink" runat="server"></asp:Literal>--%>
                            <a href='<%= vgalurl %>' class="playbtn videoFancyIframe">
                          <div class="img-container">
                             <span class="playbutton"></span>
                              <asp:Image ID="ImgVideo" runat="server" class="boxthumbnail"/>
                              </div></a>
                          <a href='<%= vgalurl %>' class="linkbtn videoFancyIframe">
                           Video
                            <span class="arrow"></span>
                            </a>
                             <%= Utility.showEditButton(Request, domainName & "Admin/A-Stay/StayEdit.aspx?lid=" + Page.RouteData.Values("id") + "&Small=1&VideoLink=0&VideoCode=0&Map=0&Big=0&Link=0&Booking=0&trip=0&pg=0&vg=1")%>
                        </div>
                    </div>

                </asp:Panel>

                <asp:Panel ID="pnlActivity" runat="server">
                    <div class="col-sm-4">

                       <%= Activity() %>

                    </div>

                </asp:Panel>




            </div>

            <div class="row">
                <div class="col-sm-6">
                    <!-- -- double-box starts here -- -->


                    <uc1:UCInterestingFactList2 runat="server" ID="UCInterestingFactList2" />



                    <!-- -- double-box ends here -- -->
                </div>

                <div class="col-sm-3">
                    <div class="yellowbg border-box medium">
                        <img src='<%=domainName &"ui/media/dist/thumbnails/flight-thumb.jpg"%>' alt="" class="boxthumbnail">

                        <a href='<%=domainName & "Travel-Air"%>' class="linkbtn">Book a Flight <span class="arrow"></span></a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <asp:Literal ID="ltTripAdvisor" runat="server"></asp:Literal>


                </div>
            </div>





            <div class="footer-nav">
                <div class="row">
                    <asp:Literal ID="ltFooterNavigation" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->
    
    <script type="text/javascript">
        $(document).ready(function () {
            inputDate = $('#ctl00_ContentPlaceHolder1_txtCheckIn').val();
            inputurl = $('#ctl00_ContentPlaceHolder1_hdnBookingCode').val();
            inputNights = $('#ctl00_ContentPlaceHolder1_txtNights').val();
            inputPromo = $('#ctl00_ContentPlaceHolder1_txtPromo').val();
            checkDate = inputDate;
            checkNights = inputNights;
            checkPromo = inputPromo;

            //alert(checkDate + ' ' + checkNights + ' ' + checkPromo);

            $('#ctl00_ContentPlaceHolder1_txtCheckIn').change(function () {
                checkDate = $(this).val();
                $(this).val(checkDate);
               // alert(checkDate);
            });
            $('#ctl00_ContentPlaceHolder1_txtNights').change(function () {
                checkNights = $(this).val();
                $(this).val(checkNights);
              //  alert(checkNights);
            });
            $('#ctl00_ContentPlaceHolder1_txtPromo').change(function () {
                checkPromo = $(this).val();
                $(this).val(checkPromo);
              //  alert(checkPromo);
            });

            $('#checkSubmit').click(function () {
                if ((checkDate) && (checkNights)) {
                    $('#bookingerror').hide();
                    var newURL = inputurl + "&checkin=" + checkDate + "&nights=" + checkNights + "&promocode=" + checkPromo + "";
                    $(this).attr("href", newURL);
                }
                else {
                    $('#bookingerror').show();
                }
            });
        });

    </script>
</asp:Content>

