﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Flights.aspx.vb" Inherits="TravelAir" %>

<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>





<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li class="active">Flights</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">Flights</h1>
                <%--<p>
                    
                </p>--%>
                
                <%--<div class="row">
                    <div class="col-sm-6">
                        <h2 class="subtitlepage">
                            <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="65" />
                        </h2>
                        <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt2" HTMLID="65" />
                    </div>
                    <div class="col-sm-6">
                        <h2 class="subtitlepage">
                            
                            <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle1" HTMLID="66" />
                        </h2>
                        <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt3" HTMLID="66" />
                    </div>
                </div>--%>

                <div class="row">
                    <div class="col-sm-8">
                        <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt"  HTMLID="61"/>
                        <%--<uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt1" HTMLID="64" />--%>
                    </div>
                   
                    <div class="col-sm-4">
                        <div class="imagethumb">
                            <a href='<%= domainName & "Link" %>'>
                                <img src='<%= domainName & "ui/media/dist/travel/moreinforotana.jpg" %>' alt="">
                            </a>
                        </div>
                        <div class="imagethumb">
                            
                            <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1"  HTMLID="64"  ShowEdit="true" ImageType="Big" />
                        </div>

                    </div>
                </div>
                 <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="255" />
            </section>
            <!-- -- welcome-text ends here -- -->
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <!-- -- section-footer starts here
    -------------------------------------------- -->
       <nav id="multiple-btns">
        <div class="container">
             <ul class="list-unstyled">
                <li class="purplebg">
                    <a href='<%= domainName & "PhotoGallery"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/photo-gallery.png"%>' height="25" width="29" alt=""></span>
                        Photo gallery
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "VideoGallery"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/video-gallery.png"%>' height="20" width="25" alt=""></span>
                        Video gallery
                    </a>
                </li>
                <li class="bluebg">
                    <a class="bookNowIframe" href='<%= domainName & "Booknow-Pop"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/book-now.png"%>' height="27" width="27" alt=""></span>
                        Book now
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "Map-Pop"%>' class="mapFancyIframe">
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/location-map.png"%>' height="26" width="32" alt=""></span>
                        Location map
                    </a>
                </li>
                <li class="purplebg">
                    <a href='<%= domainName & "Flights"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/flights.png"%>' height="14" width="38" alt=""></span>
                        Flights
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "History"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/history.png"%>' height="20" width="27" alt=""></span>
                        History
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "WhatsNew"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/whats-new.png"%>' height="17" width="18" alt=""></span>
                        What's New
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- -- section-footer ends here
    -------------------------------------------- -->
</asp:Content>

