﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="MeetAnimal.aspx.vb" Inherits="MeetAnimal" %>

<%@ Register Src="~/CustomControl/UCInterestingFactList.ascx" TagPrefix="uc1" TagName="UCInterestingFactList" %>
<%@ Register Src="~/CustomControl/UCHTMLDetails.ascx" TagPrefix="uc1" TagName="UCHTMLDetails" %>
<%@ Register Src="~/CustomControl/UCTestimonial.ascx" TagPrefix="uc1" TagName="UCTestimonial" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserVetConservationTeam.ascx" TagPrefix="uc1" TagName="UserVetConservationTeam" %>
<%@ Register Src="~/CustomControl/UserDynamicPhotoAlbum.ascx" TagPrefix="uc1" TagName="UserDynamicPhotoAlbum" %>

<%@ Register Src="~/CustomControl/UserStaticPhotoAlbum.ascx" TagPrefix="uc1" TagName="UserStaticPhotoAlbum" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>





<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:HiddenField ID="hdnCategoryID" runat="server" />
    <asp:HiddenField ID="hdnTitle" runat="server" />
        <asp:HiddenField ID="hdnMasterID" runat="server" />


    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%=domainName%>'>Home</a></li>
            <li><a href='<%=domainName & "WildLife"%>'>Wildlife</a></li>
            <li><a href='<%=domainName & "Animal"%>'>Animals</a></li>
            <li class="active">
                <asp:Literal ID="ltBreadcrumb" runat="server"></asp:Literal>

            </li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->


    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <asp:Literal ID="ltIntro" runat="server"></asp:Literal>
            </section>
            <uc1:DynamicSEO runat="server" ID="DynamicSEO"  PageType="AnimalCategory" />
            <!-- -- welcome-text ends here -- -->
        </div>

        <!-- -- title-bar starts here -- -->
        <div class="title-bar orangebg">
            <div class="container">
                <h2 class="title">
                   <!--  <asp:Literal ID="ltCategoryName" runat="server"></asp:Literal> -->
                </h2>
                <span class="arrow"></span>
            </div>
        </div>
        <!-- -- title-bar ends here -- -->

        <!-- -- page-breadcrumb starts here -- -->
        <div class="container">
            <div class="page-breadcrumb">
                <ul class="list-unstyled">
                    <asp:Literal ID="ltAnimalName" runat="server"></asp:Literal>
                </ul>
            </div>
        </div>
        <!-- -- page-breadcrumb ends here -- -->

        <!-- -- animals-listings starts here -- -->
        <div class="container">
            <div class="animals-listings">
                <asp:Literal ID="ltAddAnimal" runat="server"></asp:Literal>
                <ul class="list-unstyled">
                    <asp:Literal ID="ltAnimalList" runat="server"></asp:Literal>
                </ul>
            </div>
        </div>
        <!-- -- animals-listings starts here -- -->
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-5">
                    <!-- Intresting Facts Slider -->
                      
                   
                            <uc1:UCInterestingFactList runat="server" ID="UCInterestingFactList" />

                      
                    <!-- Intresting Facts Slider -->
                </div>
                <div class="col-sm-4 col-md-3">
                    
                       <%-- <img src='<%=domainName & "ui/media/dist/thumbnails/vets.jpg"%>' alt="" class="transparent"/>
                      
                        <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="105" />
                         <a href='<%= domainName & "Vet-Conservation-Team"%>' class="readmorebtn">Read more</a>--%>
                        <uc1:UserVetConservationTeam runat="server" ID="UserVetConservationTeam" HTMLID="105" />

                  
                </div>

                <div class="col-sm-4 col-md-2  col-xs-6">
                    <div class="greenbg border-box small">
                        
                        <asp:Literal ID="ltVideoAlbum" runat="server"></asp:Literal>

                      <%--  <a href="video.html" class="linkbtn">Video Gallery<span class="arrow"></span></a>--%>
                        <asp:Literal ID="ltVideoLink" runat="server"></asp:Literal>

                    </div>
                </div>


                <div class="col-sm-4 col-md-2 col-xs-6">
                    <div class="bluebg border-box small">
                        
                       <asp:Literal ID="lblPhotoGallery" runat="server"></asp:Literal>
                        <asp:Image ID="ImgPGImage" Width="165" Height="225" runat="server" />
                     
                   <a href='<%= galurl %>' class="linkbtn withthumbnail" rel="Spa">Photo Gallery<span class="arrow"></span></a>
                         <%= Utility.showEditButton(Request, domainName & "Admin/A-WildLifeAnimal/List1Edit.aspx?l1id=" & Page.RouteData.Values("id") & "&Lang=en&Small=0&Big=0&Link=0&pg=1&vg=0")%>
                        <% If pgalid <> 0 then %>
                         <%= Utility.showAddButton(Request, domainName & "Admin/A-Gallery/AllGalleryItemEdit.aspx?galleryId=" & pgalid & "&Lang=en")%>
                        <% End if %>
                    </div>
                </div>


            </div>

            <div class="row">
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox blue inner">
                             <asp:Literal ID="ltIslandMap" runat="server"></asp:Literal>
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-6">
                    <!-- Testimonial Slider -->
                    <asp:Literal ID="ltAddTestimonial" runat="server"></asp:Literal>
                    <div class="testimonialSlider">
                        
                        <ul class="list-unstyled slides">

                            <uc1:UCTestimonial runat="server" ID="UCTestimonial" />
                            
                        </ul>
                    </div>
                    <!-- Testimonial Slider -->
                </div>
            </div>

        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns" class="meetanimalsbtn">
        <div class="container">
            <ul class="list-unstyled">
                <asp:Literal ID="ltFooterNavigation" runat="server"></asp:Literal> 
            </ul>
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
</asp:Content>

