﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="ContactUs.aspx.vb" Inherits="ContactUs" %>


<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/MyCaptcha.ascx" TagPrefix="uc1" TagName="MyCaptcha" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>





<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>

            <li class="active">Contact Us</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">Contact Us</h1>

                <p>
                    <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText" HTMLID="46" />
                </p>
                <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="259" />

                <!-- Contact Form Section -->
                <div class="contactFormSection">

                    <div class="row">

                        <!-- Left Form Section -->
                        <div class="col-md-7 col-sm-7">

                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">
                                        Subject
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="form2" ControlToValidate="txtContactfor" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtContactfor" runat="server" class="form-control"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server"
                                            Enabled="True" TargetControlID="txtContactfor" WatermarkText="Subject">
                                        </cc1:TextBoxWatermarkExtender>

                                    </div>
                                </div>
                                <!-- Form Row Section -->
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">
                                        Full Name
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="form2" ControlToValidate="txtName" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtName" runat="server" class="form-control"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender ID="txtFName_TextBoxWatermarkExtender" runat="server"
                                            Enabled="True" TargetControlID="txtName" WatermarkText="Full Name">
                                        </cc1:TextBoxWatermarkExtender>

                                    </div>
                                </div>
                                <!-- Form Row Section -->

                                <!-- Form Row Section -->
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">
                                        Email
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="form2" ControlToValidate="txtEmail" ErrorMessage="*"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="form2" ControlToValidate="txtEmail" ErrorMessage="Invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                    </label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                            Enabled="True" TargetControlID="txtEmail" WatermarkText="Email">
                                        </cc1:TextBoxWatermarkExtender>
                                    </div>
                                </div>
                                <!-- Form Row Section -->

                                <!-- Form Row Section -->
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">
                                        Phone
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="form2" ControlToValidate="txtPhone" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtPhone" runat="server" class="form-control"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                                            Enabled="True" TargetControlID="txtPhone" WatermarkText="Phone">
                                        </cc1:TextBoxWatermarkExtender>
                                    </div>
                                </div>
                                <!-- Form Row Section -->

                                <!-- Form Row Section -->
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">
                                        Country
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="form2" ControlToValidate="ddlCountry" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </label>
                                    <div class="col-sm-10">
                                        <asp:DropDownList ID="ddlCountry" runat="server" AppendDataBoundItems="true" class="form-control">
                                            <asp:ListItem Text="Select Country" Value=""></asp:ListItem>
                                            <asp:ListItem Value="Afghanistan">Afghanistan</asp:ListItem>
                                            <asp:ListItem Value="Albania">Albania</asp:ListItem>
                                            <asp:ListItem Value="Algeria">Algeria</asp:ListItem>
                                            <asp:ListItem Value="American Samoa">American Samoa</asp:ListItem>
                                            <asp:ListItem Value="Andorra">Andorra</asp:ListItem>
                                            <asp:ListItem Value="Angola">Angola</asp:ListItem>
                                            <asp:ListItem Value="Anguilla">Anguilla</asp:ListItem>
                                            <asp:ListItem Value="Antartica">Antarctica</asp:ListItem>
                                            <asp:ListItem Value="Antigua Barbuda">Antigua Barbuda</asp:ListItem>
                                            <asp:ListItem Value="Argentina">Argentina</asp:ListItem>
                                            <asp:ListItem Value="Armenia">Armenia</asp:ListItem>
                                            <asp:ListItem Value="Aruba">Aruba</asp:ListItem>
                                            <asp:ListItem Value="Ascension Island">Ascension Island</asp:ListItem>
                                            <asp:ListItem Value="Australia">Australia</asp:ListItem>
                                            <asp:ListItem Value="Austria">Austria</asp:ListItem>
                                            <asp:ListItem Value="Azerbaijan">Azerbaijan</asp:ListItem>
                                            <asp:ListItem Value="Bahamas">Bahamas</asp:ListItem>
                                            <asp:ListItem Value="Bahrain">Bahrain</asp:ListItem>
                                            <asp:ListItem Value="Bangladesh">Bangladesh</asp:ListItem>
                                            <asp:ListItem Value="Barbados">Barbados</asp:ListItem>
                                            <asp:ListItem Value="Belarus">Belarus</asp:ListItem>
                                            <asp:ListItem Value="Belguim">Belgium</asp:ListItem>
                                            <asp:ListItem Value="Belize">Belize</asp:ListItem>
                                            <asp:ListItem Value="Benin">Benin</asp:ListItem>
                                            <asp:ListItem Value="Bermuda">Bermuda</asp:ListItem>
                                            <asp:ListItem Value="Bhutan">Bhutan</asp:ListItem>
                                            <asp:ListItem Value="Bolivia">Bolivia</asp:ListItem>
                                            <asp:ListItem Value="Bosnia Herzegovina">Bosnia Herzegovina</asp:ListItem>
                                            <asp:ListItem Value="Botswana">Botswana</asp:ListItem>
                                            <asp:ListItem Value="Bouvet Island">Bouvet Island</asp:ListItem>
                                            <asp:ListItem Value="Brazil">Brazil</asp:ListItem>
                                            <asp:ListItem Value="British Indian Ocean Territory">British Indian Ocean 
                    Territory</asp:ListItem>
                                            <asp:ListItem Value="Brunei Darussalam">Brunei Darussalam</asp:ListItem>
                                            <asp:ListItem Value="Bulgaria">Bulgaria</asp:ListItem>
                                            <asp:ListItem Value="Burkina Faso">Burkina Faso</asp:ListItem>
                                            <asp:ListItem Value="Burundi">Burundi</asp:ListItem>
                                            <asp:ListItem Value="Cambodia">Cambodia</asp:ListItem>
                                            <asp:ListItem Value="Cameroon">Cameroon</asp:ListItem>
                                            <asp:ListItem Value="Canada">Canada</asp:ListItem>
                                            <asp:ListItem Value="Cap Verde">Cap Verde</asp:ListItem>
                                            <asp:ListItem Value="Cayman Islands">Cayman Islands</asp:ListItem>
                                            <asp:ListItem Value="Central African Republic">Central African Republic</asp:ListItem>
                                            <asp:ListItem Value="Chad">Chad</asp:ListItem>
                                            <asp:ListItem Value="Chile">Chile</asp:ListItem>
                                            <asp:ListItem Value="China">China</asp:ListItem>
                                            <asp:ListItem Value="Christmas Island">Christmas Island</asp:ListItem>
                                            <asp:ListItem Value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</asp:ListItem>
                                            <asp:ListItem Value="Colombia">Colombia</asp:ListItem>
                                            <asp:ListItem Value="Comoros">Comoros</asp:ListItem>
                                            <asp:ListItem Value="Congo, Democratic People's Republic">Congo, Democratic 
                    People's Republic</asp:ListItem>
                                            <asp:ListItem Value="Congo, Republic of">Congo, Republic of</asp:ListItem>
                                            <asp:ListItem Value="Cook Islands">Cook Islands</asp:ListItem>
                                            <asp:ListItem Value="Costa Rica">Costa Rica</asp:ListItem>
                                            <asp:ListItem Value="Cote d'Ivoire">Cote d'Ivoire</asp:ListItem>
                                            <asp:ListItem Value="Crotia/Hrvatska">Croatia/Hrvatska</asp:ListItem>
                                            <asp:ListItem Value="Cuba">Cuba</asp:ListItem>
                                            <asp:ListItem Value="Cyprus">Cyprus</asp:ListItem>
                                            <asp:ListItem Value="Czek Republic">Czech Republic</asp:ListItem>
                                            <asp:ListItem Value="Denmark">Denmark</asp:ListItem>
                                            <asp:ListItem Value="Djibouti">Djibouti</asp:ListItem>
                                            <asp:ListItem Value="Dominica">Dominica</asp:ListItem>
                                            <asp:ListItem Value="Dominican Republic">Dominican Republic</asp:ListItem>
                                            <asp:ListItem Value="East Timor">East Timor</asp:ListItem>
                                            <asp:ListItem Value="Ecuador">Ecuador</asp:ListItem>
                                            <asp:ListItem Value="Egypt">Egypt</asp:ListItem>
                                            <asp:ListItem Value="El Salvador">El Salvador</asp:ListItem>
                                            <asp:ListItem Value="Equatorial Guinea">Equatorial Guinea</asp:ListItem>
                                            <asp:ListItem Value="Eritrea">Eritrea</asp:ListItem>
                                            <asp:ListItem Value="Estonia">Estonia</asp:ListItem>
                                            <asp:ListItem Value="Ethopia">Ethiopia</asp:ListItem>
                                            <asp:ListItem Value="Falkland Island (Malvina)">Falkland Island (Malvina)</asp:ListItem>
                                            <asp:ListItem Value="Faroe Islands">Faroe Islands</asp:ListItem>
                                            <asp:ListItem Value="Fiji">Fiji</asp:ListItem>
                                            <asp:ListItem Value="Finland">Finland</asp:ListItem>
                                            <asp:ListItem Value="France">France</asp:ListItem>
                                            <asp:ListItem Value="French Guiana">French Guiana</asp:ListItem>
                                            <asp:ListItem Value="French Polynesia">French Polynesia</asp:ListItem>
                                            <asp:ListItem Value="French Southern Territories">French Southern Territories</asp:ListItem>
                                            <asp:ListItem Value="Gabon">Gabon</asp:ListItem>
                                            <asp:ListItem Value="Gambia">Gambia</asp:ListItem>
                                            <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
                                            <asp:ListItem Value="Germany">Germany</asp:ListItem>
                                            <asp:ListItem Value="Ghana">Ghana</asp:ListItem>
                                            <asp:ListItem Value="Gibraltar">Gibraltar</asp:ListItem>
                                            <asp:ListItem Value="Greece">Greece</asp:ListItem>
                                            <asp:ListItem Value="Greenland">Greenland</asp:ListItem>
                                            <asp:ListItem Value="Grenada">Grenada</asp:ListItem>
                                            <asp:ListItem Value="Guadeloupe">Guadeloupe</asp:ListItem>
                                            <asp:ListItem Value="Guam">Guam</asp:ListItem>
                                            <asp:ListItem Value="Guatemala">Guatemala</asp:ListItem>
                                            <asp:ListItem Value="Guernsey">Guernsey</asp:ListItem>
                                            <asp:ListItem Value="Guinea">Guinea</asp:ListItem>
                                            <asp:ListItem Value="Guinea-Bissau">Guinea-Bissau</asp:ListItem>
                                            <asp:ListItem Value="Guyana">Guyana</asp:ListItem>
                                            <asp:ListItem Value="Haiti">Haiti</asp:ListItem>
                                            <asp:ListItem Value="Heard McDonald Islands">Heard McDonald Islands</asp:ListItem>
                                            <asp:ListItem Value="Holy See (City Vatican State)">Holy See (City Vatican 
                    State)</asp:ListItem>
                                            <asp:ListItem Value="Honduras">Honduras</asp:ListItem>
                                            <asp:ListItem Value="Hong Kong">Hong Kong</asp:ListItem>
                                            <asp:ListItem Value="Hungary">Hungary</asp:ListItem>
                                            <asp:ListItem Value="Iceland">Iceland</asp:ListItem>
                                            <asp:ListItem Value="India">India</asp:ListItem>
                                            <asp:ListItem Value="Indonesia">Indonesia</asp:ListItem>
                                            <asp:ListItem Value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</asp:ListItem>
                                            <asp:ListItem Value="Iraq">Iraq</asp:ListItem>
                                            <asp:ListItem Value="Ireland">Ireland</asp:ListItem>
                                            <asp:ListItem Value="Isle of Man">Isle of Man</asp:ListItem>
                                            <%--<asp:ListItem value="Israel">Israel</asp:ListItem>--%>
                                            <asp:ListItem Value="Italy">Italy</asp:ListItem>
                                            <asp:ListItem Value="Jamaica">Jamaica</asp:ListItem>
                                            <asp:ListItem Value="Japan">Japan</asp:ListItem>
                                            <asp:ListItem Value="Jersey">Jersey</asp:ListItem>
                                            <asp:ListItem Value="Jordan">Jordan</asp:ListItem>
                                            <asp:ListItem Value="Kazakhstan">Kazakhstan</asp:ListItem>
                                            <asp:ListItem Value="Kenya">Kenya</asp:ListItem>
                                            <asp:ListItem Value="Kiribati">Kiribati</asp:ListItem>
                                            <asp:ListItem Value="Korea, Democratic People's Republic">Korea, Democratic 
                    People's Republic</asp:ListItem>
                                            <asp:ListItem Value="Korea, Republic of">Korea, Republic of</asp:ListItem>
                                            <asp:ListItem Value="Kuwait">Kuwait</asp:ListItem>
                                            <asp:ListItem Value="Kyrgyzstan">Kyrgyzstan</asp:ListItem>
                                            <asp:ListItem Value="Lao, People's Democratic Republic">Lao, People's Democratic 
                    Republic</asp:ListItem>
                                            <asp:ListItem Value="Latvia">Latvia</asp:ListItem>
                                            <asp:ListItem Value="Lebanon">Lebanon</asp:ListItem>
                                            <asp:ListItem Value="Lesotho">Lesotho</asp:ListItem>
                                            <asp:ListItem Value="Liberia">Liberia</asp:ListItem>
                                            <asp:ListItem Value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</asp:ListItem>
                                            <asp:ListItem Value="Liechtenstein">Liechtenstein</asp:ListItem>
                                            <asp:ListItem Value="Lithuania">Lithuania</asp:ListItem>
                                            <asp:ListItem Value="Luzembourg">Luxembourg</asp:ListItem>
                                            <asp:ListItem Value="Macau">Macau</asp:ListItem>
                                            <asp:ListItem Value="Macedonia, Former Yugoslav Republic">Macedonia, Former 
                    Yugoslav Republic</asp:ListItem>
                                            <asp:ListItem Value="Madagascar">Madagascar</asp:ListItem>
                                            <asp:ListItem Value="Malawi">Malawi</asp:ListItem>
                                            <asp:ListItem Value="Malaysia">Malaysia</asp:ListItem>
                                            <asp:ListItem Value="Maldives">Maldives</asp:ListItem>
                                            <asp:ListItem Value="Mali">Mali</asp:ListItem>
                                            <asp:ListItem Value="Malta">Malta</asp:ListItem>
                                            <asp:ListItem Value="Marshall Islands">Marshall Islands</asp:ListItem>
                                            <asp:ListItem Value="Marinique">Martinique</asp:ListItem>
                                            <asp:ListItem Value="Mauritania">Mauritania</asp:ListItem>
                                            <asp:ListItem Value="Mauritius">Mauritius</asp:ListItem>
                                            <asp:ListItem Value="Mayotte">Mayotte</asp:ListItem>
                                            <asp:ListItem Value="Mexico">Mexico</asp:ListItem>
                                            <asp:ListItem Value="Micronesia, Federal State of">Micronesia, Federal State of</asp:ListItem>
                                            <asp:ListItem Value="Moldova, Republic of">Moldova, Republic of</asp:ListItem>
                                            <asp:ListItem Value="Monaco">Monaco</asp:ListItem>
                                            <asp:ListItem Value="Mongolia">Mongolia</asp:ListItem>
                                            <asp:ListItem Value="Montserrat">Montserrat</asp:ListItem>
                                            <asp:ListItem Value="Morocco">Morocco</asp:ListItem>
                                            <asp:ListItem Value="Mozambique">Mozambique</asp:ListItem>
                                            <asp:ListItem Value="Myanmar">Myanmar</asp:ListItem>
                                            <asp:ListItem Value="Namibia">Namibia</asp:ListItem>
                                            <asp:ListItem Value="Nauru">Nauru</asp:ListItem>
                                            <asp:ListItem Value="Nepal">Nepal</asp:ListItem>
                                            <asp:ListItem Value="Netherlands">Netherlands</asp:ListItem>
                                            <asp:ListItem Value="Netherlands Antilles">Netherlands Antilles</asp:ListItem>
                                            <asp:ListItem Value="New Zealand">New Zealand</asp:ListItem>
                                            <asp:ListItem Value="Nicaragua">Nicaragua</asp:ListItem>
                                            <asp:ListItem Value="Niger">Niger</asp:ListItem>
                                            <asp:ListItem Value="Nigeria">Nigeria</asp:ListItem>
                                            <asp:ListItem Value="Niue">Niue</asp:ListItem>
                                            <asp:ListItem Value="Norfolk Island">Norfolk Island</asp:ListItem>
                                            <asp:ListItem Value="Northern Mariana Islands">Northern Mariana Islands</asp:ListItem>
                                            <asp:ListItem Value="Norway">Norway</asp:ListItem>
                                            <asp:ListItem Value="Oman">Oman</asp:ListItem>
                                            <asp:ListItem Value="Pakistan">Pakistan</asp:ListItem>
                                            <asp:ListItem Value="Palau">Palau</asp:ListItem>
                                            <asp:ListItem Value="Panama">Panama</asp:ListItem>
                                            <asp:ListItem Value="Papua New Guinea">Papua New Guinea</asp:ListItem>
                                            <asp:ListItem Value="Paraguay">Paraguay</asp:ListItem>
                                            <asp:ListItem Value="Palestine">Palestine</asp:ListItem>
                                            <asp:ListItem Value="Peru">Peru</asp:ListItem>
                                            <asp:ListItem Value="Philippines">Philippines</asp:ListItem>
                                            <asp:ListItem Value="Pitcairn Island">Pitcairn Island</asp:ListItem>
                                            <asp:ListItem Value="Poland">Poland</asp:ListItem>
                                            <asp:ListItem Value="Portugal">Portugal</asp:ListItem>
                                            <asp:ListItem Value="Puerto Rico">Puerto Rico</asp:ListItem>
                                            <asp:ListItem Value="Qatar">Qatar</asp:ListItem>
                                            <asp:ListItem Value="Reunion Island">Reunion Island</asp:ListItem>
                                            <asp:ListItem Value="Romania">Romania</asp:ListItem>
                                            <asp:ListItem Value="Russian Federation">Russian Federation</asp:ListItem>
                                            <asp:ListItem Value="Rwanda">Rwanda</asp:ListItem>
                                            <asp:ListItem Value="Saint Kitts Nevis">Saint Kitts Nevis</asp:ListItem>
                                            <asp:ListItem Value="Saint Lucia">Saint Lucia</asp:ListItem>
                                            <asp:ListItem Value="Saint Vincent &amp; the Grenadines">Saint Vincent &amp; the 
                    Grenadines</asp:ListItem>
                                            <asp:ListItem Value="San Marino">San Marino</asp:ListItem>
                                            <asp:ListItem Value="Sao Tome Principe">Sao Tome Principe</asp:ListItem>
                                            <asp:ListItem Value="Saudi Arabia">Saudi Arabia</asp:ListItem>
                                            <asp:ListItem Value="Senegal">Senegal</asp:ListItem>
                                            <asp:ListItem Value="Seychelles">Seychelles</asp:ListItem>
                                            <asp:ListItem Value="Sierra Leone">Sierra Leone</asp:ListItem>
                                            <asp:ListItem Value="Singapore">Singapore</asp:ListItem>
                                            <asp:ListItem Value="Slovak Republic">Slovak Republic</asp:ListItem>
                                            <asp:ListItem Value="Slivenia">Slovenia</asp:ListItem>
                                            <asp:ListItem Value="Solomon Islands">Solomon Islands</asp:ListItem>
                                            <asp:ListItem Value="Somalia">Somalia</asp:ListItem>
                                            <asp:ListItem Value="South Africa">South Africa</asp:ListItem>
                                            <asp:ListItem Value="South Georgia &amp; the South Sandwich Islands">South 
                    Georgia &amp; the South Sandwich Islands</asp:ListItem>
                                            <asp:ListItem Value="Spain">Spain</asp:ListItem>
                                            <asp:ListItem Value="Sri Lanka">Sri Lanka</asp:ListItem>
                                            <asp:ListItem Value="St. Helena">St. Helena</asp:ListItem>
                                            <asp:ListItem Value="St. Pierre Miquelon">St. Pierre Miquelon</asp:ListItem>
                                            <asp:ListItem Value="Sudan">Sudan</asp:ListItem>
                                            <asp:ListItem Value="Suriname">Suriname</asp:ListItem>
                                            <asp:ListItem Value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen 
                    Islands</asp:ListItem>
                                            <asp:ListItem Value="Swaziland">Swaziland</asp:ListItem>
                                            <asp:ListItem Value="Sweden">Sweden</asp:ListItem>
                                            <asp:ListItem Value="Switzerland">Switzerland</asp:ListItem>
                                            <asp:ListItem Value="Syrian Arab Republic">Syrian Arab Republic</asp:ListItem>
                                            <asp:ListItem Value="Taiwan">Taiwan</asp:ListItem>
                                            <asp:ListItem Value="Tajikistan">Tajikistan</asp:ListItem>
                                            <asp:ListItem Value="Tanzania">Tanzania</asp:ListItem>
                                            <asp:ListItem Value="Thailand">Thailand</asp:ListItem>
                                            <asp:ListItem Value="Togo">Togo</asp:ListItem>
                                            <asp:ListItem Value="Tokelau">Tokelau</asp:ListItem>
                                            <asp:ListItem Value="Tonga">Tonga</asp:ListItem>
                                            <asp:ListItem Value="Trinidad Tobago">Trinidad Tobago</asp:ListItem>
                                            <asp:ListItem Value="Tunisia">Tunisia</asp:ListItem>
                                            <asp:ListItem Value="Turkey">Turkey</asp:ListItem>
                                            <asp:ListItem Value="Turkmenistan">Turkmenistan</asp:ListItem>
                                            <asp:ListItem Value="Turks Ciacos Islands">Turks Ciacos Islands</asp:ListItem>
                                            <asp:ListItem Value="Tuvalu">Tuvalu</asp:ListItem>
                                            <asp:ListItem Value="Uganda">Uganda</asp:ListItem>
                                            <asp:ListItem Value="Ukraine">Ukraine</asp:ListItem>
                                            <asp:ListItem Value="UAE">UAE</asp:ListItem>
                                            <asp:ListItem Value="UK">UK</asp:ListItem>
                                            <asp:ListItem Value="USA">USA</asp:ListItem>
                                            <asp:ListItem Value="Uruguay">Uruguay</asp:ListItem>
                                            <asp:ListItem Value="US Minor Outlying Islands">US Minor Outlying Islands</asp:ListItem>
                                            <asp:ListItem Value="Uzbekistan">Uzbekistan</asp:ListItem>
                                            <asp:ListItem Value="Vanuatu">Vanuatu</asp:ListItem>
                                            <asp:ListItem Value="Venezuela">Venezuela</asp:ListItem>
                                            <asp:ListItem Value="Vietnam">Vietnam</asp:ListItem>
                                            <asp:ListItem Value="Virgin Islands (British)">Virgin Islands (British)</asp:ListItem>
                                            <asp:ListItem Value="Virgin Islands (USA)">Virgin Islands (USA)</asp:ListItem>
                                            <asp:ListItem Value="Virgin Islands (USA)">Wallis Futuna Islands</asp:ListItem>
                                            <asp:ListItem Value="Western Sahara">Western Sahara</asp:ListItem>
                                            <asp:ListItem Value="Western Samoa">Western Samoa</asp:ListItem>
                                            <asp:ListItem Value="Yemen">Yemen</asp:ListItem>
                                            <asp:ListItem Value="Yugoslavia">Yugoslavia</asp:ListItem>
                                            <asp:ListItem Value="Zaire">Zaire</asp:ListItem>
                                            <asp:ListItem Value="Zambia">Zambia</asp:ListItem>
                                            <asp:ListItem Value="Zimbabwe">Zimbabwe</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>
                                <!-- Form Row Section -->

                                <!-- Form Row Section -->
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Message</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtmsg" TextMode="MultiLine" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <!-- Form Row Section -->
                                <div class="form-group">
                                    <label></label>
                                    <uc1:MyCaptcha runat="server" ID="MyCaptcha" vgroup="form2" />
                                    <asp:Label ID="lbCaptchaError" ForeColor="Red" runat="server"></asp:Label>
                                </div>
                                <asp:SqlDataSource ID="sdsContact" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [Contact] WHERE [ContactID] = @ContactID" InsertCommand="INSERT INTO [Contact] ([FullName],  [Email], [Phone], [ContactFor], [Subject], [Message], [ContactDate], [Country]) VALUES (@FullName, @Email, @Phone, @ContactFor, @Subject, @Message, @ContactDate, @Country)" SelectCommand="SELECT * FROM [Contact]" UpdateCommand="UPDATE [Contact] SET [FullName] = @FullName, [Designation] = @Designation, [Email] = @Email, [Phone] = @Phone, [Mobile] = @Mobile, [Address] = @Address, [ContactFor] = @ContactFor, [Subject] = @Subject, [Message] = @Message, [ContactDate] = @ContactDate, [Country] = @Country WHERE [ContactID] = @ContactID">
                                    <DeleteParameters>
                                        <asp:Parameter Name="ContactID" Type="Int32" />
                                    </DeleteParameters>
                                    <InsertParameters>
                                        <asp:ControlParameter ControlID="txtName" Name="FullName" PropertyName="Text" Type="String" />
                                        <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" Type="String" />
                                        <asp:ControlParameter ControlID="txtPhone" Name="Phone" PropertyName="Text" Type="String" />
                                        <asp:ControlParameter ControlID="txtContactfor" Name="ContactFor" PropertyName="Text" Type="String" />
                                        <asp:Parameter Name="Subject" Type="String" DefaultValue="ContactUs" />
                                        <asp:ControlParameter ControlID="txtmsg" Name="Message" PropertyName="Text" Type="String" />
                                        <asp:ControlParameter ControlID="hdnDate" Name="ContactDate" PropertyName="Value" Type="DateTime" />
                                        <asp:ControlParameter ControlID="ddlCountry" Name="Country" PropertyName="SelectedValue" Type="String" />
                                    </InsertParameters>

                                </asp:SqlDataSource>
                                <asp:Button ID="btnSend" runat="server" ValidationGroup="form2" class="sendButton" Text="Send" />
                                <asp:HiddenField ID="hdnDate" runat="server" />

                            </div>

                        </div>
                        <!-- Left Form Section -->

                        <!-- Right Address Section -->
                        <div class="col-md-5 col-sm-5">

                            <div class="addressSection">
                                <h2>Head office</h2>

                                <%= HeadOffice() %>
                            </div>
                            <a href='<%= domainName & "Map-Pop" %>' class="bookingmourning bluefont mapFancyIframe">View in Google Map</a>
                        </div>
                        <!-- Right Address Section -->

                    </div>

                </div>
                <!-- Contact Form Section -->

            </section>
            <!-- -- welcome-text ends here -- -->
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->


    <!-- Other Contact Info Box -->
    <section class="otherContactInfoBox">

        <section class="container">

            <h2>Other Contact Information</h2>

          <%= OtherAddress() %>
        </section>

    </section>
    <!-- Other Contact Info Box -->


    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">

        <div class="container">

            <div class="row">
                <div class="col-sm-6 col-sm-6">
                    <!-- -- double-box starts here -- -->
                    <div class="double-box greenbg">
                        <!-- -- twopanel starts here -- -->
                        <%= HtmlContent(121)%>
                        
                        <!-- -- twopanel imageholder ends here -- -->
                    </div>
                    <uc1:UserControlEditButton runat="server" ID="EditPhiloshophy" HTMLID="121" TitleEdit="true" ImageEdit="true" ImageType="Small" ImageHeight="233" ImageWidth="478" TextEdit="true"  TextType="Small"/>
                    <!-- -- double-box ends here -- -->
                </div>

                <div class="col-md-6 col-sm-6">
                   <%= HtmlContentMap(75)%>
                    <uc1:UserControlEditButton runat="server" ID="EditMap" HTMLID="75" TitleEdit="true" ImageEdit="true" ImageType="Big" ImageHeight="233" ImageWidth="555" TextEdit="true"  TextType="Small"/>
                </div>
                
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox blue inner">
                        <div class="thumbnail-round">
                            <img src='<%= domainName &"ui/media/dist/round/blue.png"%>' height="113" width="122" alt="" class="roundedcontainer">
                            <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="108" ShowEdit="false" ImageType="Small" ImageClass="normal-thumb" />
                            
                        </div>
                        <div class="box-content">
                            <h5 class="title-box"><a href='<%= domainName & "Admin/Content/The-Island-Map-fileupload22112014124423.pdf"%>' target="_blank" class="bluefont">Island Map</a></h5>
                            <p>
                                <uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText1" ShowEdit="False" HTMLID="108"  />
                            </p>
                            <a href='<%= domainName & "Admin/Content/The-Island-Map-fileupload22112014124423.pdf"%>' target="_blank" class=" readmorebtn">View Map</a>
                            <uc1:UserControlEditButton runat="server" ID="editIslandMap" HTMLID="108"  ImageEdit="true" ImageType="Small" ImageHeight="119" ImageWidth="139" TextEdit="true"  TextType="Small"/>
                        </div>
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
                <div class="col-sm-6">
                    <!-- -- roundedbox starts here -- -->
                    <div class="roundedbox orange inner">
                        <div class="thumbnail-round">
                            <img src='<%= domainName & "ui/media/dist/round/orange.png"%>' height="113" width="122" alt="" class="roundedcontainer">
                            <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" HTMLID="79" ShowEdit="false" ImageType="Small" ImageHeight="132" ImageWidth="152" ImageClass="normal-thumb" />
                           
                        </div>
                        <div class="box-content">
                            <h5 class="title-box"><a href='<%= domainName & "Historical-Timeline" %>' class="">Historical Timeline</a></h5>
                            <p><uc1:UserHTMLSmallText runat="server" ID="UserHTMLSmallText2" ShowEdit="False" ImageEdit="True" HTMLID="79"  /></p>
                            <a href='<%= domainName & "Historical-Timeline" %>' class="readmorebtn">View All</a>
                             <uc1:UserControlEditButton runat="server" ID="editTimeline" HTMLID="79"  ImageEdit="true" ImageType="Small" ImageHeight="119" ImageWidth="139" TextEdit="true"  TextType="Small"/>
                        </div>
                    </div>
                    <!-- -- roundedbox ends here -- -->
                </div>
            </div>

        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->
</asp:Content>

