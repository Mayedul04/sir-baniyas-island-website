﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="VideoPop.aspx.vb" Inherits="VideoPop" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sir Bani Yas</title>

    <!-- Bootstrap -->
    <link href="ui/style/css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ui/media/std/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ui/media/std/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ui/media/std/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="ui/media/std/ico/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="ui/media/std/ico/apple-touch-icon-57-precomposed.png">
</head>
<body>
    <form id="form1" runat="server">
        <div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=376113352542968&version=v2.0";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
        <div class="popUpContainer">

            <div class="titleSection">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <h2>
                            <asp:Literal ID="lblTitle" runat="server"></asp:Literal>
                        </h2>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <ul class="innerSocialIcons">
                            
                            <li>
                                <div class="g-plusone" data-size="medium" data-href="<%= shareurl %>"></div>
                            </li>
                            <li>
                                
                                <a class="twitter-share-button"
                                    href="<%= shareurl %>">Tweet
                                </a>
                                <script type="text/javascript">
                                    window.twttr = (function (d, s, id) { var t, js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) { return } js = d.createElement(s); js.id = id; js.src = "https://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); return window.twttr || (t = { _e: [], ready: function (f) { t._e.push(f) } }) }(document, "script", "twitter-wjs"));
                                </script>
                            </li>
                            <li>
                                <div class="fb-share-button" data-href="<%= shareurl %>" data-layout="button"></div>
                               
                                
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="videoHold">
                <%= embedcode %>
                <%-- <iframe style="margin-bottom: 20px" width="100%" height="450" src='<%= embedcode %>' frameborder="0" allowfullscreen></iframe>--%>
            </div>


        </div>
        <!-- Pop Up Container -->

        <!-- Place this tag in your head or just before your close body tag. -->
        <script src="https://apis.google.com/js/platform.js" async defer></script>

        <!-- Place this tag where you want the +1 button to render. -->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="ui/js/dist/bootstrap.min.js"></script>
        <script src="ui/js/dist/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="ui/js/dist/uniform.min.js"></script>
        <script src="ui/js/dist/jquery.fancybox.js?v=2.1.5"></script>
        <script src="ui/js/dist/jquery.flexslider.js"></script>
        <script src="ui/js/dist/smooth-scroll.min.js"></script>
        <script src="ui/js/dist/custom.js"></script>
    </form>
</body>
</html>
