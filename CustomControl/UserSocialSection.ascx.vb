﻿Imports System.Data.SqlClient
Partial Class CustomControl_UserSocialSection
    Inherits System.Web.UI.UserControl
    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub


    Public Function GetInstagram() As String
        Dim M As String = String.Empty

        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM InstagramGalleryItem where Status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()



        Try
            M += "<li class=""col-21"">"
            M += "<ul class=""instagramImages"">"
            While reader.Read()
                i = i + 1
                M += "<li>"
                M += "<a class=""fancyMeiframe3"" href=""" & domainName & "instagrampopup/" & reader("GalleryItemID") & """ rel=""instagallery"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString & """ alt="""">"
                M += "</a>"
                M += "</li>"

                If i Mod 12 = 0 Then
                    M += "</ul>"
                    M += "</li>"
                    M += "<li class=""col-21"">"
                    M += "<ul class=""instagramImages"">"
                End If

            End While
            M += "</ul>"
            M += "</li>"
            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function


    Public Function getAllBlog() As String

        Dim M As String = ""
        Dim i As Integer = 0
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT Top 2 * FROM List_NexaBlog WHERE Status = '1' and BlogGroup='0' and Featured='1' Order by BlogDate desc"
        Dim cmd = New SqlCommand(sql, con)
        'cmd.Parameters.Clear()
        'cmd.Parameters.AddWithValue("Blogid", Page.RouteData.Values("id"))

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                While reader.Read()
                    i = i + 1

                    'M += "<li>"
                    'M += "<div class=""title""><a href=""" & domainName & "Blogs-Details/" & reader("Blogid") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>" & Mid(reader("Title"), 1, 40) & "...</a></div>"
                    'M += "<p>" & Mid(reader("SmallDetails"), 1, 80) & "..</p>"
                    'M += " <h6>" & Date.Parse(reader("BlogDate")).ToString("MMMM dd, yyyy") & "</h6>"



                    'M += "<a href=""" & domainName & "Blogs-Details/" & reader("Blogid") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>Read More</a>"

                    'M += Utility.showEditButton(Request, domainName & "Admin/A-Blog/BlogArticleEdit.aspx?aid=" + reader("Blogid").ToString())
                    'M += "</li>"


                    M += " <li>"
                    M += "<p>" & Mid(reader("Title"), 1, 40) & "...  - <span><a href=""" & domainName & "Blogs-Details/" & reader("Blogid") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """>" & Date.Parse(reader("BlogDate")).ToString("MMMM dd, yyyy") & "</a></span></p>"

                    M += Utility.showEditButton(Request, domainName & "Admin/A-Blog/BlogArticleEdit.aspx?aid=" + reader("Blogid").ToString())

                    M += " </li>"




                End While


            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        ltInstagramImage.Text = GetInstagram()
        ltBlogs.Text = getAllBlog()
    End Sub
End Class
