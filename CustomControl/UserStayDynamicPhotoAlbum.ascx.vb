﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class CustomControl_UserListControl
    Inherits System.Web.UI.UserControl
    Public domainName As String
    Public galleryID As String
    Public listID As String
    Public masterID As String
    Public tableName As String


    Public Property Gallery_ID() As String
        Get
            Return galleryID
        End Get

        Set(ByVal value As String)
            galleryID = value
        End Set
    End Property

    Public Property Master_ID() As String
        Get
            Return masterID
        End Get

        Set(ByVal value As String)
            masterID = value
        End Set
    End Property

    Public Property Table_Name() As String
        Get
            Return tableName
        End Get

        Set(ByVal value As String)
            tableName = value
        End Set
    End Property

    Public Property List_ID() As String
        Get
            Return listID
        End Get

        Set(ByVal value As String)
            listID = value
        End Set
    End Property


    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Public Sub LoadGallery()
        Dim sConn As String
        Dim selectString1 As String = "Select * from CommonGallery where  TableName=@TableName and TableMasterID=@TableMasterID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar)
        cmd.Parameters("TableName").Value = "Stay"
        cmd.Parameters.Add("TableMasterID", Data.SqlDbType.NVarChar)
        cmd.Parameters("TableMasterID").Value = Master_ID


        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()

                lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Spa"">Photo Gallery<span class=""arrow""></span></a>"

            End While
        Else
            lblPhotoGallery.Text += "<a href=""javascript:;"" class=""linkbtn"" rel=""Spa"">Photo Gallery<span class=""arrow""></span></a>"
        End If
        cn.Close()
    End Sub

    Public Function PhotoGalleryImage() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM List_Restaurant where Status='1' and RestaurantID=@RestaurantID order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("RestaurantID", Data.SqlDbType.Int)
        cmd.Parameters("RestaurantID").Value = List_ID

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read

                M += "<img src=""" & domainName & "Admin/" & reader("LogoSmall") & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"
                'M += Utility.showEditButton(Request, domainName & "Admin/A-WildLifeAnimal/AllCommonGallery.aspx?cgid="&  &"&TableName=Animal&TableMasterID=" & reader("MasterID") & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&lang=en&BigImageWidth=841&BigImageHeight=254")

            End While

        End If
        conn.Close()
        Return M
    End Function


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        LoadGallery()
        ltPhotoImage.Text = PhotoGalleryImage()
        ltAdd.Text = Utility.showAddButton(Request, domainName & "Admin/A-CommonGallery/AllCommonGalleryEdit.aspx?TableName=Stay&TableMasterID=" & Master_ID & "&t=&lang=en&BigImageWidth=841&BigImageHeight=254")
    End Sub



End Class
