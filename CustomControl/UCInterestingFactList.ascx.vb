﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class CustomControl_UserListControl
    Inherits System.Web.UI.UserControl
    Public domainName As String
    Public ParentSlidingClass As String
    Public ChildSlidingClass As String
    Public IMGClass As String
    Public PClass As String
    Public H2Class As String
    Public LinkClass As String
    Public LinkName As String
    Public IMGTrueFalse As String
    Public SlideLeftClass As String
    Public SlideRightClass As String




    Public TableName As String
    Public TableMasterID As String



    Public Property Table_Name() As String
        Get
            Return TableName
        End Get

        Set(ByVal value As String)
            TableName = value
        End Set
    End Property

    Public Property Table_MasterID() As String
        Get
            Return TableMasterID
        End Get

        Set(ByVal value As String)
            TableMasterID = value
        End Set
    End Property


    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Public Function Content() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT   List_InterestingFacts.* FROM  List_InterestingFacts  where List_InterestingFacts.Status='1' and [TableName] not in ('Treatments','Stay') and TableMasterID not in ('44','73') and Lang='en' order by List_InterestingFacts.Sortindex"
        ''Dim selectString = "SELECT * FROM List_Restaurant where Status='1' and Category='Restaurant' and RestaurantID <> 0 order by Sortindex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.AddWithValue("TableName", TableName)
        'cmd.Parameters.AddWithValue("TableMasterID", TableMasterID)

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        M += "<div class=""intrestingFactsSlider"">"
        M += "<ul class=""slides unstyled"">"
        If reader.HasRows Then
           
            While reader.Read
                M += "<li>"
                M += "<div class=""double-box greenbg"">"
                M += "<div class=""twopanel"">"
                M += "<h3 class=""title""><a href=""" & domainName & "Interesting-Facts"">Interesting Facts</a></h3>"
                'M += "<h3 class=""subtitle"">" & reader("Title") & "</h3>"
                M += reader("BigDetails")
                M += Utility.showEditButton(Request, domainName & "Admin/A-InterestingFacts/CommonFactsEdit.aspx?cgid=" & reader("ID"))
                M += "<span class=""arrow""></span>"
                M += "<br/><h5 class=""subtitle""><a href=""" & domainName & "Interesting-Facts"" class=""readmorebtn"" >Read More</a></h5>"
                M += "</div>"
                M += "<div class=""twopanel imageholder"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ width=""307px"" alt=""" & reader("ImageAltText") & """>"
                M += " </img>"
                M += "</div>"

                M += "</li>"
            End While
           
        End If
        M += "</ul>"
        M += " </div>"
        conn.Close()
        Return M
    End Function



    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ltAddInteresting.Text = Utility.showAddButton(Request, domainName & "Admin/A-InterestingFacts/CommonFactsEdit.aspx?smallImageWidth=478&smallImageHeight=233&TableName=HTML&TableMasterID=3&t=")
    End Sub
End Class
