﻿Imports System.Data.SqlClient
Partial Class CustomControl_UserBanner
    Inherits System.Web.UI.UserControl
    Public domainName As String

    Public html_ID As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Public Property HTMLID() As Integer
        Get
            Return html_ID
        End Get

        Set(ByVal value As Integer)
            html_ID = value
        End Set
    End Property
    Function Activity() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", html_ID) '108

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()
                M += "<div class=""brownbg single-box "">"
                M += "<img src=""" & domainName & "Admin/" & reader("BigImage") & """ alt="""" class=""transparent"" />"
                M += " <h3 class=""title"">"
                M += "<a href=""" & domainName & "Adventure"" > "
                M += "Activity"
                M += "</a>"
                M += "</h3>"

                M += "<p>" & reader("SmallDetails").ToString & "</p>"
                M += "<a href=""" & domainName & "Adventure"" class=""readmorebtn"">Read more</a>"

                M += "<p class=""pull-right"">" & Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SecondImage=0&File=0&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&Link=0&SmallImageWidth=151&SmallImageHeight=129&BigImageWidth=140&BigImageHeight=120") & "</p>"
                M += "</div>"

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ltAdd.Text = Utility.showAddButton(Request, domainName & "Admin/A-AdvantureActivity/ListEdit.aspx")

    End Sub
End Class
