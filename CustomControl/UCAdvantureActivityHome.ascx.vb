﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class CustomControl_UserListControl
    Inherits System.Web.UI.UserControl
    Public domainName As String
    Public ParentSlidingClass As String
    Public ChildSlidingClass As String
    Public IMGClass As String
    Public PClass As String
    Public H2Class As String
    Public LinkClass As String
    Public LinkName As String
    Public IMGTrueFalse As String
    Public SlideLeftClass As String
    Public SlideRightClass As String


    Public TableName As String
    Public OrderByField As String
    Public VisualEditLink As String
    Public DetailsPageLink As String
    Public IDFieldName As String

    Public Property parentsliding_class() As String
        Get
            Return ParentSlidingClass
        End Get

        Set(ByVal value As String)
            ParentSlidingClass = value
        End Set
    End Property

    Public Property childsliding_class() As String
        Get
            Return ChildSlidingClass
        End Get

        Set(ByVal value As String)
            ChildSlidingClass = value
        End Set
    End Property


    Public Property img_class() As String
        Get
            Return IMGClass
        End Get

        Set(ByVal value As String)
            IMGClass = value
        End Set
    End Property

    Public Property h2_class() As String
        Get
            Return H2Class
        End Get

        Set(ByVal value As String)
            H2Class = value
        End Set
    End Property

    Public Property slideleft_class() As String
        Get
            Return SlideLeftClass
        End Get

        Set(ByVal value As String)
            SlideLeftClass = value
        End Set
    End Property


    Public Property slideright_class() As String
        Get
            Return SlideRightClass
        End Get

        Set(ByVal value As String)
            SlideRightClass = value
        End Set
    End Property



    Public Property p_class() As String
        Get
            Return PClass
        End Get

        Set(ByVal value As String)
            PClass = value
        End Set
    End Property

    Public Property link_class() As String
        Get
            Return LinkClass
        End Get

        Set(ByVal value As String)
            LinkClass = value
        End Set
    End Property

    Public Property link_name() As String
        Get
            Return LinkName
        End Get

        Set(ByVal value As String)
            LinkName = value
        End Set
    End Property

    Public Property img_truefalse() As String
        Get
            Return IMGTrueFalse
        End Get

        Set(ByVal value As String)
            IMGTrueFalse = value
        End Set
    End Property

    Public Property table_name() As String
        Get
            Return TableName
        End Get

        Set(ByVal value As String)
            TableName = value
        End Set
    End Property





    Public Property orderby_field() As String
        Get
            Return OrderByField
        End Get

        Set(ByVal value As String)
            OrderByField = value
        End Set
    End Property

    Public Property visual_edit() As String
        Get
            Return VisualEditLink
        End Get

        Set(ByVal value As String)
            VisualEditLink = value
        End Set
    End Property

    Public Property detailspage_Link() As String
        Get
            Return DetailsPageLink
        End Get

        Set(ByVal value As String)
            DetailsPageLink = value
        End Set
    End Property

    Public Property idfield_name() As String
        Get
            Return IDFieldName
        End Get

        Set(ByVal value As String)
            IDFieldName = value
        End Set
    End Property



    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Public Function Content() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  List_Advanture_Activity.* FROM  List_Advanture_Activity  where List_Advanture_Activity.Status='1' and List_Advanture_Activity.Category<>'Day Trip' and Lang=@Lang order by List_Advanture_Activity.Sortindex"
        ''    Dim selectString = "SELECT * FROM List_Restaurant where Status='1' and Category='Bar' and RestaurantID <> 0 order by Sortindex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.AddWithValue("Lang", p_class)
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then

            M += "<ul class=""list-unstyled"">"

            While reader.Read


                M += "<li class=""col-md-3 col-sm-6 col-xs-6"">"
                M += "<div class=""activity"">"
                M += "<a href=""" & domainName & "Adventure-Activity/" & reader("ListID") & "/" & Utility.EncodeTitle(reader("Title").ToString, "-") & """>"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText").ToString & """>"
                M += "<h5 class=""title " & reader("Link") & """>" & reader("Title") & "<span class=""arrow""></span></h5>"
                M += "</a>"
                M += "</div>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-AdvantureActivity/ListEdit.aspx?lid=" & reader("ListID"))

                M += "</li>"


            End While

            M += "</ul> "
            reader.Read()

        End If
        conn.Close()
        Return M
    End Function



End Class
