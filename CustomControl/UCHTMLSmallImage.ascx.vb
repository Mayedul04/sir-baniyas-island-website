﻿Imports System.Data.SqlClient
Partial Class CustomControl_UserHTMLTxt
    Inherits System.Web.UI.UserControl
    Public html_ID As Integer
    Public Image_Width As Integer
    Public Image_Height As Integer


    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Property HTMLID() As Integer
        Get
            Return html_ID
        End Get

        Set(ByVal value As Integer)
            html_ID = value
        End Set
    End Property

    Public Property ImageWidth() As Integer
        Get
            Return Image_Width
        End Get

        Set(ByVal value As Integer)
            Image_Width = value
        End Set
    End Property

    Public Property ImageHeight() As Integer
        Get
            Return Image_Height
        End Get

        Set(ByVal value As Integer)
            Image_Height = value
        End Set
    End Property

    Function HTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", HTMLID)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ height=""" & ImageHeight & """ width=""" & ImageWidth & """ class=""pull-right"" style=""margin-left:20px;margin-bottom:20px;"" alt=""" & reader("ImageAltText") & """>"

                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0&SmallImageWidth=308&SmallImageHeight=288&BigImageWidth=&BigImageHeight=")
            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


End Class
