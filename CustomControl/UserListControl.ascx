﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UserListControl.ascx.vb" Inherits="CustomControl_UserListControl" %>
<asp:ListView ID="ListView1" runat="server" DataKeyNames="Blogid" DataSourceID="SqlDataSource1">
     <EmptyDataTemplate>
        No data was returned.
    </EmptyDataTemplate>
    <ItemTemplate>

                      <li>
                          <article>                           
                            <div class="post">
                              <h2><a href='<%# domainName & "Product-Details/" & Eval("Blogid") & "/" & Utility.EncodeTitle(Eval("Title"),"-") %>' ><%# Eval("Title") %></a></h2>
                              <ul class="postmetadata">
                                <li class="author">                                  
                                  <a href="javascript:;"><%# Eval("BlogAuthor")%></a>
                                </li>
                                <li class="date">                                  
                                 <%# Date.Parse(Eval("BlogDate")).ToString("MMMM dd, yyyy")%> </li>
                              </ul>
                            </div>

                            <div class="postDesc">
                              <div class="postImg">
                                <img src='<%# if(isDBNull(Eval("SmallImage")) = False,domainName & "Admin/" & Eval("SmallImage"),domainName & "Admin/content/DemoImage.jpg" ) %>' alt="">
                              </div>
                              <p>
                                <%# Eval("SmallIMage")%>
                              </p>                              
                            </div>

                            <div class="row">
                               <a href='<%# domainName & "Product-Details/" & Eval("Blogid") & "/" & Utility.EncodeTitle(Eval("Title"),"-") %>' class="readMore">Read More</a>
                              <div class="socialSharingLinks">
                                  <ul class="pull-left socialList">
                                    <li>
                                      <a href="javascript:;"> <img src="ui/media/dist/icons/fb-share.png" height="22" width="60"> </a>
                                    </li>                       
                                     <li>
                                      <a href="javascript:;"> <img src="ui/media/dist/icons/tweet.png" height="22" width="60"> </a>
                                    </li>
                                    <li>
                                      <a href="javascript:;"> <img src="ui/media/dist/icons/fb-like.png" height="19" width="44"> </a>
                                    </li>
                                    <li>
                                      <a href="javascript:;"> <img src="ui/media/dist/icons/googleplus.png" height="21" width="67"> </a>
                                    </li>
                                  </ul>
                                </div>  
                            </div>
                          </article>
                      </li>

    </ItemTemplate>
    <LayoutTemplate>
        <ul id="itemPlaceholderContainer" runat="server" class="blogSection" style="">
            <li runat="server" id="itemPlaceholder" />
        </ul>
        <div style="" class="pagination">
            <asp:DataPager ID="DataPager1"  PageSize="5"  runat="server">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Link"  ShowFirstPageButton="False" ShowNextPageButton="False" ShowPreviousPageButton="True" />
                    <asp:NumericPagerField />
                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True" ShowPreviousPageButton="False" />
                </Fields>
            </asp:DataPager>
        </div>
    </LayoutTemplate>
</asp:ListView>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [List_NexaBlog] order by BlogDate">
</asp:SqlDataSource>
