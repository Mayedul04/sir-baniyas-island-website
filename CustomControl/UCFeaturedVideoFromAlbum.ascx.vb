﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class CustomControl_UserListControl
    Inherits System.Web.UI.UserControl
    Public domainName As String

    Public ContentClass As String
    Public H2Class As String
    Public PClass As String

    Public TableName As String
    Public VisualEditLink As String
    Public IDFieldName As String

    Public Image_Width As Integer
    Public Image_Height As Integer


    Public Property ImageWidth() As Integer
        Get
            Return Image_Width
        End Get

        Set(ByVal value As Integer)
            Image_Width = value
        End Set
    End Property

    Public Property ImageHeight() As Integer
        Get
            Return Image_Height
        End Get

        Set(ByVal value As Integer)
            Image_Height = value
        End Set
    End Property

    Public Property content_class() As String
        Get
            Return ContentClass
        End Get

        Set(ByVal value As String)
            ContentClass = value
        End Set
    End Property



    Public Property h2_class() As String
        Get
            Return H2Class
        End Get

        Set(ByVal value As String)
            H2Class = value
        End Set
    End Property

    Public Property p_class() As String
        Get
            Return PClass
        End Get

        Set(ByVal value As String)
            PClass = value
        End Set
    End Property

    Public Property table_name() As String
        Get
            Return TableName
        End Get

        Set(ByVal value As String)
            TableName = value
        End Set
    End Property

    Public Property visual_edit() As String
        Get
            Return VisualEditLink
        End Get

        Set(ByVal value As String)
            VisualEditLink = value
        End Set
    End Property



    Public Property idfield_name() As String
        Get
            Return IDFieldName
        End Get

        Set(ByVal value As String)
            IDFieldName = value
        End Set
    End Property



    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub


    Public Function Content() As String
         Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT Top 1 * FROM VideoItem where GalleryID=@GalleryID and Status='1' and Featured='1' "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        cmd.Parameters("GalleryID").Value = IDFieldName 'Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ height=""" & ImageHeight & """ width=""" & ImageWidth & """ alt=""" & reader("ImageAltText") & """>"
            M += "<div>" & Utility.showEditButton(Request, domainName & "Admin/A-Video/GalleryItemEdit.aspx?galleryItemId=" & reader("GalleryItemID")) & "</div>"

            M += "<a href=""" & domainName & "VideoPop/Gallery/" & reader("GalleryItemID") & """ class=""playbtn videoFancyIframe""></a>"
        End If
        conn.Close()
        Return M
    End Function

End Class
