﻿Imports System.Data.SqlClient
Partial Class CustomControl_UserHTMLTxt
    Inherits System.Web.UI.UserControl
    Public html_ID As Integer
    Public domainName As String
    Public IMG_Class As String
    Public IMG_Width As String
    Public IMG_Height As String
    Public IMG_Style As String
    Public show_Edit As Boolean = False
    Public IMG_Type As String
    Public IMG_SmallWidth As String
    Public IMG_SmallHeight As String
    Public IMG_BigWidth As String
    Public IMG_BigHeight As String

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Public Property IMGSmallWidth() As String
        Get
            Return IMG_SmallWidth
        End Get

        Set(ByVal value As String)
            IMG_SmallWidth = value
        End Set
    End Property
    Public Property IMGSmallHeight() As String
        Get
            Return IMG_SmallHeight
        End Get

        Set(ByVal value As String)
            IMG_SmallHeight = value
        End Set
    End Property

    Public Property IMGBigWidth() As String
        Get
            Return IMG_BigWidth
        End Get

        Set(ByVal value As String)
            IMG_BigWidth = value
        End Set
    End Property
    Public Property IMGBigHeight() As String
        Get
            Return IMG_BigHeight
        End Get

        Set(ByVal value As String)
            IMG_BigHeight = value
        End Set
    End Property

    Public Property HTMLID() As Integer
        Get
            Return html_ID
        End Get

        Set(ByVal value As Integer)
            html_ID = value
        End Set
    End Property
    Public Property ImageClass() As String
        Get
            Return IMG_Class
        End Get

        Set(ByVal value As String)
            IMG_Class = value
        End Set
    End Property
    Public Property ImageWidth() As String
        Get
            Return IMG_Width
        End Get

        Set(ByVal value As String)
            IMG_Width = value
        End Set
    End Property
    Public Property ImageHeight() As String
        Get
            Return IMG_Height
        End Get

        Set(ByVal value As String)
            IMG_Height = value
        End Set
    End Property
    Public Property ImageStyle() As String
        Get
            Return IMG_Style
        End Get

        Set(ByVal value As String)
            IMG_Style = value
        End Set
    End Property
    Public Property ImageType() As String
        Get
            Return IMG_Type
        End Get

        Set(ByVal value As String)
            IMG_Type = value
        End Set
    End Property
    Public Property ShowEdit() As Boolean
        Get
            Return show_Edit
        End Get

        Set(ByVal value As Boolean)
            show_Edit = value
        End Set
    End Property
    Function HTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", HTMLID)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()
                If ImageType = "Small" Then
                    M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""" & IMG_Class & """ height=""" & IMG_Height & """ Width=""" & IMG_Width & """ style=""" & IMG_Style & """ alt=""" & reader("ImageAltText") & """>"
                    If show_Edit = True Then
                        M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SecondImage=0&File=0&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0&BigDetails=0&SmallImageWidth=" & IMGSmallWidth & "&SmallImageHeight=" & IMGSmallHeight & "&BigImageWidth=" & IMGBigWidth & "&BigImageHeight=" & IMGBigHeight & "")
                    End If
                Else
                    M += "<img src=""" & domainName & "Admin/" & reader("BigImage") & """ class=""" & IMG_Class & """ height=""" & IMG_Height & """ Width=""" & IMG_Width & """ style=""" & IMG_Style & """ alt=""" & reader("ImageAltText") & """>"
                    If show_Edit = True Then
                        M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SecondImage=0&File=0&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&Link=0&BigDetails=0&SmallImageWidth=" & IMGSmallWidth & "&SmallImageHeight=" & IMGSmallHeight & "&BigImageWidth=" & IMGBigWidth & "&BigImageHeight=" & IMGBigHeight & "")
                    End If
                End If
               
            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


End Class
