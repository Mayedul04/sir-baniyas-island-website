﻿Imports System.Data.SqlClient
Partial Class CustomControl_UserHTMLTxt
    Inherits System.Web.UI.UserControl
    Public html_ID As Integer
    Public domainName As String
    Public show_Edit As Boolean = True
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Property HTMLID() As Integer
        Get
            Return html_ID
        End Get

        Set(ByVal value As Integer)
            html_ID = value
        End Set
    End Property

    Public Property ShowEdit() As Boolean
        Get
            Return show_Edit
        End Get

        Set(ByVal value As Boolean)
            show_Edit = value
        End Set
    End Property
    Function HTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", HTMLID)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += reader("BigDetails").ToString

                If show_Edit = True Then
                    M += "<p class=""pull-right"">" & Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&Link=0") & "</p>"

                End If
               
            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function


End Class
