﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class CustomControl_UserListControl
    Inherits System.Web.UI.UserControl
    Public domainName As String


    Public TableName As String
    Public TableMasterID As String



    Public Property Table_Name() As String
        Get
            Return TableName
        End Get

        Set(ByVal value As String)
            TableName = value
        End Set
    End Property

    Public Property Table_MasterID() As String
        Get
            Return TableMasterID
        End Get

        Set(ByVal value As String)
            TableMasterID = value
        End Set
    End Property


    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    'Public Function Content() As String
    '    Dim M As String = ""
    '    Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
    '    conn.Open()
    '    Dim selectString = "SELECT  Top 3 List_Testimonial.* FROM  List_Testimonial  where List_Testimonial.Status='1' order by List_Testimonial.Sortindex"
    '    ''Dim selectString = "SELECT * FROM List_Restaurant where Status='1' and Category='Restaurant' and RestaurantID <> 0 order by Sortindex"
    '    Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
    '    'cmd.Parameters.AddWithValue("TableName", TableName)
    '    'cmd.Parameters.AddWithValue("TableMasterID", TableMasterID)

    '    Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
    '    If reader.HasRows Then

    '        While reader.Read

    '            M += "<li>"

    '            M += "<div class=""roundedbox orange inner"">"
    '            M += " <div class=""thumbnail-round"">"
    '            M += "<img src=""" & domainName & "ui/media/dist/round/orange.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
    '            M += "<img src=""" & domainName & "ui/media/dist/thumbnails/testimonials.jpg"" class=""normal-thumb"" alt="""">"
    '            M += "</div>"
    '            M += "<div class=""box-content"">"
    '            M += " <h5 class=""title-box"">" & reader("Title") & "</h5>"
    '            M += " <h6 class=""sub-title"">" & reader("TestimonialBy") & "</h6>"
    '            M += reader("BigDetails").ToString

    '            M += " </div>"
    '            M += "</div>"
    '            M += Utility.showEditButton(Request, domainName & "Admin/A-Testimonial/TestimonialEdit.aspx?cgid=" & reader("ID"))

    '            M += "</li>"




    '        End While


    '        reader.Read()

    '    End If
    '    conn.Close()
    '    Return M
    'End Function

    Public Function IslandMap() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM HTML where  HTMLID=108"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        'cmd.Parameters("Gallery").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read
                Dim pdfurl As String = domainName & "Admin/" & reader("FileUploaded").ToString()
                M += "<div class=""thumbnail-round"">"
                M += " <img src=""" & domainName & "ui/media/dist/round/blue.png"" height=""113"" width=""122"" alt="""" class=""roundedcontainer"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ class=""normal-thumb"" alt="""">"
                M += "</img>"
                M += "</div>"

                M += "<div class=""box-content"">"
                M += " <h5 class=""title-box""><a href=""" & pdfurl & """ class="" bluefont"" target=""_blank"">Island Map</a></h5>"
                ' M += " <h5 class=""title-box""><a href=""" & domainName & "IslandMap-Pop"" class=""mapFancyIframe2 bluefont"">Island Map</a></h5>"
                'M += " <h6 class=""sub-title"">" & reader("Title") & "</h6>"
                M += "<p>" & reader("SmallDetails") & "</p>"
                M += "<a href=""" & pdfurl & """ class="" bluefont"" target=""_blank"" class=""readmorebtn"">View Map</a>"
                M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&SmallDetails=1&VideoLink=0&VideoCode=0&Map=0&BigImage=1&BigDetails=0&Link=0&SmallImageWidth=151&SmallImageHeight=129&BigImageWidth=733&BigImageHeight=458")
                M += "</div>"



            End While

        End If
        conn.Close()
        Return M
    End Function



End Class
