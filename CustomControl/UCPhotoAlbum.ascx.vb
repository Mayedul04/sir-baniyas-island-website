﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class CustomControl_UserListControl
    Inherits System.Web.UI.UserControl
    Public domainName As String
    Public ParentSlidingClass As String
    Public ChildSlidingClass As String
    Public IMGClass As String
    Public PClass As String
    Public H2Class As String
    Public LinkClass As String
    Public LinkName As String
    Public IMGTrueFalse As String
    Public SlideLeftClass As String
    Public SlideRightClass As String


    Public TableName As String
    Public OrderByField As String
    Public VisualEditLink As String
    Public DetailsPageLink As String
    Public IDFieldName As String

    Public Property idfield_name() As String
        Get
            Return IDFieldName
        End Get

        Set(ByVal value As String)
            IDFieldName = value
        End Set
    End Property



    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Public Function Content() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM Gallery where Status='1' and GalleryID=@GalleryID order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        cmd.Parameters("GalleryID").Value = IDFieldName

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read

                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """ class=""boxthumbnail"">"
                M += Utility.showEditButton(Request, domainName & "Admin/A-Gallery/GalleryEdit.aspx?galleryId=" + reader("GalleryID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

                M += "<a href=""" & domainName & "Photos/Gallery/" & reader("GalleryID") & "/" & Utility.EncodeTitle(reader("Title"), "-") & """ class=""linkbtn"">Photo Gallery<span class=""arrow""></span></a>"


            End While

        End If
        conn.Close()
        Return M
    End Function



End Class
