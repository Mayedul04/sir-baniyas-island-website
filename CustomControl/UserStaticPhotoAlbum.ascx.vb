﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class CustomControl_UserListControl
    Inherits System.Web.UI.UserControl
    Public domainName As String
    Public galleryID As String

    Public Property Gallery_ID() As String
        Get
            Return galleryID
        End Get

        Set(ByVal value As String)
            galleryID = value
        End Set
    End Property



    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Public Sub LoadGallery()
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galleryID
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()

                lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Spa"">Photo Gallery<span class=""arrow""></span></a>"

            End While
        End If
        cn.Close()
    End Sub

    Public Function PhotoGalleryImage() As String
        Dim M As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM Gallery where Status='1' and GalleryID=@GalleryID order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int)
        cmd.Parameters("GalleryID").Value = galleryID

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then


            While reader.Read

                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """  class=""boxthumbnail"">"
                M += Utility.showEditButton(Request, domainName & "Admin/A-Gallery/GalleryEdit.aspx?galleryId=" + reader("GalleryID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

            End While

        End If
        conn.Close()
        Return M
    End Function


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        LoadGallery()
        ltPhotoImage.Text = PhotoGalleryImage()
        ltAdd.Text = Utility.showAddButton(Request, domainName & "Admin/A-Gallery/AllGalleryItemEdit.aspx?galleryId=" & galleryID)
    End Sub



End Class
