﻿Imports System.Data.SqlClient
Partial Class CustomControl_UserBanner
    Inherits System.Web.UI.UserControl
    Public domainName As String
    Public section_name As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Property SectionName() As String
        Get
            Return section_name
        End Get

        Set(ByVal value As String)
            section_name = value
        End Set
    End Property
    Function GetBanner() As String
        Dim M As String = ""
        Dim retstr As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        'SectionName=@SectionName
        Dim sql = "SELECT Top 1 * FROM Footer WHERE  Status='1' order by NEWID()"
        Dim cmd = New SqlCommand(sql, con)
        'cmd.Parameters.Clear()
        'cmd.Parameters.AddWithValue("SectionName", SectionName)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then

                While reader.Read()
                   
                    
                    M = "<img src=""" & domainName & "Admin/" & reader("BigImage").ToString & """ height=""90"" width=""728"" alt=""" & reader("ImageAltText").ToString & """>"
                    If IsDBNull(reader("Link")) = False Then
                        retstr += "<a href=""" & reader("Link").ToString & """ target=""_blank"" class=""footer-banner"">" + M + "</a>"
                        M = ""
                    Else
                        retstr += M
                        M = ""
                    End If
                    retstr += Utility.showEditButton(Request, domainName & "Admin/A-Footer-Banner/FooterBannerEdit.aspx?bannerId=" + reader("BannerID").ToString() + "&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&Link=1")
                    retstr += Utility.showAddButton(Request, domainName & "Admin/A-Footer-Banner/FooterBannerEdit.aspx?SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&Link=1")

                End While
            Else
                retstr += Utility.showAddButton(Request, domainName & "Admin/A-Footer-Banner/FooterBannerEdit.aspx?SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=1&Link=1")
            End If
            reader.Close()
            con.Close()
            Return retstr
        Catch ex As Exception
            Return retstr
        End Try

    End Function

End Class
