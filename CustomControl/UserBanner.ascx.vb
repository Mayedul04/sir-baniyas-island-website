﻿Imports System.Data.SqlClient
Partial Class CustomControl_UserBanner
    Inherits System.Web.UI.UserControl
    Public domainName As String
    Public section_name As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Property SectionName() As String
        Get
            Return section_name
        End Get

        Set(ByVal value As String)
            section_name = value
        End Set
    End Property
    Function GetBanner() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM Banner WHERE SectionName=@SectionName and Status='1' order by SortIndex"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("SectionName", SectionName)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                M += "<ul class=""list-unstyled slides"">"
                While reader.Read()
                    M += "<li>"

                    M += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString & """ alt=""" & reader("ImageAltText").ToString & """ >"
                    M += Utility.showEditButton(Request, domainName & "Admin/A-Banner/TopBannerEdit.aspx?bannerId=" + reader("BannerID").ToString() + "&SmallImage=1&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

                    M += "</li>"
                End While
                M += "</ul>"
            Else
                M = "<ul class=""list-unstyled slides""><li><img src=""http://www.sirbaniyasisland.com/Admin/Content/Home-Small181120142340.jpg"" >" & Utility.showAddButton(Request, domainName & "Admin/A-Banner/TopBannerEdit.aspx") & "</li></ul>"
            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

End Class
