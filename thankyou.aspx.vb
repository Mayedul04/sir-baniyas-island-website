﻿
Partial Class thankyou
    Inherits System.Web.UI.Page
    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Page.RouteData.Values("section") = "media" Then
                lblmsg.Text = "An email will be sent to you with a password shortly."
            ElseIf Page.RouteData.Values("section") = "wrong-media" Then
                lblmsg.Text = "Your account is not approved. Please try after getting the password."
            Else
                lblmsg.Text = "Thank you for your enquiry. A representative will contact you shortly."
            End If
        End If
    End Sub
End Class
