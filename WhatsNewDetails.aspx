﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="WhatsNewDetails.aspx.vb" Inherits="WhatsNewDetails" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName  %>'>Home</a></li>
            <li><a href='<%= domainName & "WhatsNew" %>'>what's new</a></li>
            <li class="active">'<%= title %>'</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">
                    <%= title %>
                </h1>
                <asp:Image ID="ImgDetails" runat="server" class="pull-left" style="margin-right: 30px; margin-bottom: 20px" />
                <asp:Literal ID="lblDetails" runat="server"></asp:Literal>
                
                <uc1:DynamicSEO runat="server" ID="DynamicSEO" PageType="What New" />
            </section>
            <!-- -- welcome-text ends here -- -->
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->





    <section class="container">
        <div class="footer-nav">
            <div class="row">

                 <div class="col-sm-4">
                    <a href='<%= domainName & "Island-News" %>' class="orangebg">
                        <img src='<%= domainName & "ui/media/dist/media-center/island-news-icon.png"%>' alt="" style="margin-top: -5px">
                        Island News</a>
                </div>

                <div class="col-sm-4">
                    <a href='<%= domainName & "Press-Release-News" %>' class="bluebg">
                        <img src='<%= domainName & "ui/media/dist/media-center/pressrelese-icon.png"%>' alt="">
                        Press Release</a>
                </div>

                <div class="col-sm-4">
                    <a href='<%= domainName & "Media-Library-Request" %>' class="purplebg">
                        <img src='<%= domainName & "ui/media/dist/media-center/image-library.png"%>' alt="">
                        Media Library Request</a>
                </div>

            </div>
        </div>
    </section>
</asp:Content>

