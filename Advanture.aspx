﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Advanture.aspx.vb" Inherits="Advanture" %>
<%@ Register Src="~/CustomControl/UCHTMLTitle.ascx" TagPrefix="uc1" TagName="UCHTMLTitle" %>
<%@ Register Src="~/CustomControl/UCHTMLSmallImage.ascx" TagPrefix="uc1" TagName="UCHTMLSmallImage" %>
<%@ Register Src="~/CustomControl/UCHTMLDetails.ascx" TagPrefix="uc1" TagName="UCHTMLDetails" %>
<%@ Register Src="~/CustomControl/UCInterestingFactList.ascx" TagPrefix="uc1" TagName="UCInterestingFactList" %>
<%@ Register Src="~/CustomControl/UCPhotoAlbum.ascx" TagPrefix="uc1" TagName="UCPhotoAlbum" %>
<%@ Register Src="~/CustomControl/UCVideoAlbum.ascx" TagPrefix="uc1" TagName="UCVideoAlbum" %>
<%@ Register Src="~/CustomControl/UCTestimonial.ascx" TagPrefix="uc1" TagName="UCTestimonial" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href=href='<%= domainName%>'>Home</a></li>
            <li class="active"><a href="#">Adventure</a></li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->


    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
            
                
                  <h1 class="capital">
              
                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle" HTMLID="6" ShowEdit="false"  />

                </h1>
            
                <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="6"  />
                 </section>
            <!-- -- welcome-text ends here -- -->
        </div>


        <!-- Land & Water Activites -->
        <div class="activitesContainerSection">

            <!-- Bottom FIx -->
            <div class="bottomFix">

                <div class="container">

                    <div class="row">
                    
                        <!-- Land Activities -->
                        <div class="col-md-5 col-sm-12">
                            <div class="landActivitesSection">
                                <h2 class="greenbg">
                                    <a href='<%= domainName & "Adventure-Activity-Land"%>'>Land Activities</a>
                                </h2>

                                <div class="row">

                                    <div class="col-md-6 col-sm-6">
                                        <!-- Bullet Listings -->
                                        <ul class="bulletListings">

                                           <asp:Literal ID="ltLandActivity" runat="server"></asp:Literal>

                                        </ul>
                                        <!-- Bullet Listings -->
                                    </div>


                                    <asp:Literal ID="ltFeaturedActivity" runat="server"></asp:Literal>

                                </div>

                            </div>
                        </div>
                        <!-- Land Activities -->

                         <!-- day trip -->
                        <div class="col-md-2 col-sm-12">
                           <div class="dayTripActivitesSection">
                                <h2 class="orangebg">
                                    <a href='<%= domainName & "Adventure-Activity-Day-Trip" %>'>Day Trip</a>
                                </h2>

                                <asp:Literal ID="ltrDayTrip" runat="server"></asp:Literal>
                                
                            </div>
                        </div>
                        <!-- day trip -->

                        <!-- Water Activities -->
                        <div class="col-md-5 col-sm-12">
                            <div class="waterActivitesSection">
                                <h2 class="bluebg">
                                    <a href='<%= domainName & "Adventure-Activity-Water"%>'>Water Activities</a>
                                </h2>

                                <!-- Water Activites Slider -->
                                <div class="waterActivitiesSlider">

                                    <ul class="slides">

                                      <asp:Literal ID="ltWaterActivity" runat="server"></asp:Literal>

                                    </ul>

                                </div>
                                <!-- Water Activites Slider -->

                            </div>
                        </div>
                        <!-- Water Activities -->

                    </div>

                </div>

            </div>
            <!-- Bottom FIx -->

        </div>
        <!-- Land & Water Activites -->

        
        
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->


    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns">
        <div class="container">
                      <ul class="list-unstyled">
                <li class="purplebg">
                    <a href='<%= domainName & "PhotoGallery"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/photo-gallery.png"%>' height="25" width="29" alt=""></span>
                        Photo gallery
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "VideoGallery"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/video-gallery.png"%>' height="20" width="25" alt=""></span>
                        Video gallery
                    </a>
                </li>
                <li class="bluebg">
                    <a class="bookNowIframe" href='<%= domainName & "Booknow-Pop"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/book-now.png"%>' height="27" width="27" alt=""></span>
                        Book now
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "Map-Pop"%>' class="mapFancyIframe">
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/location-map.png"%>' height="26" width="32" alt=""></span>
                        Location map
                    </a>
                </li>
                <li class="purplebg">
                    <a href='<%= domainName & "Travel-Air"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/flights.png"%>' height="14" width="38" alt=""></span>
                        Flights
                    </a>
                </li>
                <li class="orangebg">
                    <a href='<%= domainName & "Sir-Bani-Yas-History"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/history.png"%>' height="20" width="27" alt=""></span>
                        History
                    </a>
                </li>
                <li class="greenbg">
                    <a href='<%= domainName & "WhatsNew"%>'>
                        <span>
                            <img src='<%= domainName & "ui/media/dist/icons/whats-new.png"%>' height="17" width="18" alt=""></span>
                        What's New
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="19" />
</asp:Content>

