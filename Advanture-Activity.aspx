﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageAdvanture.master" AutoEventWireup="false" CodeFile="Advanture-Activity.aspx.vb" Inherits="Advanture_Activity" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:HiddenField ID="hdnID" runat="server" />
    <asp:HiddenField ID="hdnMasterID" runat="server" />
<asp:HiddenField ID="hdnTitle" runat="server" />
    <asp:HiddenField ID="hdnSection" runat="server" />
     <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%=domainName%>'>Home</a></li>
         <li class="active"><a href='<%=domainName & "Adventure"%>'>Adventure</a></li>
            <li class="active">
         <asp:Literal ID="ltBreadCrum" runat="server"></asp:Literal>
            </li>

        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->


    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">

            <h1 class="capital">
                <asp:Literal ID="ltTitle" runat="server"></asp:Literal>

            </h1>
            
            <div class="row marginBtm20">

           <asp:Literal ID="ltHtmlBody" runat="server"></asp:Literal>

            </div>
<uc1:DynamicSEO runat="server" ID="DynamicSEO"  PageType="List_Advanture_Activity"/>

        </div>
        
        
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->
    <nav id="multiple-btns" class="detailPage wateractivities">
        <div class="container">
        
               <asp:Literal ID="ltFooterNavigation" runat="server"></asp:Literal> 
                <%--<li class="greenbg">
                    <a class="oneline" href='<%=domainName & "Adventure"%>'>
                        <span><img src='<%=domainName & "ui/media/dist/land/activite-icon.png"%>' alt=""></span>
                        <h3>aCTIVITIES</h3>
                    </a>
                </li>--%>
           
        </div>
    </nav>
    <!-- -- multiple-btns starts here 
    -------------------------------------------- -->

</asp:Content>

