﻿Imports System.Data.SqlClient

Partial Class Experience
    Inherits System.Web.UI.Page
    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Function InstgramFeed() As String
        Dim retstr As String = ""
        Dim secondinner As String = ""
        Dim M As String = ""
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select  BigImage, InstaUser, Title from InstagramImage where Status=1 "
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<div class=""col-md-6"">"
                retstr += "<div class=""imgHold""><a title=""" & "@" & reader("InstaUser") & "<br/>" & reader("Title").ToString() & """ class=""fancyMeiframe3"" href=""" & domainName & "Admin/" & reader("BigImage") & """ height=""55"" width=""52"" rel=""instagallery"" ><img src=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ width=""263"" alt=""""></a></div></div>"
                If count Mod 2 = 0 Then
                    secondinner += "<div class=""row marginBtm30"">" & retstr & "</div>"
                    retstr = ""
                End If
                If count > 2 Then
                    If count Mod 4 = 0 Then
                        M += "<li>" & secondinner & "</li>"
                        secondinner = ""
                    End If
                End If
                count += 1

            End While
        End If
        cn.Close()
        Return M
    End Function
End Class
