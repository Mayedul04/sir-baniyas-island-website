﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllLocations.aspx.vb" Inherits="Admin_A_Partners_AllPartner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <h1 class="page-title">Contact Address</h1>

   <div class="btn-toolbar">
        <button runat="server" id ="btnAddNew"  class="btn btn-primary"><i class="icon-save"></i> Add New</button>
        <div class="btn-group"></div>
    </div>

    <!-- content -->

    <h2><asp:Label ID="lblTabTitle" runat="server" Text="All Locations"></asp:Label></h2>
    <div>

         <p>
                    <label>Language</label>
                    <asp:DropDownList ID="ddlLang" runat="server" CssClass ="input-xlarge" AutoPostBack="true"  DataSourceID="sdsLang"  DataTextField="LangFullName" DataValueField="Lang"> </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsLang" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                        SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
                    </asp:SqlDataSource>
        </p>

        <div class="well">
           
           <asp:ListView ID="ListView1" runat="server" DataSourceID="sdsAdd" 
                DataKeyNames="ContactID">
                
                <ItemTemplate>
                    <tr style="">
                       
                        <td>
                            <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Heading") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text='<%# "Phone:" & Eval("Phone") %>' />
                            <br/>
                            <asp:Label ID="LinkLabel" runat="server" Text='<%# "Fax:" & Eval("Fax")%>' />
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("Email") %>' />
                        </td>
                        <td>
                            <asp:Label ID="SortIndexLabel" runat="server" Text='<%# Eval("SortIndex") %>' />
                        </td>
                        <td>
                            <asp:CheckBox ID="StatusCheckBox" runat="server" 
                                Checked='<%# Eval("HeadOffice") %>' Enabled="false" />
                        </td>
                        
                        <td>
                            <asp:Label ID="MasterIDLabel" runat="server" Text='<%# Eval("MasterID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="LangLabel" runat="server" Text='<%# Eval("Lang") %>' />
                        </td>
                        <td>
                            <a href='<%# "Location.aspx?lid=" & Eval("ContactID")%>' title="Edit"><i class="icon-pencil">
                            </i></a>&nbsp
                            <a href='<%# "Location.aspx?lid=" & Eval("ContactID") & "&new=1"%>' title="New Lang" style='<%# if(Eval("Lang")="en","","display:none;") %>'><i class="icon-plus-sign"></i> </a>&nbsp 
                            <a href='<%# "#" & Eval("ContactID")%>' data-toggle="modal"><i class="icon-remove"></i></a>
                            <div class="modal small hide fade" id='<%# Eval("ContactID")%>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">
                                        Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer" class="table">
                        <thead>
                            <tr id="Tr1" runat="server" style="">
                                
                                <th id="Th2" runat="server">
                                    Title</th>
                                <th id="Th5" runat="server">
                                    Contacts</th>
                                <th id="Th7" runat="server">
                                    Email</th>
                                
                                <th id="Th9" runat="server">
                                    Sort Order</th>
                                <th id="Th8" runat="server">
                                    Head Office</th>
                               
                                <th id="Th12" runat="server">
                                    MasterID</th>
                                <th id="Th13" runat="server">
                                    Lang</th>
                                <th>
                                    Actions
                                </th>
                            </tr>
                        </thead>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <div class="paginationNew pull-right">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                        ShowPreviousPageButton="True" />
                                    <asp:NumericPagerField />
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                        ShowPreviousPageButton="False" />
                                </Fields>
                            </asp:DataPager>
                        </div>
                    </table>
                </LayoutTemplate>
                
            </asp:ListView>
            <asp:SqlDataSource ID="sdsAdd" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                
                SelectCommand="SELECT * FROM [ContactDetails]" 
                DeleteCommand="DELETE FROM [ContactDetails] WHERE [ContactID] = @ContactID" >
                <DeleteParameters>
                    <asp:Parameter Name="ContactID" Type="Int32" />
                </DeleteParameters>
               
            </asp:SqlDataSource>
                                
        </div> 
    <!-- Eof content -->

            
</div>
    <!-- Eof content -->

    


                    
</asp:Content>

