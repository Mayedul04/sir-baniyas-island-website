﻿
Partial Class Admin_A_List_List
    Inherits System.Web.UI.Page

    Protected smallImageWidth As String = "", smallImageHeight As String = "", bigImageWidth As String = "", bigImageHeight As String = "", videoWidth As String = "", videoHeight As String = ""
    Protected smallLogoImageWidth As String = "", smallLogoImageHeight As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Utility.GetDimentionSetting("Stay", "All", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, videoWidth, videoHeight)

        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("lid")) Then
                btnSubmit.InnerHtml = "<i class='icon-save'></i>  Update"
                lblTabTitle.Text = "Update Hotel & Resorts"
                LoadContent(Request.QueryString("lid"))
            Else

            End If
        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnUpdateDate.Value = DateTime.Now

        If fuSmallImage.FileName <> "" Then
            hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
        End If
        If fuBigImage.FileName <> "" Then
            hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
        End If

        If fuLogoSmall.FileName <> "" Then
            hdnLogoSmall.Value = Utility.AddImage(fuLogoSmall, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-SmallLogo", Utility.EncodeTitle(txtTitle.Text, "-") & "-SmallLogo"), Server)
        End If

        If hdnSmallImage.Value <> "" Then
            imgSmallImage.Visible = True
            imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
        End If
        If hdnBigImage.Value <> "" Then
            imgBigImage.Visible = True
            imgBigImage.ImageUrl = "../" & hdnBigImage.Value
        End If

        If hdnLogoSmall.Value <> "" Then
            imgLogoSmall.Visible = True
            imgLogoSmall.ImageUrl = "../" & hdnLogoSmall.Value
        End If


        If chkSmallLogoImageDelete.Checked Then
            hdnLogoSmall.Value = ""
        End If

        If chkBigLogoImageDelete.Checked Then
            hdnSmallImage.Value = ""
        End If

        If chkBigImageDelete.Checked Then
            hdnBigImage.Value = ""
        End If
       

        If String.IsNullOrEmpty(Request.QueryString("lid")) Or Request.QueryString("new") = 1 Then
            'If Request.QueryString("new") <> "1" Then
            '    hdnMasterID.Value = GetMasterID()
            'End If
            InsertIntoSEO()
            'If sdsList.Insert() > 0 Then
            '    InsertIntoSEO()
            '    Response.Redirect("AllStay.aspx")
            'Else
            '    divError.Visible = True
            'End If
        Else
            If sdsList.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = False
            End If
        End If

    End Sub

    Protected Sub InsertIntoSEO()
        '; INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID], [Lang]) VALUES (@Title, @SmallDetails, @SmallDetails,1, 'HTMLChild', @PageID, @Lang)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID],Lang) SELECT top 1  List_Restaurant.Title,List_Restaurant.SmallDetails,List_Restaurant.SmallDetails,1,'Stay', List_Restaurant.RestaurantID,Lang  FROM List_Restaurant where List_Restaurant.RestaurantID=" & Request.QueryString("lid") & " order by List_Restaurant.RestaurantID  desc "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.CommandText = selectString
        cmd.ExecuteNonQuery()

        conn.Close()
    End Sub

    Protected Function GetMasterID() As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT (Max( MasterID)+1) as MaxMasterID  FROM   List_Restaurant "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)


        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = If(reader("MaxMasterID") & "" = "", "1", reader("MaxMasterID") & "")
        End If
        conn.Close()

        Return retVal
    End Function

    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT   * FROM   List_Restaurant where RestaurantID=@RestaurantID  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("RestaurantID", Data.SqlDbType.Int)
        cmd.Parameters("RestaurantID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()

            txtTitle.Text = reader("Title") & ""
            txtSmallDetails.Text = reader("SmallDetails") & ""
            txtDetails.Text = reader("BigDetails") & ""
            hdnSmallImage.Value = reader("SmallImage") & ""
            hdnBigImage.Value = reader("BigImage") & ""
            hdnLogoSmall.Value = reader("LogoSmall") & ""
            If hdnSmallImage.Value <> "" Then
                imgSmallImage.Visible = True
                imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
            End If
            If hdnBigImage.Value <> "" Then
                imgBigImage.Visible = True
                imgBigImage.ImageUrl = "../" & hdnBigImage.Value
            End If

            If hdnLogoSmall.Value <> "" Then
                imgLogoSmall.Visible = True
                imgLogoSmall.ImageUrl = "../" & hdnLogoSmall.Value
            End If

            txtLinkTextBox.SelectedValue = reader("Link") & ""
            Boolean.TryParse(reader("Featured") & "", chkFeatured.Checked)
            txtSortIndex.Text = reader("SortIndex") & ""
            Boolean.TryParse(reader("Status") & "", chkStatus.Checked)
            hdnUpdateDate.Value = reader("LastUpdated") & ""
            txtImgAlt.Text = reader("ImageAltText") & ""
            hdnMasterID.Value = reader("MasterID") & ""
            ddlLang.SelectedValue = reader("Lang") & ""

            txtBookingCode.Text = reader("Booking").ToString
            txtOpeningTime.Text = reader("OpeningTime").ToString
            txtTripAdvisor.Text = reader("TripAdvisor").ToString
            If IsDBNull(reader("GalleryID")) = False Then
                ddlPGallery.SelectedValue = reader("GalleryID").ToString()
            End If
            If IsDBNull(reader("VGalleryID")) = False Then
                ddlVGallery.SelectedValue = reader("VGalleryID").ToString
            End If
         
        End If
        conn.Close()
    End Sub


End Class
