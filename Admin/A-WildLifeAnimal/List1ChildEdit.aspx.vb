﻿
Partial Class Admin_A_List1_List1Child
    Inherits System.Web.UI.Page



    Protected smallImageWidth As String = "119", smallImageHeight As String = "55", bigImageWidth As String = "274", bigImageHeight As String = "317", videoWidth As String = "", videoHeight As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Utility.GetDimentionSetting("List1Child", "Animal", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, videoWidth, videoHeight)

        If Not IsPostBack Then
            If Not Request.QueryString("Small") Is Nothing Then
                pnlSmallImage.Visible = False
            End If
            If Not Request.QueryString("Big") Is Nothing Then
                pnlBigImage.Visible = False
            End If
            If Not Request.QueryString("Link") Is Nothing Then
                pnlLink.Visible = False
            End If
            If Not Request.QueryString("Sound") Is Nothing Then
                pnlSound.Visible = False
            End If
          
            If Not Request.QueryString("pg") Is Nothing Then
                If Request.QueryString("pg") = "1" Then
                    pnlPGallery.Visible = True
                End If
            End If
            If Not Request.QueryString("vg") Is Nothing Then
                If Request.QueryString("vg") = "1" Then
                    pnlVGallery.Visible = True
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("l1cid")) Then

                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
                lblTabTitle.Text = "Update Animal"
                LoadContent(Request.QueryString("l1cid"))
            Else

            End If
        End If

    End Sub


    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnUpdateDate.Value = DateTime.Now

        If fuSmallImage.FileName <> "" Then
            hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
        End If
        If fuBigImage.FileName <> "" Then
            hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
        End If
        If fuAnimalSound.FileName <> "" Then
            hdnAnimalSound.Value = Utility.AddSound(fuAnimalSound, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Sound"), Server)
        End If
        If hdnSmallImage.Value <> "" Then
            imgSmallImage.Visible = True
            imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
        End If
        If hdnBigImage.Value <> "" Then
            imgBigImage.Visible = True
            imgBigImage.ImageUrl = "../" & hdnBigImage.Value
        End If
        If fuPGal.FileName <> "" Then
            hdnPGal.Value = Utility.AddImage(fuPGal, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Other", Utility.EncodeTitle(txtTitle.Text, "-") & "-Other"), Server)
            ImgPGal.ImageUrl = "~/Admin/" + hdnPGal.Value
            ImgPGal.Visible = True
        End If
        If fuVGallery.FileName <> "" Then
            hdnVGal.Value = Utility.AddImage(fuVGallery, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Other", Utility.EncodeTitle(txtTitle.Text, "-") & "-Other"), Server)
            ImgVGal.ImageUrl = "~/Admin/" + hdnVGal.Value
            ImgVGal.Visible = True
        End If
        If chlDelSound.Checked = True Then
            hdnAnimalSound.Value = ""
        End If
        If hdnAnimalSound.Value <> "" Then
            ltSound.Visible = True
            ltSound.Text = "../" & hdnBigImage.Value
        End If



        If String.IsNullOrEmpty(Request.QueryString("l1cid")) Then
            hdnMasterID.Value = GetMasterID(Request.QueryString("l1id"), Request.QueryString("lang"))
            If sdsList1_Child.Insert() > 0 Then
                InsertIntoSEO()
                Response.Redirect("AllList1Child.aspx?l1id=" & Request.QueryString("l1id") & "&Lang=" & Request.QueryString("Lang"))
            Else
                divError.Visible = True
            End If
        Else
            If sdsList1_Child.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = False
            End If
        End If

    End Sub

    Protected Sub InsertIntoSEO()
        '; INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID], [Lang]) VALUES (@Title, @SmallDetails, @SmallDetails,1, 'HTMLChild', @PageID, @Lang)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID]) SELECT top 1  List1_Child.Title,List1_Child.SmallDetails,List1_Child.SmallDetails,1,'Animal', List1_Child.ListID  FROM List1_Child order by List1_Child.ChildID  desc "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.CommandText = selectString
        cmd.ExecuteNonQuery()

        conn.Close()
    End Sub


    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT   *  FROM   List1_Child where ChildID=@ChildID  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ChildID", Data.SqlDbType.Int)
        cmd.Parameters("ChildID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            ddlCategory.SelectedValue = reader("StyleID").ToString()
            txtTitle.Text = reader("Title") & ""
            txtSmallDetails.Text = reader("SmallDetails") & ""
            txtDetails.Text = reader("BigDetails") & ""
            hdnSmallImage.Value = reader("SmallImage") & ""
            hdnBigImage.Value = reader("BigImage") & ""
            hdnAnimalSound.Value = reader("Sound") & ""
            If hdnSmallImage.Value <> "" Then
                imgSmallImage.Visible = True
                imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
            End If
            If hdnBigImage.Value <> "" Then
                imgBigImage.Visible = True
                imgBigImage.ImageUrl = "../" & hdnBigImage.Value
            End If

            If hdnAnimalSound.Value <> "" Then
                ltSound.Visible = True
                ltSound.Text = hdnAnimalSound.Value

            End If

            txtLinkTextBox.Text = reader("Link") & ""
            Boolean.TryParse(reader("Featured") & "", chkFeatured.Checked)
            Boolean.TryParse(reader("NewArrival") & "", chkNewArrival.Checked)
            txtSortIndex.Text = reader("SortIndex") & ""
            Boolean.TryParse(reader("Status") & "", chkStatus.Checked)
            hdnUpdateDate.Value = reader("LastUpdated") & ""
            txtImgAlt.Text = reader("ImageAltText") & ""
            ImgPGal.ImageUrl = "~/Admin/" + reader("PGalleryImage").ToString()
            hdnPGal.Value = reader("PGalleryImage").ToString()
            ImgVGal.ImageUrl = "~/Admin/" + reader("VGalleryImage").ToString()
            hdnVGal.Value = reader("VGalleryImage").ToString()
            hdnMasterID.Value = reader("MasterID").ToString()
            If IsDBNull(reader("GalleryID")) = False Then
                ddlPGallery.SelectedValue = reader("GalleryID").ToString()
            End If
            If IsDBNull(reader("VGalleryID")) = False Then
                ddlVGallery.SelectedValue = reader("VGalleryID").ToString
            End If
        End If
        conn.Close()
    End Sub

    'Protected Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.ServerClick
    '    Response.Redirect("AllList1Child.aspx?l1id=" & Request.QueryString("l1id") & "&Lang=" & Request.QueryString("Lang"))
    'End Sub

    Protected Function GetMasterID(ByVal parentID As Int16, ByVal Lang As String) As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT (isNull( Max( MasterID),0)+1) as MaxMasterID  FROM   List1_Child where ListID=@ListID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int).Value = parentID
        cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Lang


        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = If(reader("MaxMasterID") & "" = "", "1", reader("MaxMasterID") & "")
        End If
        conn.Close()

        Return retVal
    End Function

End Class
