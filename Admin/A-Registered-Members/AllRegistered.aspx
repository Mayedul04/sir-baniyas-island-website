﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllRegistered.aspx.vb" Inherits="Admin_A_Registered_Members_AllRegistered" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">All Registered Members</h1>

    <div class="btn-toolbar">
        <button runat="server" id="btnDownload" class="btn btn-primary"><i class="icon-save"></i>Download </button>
        <div class="btn-group">
        </div>
    </div>

    <!-- content -->

    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="All Contacts"></asp:Label></h2>
    <div>

        <div class="well">

            <table class="table">

                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>

                        <th>Registered Date </th>
                        <th>Approved</th>
                        <th>Manage Permissions</th>
                        <th style="width: 20px;"></th>
                    </tr>

                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="RegisterUserID"
                        DataSourceID="sdsMember">
                        <EmptyDataTemplate>
                            <table id="Table1" runat="server" style="">
                                <tr>
                                    <td>No data was returned.</td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                <td>
                                    <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("FName") & " " & Eval("LName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>' /><br />
                                    <asp:HiddenField ID="hdnUID" Value='<%# Eval("RegisterUserID") %>' runat="server" />

                                </td>
                               
                                <td>
                                    <asp:Label ID="LastUpdatedLabel" runat="server"
                                        Text='<%# Convert.ToDateTime(Eval("RegisteredDate")).Tostring("MMM dd, yyyy") %>' />
                                </td>

                                 <td>
                                     <asp:CheckBox ID="chkApprove" Enabled="True" AutoPostBack="true" OnCheckedChanged="chkchkApprove_CheckedChanged"  Checked='<%# Eval("Approve") %>' runat="server" />
                                </td>
                                <td>
                                    <a href='<%# "MediaPermissions.aspx?mid=" & Eval("RegisterUserID")%>' >Permissions</a>
                                </td>
                                <td>
                                   <asp:HiddenField ID="hdnName" Value='<%# Eval("FName") %>' runat="server" />
                                    <asp:HiddenField ID="hdnEmail" Value='<%# Eval("Email") %>' runat="server" />
                                    <asp:HiddenField ID="hdnPassword" Value='<%# Eval("Pass1") %>' runat="server" />
                                    <a href='<%# "#" & Eval("RegisterUserID") %>' data-toggle="modal"><i class="icon-remove"></i></a>

                                    <div class="modal small hide fade" id='<%# Eval("RegisterUserID") %>' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h3 id="myModalLabel">Delete Confirmation</h3>
                                        </div>
                                        <div class="modal-body">

                                            <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                            <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server" Text="Delete" />

                                        </div>
                                    </div>

                                </td>


                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>

                            <tr id="itemPlaceholder" runat="server">
                            </tr>

                            <div class="paginationNew pull-right">
                                <asp:DataPager ID="DataPager1" PageSize="25" runat="server">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False" ShowPreviousPageButton="True" />
                                        <asp:NumericPagerField />
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True" ShowPreviousPageButton="False" />



                                    </Fields>
                                </asp:DataPager>
                            </div>

                        </LayoutTemplate>

                    </asp:ListView>

                </tbody>
            </table>

        </div>
        <!-- Eof content -->

        <asp:SqlDataSource ID="sdsMember" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [RegisteredUser] WHERE [RegisterUserID] = @RegisterUserID" 
            SelectCommand="SELECT * FROM [RegisteredUser] where Approve=@Approve">
            <DeleteParameters>
                <asp:Parameter Name="RegisterUserID" Type="Int32" />
            </DeleteParameters>
            <SelectParameters>
                <asp:Parameter Name="Approve" Type="Boolean" DefaultValue="True" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    <!-- Eof content -->
</asp:Content>

