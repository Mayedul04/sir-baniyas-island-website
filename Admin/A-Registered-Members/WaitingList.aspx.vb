﻿Imports System.Data.SqlClient

Partial Class Admin_A_Registered_Members_WaitingList
    Inherits System.Web.UI.Page
    Protected Sub chkchkApprove_CheckedChanged(sender As Object, e As EventArgs)
        Dim a As Integer
        For Each row As ListViewDataItem In GridView1.Items
            Dim chkBox = DirectCast(row.FindControl("chkApprove"), CheckBox)
            Dim userID = DirectCast(row.FindControl("hdnUID"), HiddenField).Value
            Dim userName = DirectCast(row.FindControl("NameLabel"), Label).Text
            Dim userEmail = DirectCast(row.FindControl("hdnEmail"), HiddenField).Value
            Dim userPassWord = DirectCast(row.FindControl("hdnPassword"), HiddenField).Value
            If chkBox.Checked = True Then
                Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                Dim sqlConn As SqlConnection = New SqlConnection(strConn)
                sqlConn.Open()
                Dim sqlString As String
                sqlString = "Update RegisteredUser Set Approve=1 where RegisterUserID=@UID"
                Dim sqlcomm As SqlCommand = New SqlCommand()
                sqlcomm.CommandText = sqlString
                sqlcomm.Connection = sqlConn
                sqlcomm.CommandType = Data.CommandType.Text
                sqlcomm.Parameters.Add("UID", Data.SqlDbType.Int, 32).Value = userID
                a = sqlcomm.ExecuteNonQuery()
                sqlcomm.Dispose()
                If a = 1 Then
                    GridView1.DataBind()
                    Dim msg As String = "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
                    msg += "<tr><td style=""border:1px solid #ccc;""><table width=""740"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
                    msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;""> Name</td>"
                    msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & userName & "</td></tr>"
                    msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Email Address</td>"
                    msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & userEmail & "</td></tr>"
                    msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Password</td>"
                    msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & userPassWord & "</td></tr>"
                    msg += "</table></td></tr></table>"
                    Utility.SendMail("Admin", "noreply@sirbaniyasisland.com", userEmail, "lstagg@tdic.ae;rsanchez@tdic.ae", "mayedul.islam@wvss.net", "Sir Bani Yas Island Media Request: Here is your Password", msg)
                End If
                sqlConn.Close()
            Else
                Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                Dim sqlConn As SqlConnection = New SqlConnection(strConn)
                sqlConn.Open()
                Dim sqlString As String
                sqlString = "Update RegisteredUser Set Approve=0 where RegisterUserID=@UID"
                Dim sqlcomm As SqlCommand = New SqlCommand()
                sqlcomm.CommandText = sqlString
                sqlcomm.Connection = sqlConn
                sqlcomm.CommandType = Data.CommandType.Text
                sqlcomm.Parameters.Add("UID", Data.SqlDbType.Int, 32).Value = userID
                a = sqlcomm.ExecuteNonQuery()
                sqlcomm.Dispose()
                If a = 1 Then
                    GridView1.DataBind()
                End If
                sqlConn.Close()
            End If
        Next
    End Sub
End Class
