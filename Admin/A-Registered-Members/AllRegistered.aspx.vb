﻿Imports System.Data.SqlClient

Partial Class Admin_A_Registered_Members_AllRegistered
    Inherits System.Web.UI.Page
    Protected Sub btnDownload_Click(sender As Object, e As System.EventArgs) Handles btnDownload.ServerClick
        Utility.DownloadCSV(Utility.EncodeTitle("Registered Members " & DateTime.Now, "-") & ".csv", sdsMember.SelectCommand, Response)
    End Sub
    Protected Sub chkchkApprove_CheckedChanged(sender As Object, e As EventArgs)
        Dim a As Integer
        For Each row As ListViewDataItem In GridView1.Items
            Dim chkBox = DirectCast(row.FindControl("chkApprove"), CheckBox)
            Dim userID = DirectCast(row.FindControl("hdnUID"), HiddenField).Value
            Dim userName = DirectCast(row.FindControl("NameLabel"), Label).Text
            Dim userEmail = DirectCast(row.FindControl("hdnEmail"), HiddenField).Value
            Dim userPassWord = DirectCast(row.FindControl("hdnPassword"), HiddenField).Value
            If chkBox.Checked = False Then
                Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                Dim sqlConn As SqlConnection = New SqlConnection(strConn)
                sqlConn.Open()
                Dim sqlString As String
                sqlString = "Update RegisteredUser Set Approve=0 where RegisterUserID=@UID"
                Dim sqlcomm As SqlCommand = New SqlCommand()
                sqlcomm.CommandText = sqlString
                sqlcomm.Connection = sqlConn
                sqlcomm.CommandType = Data.CommandType.Text
                sqlcomm.Parameters.Add("UID", Data.SqlDbType.Int, 32).Value = userID
                a = sqlcomm.ExecuteNonQuery()
                sqlcomm.Dispose()
                If a = 1 Then
                    GridView1.DataBind()
                End If
                sqlConn.Close()
                'Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                'Dim sqlConn As SqlConnection = New SqlConnection(strConn)
                'sqlConn.Open()
                'Dim sqlString As String
                'sqlString = "Update RegisteredUser Set Approve=1 where RegisterUserID=@UID"
                'Dim sqlcomm As SqlCommand = New SqlCommand()
                'sqlcomm.CommandText = sqlString
                'sqlcomm.Connection = sqlConn
                'sqlcomm.CommandType = Data.CommandType.Text
                'sqlcomm.Parameters.Add("UID", Data.SqlDbType.Int, 32).Value = userID
                'a = sqlcomm.ExecuteNonQuery()
                'sqlcomm.Dispose()
                'If a = 1 Then
                '    GridView1.DataBind()

                'End If
                'sqlConn.Close()
           
            End If
        Next
    End Sub
End Class
