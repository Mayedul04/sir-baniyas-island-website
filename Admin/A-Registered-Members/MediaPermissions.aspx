﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="MediaPermissions.aspx.vb" Inherits="Admin_A_Registered_Members_MediaPermissions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 class="page-title">Handling Permissions</h1>

    <div class="btn-toolbar">
        <a href="../A-Registered-Members/AllRegistered.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
      
        <div class="btn-group">
        </div>
        <asp:Button ID="btnTransfer" runat="server" Text="Set Permission" CssClass="btn btn-primary"  />
    </div>

    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

           
        <div class="well">
            <p>
                <label>Section:</label>
                <asp:DropDownList ID="ddlCat" AutoPostBack="true" runat="server">
                    <asp:ListItem Selected="True">Photo</asp:ListItem>
                    <asp:ListItem>Video</asp:ListItem>
                    <asp:ListItem>News</asp:ListItem>
                    <asp:ListItem>Brochure</asp:ListItem>
                </asp:DropDownList>
                <asp:CheckBox ID="chkAll" runat="server" TextAlign="Left" AutoPostBack="true" Text="Select All" />
            </p>
         
            
            <table class="table">

                <thead>
                    <tr>
                        <th>Section Name</th>
                        <th>Title</th>
                        <th>File Name</th>
                        <th>File Upadated on </th>
                        <th>Access</th>
                        
                    </tr>

                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="AlbumID"
                        DataSourceID="sdsMedia">
                        <EmptyDataTemplate>
                            <table id="Table1" runat="server" style="">
                                <tr>
                                    <td>No data was returned.</td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                <td>
                                    <asp:Label ID="Section" runat="server" Text='<%# Eval("Section")%>' />
                                </td>
                                <td>
                                    <asp:Label ID="File" runat="server" Text='<%# Eval("Title") %>' /><br />
                                    <asp:HiddenField ID="hdnMediaID" Value='<%# Eval("AlbumID") %>' runat="server" />

                                </td>
                               <td>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>' /><br />
                               </td>
                                <td>
                                    <asp:Label ID="LastUpdatedLabel" runat="server"
                                        Text='<%# Convert.ToDateTime(Eval("LastUpdated")).Tostring("MMM dd, yyyy") %>' />
                                </td>

                                 <td>
                             
                                     <asp:CheckBox ID="chkApprove" Enabled="True"  Checked='<%# getStatus(Eval("AlbumID")) %>' runat="server" />
                                </td>
                               
                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>

                            <tr id="itemPlaceholder" runat="server">
                            </tr>

                          <%--  <div class="paginationNew pull-right">
                                <asp:DataPager ID="DataPager1" PageSize="25" runat="server">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False" ShowPreviousPageButton="True" />
                                        <asp:NumericPagerField />
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True" ShowPreviousPageButton="False" />



                                    </Fields>
                                </asp:DataPager>
                            </div>--%>

                        </LayoutTemplate>

                    </asp:ListView>

                </tbody>
            </table>

        </div>
        <!-- Eof content -->
         </ContentTemplate>
        </asp:UpdatePanel>
        <asp:SqlDataSource ID="sdsMedia" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [MediaFiles] WHERE [AlbumID] = @AlbumID" 
            SelectCommand="SELECT * FROM [MediaFiles] where Section=@Section order by LastUpdated Desc ">
            <DeleteParameters>
                <asp:Parameter Name="AlbumID" Type="Int32" />
            </DeleteParameters>
            
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlCat" Name="Section" PropertyName="SelectedValue" />
            </SelectParameters>
            
        </asp:SqlDataSource>
    </div>
</asp:Content>

