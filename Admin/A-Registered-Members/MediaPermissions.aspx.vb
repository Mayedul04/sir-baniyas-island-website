﻿Imports System.Data.SqlClient

Partial Class Admin_A_Registered_Members_MediaPermissions
    Inherits System.Web.UI.Page
    Public fieldname As String = ""
    Public Function getStatus(ByVal mediaid As Integer) As Boolean
        If chkAll.Checked = True Then
            Return True
        Else
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT " & fieldname & "   FROM RegisteredUser where RegisterUserID=@ID"
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("ID", Data.SqlDbType.Int).Value = Request.QueryString("MID")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()
                If reader(fieldname).ToString().Contains("," & mediaid) Then
                    conn.Close()
                    Return True
                Else
                    If reader(fieldname).ToString().Contains(mediaid) Then
                        conn.Close()
                        Return True
                    Else
                        conn.Close()
                        Return False
                    End If
                End If
            Else
                conn.Close()
                Return False
            End If
        End If

    End Function
    
    
    Protected Sub chkAll_CheckedChanged(sender As Object, e As EventArgs) Handles chkAll.CheckedChanged
        GridView1.DataBind()
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
       
        If ddlCat.SelectedValue = "Photo" Then
            fieldname = "PGalleryPermissions"
        ElseIf ddlCat.SelectedValue = "Video" Then
            fieldname = "VGalleryPermissions"
        ElseIf ddlCat.SelectedValue = "News" Then
            fieldname = "NewsPermissions"
        Else
            fieldname = "OthersPermissions"
        End If
        
    End Sub

    Protected Sub btnTransfer_Click(sender As Object, e As EventArgs) Handles btnTransfer.Click
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        If chkAll.Checked = True Then
            Dim permissions As String = ""
            For Each row As ListViewDataItem In GridView1.Items
                Dim chkBox = DirectCast(row.FindControl("chkApprove"), CheckBox)
                Dim MediaID = DirectCast(row.FindControl("hdnMediaID"), HiddenField).Value
                If chkBox.Checked Then
                    If Not Request.Cookies("MPermissions") Is Nothing Then
                        If Request.Cookies("MPermissions").Value <> "" Then
                            Response.Cookies("MPermissions").Value += MediaID + ","
                            Response.Cookies("MPermissions").Expires = Date.Now.AddHours(+1)
                        End If
                    Else
                        Response.Cookies("MPermissions").Value = MediaID + ","
                        Response.Cookies("MPermissions").Expires = Date.Now.AddHours(+1)
                    End If
                Else
                End If
            Next
            Dim selectString1 As String = " Update RegisteredUser Set " & fieldname & "=@Permissions where RegisterUserID=@RegisterUserID  "
            Dim cmd1 As SqlCommand = New SqlCommand(selectString1, conn)
            cmd1.Parameters.Add("RegisterUserID", Data.SqlDbType.Int)
            cmd1.Parameters("RegisterUserID").Value = Request.QueryString("MID")
            cmd1.Parameters.Add("Permissions", Data.SqlDbType.NVarChar)
            cmd1.Parameters("Permissions").Value = Left(Request.Cookies("MPermissions").Value, Request.Cookies("MPermissions").Value.Length - 1)
            Response.Cookies("MPermissions").Value = ""
            Response.Cookies("MPermissions").Expires = Date.Now.AddHours(-1)
            cmd1.ExecuteNonQuery()
        Else
            Dim selectString1 As String = " Update RegisteredUser Set " & fieldname & "=@Permissions where RegisterUserID=@RegisterUserID  "
            Dim cmd1 As SqlCommand = New SqlCommand(selectString1, conn)
            cmd1.Parameters.Add("RegisterUserID", Data.SqlDbType.Int)
            cmd1.Parameters("RegisterUserID").Value = Request.QueryString("MID")
            For Each row As ListViewDataItem In GridView1.Items
                Dim chkBox = DirectCast(row.FindControl("chkApprove"), CheckBox)
                Dim MediaID = DirectCast(row.FindControl("hdnMediaID"), HiddenField).Value
                If chkBox.Checked Then
                    If Not Request.Cookies("MPermissions") Is Nothing Then
                        If Request.Cookies("MPermissions").Value <> "" Then
                            Response.Cookies("MPermissions").Value += MediaID + ","
                            Response.Cookies("MPermissions").Expires = Date.Now.AddHours(+1)
                        End If
                    Else
                        Response.Cookies("MPermissions").Value = MediaID + ","
                        Response.Cookies("MPermissions").Expires = Date.Now.AddHours(+1)
                    End If
                Else
                End If
            Next
            cmd1.Parameters.Add("Permissions", Data.SqlDbType.NVarChar)
            cmd1.Parameters("Permissions").Value = Left(Request.Cookies("MPermissions").Value, Request.Cookies("MPermissions").Value.Length - 1)
            Response.Cookies("MPermissions").Value = ""
            Response.Cookies("MPermissions").Expires = Date.Now.AddHours(-1)
            cmd1.ExecuteNonQuery()

        End If
        
       
        conn.Close()
    End Sub
    Private Sub ApproveUser()
        Dim a As Integer
        Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(strConn)
        sqlConn.Open()
        Dim sqlString As String
        sqlString = "Update RegisteredUser Set Approve=1 where RegisterUserID=@UID"
        Dim sqlcomm As SqlCommand = New SqlCommand()
        sqlcomm.CommandText = sqlString
        sqlcomm.Connection = sqlConn
        sqlcomm.CommandType = Data.CommandType.Text
        sqlcomm.Parameters.Add("UID", Data.SqlDbType.Int, 32).Value = Request.QueryString("MID")
        a = sqlcomm.ExecuteNonQuery()
        sqlcomm.Dispose()
        If a = 1 Then
            Dim selectString1 As String = "Select RegisterUserID from RegisteredUser where RegisterUserID=@RegisterUserID"
            Dim cmd As SqlCommand = New SqlCommand(selectString1, sqlConn)
            cmd.Parameters.Add("RegisterUserID", Data.SqlDbType.Int, 32).Value = Request.QueryString("MID")
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()
                Dim emailtemplate As String = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
                emailtemplate += "<html xmlns=""http://www.w3.org/1999/xhtml"">"
                emailtemplate += "<head><meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />"
                emailtemplate += "<title>Sir Bani Yas</title></head>"
                emailtemplate += "<body style=""margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; font-size: 12px; background-color: #f1f1f1;"">"
                emailtemplate += "<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
                emailtemplate += "<tr><td style=""background-color: #f1f1f1;"">"
                emailtemplate += "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
                emailtemplate += "<tr><td>&nbsp;</td></tr>"
                emailtemplate += "<tr><td><a href=""http://www.sirbaniyasisland.com/"" target=""_blank""><img src=""http://sby.nexadesigns.com/images/banner-img.jpg"" width=""800"" /></a>"
                emailtemplate += "</td></tr><tr><td style=""background-color: #fff;"">"
                emailtemplate += "<table width=""712"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
                emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
                emailtemplate += "<tr><td><h1 style=""color: #f58023; text-align: center; text-transform: uppercase; font-weight: normal"">Congratulation! Here is your Credentials to access on Media Librabry</h1>"
                emailtemplate += "<p style=""color: #333; text-align: center;""><strong>Email :</strong> " & reader("FName").ToString() & "  " & reader("LName").ToString() & "</p>"
                emailtemplate += "<p style=""color: #333; text-align: center;""><strong>Password :</strong>  " & reader("Password").ToString() & "</p>"
                emailtemplate += "</tr><tr><td></td></tr>"
                emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
                emailtemplate += "<tr><td style=""height: 52px; text-align: center;""><a href=""mailto:info@tdic.ae"" style=""color: #f58023"">info@tdic.ae</a> or call us"
                emailtemplate += "800-TDIC (8342)<td></tr></table>"
                emailtemplate += "</td></tr><tr><td>&nbsp;</td></tr></table></td></tr><tr>"
                emailtemplate += "<td style="""">&nbsp;</td></tr></table></body></html>"
                Utility.SendMail("Admin", "noreply@sirbaniyasisland.com", reader("Email").ToString(), "anantaraspa.dirs@anantara.com", "mayedul.islam@wvss.net", "Media Library Password", emailtemplate)
            End If
            reader.Close()
        End If
        sqlConn.Close()
    End Sub
End Class
