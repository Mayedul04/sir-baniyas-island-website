﻿<%@ Page Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" ValidateRequest="false"
    AutoEventWireup="false" ViewStateEncryptionMode="Never" EnableViewStateMac="false"
    CodeFile="Gallery.aspx.vb" Inherits="Gallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Gallery</h1>
    <div class="btn-toolbar">

        <a href="../A-Gallery/AllGallery.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add Gallery"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->
    <div class="well">
        <asp:UpdatePanel ID="UpdatePanel1"  runat="server">
                <ContentTemplate>
                    <div style="border-color:burlywood; border-style:dashed; border-width:thin;padding:10px;">
            <p>
                <label>Parent Gallery Type </label>
                <asp:DropDownList ID="ddPCategory" CssClass="input-xlarge" runat="server" AutoPostBack="true"  AppendDataBoundItems="True" DataSourceID="sdsPCategory" DataTextField="TableName" DataValueField="CatID">
                    <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsPCategory" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [CatID], [TableName] FROM [GCategory] WHERE ([Status] = @Status)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
           </p>
            <p style="float:right; margin-right:335px; margin-top:-75px;">
                <label>Parent Gallery </label>
                <asp:DropDownList ID="ddlPGallery" CssClass="input-xlarge" runat="server"  AppendDataBoundItems="True" DataSourceID="sdsPGallery" DataTextField="Title" DataValueField="GalleryID">
                    <asp:ListItem Text="Make this Parent Gallery" Value="0"></asp:ListItem>
                </asp:DropDownList>
               <asp:SqlDataSource ID="sdsPGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [GalleryID], [Title] FROM [Gallery] WHERE ([CategoryID] = @CategoryID)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddPCategory" Name="CategoryID" PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </p>
                        </div>
            </ContentTemplate>
            </asp:UpdatePanel>
        <div id="myTabContent" class="tab-content">
            
            <p>
                <label>
                    Title</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge"></asp:TextBox>
                <label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" Enabled="false"
                        ControlToValidate="txtTitle" ValidationGroup="form" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                </label>
            </p>
            <p>
                <label>Gallery Category </label>
                <asp:DropDownList ID="ddCategory" CssClass="input-xlarge" runat="server"  AppendDataBoundItems="True" DataSourceID="sdsCategory" DataTextField="TableName" DataValueField="CatID">
                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                        ControlToValidate="ddCategory" validationgroup="form" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                <asp:SqlDataSource ID="sdsCategory" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [CatID], [TableName] FROM [GCategory] WHERE ([Status] = @Status)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </p>
            <p>
                <label>
                    Arabic Title</label>
                <asp:TextBox ID="txtArTitle" runat="server" CssClass="input-xlarge"></asp:TextBox>

            </p>
            <p>
                <label>
                    Small Image (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                <asp:Image ID="imgSmallImage" runat="server" />
            </p>
            <p>
                <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                <label>
                    <asp:RequiredFieldValidator ID="rfvSmallImage" runat="server" Display="Dynamic" ControlToValidate="fuSmallImage"
                        ErrorMessage="* Required"></asp:RequiredFieldValidator>
                </label>
                <asp:HiddenField ID="hdnSmallImage" runat="server" />
            </p>
            <p>
                <label>
                    Image Alt Text</label>
                <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
            </p>
            <p>
                <label>
                    Sort Order:
                </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" Display="Dynamic"
                        SetFocusOnError="true" MinimumValue="1" MaximumValue="999999" runat="server"
                        ErrorMessage="* range from 1 to 999999"></asp:RangeValidator>
                </label>
            </p>
            <p>
                <label>
                    Status:</label>
                <asp:CheckBox ID="chkStatus" CssClass="input-xlarge" runat="server" Checked="true" />
            </p>
            <p>
                <label>
                    Language</label>
                <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                    DataTextField="LangFullName" DataValueField="Lang">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
            </p>
            <asp:HiddenField ID="hdnMasterID" runat="server" />
            <p>
                <label>
                    Last Updated :</label>
                <asp:Label ID="lbLastUpdated" runat="server"></asp:Label>
            </p>
        </div>

        <div class="btn-toolbar">
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">
                <i class="icon-save"></i>Add New</button>

            <div class="btn-group">
            </div>
        </div>

    </div>
    <!-- Eof content -->
    <asp:SqlDataSource ID="SqlDataSourceGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [Gallery] WHERE [GalleryID] = @GalleryID" InsertCommand="INSERT INTO [Gallery] ([CategoryID],[ParentGalleryID], [Title], [SmallImage], [ImageAltText], [SortIndex], [Status], [LastUpdated],MasterID, Lang, ArTitle) VALUES (@CategoryID,@ParentGalleryID, @Title, @SmallImage, @ImageAltText, @SortIndex, @Status, @LastUpdated,@MasterID, @Lang,@ArTitle)"
        SelectCommand="SELECT * FROM [Gallery]" UpdateCommand="UPDATE [Gallery] SET [CategoryID] = @CategoryID, [ParentGalleryID]=@ParentGalleryID, [Title] = @Title, [SmallImage] = @SmallImage, [ImageAltText] = @ImageAltText, [SortIndex] = @SortIndex, [Status] = @Status, [LastUpdated] = @LastUpdated,MasterID=@MasterID, Lang=@Lang , ArTitle=@ArTitle WHERE [GalleryID] = @GalleryID">
        <DeleteParameters>
            <asp:Parameter Name="GalleryID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="ddCategory" Name="CategoryID" Type="Int32" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                Type="Int32" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                Type="Boolean" />
            <asp:ControlParameter ControlID="lbLastUpdated" Name="LastUpdated" PropertyName="Text"
                Type="DateTime" />
            <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
            <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="txtArTitle" Name="ArTitle" PropertyName="Text" />
            <asp:ControlParameter ControlID="ddlPGallery" Name="ParentGalleryID" PropertyName="SelectedValue" />
        </InsertParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="ddCategory" Name="CategoryID" Type="Int32" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                Type="Int32" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                Type="Boolean" />
            <asp:ControlParameter ControlID="lbLastUpdated" Name="LastUpdated" PropertyName="Text"
                Type="DateTime" />
            <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
            <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
            <asp:QueryStringParameter Name="GalleryID" QueryStringField="galleryId" Type="Int32" />
            <asp:ControlParameter ControlID="txtArTitle" Name="ArTitle" PropertyName="Text" />
            <asp:ControlParameter ControlID="ddlPGallery" Name="ParentGalleryID" PropertyName="SelectedValue" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:HiddenField ID="hdnNewID" runat="server" />
    <asp:SqlDataSource ID="SqlDataSourceSEO" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [SEO] ([PageID], [PageType], [SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot],Lang) VALUES (@PageID, @PageType, @SEOTitle, @SEODescription, @SEOKeyWord, @SEORobot,@Lang)">
        <InsertParameters>
            <asp:ControlParameter ControlID="hdnNewID" DefaultValue="" Name="PageID" PropertyName="Value"
                Type="Int32" />
            <asp:Parameter Name="PageType" Type="String" DefaultValue="Gallery" />
            <asp:ControlParameter ControlID="txtTitle" Name="SEOTitle" PropertyName="Text" Type="String" />
            <asp:Parameter Name="SEODescription" Type="String" DefaultValue="" />
            <asp:Parameter Name="SEOKeyWord" Type="String" DefaultValue="" />
            <asp:Parameter Name="SEORobot" Type="Boolean" DefaultValue="true" />
            <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" DefaultValue="en" />
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>
