﻿Imports System.Data.OleDb
Imports System.Drawing.Imaging
Imports System.Data.SqlClient

Partial Class Gallery
    Inherits System.Web.UI.Page
    Protected smallImageWidth As String = "", smallImageHeight As String = "", bigImageWidth As String = "", bigImageHeight As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Utility.GetDimentionSetting("Gallery", "All", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, "", "")

        If IsPostBack = False Then
            If Request.QueryString("CategoryID") <> "" Then
                ddCategory.SelectedValue = Request.QueryString("CategoryID")
            End If
            If Request.QueryString("galleryId") <> "" Then
                Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                Dim sqlConn As SqlConnection = New SqlConnection(strConn)
                sqlConn.Open()
                Dim sqlcomm As SqlCommand = New SqlCommand()
                sqlcomm.CommandText = "SELECT * FROM Gallery where GalleryID=" & Request.QueryString("galleryId")
                sqlcomm.Connection = sqlConn
                sqlcomm.CommandType = Data.CommandType.Text
                Dim reader As SqlDataReader = sqlcomm.ExecuteReader()
                reader.Read()
                If reader("CategoryID") <> 0 Then
                    ddCategory.SelectedValue = reader("CategoryID").ToString()
                End If
                If IsDBNull(reader("ParentGalleryID")) = False Then
                    If reader("ParentGalleryID") <> 0 Then
                        ddPCategory.SelectedValue = getParentCategory(reader("ParentGalleryID").ToString())
                    End If
                    ddlPGallery.SelectedValue = reader("ParentGalleryID").ToString()
                Else
                    'ddlPGallery.Items.Clear()
                    'ddlPGallery.Items.Add("Make Unique Gallery")
                    'ddlPGallery.DataBind()
                End If

                txtTitle.Text = reader("Title").ToString()
                imgSmallImage.ImageUrl = "~/Admin/" + reader("SmallImage").ToString()
                hdnSmallImage.Value = reader("SmallImage").ToString()
                txtSortIndex.Text = reader("SortIndex").ToString()
                chkStatus.Checked = reader("Status").ToString()
                lbLastUpdated.Text = reader("LastUpdated").ToString()
                txtImgAlt.Text = reader("ImageAltText").ToString()
                hdnMasterID.Value = reader("MasterID") & ""
                ddlLang.SelectedValue = reader("Lang") & ""
                txtArTitle.Text = reader("ArTitle").ToString()
                sqlConn.Close()
                imgSmallImage.Visible = True
                rfvSmallImage.Enabled = False
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
            End If

        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.ServerClick
        Try
            lbLastUpdated.Text = DateTime.Now()
            If fuSmallImage.FileName <> "" Then
                hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
                imgSmallImage.ImageUrl = "~/Admin/" + hdnSmallImage.Value
                imgSmallImage.Visible = True
            End If

            Dim a As Integer

            If String.IsNullOrEmpty(Request.QueryString("galleryId")) Or Request.QueryString("new") = 1 Then
                If Request.QueryString("new") <> "1" Then
                    hdnMasterID.Value = GetMasterID()
                End If
                a = SqlDataSourceGallery.Insert()
                hdnNewID.Value = getNewID()
                insertSEO()
                insertArSEO()
                Response.Redirect("AllGallery.aspx")
            Else

                a = SqlDataSourceGallery.Update()
            End If



            If a > 0 Then
                divSuccess.Visible = True
                divError.Visible = False
            Else
                divSuccess.Visible = False
                divError.Visible = True
            End If
        Catch ex As Exception
            divSuccess.Visible = False
            divError.Visible = True
        End Try
    End Sub

    Protected Function GetMasterID() As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT (Max( MasterID)+1) as MaxMasterID  FROM   Gallery "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)


        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = If(reader("MaxMasterID") & "" = "", "1", reader("MaxMasterID") & "")
        End If
        conn.Close()

        Return retVal
    End Function

    Public Function getParentCategory(ByVal galid As Integer) As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT CategoryID  FROM   Gallery Where GalleryID=@GalleryID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = galid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = reader("CategoryID").ToString()
        End If
        conn.Close()
        Return retVal
    End Function
    Private Function getNewID() As String
        Dim newID As String = ""
        Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(strConn)
        sqlConn.Open()
        Dim sqlcomm As SqlCommand = New SqlCommand()
        sqlcomm.CommandText = "SELECT Max(GalleryID) as MaxID FROM Gallery"
        sqlcomm.Connection = sqlConn
        sqlcomm.CommandType = Data.CommandType.Text
        Dim reader As SqlDataReader = sqlcomm.ExecuteReader()
        reader.Read()
        newID = CInt(reader("MaxID").ToString())
        sqlConn.Close()
        Return newID
    End Function

    Private Sub insertSEO()
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID],lang) SELECT top 1  Gallery.Title,Gallery.Title,Gallery.Title,1,'Gallery', Gallery.GalleryID, 'en'  FROM Gallery order by Gallery.GalleryID desc "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.CommandText = selectString
        cmd.ExecuteNonQuery()

        conn.Close()
    End Sub
    Private Sub insertArSEO()
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString As String = ""
        If Request.QueryString("galleryid") Is Nothing Then
            selectString = "INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID],lang) SELECT top 1  Gallery.ArTitle,Gallery.ArTitle,Gallery.ArTitle,1,'Gallery', Gallery.GalleryID, 'ar'  FROM Gallery where GalleryID=" & hdnNewID.Value & " order by Gallery.GalleryID desc "
        Else
            selectString = "INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID],lang) SELECT top 1  Gallery.ArTitle,Gallery.ArTitle,Gallery.ArTitle,1,'Gallery', Gallery.GalleryID, 'ar'  FROM Gallery where GalleryID=" & Request.QueryString("galleryid") & " order by Gallery.GalleryID desc "
        End If
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.CommandText = selectString
        cmd.ExecuteNonQuery()

        conn.Close()
    End Sub
    Protected Sub ddPCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddPCategory.SelectedIndexChanged
        ddlPGallery.Items.Clear()
        ddlPGallery.Items.Add("Make this Parent Gallery")
        ddlPGallery.DataBind()
    End Sub
End Class
