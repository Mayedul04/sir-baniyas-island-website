﻿Imports System.Data.SqlClient

Partial Class Admin_HTMLEdit
    Inherits System.Web.UI.Page

    Protected smallImageWidth As String = "", smallImageHeight As String = "", bigImageWidth As String = "", bigImageHeight As String = "", videoWidth As String = "", videoHeight As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Utility.GetDimentionSetting("HTML", "DelmaIsland", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, videoWidth, videoHeight)
        'HTMLEdit.aspx?hid=1&Title=1&SmallImage=1&BigImage=1&ImageAltText=1&SmallDetails=1&BigDetails=1&SmallImageWidth=100&SmallImageHeight=100&BigImageWidth=200&BigImageHeight=200
        If Not IsPostBack Then
      

                If Not String.IsNullOrEmpty(Request.QueryString("hid")) Then


                    btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
                    lblTabTitle.Text = "Update Fixed Text"
                    LoadContent(Request.QueryString("hid"))
                End If

            End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick


        If fuSmallImage.FileName <> "" Then
            If Request.QueryString("file") <> "1" Then
                If fuSmallImage.FileName <> "" Then
                    hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
                End If
                If hdnSmallImage.Value <> "" Then
                    imgSmallImage.Visible = True
                    imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
                End If
            Else
                hdnSmallImage.Value = Utility.UploadFile(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "Small", Utility.EncodeTitle(txtTitle.Text, "-") & "Small"), Server)
            End If
        End If

        If fuBigImage.FileName <> "" Then
            hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
        End If
        If hdnBigImage.Value <> "" Then
            imgBigImage.Visible = True
            imgBigImage.ImageUrl = "../" & hdnBigImage.Value
        End If


        If fuFileUpload.FileName <> "" Then
            hdnFileupload.Value = Utility.AddOtherFiles(fuFileUpload, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-OtherFiles", Utility.EncodeTitle(txtTitle.Text, "-") & "-OtherFiles"), Server)
        End If
        If hdnFileupload.Value <> "" Then
            ltFileUpload.Visible = True
            ltFileUpload.Text = "../" & ltFileUpload.Text
        End If

        hdnUpdateDate.Value = DateTime.Now.ToString()



        If String.IsNullOrEmpty(Request.QueryString("hid")) Or Request.QueryString("new") = 1 Then
            If Request.QueryString("new") <> "1" Then
                hdnMasterID.Value = GetMasterID()
            End If
            If sdsObject.Insert() > 0 Then
                InsertIntoSEO()
                Response.Redirect("AllHTML.aspx")
            Else
                divError.Visible = True
            End If
        Else
            If sdsObject.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = True
            End If
        End If

    End Sub

    Protected Function GetMasterID() As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT (Max( MasterID)+1) as MaxMasterID  FROM            HTML "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)


        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = If(reader("MaxMasterID") & "" = "", "1", reader("MaxMasterID") & "")
        End If
        conn.Close()

        Return retVal
    End Function


    Protected Sub InsertIntoSEO()
        '; INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID], [Lang]) VALUES (@Title, @SmallDetails, @SmallDetails,1, 'HTMLChild', @PageID, @Lang)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID],lang) SELECT top 1  HTML.Title,HTML.SmallDetails,HTML.SmallDetails,1,'HTML', HTML.HTMLID,HTML.Lang  FROM HTML order by HTML.HTMLID desc "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.CommandText = selectString
        cmd.ExecuteNonQuery()

        conn.Close()
    End Sub



    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID,FileUploaded  FROM            HTML where HTMLID=@HTMLID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("HTMLID", Data.SqlDbType.Int)
        cmd.Parameters("HTMLID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            txtTitle.Text = reader("Title") & ""
            txtSmallDetails.Text = reader("SmallDetails") & ""

            txtDetails.Text = reader("BigDetails") & "" 'hdnDetails.Value

            hdnSmallImage.Value = reader("SmallImage") & ""
            hdnBigImage.Value = reader("BigImage") & ""
            hdnFileupload.Value = reader("FileUploaded") & ""
            imgBigImage.Visible = True
            imgSmallImage.Visible = True
            imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
            imgBigImage.ImageUrl = "../" & hdnBigImage.Value
            ltFileUpload.Visible = True
            ltFileUpload.Text = "../" & hdnFileupload.Value


            hdnUpdateDate.Value = reader("LastUpdated") & ""

            hdnMasterID.Value = reader("MasterID") & ""
            ddlLang.SelectedValue = reader("Lang") & ""

            txtImgAlt.Text = reader("ImageAltText") & ""
        Else
            conn.Close()
            Response.Redirect("AllHTML.aspx")
        End If
        conn.Close()
    End Sub


End Class
