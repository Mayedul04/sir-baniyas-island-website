﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    ViewStateEncryptionMode="Never" EnableViewStateMac="false" CodeFile="AllTopBanner.aspx.vb"
    Inherits="AllTopBanner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Banners</h1>
    <div class="btn-toolbar">
        <button runat="server" id="btnAddNew" class="btn btn-primary">
            <i class="icon-save"></i>Add New</button>
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="All Banners"></asp:Label></h2>
    <div>
        <p>
            <label>
                Language</label>
            <asp:DropDownList ID="ddlLang" runat="server" CssClass="input-xlarge" AutoPostBack="true"
                DataSourceID="sdsLang" DataTextField="LangFullName" DataValueField="Lang">
            </asp:DropDownList>
            <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
        </p>
        <p>
            <label>
                Section</label>
            <asp:DropDownList ID="ddSection" runat="server" CssClass="input-xlarge" AutoPostBack="True">
                  <asp:ListItem Selected="True" Text="Home" Value="Home"></asp:ListItem>
                            <asp:ListItem Text="WildLife" Value="WildLife"></asp:ListItem>
                            <asp:ListItem Text="Arabian WildLife" Value="Arabian_WildLife"></asp:ListItem>
                            <asp:ListItem Text="Animal" Value="Animal"></asp:ListItem>
                            <asp:ListItem Text="Breeding" Value="Breeding"></asp:ListItem>
                            <asp:ListItem Text="Meet Animal" Value="MeetAnimal"></asp:ListItem>
                            <asp:ListItem Text="Vet & Conservation" Value="Vet_Conservation"></asp:ListItem>
                            <asp:ListItem Text="Adventure" Value="Adventure"></asp:ListItem>
                            <asp:ListItem Text="Relax" Value="Relax"></asp:ListItem>
                            <asp:ListItem Text="Spa" Value="Spa"></asp:ListItem>
                            <asp:ListItem Text="Beach & Pool" Value="Beach_Pool"></asp:ListItem>
                            <asp:ListItem Text="Dine" Value="Dine"></asp:ListItem>
                            <asp:ListItem Text="Stay" Value="Stay"></asp:ListItem>
                            <asp:ListItem Text="Travel" Value="Travel"></asp:ListItem>
                            <asp:ListItem Text="Travel by Air" Value="Travel_Air"></asp:ListItem>
                            <asp:ListItem Text="Travel by Road" Value="Travel_Road"></asp:ListItem>
                            <asp:ListItem Text="Travel by Sea" Value="Travel_Sea"></asp:ListItem>
                            <asp:ListItem Text="Desert Island" Value="Desert_Island"></asp:ListItem>
                            <asp:ListItem Text="Sir Bani Yas" Value="Sir_Bani_Yas"></asp:ListItem>
                            <asp:ListItem Text="Sir Bani Yas History" Value="Sir_Bani_Yas_History"></asp:ListItem>
                            <asp:ListItem Text="Delma Island" Value="Delma_Island"></asp:ListItem>
                            <asp:ListItem Text="Discovery Island" Value="Discovery_Island"></asp:ListItem>
                            <asp:ListItem Text="Gallery" Value="Gallery"></asp:ListItem>
                            <asp:ListItem Text="Saltdomes" Value="Saltdomes"></asp:ListItem>
                            <asp:ListItem Text="Conservation" Value="Conservation"></asp:ListItem>
                            <asp:ListItem Text="Mangrove" Value="Mangrove"></asp:ListItem>
                            <asp:ListItem Text="Flora" Value="Flora"></asp:ListItem>
                            <asp:ListItem Text="Photo Gallery" Value="Photo_Gallery"></asp:ListItem>
                            <asp:ListItem Text="Video Gallery" Value="Video_Gallery"></asp:ListItem>
                            <asp:ListItem Text="Experience Gallery" Value="Experience_Gallery"></asp:ListItem>
                            <asp:ListItem Text="Media Centre" Value="Media_Centre"></asp:ListItem>
                            <asp:ListItem Text="Island News" Value="Island_News"></asp:ListItem>
                            <asp:ListItem Text="What's New" Value="Whats_New"></asp:ListItem>
                            <asp:ListItem Text="Press Release" Value="Press_Release"></asp:ListItem>
                            <asp:ListItem Text="Image Library" Value="Image_Library"></asp:ListItem>
                            <asp:ListItem Text="Events" Value="Events"></asp:ListItem>
                            <asp:ListItem Text="Contact Us" Value="Contact_us"></asp:ListItem>
                            <asp:ListItem Text="Historical Timeline" Value="Historical_Timeline"></asp:ListItem>
                            <asp:ListItem Text="Search" Value="Search"></asp:ListItem>
                            <asp:ListItem Text="Success Story" Value="Success_Story"></asp:ListItem>
                            <asp:ListItem Text="Success Story" Value="Success_Story"></asp:ListItem>
                            <asp:ListItem Text="Disclaimer" Value="Disclaimer"></asp:ListItem>
                            <asp:ListItem Text="Interesting Factors" Value="Interesting_Fact"></asp:ListItem>
                            <asp:ListItem Text="Archaelogy" Value="Archaelogy"></asp:ListItem>
                            <asp:ListItem Text="Philosophy" Value="Philosophy"></asp:ListItem>
                            <asp:ListItem Text="Sitemap" Value="Sitemap"></asp:ListItem>
                            <asp:ListItem Text="Privacy" Value="Privacy"></asp:ListItem>
                            <asp:ListItem Text="Terms & Condition" Value="Terms_Condition"></asp:ListItem>
                <asp:ListItem Text="About-TDIC" Value="About_TDIC"></asp:ListItem>
                     <asp:ListItem Text="History" Value="History"></asp:ListItem>
            </asp:DropDownList>
        </p>
        <div class="well">
            <table class="table">
                <thead>
                    <tr>
                        <th>Title
                        </th>
                        <th>Image
                        </th>
                        <th>Sort Order
                        </th>
                        <th>
                            Status
                        </th>
                        <th>Date
                        </th>
                        <th style="width: 40px;"></th>
                    </tr>
                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="BannerID" DataSourceID="SqlDataSourceBanner">
                        <EmptyDataTemplate>
                            <table id="Table1" runat="server" style="">
                                <tr>
                                    <td>No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                <td>
                                    <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                                </td>
                                <td>
                                    <img src='<%# "../"& Eval("SmallImage") %>' alt="Edit" width="100px" />
                                </td>
                                <td>
                                    <asp:Label ID="SortIndex" runat="server" Text='<%# Eval("Sortindex") %>' />
                                </td>
                                <td>
                                    <asp:CheckBox ID="CheckBox1" Enabled="false" Checked='<%# Eval("Status") %>' runat="server" />
                                    
                                </td>
                                <td>
                                    <asp:Label ID="LastUpdatedLabel" runat="server" Text='<%# Convert.ToDateTime(Eval("LastUpdated")).Tostring("MMM dd, yyyy") %>' />
                                </td>
                                <td>
                                    <a href='<%# "TopBanner.aspx?bannerId=" & Eval("BannerID") %>' title="Edit"><i class="icon-pencil"></i></a>&nbsp <a href='<%# "#" & Eval("BannerID") %>' data-toggle="modal"><i class="icon-remove"></i></a>
                                    <div class="modal small hide fade" id='<%# Eval("BannerID") %>' tabindex="-1" role="dialog"
                                        aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                ×</button>
                                            <h3 id="myModalLabel">Delete Confirmation</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p class="error-text">
                                                <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">
                                                Cancel</button>
                                            <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                                Text="Delete" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <div class="paginationNew pull-right">
                                <asp:DataPager ID="DataPager1" PageSize="10" runat="server">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                            ShowPreviousPageButton="True" />
                                        <asp:NumericPagerField />
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                            ShowPreviousPageButton="False" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </LayoutTemplate>
                    </asp:ListView>
                </tbody>
            </table>
            <%--<asp:GridView ID="GridView1" runat="server" DataKeyNames="BannerID" DataSourceID="SqlDataSourceBanner"
                SkinID="grid">
                <Columns>
                    <asp:BoundField DataField="BannerID" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                        SortExpression="BannerID" />
                    <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
                    <asp:TemplateField HeaderText="Image">
                        <ItemTemplate>
                            <asp:Image Width="100" Height="100" ID="imgThum" runat="server" ImageUrl='<%# "~/Admin/"+Eval("BigImage") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="SortIndex" HeaderText="Sort Order" SortExpression="SortIndex" />
                   <asp:CheckBoxField DataField="Status" HeaderText="Status" 
                        SortExpression="Status" />
                    <asp:BoundField DataField="LastUpdated" DataFormatString="{0:d}" HeaderText="Last Updated"
                        SortExpression="LastUpdated" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="linkEdit" Text="&lt;img src='../assets/images/icons/edit.jpg' alt='Update' border='0'/&gt;"
                                runat="server" NavigateUrl='<%# GetUrl(Eval("BannerID"))%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ValidationGroup="del" runat="server" ID="DeleteButton" CommandName="delete"
                                ImageUrl="../assets/images/icons/delete.png" OnClientClick="if (!window.confirm('Do you want to delete this record?')) return false;" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>--%>
        </div>
        <!-- Eof content -->
        <asp:SqlDataSource ID="SqlDataSourceBanner" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [Banner] WHERE [BannerID] = @BannerID" SelectCommand="SELECT * FROM [Banner] where SectionName=@SectionName and Lang=@Lang order by SortIndex">
            <DeleteParameters>
                <asp:Parameter Name="BannerID" Type="Int32" />
            </DeleteParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="ddSection" Name="SectionName" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>
