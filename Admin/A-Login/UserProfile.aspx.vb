﻿Imports System.Data.SqlClient

Partial Class Admin_UserProfile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadUser()
        End If
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.ServerClick
        divSuccess.Visible = False
        divError.Visible = False
        hdnLastUpdated.Value = DateTime.Now
        lblMessage.Text = ""
        txtEmail.Text = txtEmail.Text.Trim()
        txtUserID.Text = txtUserID.Text.Trim()
        txtUserName.Text = txtUserName.Text.Trim()

        'check whether the new user id  is existed
        If hdnUserID.Value.ToLower() <> txtUserID.Text.Trim().ToLower() Then
            If IsIDExists(txtUserID.Text) Then
                lblMessage.Text = "The ID is already exists. Please try another ID."
                lblMessage.ForeColor = Drawing.Color.Red
                Exit Sub
            End If
        End If

        'if a new email is inserted, check whether the email is existed
        If hdhPrevEmail.Value.ToLower() <> txtEmail.Text.Trim().ToLower() Then
            If IsEmailExists(txtEmail.Text) Then
                lblMessage.Text = "The email is already exists. Please try another email."
                lblMessage.ForeColor = Drawing.Color.Red
                Exit Sub
            End If
        End If

        'if the password is inserted here then save it to hdnPasswort to update the database field
        If txtPassword.Text <> "" Then
            If System.Text.RegularExpressions.Regex.IsMatch(txtPassword.Text, "^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$") = True Then
                If IsNotSequentialChars(txtUserID.Text.Trim(), txtPassword.Text, 3) = True And IsNotSequentialChars(txtUserName.Text.Replace(" ", "").Trim(), txtPassword.Text, 3) = True Then
                    If txtPassword.Text <> txtRetypePassword.Text Then
                        lblMessage.Text = "Password not matched"
                        lblMessage.ForeColor = Drawing.Color.Red
                        Exit Sub
                    Else
                        hdnPassword.Value = txtPassword.Text
                    End If
                Else
                    lblMessage.Text = "Password should not contain parts of the user name or login id that exceed 2 consecutive characters (ex: part of admin like adm, dmi, min etc)"
                    lblMessage.ForeColor = Drawing.Color.Red
                    Exit Sub
                End If
            Else
                lblMessage.Text = "Password must contain: Minimum 8 characters atleast 1 Alphabet, 1 Number and 1 Special Character"
                lblMessage.ForeColor = Drawing.Color.Red
                Exit Sub
            End If
        End If


        Try
            sdsAdminPanelLogin.Update()
            divSuccess.Visible = True
        Catch ex As Data.SqlClient.SqlException
            divError.Visible = True
        End Try
    End Sub
    Public Function IsNotSequentialChars(ByVal Src As String, ByVal Dest As String, ByVal check_len As Integer) As Boolean
        If check_len < 1 OrElse Src.Length < check_len Then
            Return True
        End If
        Dim m As Match = Regex.Match(Src, "(?=(.{" + check_len.ToString() + "})).")
        Dim bOK As Boolean = m.Success
        While bOK AndAlso m.Success
            bOK = Not Regex.Match(Dest, "(?i)" + Regex.Escape(m.Groups(1).Value)).Success
            'If Not bOK Then
            '    Console.WriteLine("Destination contains " + check_len + " sequential source letter(s) '" + m.Groups(1).Value + "'")
            'End If
            m = m.NextMatch()
        End While
        Return bOK
    End Function
    Private Function IsEmailExists(ByVal email As String) As Boolean
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim FResortName = ""
        Dim selectString = "SELECT * from AdminPanelLogin where email=@email "
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("email", Data.SqlDbType.VarChar, 100).Value = email
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        Dim retVal As Boolean = False
        retVal = reader.HasRows
        conn.Close()

        Return retVal
    End Function
    Private Function IsIDExists(ByVal UN_1 As String) As Boolean
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim FResortName = ""
        Dim selectString = "SELECT * from AdminPanelLogin where UN_1=@UN_1 "
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("UN_1", Data.SqlDbType.VarChar, 100).Value = UN_1
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        Dim retVal As Boolean = False
        retVal = reader.HasRows
        conn.Close()

        Return retVal
    End Function
    Private Sub LoadUser()
        'If Not Request.Cookies("userName") Is Nothing Then
        If Not Request.QueryString("uid") Is Nothing Then
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim FResortName = ""
            Dim selectString = "SELECT * from AdminPanelLogin where UserID= @UserID"
            Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
            cmd.Parameters.Add("UserID", Data.SqlDbType.VarChar).Value = Request.QueryString("uid")
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()
                txtEmail.Text = reader("Email") & ""
                hdhPrevEmail.Value = txtEmail.Text
                txtPassword.Text = reader("PS_1") & ""
                hdnPassword.Value = reader("PS_1") & ""
                txtRetypePassword.Text = reader("PS_1") & ""
                txtUserName.Text = reader("Title") & ""
                txtUserID.Text = reader("UN_1") & ""
                hdnUserID.Value = reader("UN_1") & ""
                hdnCreatedBy.Value = reader("CreatedBy") & ""
                hdnRole.Value = reader("Role") & ""
            End If
            conn.Close()
            btnSubmit.InnerHtml = "<i class=""icon-save""></i> Update"
        Else
            Response.Redirect("Login.aspx")
        End If
    End Sub
End Class
