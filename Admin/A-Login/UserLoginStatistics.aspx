﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="UserLoginStatistics.aspx.vb" Inherits="UserLoginStatistics" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">
        All Login Statistics</h1>
    <div class="btn-toolbar">
        <a href="../A-Login/AllUser.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="All Login Statistics"></asp:Label></h2>
    <div>
        <div class="well">
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            User ID
                        </th>
                        <th>
                            IP Address
                        </th>
                        <th>
                            Country
                        </th>
                        <th>
                            City
                        </th>
                        <th>
                            Login Time
                        </th>
                        <th>
                            Logout Time
                        </th>
                        <th>
                            Login Status
                        </th>
                        <%--<th style="width: 60px;">
                        </th>--%>
                    </tr>
                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="AuditLogID" DataSourceID="sdsAuditLog">
                        <EmptyDataTemplate>
                            <table id="Table1" runat="server" style="">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                <td>
                                    <%# Eval("UserID") %>
                                </td>
                                <td>
                                    <%# Eval("IPAddress")%>
                                </td>
                                <td>
                                    <%# Eval("Country")%>
                                </td>
                                <td>
                                    <%# Eval("City")%>
                                </td>
                                <td>
                                    <%# getFormatedTime(Eval("LoginDateTime"))%>
                                </td>
                                <td>
                                    <%# getFormatedTime(Eval("LogoutDateTime"))%>
                                </td>
                                <td>
                                   <%# getFormatedStatus(Eval("LoginStatus"))%> 
                                </td>
                                <%--<td>
                                    <a href='<%# "#" & Eval("AuditLogID") %>' data-toggle="modal"><i class="icon-remove">
                                    </i></a>
                                    <div class="modal small hide fade" id='<%# Eval("AuditLogID") %>' tabindex="-1" role="dialog"
                                        aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                ×</button>
                                            <h3 id="myModalLabel">
                                                Delete Confirmation</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p class="error-text">
                                                <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">
                                                Cancel</button>
                                            <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                                Text="Delete" />
                                        </div>
                                    </div>
                                </td>--%>
                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <div style="" class="paginationNew pull-right ">
                                <asp:DataPager ID="DataPager1" PageSize="20" runat="server">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                            ShowPreviousPageButton="True" />
                                        <asp:NumericPagerField />
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                            ShowPreviousPageButton="False" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </LayoutTemplate>
                    </asp:ListView>
                </tbody>
            </table>
        </div>
        <asp:SqlDataSource ID="sdsAuditLog" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="Select * from AuditLog order by LoginDateTime desc" DeleteCommand="delete from AuditLog where AuditLogID=@AuditLogID">
            <DeleteParameters>
                <asp:Parameter Name="AuditLogID" />
            </DeleteParameters>
        </asp:SqlDataSource>
    </div>
    <!-- Eof content -->
</asp:Content>
