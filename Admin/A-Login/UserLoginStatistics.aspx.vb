﻿Imports System.Data.SqlClient
Imports System.Collections.ObjectModel
Imports System.Net
Imports System.Web.Script.Serialization

Partial Class UserLoginStatistics
    Inherits System.Web.UI.Page

    Public Function getFormatedTime(ByVal LoginDateTime As Object) As String
        Dim retDateTime As String = ""
        If Not String.IsNullOrEmpty(LoginDateTime.ToString()) Then
            Dim timezone As String = ""
            Try
                Dim ipAddress As String = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
                If String.IsNullOrEmpty(ipAddress) Then
                    ipAddress = Request.ServerVariables("REMOTE_ADDR")
                End If
                Dim APIKey As String = "53a364977727650a5e9933d438b13217e954655cac05ff82ebcc877aedc7f00a"
                Dim url As String = String.Format("http://api.ipinfodb.com/v3/ip-city/?key={0}&ip={1}&format=json", APIKey, ipAddress)
                Using client As New WebClient()
                    Dim json As String = client.DownloadString(url)
                    Dim location As Location = New JavaScriptSerializer().Deserialize(Of Location)(json)
                    timezone = location.TimeZone.ToString().Split(":")(0)
                End Using
            Catch ex As Exception

            End Try
            Dim serverTime As DateTime = LoginDateTime.ToString()
            retDateTime = String.Format("{0:MMM dd yyyy hh:mm tt}", serverTime.AddHours(timezone))
        End If

        Return retDateTime
    End Function
    Public Function getFormatedStatus(ByVal LoginStatus As Object) As String
        If Not String.IsNullOrEmpty(LoginStatus.ToString()) Then
            If LoginStatus = "Success" Then
                Return "<font color=""green"">Success</font>"
            ElseIf LoginStatus = "Failed" Then
                Return "<font color=""red"">Failed</font>"
            Else
                Return ""
            End If
        End If
        Return ""
    End Function
    Public Class Location
        Public Property IPAddress() As String
            Get
                Return m_IPAddress
            End Get
            Set(ByVal value As String)
                m_IPAddress = value
            End Set
        End Property
        Private m_IPAddress As String
        Public Property CountryName() As String
            Get
                Return m_CountryName
            End Get
            Set(ByVal value As String)
                m_CountryName = value
            End Set
        End Property
        Private m_CountryName As String
        Public Property CityName() As String
            Get
                Return m_CityName
            End Get
            Set(ByVal value As String)
                m_CityName = value
            End Set
        End Property
        Private m_CityName As String

        Public Property TimeZone() As String
            Get
                Return m_TimeZone
            End Get
            Set(ByVal value As String)
                m_TimeZone = value
            End Set
        End Property
        Private m_TimeZone As String
    End Class
End Class
