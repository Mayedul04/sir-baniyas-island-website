﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    ViewStateEncryptionMode="Never" EnableViewStateMac="false" CodeFile="AllGallery.aspx.vb"
    Inherits="AllGallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1 class="page-title">Video Gallery</h1>

   <div class="btn-toolbar">
    <button runat="server" id ="btnAddNew"  class="btn btn-primary"><i class="icon-save"></i> Add New</button>
        <div class="btn-group">
            <input  type="button" value="Back" class="btn btn-primary" onclick="window.history.go(-1); return false;" />
           
      </div>
     </div>

    <!-- content -->

        <h2>
            <asp:Label ID="lblTabTitle" runat="server" Text="All Video Gallery Album"></asp:Label></h2>
        <div>

      <%--   <p>
        <Label>Language</Label>
                    <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" AutoPostBack ="true"  CssClass ="input-xlarge"   DataTextField="LangFullName" DataValueField="Lang"> </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsLang" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                        SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
                    </asp:SqlDataSource>
         </p>--%>


        <%-- <p>
           <Label> Section </Label>
            <asp:DropDownList ID="ddCategory" CssClass ="input-xlarge"  runat="server" Enabled="false"  AutoPostBack="True">
                <asp:ListItem Text="All" Value="0" Selected="True"></asp:ListItem>


            </asp:DropDownList>
         </p>--%>


          <div class="well">


                          <asp:ListView ID="ListView1" runat="server" DataKeyNames="GalleryID" 
        DataSourceID="SqlDataSourceGallery">
    
        <EmptyDataTemplate>
            No data was returned.
        </EmptyDataTemplate>
 
        <ItemTemplate>
            <li class="thumbnail">
							<%--<img class="grayscale" src="http://placehold.it/140x140/eee" alt="Sample Image 1">--%>
                          <img width="120px" src='<%# If(IsDBNull(Eval("SmallImage")), "../Content/Noimages.jpg", "../" & Eval("SmallImage"))%>' />
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("Title") %>' />
                            <br />
                             <br />
							   <a href='<%# "Gallery.aspx?galleryId=" & Eval("GalleryID") %>' title="Edit"><i class="icon-pencil"></i> </a>
                            &nbsp
                              <a href='<%# "Gallery.aspx?galleryId=" & Eval("GalleryID")& "&new=1" %>' title="New Lang" style='<%# if(Eval("Lang")="en","","display:none;") %>'><i class="icon-plus-sign" ></i> </a>
                            &nbsp
                            <a href='<%# "AllGalleryItem.aspx?galleryId=" & Eval("GalleryID") %>' title="View/Add Gallery Item"><i class="icon-list"></i> </a>
                            &nbsp
                                <a href='<%# "#" & Eval("GalleryID") %>' data-toggle="modal"><i class="icon-remove"></i></a>

                                <div class="modal small hide fade" id='<%# Eval("GalleryID") %>' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel">Delete Confirmation</h3>
                                  </div>
                                  <div class="modal-body">
    
                                    <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                  </div>
                                  <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                     <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete"  runat="server" Text="Delete" />
                                  
                                  </div>
                                </div>
           </li>





        </ItemTemplate>
        <LayoutTemplate>
            <ul ID="itemPlaceholderContainer" runat="server" class="thumbnails gallery">
                <li runat="server" id="itemPlaceholder" />
            </ul>
            <div style="">
            </div>
        </LayoutTemplate>

    </asp:ListView>

            </div> 



    <!-- Eof content -->
    <asp:SqlDataSource ID="SqlDataSourceGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [Video] WHERE [GalleryID] = @GalleryID" 
                SelectCommand="SELECT Video.* FROM [Video] INNER JOIN Languages ON Video.Lang = Languages.Lang where ParentGalleryID=@PGalleryID ORDER BY Video.MasterID DESC, Languages.SortIndex" 
                ProviderName="System.Data.SqlClient">
        <DeleteParameters>
            <asp:Parameter Name="GalleryID" Type="Int32" />
        </DeleteParameters>
       <SelectParameters>
            <asp:QueryStringParameter Name="PGalleryID" QueryStringField="pgid" DefaultValue="0" />
        </SelectParameters>
    </asp:SqlDataSource>

    
        </div>
       


  
    <!-- Eof content -->



</asp:Content>
