﻿
Partial Class AllGalleryItem
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsDipestGallery() = False Then
            Response.Redirect("AllGallery.aspx?pgid=" & Request.QueryString("galleryid"))
        End If
    End Sub

    Public domainName As String
    Public Function IsDipestGallery() As Boolean
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT GalleryID  FROM   Gallery Where ParentGalleryID=@PGalleryID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("PGalleryID", Data.SqlDbType.Int, 32).Value = Request.QueryString("galleryid")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            conn.Close()
            Return False
        Else
            conn.Close()
            Return True
        End If
    End Function
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Protected Sub btnAddNew_Click(sender As Object, e As System.EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("GalleryItem.aspx?galleryId=" & Request.QueryString("galleryId") & "")
    End Sub
End Class
