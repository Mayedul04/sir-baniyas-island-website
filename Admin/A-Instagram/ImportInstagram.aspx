﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="ImportInstagram.aspx.vb" Inherits="Admin_A_Instagram_InportInstagram" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript">
        $(window).load(function () {
            var d = new Date();
            var type = window.location.hash.substr(1);
            document.cookie = "InstaAccessTocken=" + type;
            //  alert(expires);+ "; " + expires
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">
        Instagram</h1>
    <div class="btn-toolbar">
        <%--<asp:LinkButton ID="btnDownload" runat="server" CssClass="btn btn-primary" ><i class='icon-save'></i> Export</asp:LinkButton>--%>
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="Label1" runat="server" Text="Import Image From Instagram"></asp:Label></h2>
    <div>
        <div class="well">
            
            <p>
                <strong>Has codes(Coma separeted) : </strong>
                <asp:TextBox ID="lblHashCode" runat="server"></asp:TextBox>
                <asp:Button ID="btnExport" CssClass="btn btn-primary" Text="Import Image From Instagram"
                    runat="server" />
            </p>
             <p>
                                   
                 <%--<strong>Restaurant : </strong>
                                    <asp:DropDownList ID="ddlRestaurant" runat="server" DataSourceID="SqlDataSource1" CssClass="input-xlarge"
                                        DataTextField="Title" DataValueField="RestaurantID" AutoPostBack="true" >
                                      
                                    </asp:DropDownList>

                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                        SelectCommand="SELECT [RestaurantID], [Title] FROM [List_Restaurant] where  status='1' ORDER BY [SortIndex]">
                                    </asp:SqlDataSource>--%>

                  <asp:Button ID="btnTransfer" runat="server" Text="Transfer" CssClass="btn btn-primary" OnClick="btnTransfer_Click" />
                                </p>
            <div class="btn-toolbar">
            
            </div>
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
            <div class="success-details" visible="false" id="div1" runat="server">
                <asp:Label ID="Label2" runat="server" Text="Operation is done"></asp:Label>
                <div class="corners">
                    <span class="success-left-top"></span><span class="success-right-top"></span><span
                        class="success-left-bot"></span><span class="success-right-bot"></span>
                </div>
            </div>
            <div class="error-details" id="div2" visible="false" runat="server">
                <asp:Label ID="Label5" runat="server" Text="There is an error, Please try again later"></asp:Label>
                <div class="corners">
                    <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
                    </span><span class="error-right-bot"></span>
                </div>
            </div>
            <asp:ListView ID="ListView1" runat="server" DataSourceID="sdsInstagram" DataKeyNames="ImportID">
                <EmptyDataTemplate>
                    <table runat="server" style="">
                        <tr>
                            <td>
                                No data was returned.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:CheckBox ID="chkSelect" runat="server" />
                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ImportID")%>' />
                            <asp:Label ID="Name" runat="server" Text='<%# Eval("InstaUser")%>' />
                        </td>
                        <td>
                            <asp:TextBox ID="TitleLabel" TextMode="MultiLine" Rows="3" Text='<%#  Eval("Title").toString()%>' Width="150" runat="server"></asp:TextBox>
                            
                        </td>
                        <td>
                         <%--   <img src='<%# "../" & Eval("SmallImage") %>' width="120" />--%>
                             <img src='<%#  Eval("SmallImage") %>' width="120" />
                            <asp:HiddenField ID="hdnimagelink" Value='<%#  Eval("SmallImage") %>' runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="SortIndexLabel" runat="server" Text='<%# Eval("SortIndex") %>' />
                        </td>
                        <td>
                            <asp:CheckBox ID="StatusCheckBox" runat="server" Checked='<%# Eval("Status") %>'
                                Enabled="false" />
                        </td>
                        <td>
                            <asp:Label ID="LastUpdatedLabel" runat="server" Text='<%# Eval("LastUpdated","{0:dd MMM yyyy}") %>' />
                        </td>
                        <td>
                            <a href='<%# "#" & Eval("ImportID") %>' data-toggle="modal"><i class="icon-remove"></i>
                            </a>
                            <div class="modal small hide fade" id='<%# Eval("ImportID") %>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">
                                        Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    <table class="table" border="0" style="">
                        <thead>
                            <tr id="Tr1" runat="server" style="">
                                <th id="Th1" runat="server">
                                    Uploaded By
                                </th>
                                <th id="Th3" runat="server">
                                    Title
                                </th>
                                <th id="Th4" runat="server">
                                    SmallImage
                                </th>
                                <th id="Th6" runat="server">
                                    Sort Order
                                </th>
                                <th id="Th7" runat="server">
                                    Status
                                </th>
                                <th id="Th8" runat="server">
                                    Last Updated
                                </th>
                                <th>
                                    Actions
                                </th>
                            </tr>
                        </thead>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <tr>
                            <td>
                               
                            </td>
                            <td colspan="2">
                               
                            </td>
                            
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <div class="paginationNew pull-right">
                            <asp:DataPager ID="DataPager1" runat="server" PageSize="20">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                                        ShowPreviousPageButton="False" />
                                    <asp:NumericPagerField />
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                                        ShowPreviousPageButton="False" />
                                </Fields>
                            </asp:DataPager>
                        </div>
                    </table>
                    
                </LayoutTemplate>
            </asp:ListView>
            <asp:SqlDataSource ID="sdsInstagram" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                SelectCommand="SELECT [ImportID],[ImageID],[Title],[SmallImage],[BigImage], [InstaUser] ,[SortIndex],[Status],[LastUpdated] FROM Instagram  "
                DeleteCommand="DELETE FROM [Instagram] WHERE [ImportID] = @ImportID" InsertCommand="INSERT INTO [Instagram] (ImageID, [Title],s [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated]) VALUES (@ImageID, @Title, @SmallImage, @BigImage, @SortIndex, @Status, @LastUpdated)">
                <DeleteParameters>
                    <asp:Parameter Name="ImportID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="AlbumID" Type="Int32" />
                    <asp:Parameter Name="Title" Type="String" />
                    <asp:Parameter Name="SmallImage" Type="String" />
                    <asp:Parameter Name="BigImage" Type="String" />
                    <asp:Parameter Name="SortIndex" Type="Int32" />
                    <asp:Parameter Name="Status" Type="Boolean" />
                    <asp:Parameter Name="LastUpdated" Type="DateTime" />
                </InsertParameters>
                <SelectParameters>
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
