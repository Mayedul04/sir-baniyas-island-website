﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="InstagramImages.aspx.vb" Inherits="Admin_A_Instagram_ImstegramImages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 class="page-title">
        Instagram</h1>
    <div class="btn-toolbar">
        <%--<asp:LinkButton ID="btnDownload" runat="server" CssClass="btn btn-primary" ><i class='icon-save'></i> Export</asp:LinkButton>--%>
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text="Selected Instagram Images"></asp:Label></h2>
    <div>
        <div class="well">
           
            <div class="success-details" visible="false" id="div1" runat="server">
                <asp:Label ID="Label2" runat="server" Text="Operation is done"></asp:Label>
                <div class="corners">
                    <span class="success-left-top"></span><span class="success-right-top"></span><span
                        class="success-left-bot"></span><span class="success-right-bot"></span>
                </div>
            </div>
            <div class="error-details" id="div2" visible="false" runat="server">
                <asp:Label ID="Label5" runat="server" Text="There is an error, Please try again later"></asp:Label>
                <div class="corners">
                    <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
                    </span><span class="error-right-bot"></span>
                </div>
            </div>

               <p>
                                   
                 

                
                                </p>

            <asp:ListView ID="ListView1" runat="server" DataSourceID="sdsInstagram" DataKeyNames="ImageID">
                <EmptyDataTemplate>
                    <table id="Table1" runat="server" style="">
                        <tr>
                            <td>
                                No data was returned.
                            </td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Label ID="lblID" runat="server" Text='<%# Eval("InstaUser")%>' />
                        </td>
                        <td>
                        
                            <asp:Label ID="TitleLabel" runat="server" Text='<%#  Mid(Eval("Title").toString(), 1, 50)%>' />
                        </td>
                        <td>
                            <img src='<%# "../" & Eval("SmallImage") %>' width="120" />
                        </td>
                        <td>
                            <asp:Label ID="SortIndexLabel" runat="server" Text='<%# Eval("SortIndex") %>' />
                        </td>
                        <td>
                            <asp:CheckBox ID="StatusCheckBox" runat="server" Checked='<%# Eval("Status") %>'
                                Enabled="false" />
                        </td>
                        <td>
                            <asp:Label ID="LastUpdatedLabel" runat="server" Text='<%# Eval("LastUpdated","{0:dd MMM yyyy}") %>' />
                        </td>
                        <td>
                            <a href='<%# "#" & Eval("ImageID") %>' data-toggle="modal"><i class="icon-remove"></i>
                            </a>
                            <div class="modal small hide fade" id='<%# Eval("ImageID") %>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">
                                        Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    <table class="table" border="0" style="">
                        <thead>
                            <tr id="Tr1" runat="server" style="">
                                <th id="Th1" runat="server">
                                    Uploaded By
                                </th>
                                <th id="Th3" runat="server">
                                    Title
                                </th>
                                <th id="Th4" runat="server">
                                    SmallImage
                                </th>
                                <th id="Th6" runat="server">
                                    Sort Order
                                </th>
                                <th id="Th7" runat="server">
                                    Status
                                </th>
                                <th id="Th8" runat="server">
                                    Last Updated
                                </th>
                                <th>
                                    Actions
                                </th>
                            </tr>
                        </thead>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        
                        <div class="paginationNew pull-right">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" ShowNextPageButton="False"
                                        ShowPreviousPageButton="False" />
                                    <asp:NumericPagerField />
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" ShowNextPageButton="False"
                                        ShowPreviousPageButton="False" />
                                </Fields>
                            </asp:DataPager>
                        </div>
                    </table>
                    
                </LayoutTemplate>
            </asp:ListView>
            <asp:SqlDataSource ID="sdsInstagram" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                SelectCommand="SELECT [ImageID], [Title],[SmallImage],[BigImage],[SortIndex],[Status], [InstaUser], [LastUpdated] FROM [InstagramImage]"
                DeleteCommand="DELETE FROM [InstagramImage] WHERE [ImageID] = @ImageID" 
                InsertCommand="INSERT INTO [InstagramImage] (ImageID,  [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated]) VALUES (@ImageID,  @Title, @SmallImage, @BigImage, @SortIndex, @Status, @LastUpdated)"
                >
                <DeleteParameters>
                    <asp:Parameter Name="ImageID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Title" Type="String" />
                    <asp:Parameter Name="SmallImage" Type="String" />
                    <asp:Parameter Name="BigImage" Type="String" />
                    <asp:Parameter Name="SortIndex" Type="Int32" />
                    <asp:Parameter Name="Status" Type="Boolean" />
                    <asp:Parameter Name="LastUpdated" Type="DateTime" />
                </InsertParameters>
                <%--<SelectParameters>
                    <asp:ControlParameter ControlID ="ddlRestaurant" Name="RestaurantID" PropertyName ="SelectedValue" Type="Int32"  />   
                </SelectParameters>--%>
            </asp:SqlDataSource>
        </div>
    </div>



  
</asp:Content>

