﻿Imports System.Data.SqlClient
Imports System.ServiceModel.Syndication
Imports System.Net
Imports System.Web.Script.Serialization

Partial Class Admin_A_Instagram_InportInstagram
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Request.Cookies("FlymeHashtags") Is Nothing Then
                lblHashCode.Text = Request.Cookies("FlymeHashtags").Value
                Dim sConn As String
                sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
                Dim cn As SqlConnection = New SqlConnection(sConn)
                cn.Open()
                Dim selectString As String = "delete from Instagram  "
                Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
                cmd.ExecuteNonQuery()
                cn.Close()

                Dim hastags As String() = Request.Cookies("FlymeHashtags").Value.Split(New Char() {","}, StringSplitOptions.RemoveEmptyEntries)
                Response.Cookies("FlymeHashtags").Value = "FlymeHashtags"
                Response.Cookies("FlymeHashtags").Expires = Date.Now.AddDays(-1)
                For Each elem In hastags
                    elem = elem.Replace("#", "")
                    If Not elem.Contains(" ") Then
                        ReadInstegram(elem.Trim())
                    Else
                        lblMessage.Text = "'" & elem & "' is not a valid hash tag word. Please don't put space."
                    End If
                Next
            End If
        End If

    End Sub

    Dim HashCode As String
    Public Sub ReadInstegram(ByVal hasCode As String)
        Try
            Dim sConn As String
            sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
            Dim cn As SqlConnection = New SqlConnection(sConn)
            cn.Open()

            Dim count As Integer = 1
            Dim objUser As Object
            Dim strusername As String
            Dim objImageTypes As Object
            Dim objname As Object
            Dim strImageTypes As String
            Dim strname As String
            Dim objCaptions As Object
            Dim struid As String
            Dim strlink As String
            Dim strtype As String
            Dim posteddate As DateTime
            Dim objText As Object
            Dim strimageurl As String
            Dim strTitle As String
            Dim strCaptions As String
            Dim objLikes As Object
            Dim strLikes As String
            Dim strlcount As String
            Dim client As WebClient = New WebClient()
            Dim instance As JavaScriptSerializer = New JavaScriptSerializer()
            Dim jsonString As String = client.DownloadString("https://api.instagram.com/v1/tags/" & hasCode & "/media/recent?" & Request.Cookies("InstaAccessTocken").Value & "&count=30")
            Dim json As Object = instance.DeserializeObject(jsonString)
            Dim ObjStatus As Object = json("data")
            Dim strstatus As String = instance.Serialize(ObjStatus)
            ''   Dim faceBookUser As InstagramEntry = New JavaScriptSerializer().Deserialize(Of InstagramEntry)(strstatus)
            Dim objMeta As Object = instance.DeserializeObject(strstatus)
            For Each meta In objMeta
                strlink = meta("link")
                strtype = meta("type")
                ''  posteddate = UnitType(meta("created_time"))

                ''User
                objUser = meta("user")
                strusername = instance.Serialize(objUser)
                objname = instance.DeserializeObject(strusername)
                strname = objname("username")
                struid = objname("id")

                ''Images
                objImageTypes = meta("images")
                strImageTypes = instance.Serialize(objImageTypes)
                objImageTypes = instance.DeserializeObject(strImageTypes)
                If strImageTypes.Contains("standard_resolution") = True Then
                    objImageTypes = objImageTypes("standard_resolution")
                    strImageTypes = instance.Serialize(objImageTypes)
                    objImageTypes = instance.DeserializeObject(strImageTypes)
                    strimageurl = objImageTypes("url")
                End If


                ''Caption
                objCaptions = meta("caption")
                strCaptions = instance.Serialize(objCaptions)
                objText = instance.DeserializeObject(strCaptions)
                If strCaptions.Contains("text") = True Then
                    strTitle = objText("text")
                End If

                ''Caption
                objLikes = meta("likes")
                strLikes = instance.Serialize(objLikes)
                objLikes = instance.DeserializeObject(strLikes)
                If strLikes.Contains("count") = True Then
                    strlcount = objLikes("count")
                End If


                count += 1
                Dim selectString As String = ""
                Dim cmd As SqlCommand = New SqlCommand(selectString, cn)


                If strtype = "image" Then
                    'Dim thumburl = Utility.AddImageFromURL(strimageurl, "InstaThumb", Server)
                    'Dim bigurl = Utility.AddImageFromURL(strimageurl, "InstaBig", Server)

                    Dim thumburl = strimageurl
                    Dim bigurl = strimageurl

                    selectString = "INSERT INTO Instagram (ImageID, Title, SmallImage, BigImage, SortIndex,InstaUser ,Status, LastUpdated,OriginalLink)  VALUES    (@ImageID, @Title, @SmallImage, @BigImage, @SortIndex, @InstaUser , 1, '" & DateTime.Now.ToShortDateString() & "',@OriginalLink ) "
                    cmd = New SqlCommand(selectString, cn)
                    cmd.Parameters.Add("ImageID", Data.SqlDbType.Int)
                    cmd.Parameters.Add("Title", Data.SqlDbType.NVarChar, 400)
                    cmd.Parameters.Add("SmallImage", Data.SqlDbType.NVarChar, 400)
                    cmd.Parameters.Add("BigImage", Data.SqlDbType.NVarChar, 400)
                    cmd.Parameters.Add("SortIndex", Data.SqlDbType.Int)
                    cmd.Parameters.Add("OriginalLink", Data.SqlDbType.VarChar, 100)
                    cmd.Parameters.Add("InstaUser", Data.SqlDbType.NVarChar, 150)
                    cmd.Parameters("ImageID").Value = count
                    cmd.Parameters("Title").Value = strTitle
                    cmd.Parameters("SmallImage").Value = thumburl
                    cmd.Parameters("BigImage").Value = bigurl
                    cmd.Parameters("SortIndex").Value = count
                    cmd.Parameters("OriginalLink").Value = strlink
                    cmd.Parameters("InstaUser").Value = strname
                    cmd.ExecuteNonQuery()

                End If
            Next
            cn.Close()
        Catch ex As Exception

        End Try

    End Sub
    Public Function UnixToTime(ByVal strUnixTime As String) As Date
        UnixToTime = DateAdd(DateInterval.Second, Val(strUnixTime), #1/1/1970#)
        If UnixToTime.IsDaylightSavingTime = True Then
            UnixToTime = DateAdd(DateInterval.Hour, 1, UnixToTime)
        End If
    End Function

 
    Dim count As Int16 = 0
    Protected Sub btnExport_Click(sender As Object, e As System.EventArgs) Handles btnExport.Click
        Response.Cookies("FlymeHashtags").Value = lblHashCode.Text.ToLower()
        Response.Cookies("FlymeHashtags").Expires = Date.Now.AddDays(+1)
        Response.Redirect("https://api.instagram.com/oauth/authorize/?client_id=30d3ee5f12ca4267b0b6cfe9692bf75f&redirect_uri=http://www.sirbaniyasisland.com/admin/A-Instagram/ImportInstagram.aspx&response_type=token")
        ''  Response.Redirect(Request.Url.ToString())
    End Sub



    Protected Sub btnTransfer_Click(sender As Object, e As System.EventArgs)
        Dim thumbimage As String = ""
        Dim bigimage As String = ""
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()

        Dim insertCount = 0
        For Each row As ListViewItem In ListView1.Items
            If row.ItemType = ListViewItemType.DataItem Then
                Dim chkBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                If chkBox.Checked Then
                    Dim ImportID As String = DirectCast(row.FindControl("hdnID"), HiddenField).Value
                    thumbimage = DirectCast(row.FindControl("hdnimagelink"), HiddenField).Value
                    bigimage = DirectCast(row.FindControl("hdnimagelink"), HiddenField).Value
                    If Not thumbimage.Contains("Content") Then
                        thumbimage = Utility.AddImageFromURL(thumbimage, "thumb", Server)
                    End If
                    If Not bigimage.Contains("Content") Then
                        bigimage = Utility.AddImageFromURL(bigimage, "Big", Server)
                    End If
                    Dim selectString As String = " INSERT INTO [InstagramImage] ( Title, SmallImage, BigImage, SortIndex, Status,InstaUser , LastUpdated) select  Title, '" & thumbimage & "' , '" & bigimage & "', SortIndex, Status, InstaUser, LastUpdated from Instagram where ImportID=@ImportID  "
                    Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
                    cmd.Parameters.Add("ImportID", Data.SqlDbType.Int)
                    cmd.Parameters("ImportID").Value = ImportID
                    If cmd.ExecuteNonQuery() > 0 Then
                        insertCount += 1
                        sdsInstagram.DeleteParameters("ImportID").DefaultValue = ImportID
                        sdsInstagram.Delete()
                    End If
                End If
            End If
        Next

        lblMessage.Text = "Inserted " & insertCount & " images "
        lblMessage.ForeColor = Drawing.Color.Green

    End Sub


End Class
