﻿
Partial Class Admin_A_CommonGallery_CommonGalleryEdit
    Inherits System.Web.UI.Page

    Protected smallImageWidth As String = "", smallImageHeight As String = "", bigImageWidth As String = "", bigImageHeight As String = "", videoWidth As String = "", videoHeight As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'Utility.GetDimentionSetting("List", "", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, videoWidth, videoHeight)
        bigImageWidth = Request.QueryString("BigImageWidth")
        bigImageHeight = Request.QueryString("BigImageHeight")
        smallImageWidth = Request.QueryString("smallImageWidth")
        smallImageHeight = Request.QueryString("smallImageHeight")

        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("cgid")) Then
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
                lblTabTitle.Text = "Update Image"
                LoadContent(Request.QueryString("cgid"))
            Else

            End If
        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnUpdateDate.Value = DateTime.Now


        If Not String.IsNullOrEmpty(txtVideoOriginalURL.Text) Then
            Dim video As VideoModule = New VideoModule()
            video.ProcessVideoURL(txtVideoOriginalURL.Text, videoWidth, videoHeight)
            hdnVideoEmbedCode.Value = video.VideoIFrame
            hdnVideoImageURL.Value = video.BigImage

        End If
        If hdnVideoImageURL.Value <> "" Then
            imgVideoImageURL.Visible = True
            imgVideoImageURL.ImageUrl = hdnVideoImageURL.Value
        End If

        If fuSmallImage.FileName <> "" Then
            hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
        End If
        If fuBigImage.FileName <> "" Then
            hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
        End If
        If hdnSmallImage.Value <> "" Then
            imgSmallImage.Visible = True
            imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
        End If
        If hdnBigImage.Value <> "" Then
            imgBigImage.Visible = True
            imgBigImage.ImageUrl = "../" & hdnBigImage.Value
        End If


        If String.IsNullOrEmpty(Request.QueryString("cgid")) Then ' Or Request.QueryString("new") = 1 Then
            'If Request.QueryString("new") <> "1" Then
            '    hdnMasterID.Value = GetMasterID()
            'End If

            If SqlDataSourceGallery.Insert() > 0 Then
                'InsertIntoSEO()
                divSuccess.Visible = True
            Else
                divError.Visible = True
            End If
        Else
            If SqlDataSourceGallery.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = False
            End If
        End If

    End Sub




    Private Sub LoadContent(CommonGalleryID As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = " SELECT [CommonGalleryID]      ,[Title]      ,[SmallImage]      ,[BigImage]      ,[ImageAltText]      ,[SortIndex]      ,[Status]      ,[LastUpdated]      ,[TableName]      ,[TableMasterID],[VideoOriginalURL] ,[VideoImageURL],[VideoEmbedCode] FROM [dbo].[CommonVideo]  where CommonGalleryID=@CommonGalleryID  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("CommonGalleryID", Data.SqlDbType.Int)
        cmd.Parameters("CommonGalleryID").Value = CommonGalleryID

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()

            txtTitle.Text = reader("Title") & ""
            hdnSmallImage.Value = reader("SmallImage") & ""
            hdnBigImage.Value = reader("BigImage") & ""
            If hdnSmallImage.Value <> "" Then
                imgSmallImage.Visible = True
                imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
            End If
            If hdnBigImage.Value <> "" Then
                imgBigImage.Visible = True
                imgBigImage.ImageUrl = "../" & hdnBigImage.Value
            End If

            txtVideoOriginalURL.Text = reader("VideoOriginalURL") & ""
            hdnVideoEmbedCode.Value = reader("VideoEmbedCode") & ""
            hdnVideoImageURL.Value = reader("VideoImageURL") & ""

            txtSortIndex.Text = reader("SortIndex") & ""
            Boolean.TryParse(reader("Status") & "", chkStatus.Checked)
            hdnUpdateDate.Value = reader("LastUpdated") & ""
            txtImgAlt.Text = reader("ImageAltText") & ""
            hdnMasterID.Value = reader("TableMasterID") & ""
            hdnTablename.Value = reader("TableName").ToString()
            imgVideoImageURL.Visible = True
            imgVideoImageURL.ImageUrl = hdnVideoImageURL.Value

        End If
        conn.Close()
    End Sub


End Class
