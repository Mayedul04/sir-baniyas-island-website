﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="HTML.aspx.vb" Inherits="Admin_A_HTML_HTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <h1 class="page-title">Fixed Text</h1>
   <div class="btn-toolbar">
    
    <a href="../A-HTML/AllHTML.aspx" data-toggle="modal" class="btn">Back</a>
        <div class="btn-group">
    </div>
    </div>

    <h2><asp:Label ID="lblTabTitle" runat="server" Text="Add Fixed Text"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
                <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
                <div class="corners">
                    <span class="success-left-top"></span><span class="success-right-top"></span><span
                        class="success-left-bot"></span><span class="success-right-bot"></span>
                </div>
   </div>
   <div class="error-details" id="divError" visible="false" runat="server">
                <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
                <div class="corners">
                    <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
                    </span><span class="error-right-bot"></span>
                </div>
    </div>


    <!-- content -->
    <div class="well">
           <div id="myTabContent" class="tab-content">
                <asp:Panel ID="pnlTitle" runat="server">
                        <p>
                            <label>
                                Title:</label>
                            <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                                ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlSmallImage" runat="server">
                        <p>
                            <asp:Image ID="imgSmallImage" runat="server" Width="100px" Visible="false" />
                            <asp:HiddenField ID="hdnSmallImage" runat="server" />
                            <label>
                                Upload Small Image :(Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)
                            </label>
                            <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlBigImage" runat="server">
                        <p>
                            <asp:Image ID="imgBigImage" runat="server" Width="140px" Visible="false" />
                            <asp:HiddenField ID="hdnBigImage" runat="server" />
                            <label>
                                Upload Big Image :(Width=<%= bigImageWidth%>; Height=<%= bigImageHeight%>)</label>
                            <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlImageAltText" runat="server">
                        <p>
                            <label>
                                Image Alt Text</label>
                            <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
                        </p>
                    </asp:Panel>

                    <asp:Panel ID="pnlFileUpload" Visible="false"  runat="server">
                        <p>
                            <asp:Literal ID="ltFileUpload" runat="server"></asp:Literal>
                            <asp:HiddenField ID="hdnFileupload" runat="server" />
                            <label>
                                Upload Files
                            </label>
                            <asp:FileUpload ID="fuFileUpload" runat="server" CssClass="input-xlarge" />
                        </p>
                    </asp:Panel>

                    <asp:Panel ID="pnlSmallDetails" runat="server">
                        <p>
                            <label>
                                Small Details :</label>
                            <asp:TextBox ID="txtSmallDetails" runat="server" TextMode="MultiLine" CssClass="input-xlarge"
                                Rows="4"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic"
                                runat="server" ControlToValidate="txtSmallDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                                ErrorMessage="* character limit is 1000" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlBigDetails" runat="server">
                        <p>
                            <label>
                                Details:</label>
                            <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <script>

                                // Replace the <textarea id="editor1"> with a CKEditor
                                // instance, using default configuration.

                                CKEDITOR.replace('<%=txtDetails.ClientID %>',
                                                {
                                                    filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                                    "extraPlugins": "imagebrowser",
                                                    "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                                                }
                                            ); 
            
                            </script>
                        </p>
                    </asp:Panel>
                    <p >
                        <label>
                            Language</label>
                        <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                            DataTextField="LangFullName" DataValueField="Lang">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
                        </asp:SqlDataSource>
                    </p>
                <asp:HiddenField ID="hdnUpdateDate" runat="server" />
                <asp:HiddenField ID="hdnMasterID" runat="server" />
         </div> 

         <div class="btn-toolbar">
            <button runat="server" id ="btnSubmit" ValidationGroup="form"  class="btn btn-primary"><i class="icon-save"></i> Add New</button>
            
            <div class="btn-group">
            </div>
        </div>

    </div>
    <!-- Eof content -->
    <asp:SqlDataSource ID="sdsObject" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
            DeleteCommand="DELETE FROM [HTML] WHERE [HtmlID] = @HtmlID" 
            InsertCommand="INSERT INTO HTML(Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText, LastUpdated, MasterID, Lang,FileUploaded) VALUES (@Title, @SmallDetails, @BigDetails, @SmallImage, @BigImage, @ImageAltText,@LastUpdated, @MasterID, @Lang,@FileUploaded)" 
            SelectCommand="SELECT * FROM [HTML]"                         

        UpdateCommand="UPDATE HTML SET Title = @Title, SmallDetails = @SmallDetails, BigDetails = @BigDetails, SmallImage = @SmallImage, BigImage = @BigImage, ImageAltText = @ImageAltText, LastUpdated = @LastUpdated, MasterID = @MasterID, Lang = @Lang,FileUploaded=@FileUploaded WHERE (HtmlID = @HtmlID)">
            <DeleteParameters>
                <asp:Parameter Name="HtmlID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" 
                    Type="String" />
                <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" 
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" 
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" 
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" 
                    PropertyName="Value" Type="String" />
                        <asp:ControlParameter ControlID="hdnFileupload" Name="FileUploaded" 
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                        Type="String" />         
                <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" 
                    PropertyName="Value" Type="DateTime" />       
                <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" 
                    PropertyName="Value" />
                <asp:ControlParameter ControlID="ddlLang" Name="Lang" 
                    PropertyName="SelectedValue" />
            </InsertParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" 
                    Type="String" />
                <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" 
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" 
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" 
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" 
                    PropertyName="Value" Type="String" />
                  <asp:ControlParameter ControlID="hdnFileupload" Name="FileUploaded" 
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" 
                    PropertyName="Value" Type="DateTime" />
                <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" 
                    PropertyName="Value" />
                <asp:ControlParameter ControlID="ddlLang" Name="Lang" 
                    PropertyName="SelectedValue" />
                <asp:QueryStringParameter Name="HtmlID" QueryStringField="hid" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
</asp:Content>

