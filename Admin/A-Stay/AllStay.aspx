﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="AllStay.aspx.vb" Inherits="Admin_A_List_AllList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">
        Hotel & Resorts</h1>
    <div class="btn-toolbar">
        <button runat="server" id="btnAddNew" class="btn btn-primary">
            <i class="icon-save"></i> Add New</button>
        <%--<asp:LinkButton ID="btnDownload" runat="server" CssClass="btn btn-primary" ><i class='icon-save'></i> Export</asp:LinkButton>--%>
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="Label1" runat="server" Text="All Hotel & Resorts"></asp:Label></h2>
    <div>
         <p>
            <label>
                Language</label>
            <asp:DropDownList ID="ddlLang" runat="server" CssClass="input-xlarge" AutoPostBack="true"
                DataSourceID="sdsLang" DataTextField="LangFullName" DataValueField="Lang">
            </asp:DropDownList>
            <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
            </asp:SqlDataSource>
        </p>
        <div class="well">
            <asp:ListView ID="ListView1" runat="server" DataSourceID="sdsList" DataKeyNames="RestaurantID">
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Label ID="ListIDLabel" runat="server" Text='<%# Eval("RestaurantID") %>' />
                        </td>
                       
                        <td>
                            <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                        </td>
                        <td>
                            <img width="120px" src='<%# If(IsDBNull(Eval("BigImage")), "../Content/Noimages.jpg", "../" & Eval("BigImage"))%>' />
                        </td>
                        <td>
                            <asp:Label ID="SortIndexLabel" runat="server" Text='<%# Eval("SortIndex") %>' />
                        </td>
                       
                        <td>
                            <asp:CheckBox ID="StatusCheckBox" runat="server" Checked='<%# Eval("Status") %>'
                                Enabled="false" />
                        </td>
                        <td>
                            <a href="AllCommonGallery.aspx?TableName=Stay&TableMasterID=<%# Eval("MasterID") %>&t=<%# Server.UrlEncode(Eval("Title").ToString()) %>&BigImageWidth=841&BigImageHeight=254">Image Gallery</a>
                        </td>

                          <td>
                            <a href="AllCommonVideo.aspx?TableName=Stay&TableMasterID=<%# Eval("MasterID") %>&SmallImageWidth=478&SmallImageHeight=233&t=<%# Server.UrlEncode(Eval("Title").ToString()) %>">Video Gallery</a>
                        </td>

                        
                        <td>
                            <a href="AllCommonFacts.aspx?TableName=Stay&TableMasterID=<%# Eval("MasterID") %>&SmallImageWidth=335&SmallImageHeight=233&t=<%# Server.UrlEncode(Eval("Title").ToString()) & "&lang=" & Eval("Lang") %>">Experience</a>
                        </td>

                        <td>
                            <asp:Label ID="MasterIDLabel" runat="server" Text='<%# Eval("MasterID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="LangLabel" runat="server" Text='<%# Eval("Lang") %>' />
                        </td>
                        
                        <td>
                            <a href='<%# "Stay.aspx?lid=" & Eval("RestaurantID")%>' title="Edit"><i class="icon-pencil">
                            </i></a>&nbsp
                            <a href='<%# "Stay.aspx?lid=" & Eval("RestaurantID") & "&new=1"%>' title="New Lang" style='<%# if(Eval("Lang")="en","","display:none;") %>'><i class="icon-plus-sign"></i> </a>&nbsp 
                            <a href='<%# "#" & Eval("RestaurantID")%>' data-toggle="modal"><i class="icon-remove"></i></a>
                            <div class="modal small hide fade" id='<%# Eval("RestaurantID")%>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">
                                        Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer" class="table">
                        <thead>
                            <tr runat="server" style="">
                                <th runat="server">
                                    HotelID
                                </th>
                               
                                <th runat="server">
                                    Title
                                </th>
                                <th runat="server">
                                    Small Image
                                </th>
                                <th runat="server">
                                    SortIndex
                                </th>
                               
                                <th runat="server">
                                    Status
                                </th>
                                <th>
                                    Photo Gallery
                                </th>
                                <th>
                                    Video Gallery
                                </th>
                                <th>
                                    Experience
                                </th>
                                <th runat="server">
                                    MasterID
                                </th>
                                <th runat="server">
                                    Lang
                                </th>
                                
                                <th>
                                    Actions
                                </th>
                            </tr>
                        </thead>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <div class="paginationNew pull-right">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                        ShowPreviousPageButton="True" />
                                    <asp:NumericPagerField />
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                        ShowPreviousPageButton="False" />
                                </Fields>
                            </asp:DataPager>
                        </div>
                    </table>
                </LayoutTemplate>
            </asp:ListView>
            <asp:SqlDataSource ID="sdsList" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                SelectCommand="SELECT List_Restaurant.RestaurantID, List_Restaurant.Category, List_Restaurant.Title, List_Restaurant.SmallDetails,  List_Restaurant.SmallImage, List_Restaurant.BigImage, List_Restaurant.Link, List_Restaurant.Featured, List_Restaurant.SortIndex, List_Restaurant.Status, List_Restaurant.LastUpdated  ,List_Restaurant.MasterID, List_Restaurant.Lang,List_Restaurant.LogoSmall  FROM List_Restaurant INNER JOIN Languages ON List_Restaurant.Lang = Languages.Lang and List_Restaurant.RestaurantID <> '0' where  List_Restaurant.Lang=@Lang ORDER BY List_Restaurant.MasterID DESC, Languages.SortIndex "
                DeleteCommand="DELETE FROM List_Restaurant WHERE (RestaurantID = @RestaurantID)">
                <DeleteParameters>
                    <asp:Parameter Name="RestaurantID" />
                </DeleteParameters>
                <SelectParameters>
               <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
            </SelectParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
