﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master" AutoEventWireup="false"
    CodeFile="StayEdit.aspx.vb" Inherits="Admin_A_List_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="block">
        <div class="block-heading">Restaurants & Bars</div>
        <div class="block-body">
            <h1 class="page-title"></h1>
            <%--<div class="btn-toolbar">
        <a href="../A-List/AllList.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>--%>
            <h2>
                <asp:Label ID="lblTabTitle" runat="server" Text="Add New"></asp:Label></h2>
            <div class="success-details" visible="false" id="divSuccess" runat="server">
                <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
                <div class="corners">
                    <span class="success-left-top"></span><span class="success-right-top"></span><span
                        class="success-left-bot"></span><span class="success-right-bot"></span>
                </div>
            </div>
            <div class="error-details" id="divError" visible="false" runat="server">
                <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
                <div class="corners">
                    <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
                </div>
            </div>
            <!-- content -->
            <div class="well">
                <div id="myTabContent" class="tab-content">

                    <p>
                        <label>
                            Title:</label>
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                        <label class="red">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" SetFocusOnError="true" Display="Dynamic" ValidationGroup="form"
                                ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                        </label>
                    </p>

                    <asp:Panel ID="pnlShorterName" runat="server">
                        <p>
                            <label>
                                Shorter Name:</label>
                            <asp:TextBox ID="txtOpeningTime" runat="server" Text="" CssClass="input-xlarge" MaxLength="400" />
                        </p>
                    </asp:Panel>


                    <asp:Panel ID="pnlBigImage" runat="server">
                        <p>
                            <asp:Image ID="imgBigImage" runat="server" Width="140px" Visible="false" />
                            <asp:HiddenField ID="hdnBigImage" runat="server" />
                        </p>
                        <p>
                            <label>
                                Upload Big Image :(Width=<%= bigImageWidth%>; Height=<%= bigImageHeight%>)</label>
                            <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                            <label class="red">
                            </label>
                            <asp:CheckBox ID="chkBigImageDelete" Text="Delete Big Image" runat="server" />
                        </p>
                        <p>
                            <label>
                                Image Alt Text</label>
                            <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
                        </p>
                    </asp:Panel>






                    <asp:Panel ID="pnlSmallDetails" runat="server">
                        <p>
                            <label>
                                Small Details:</label>
                            <asp:TextBox ID="txtSmallDetails" runat="server" TextMode="MultiLine" CssClass="input-xlarge"
                                Rows="4"></asp:TextBox>
                            <label class="red">
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="form" ControlToValidate="txtSmallDetails" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic"
                                    runat="server" ControlToValidate="txtSmallDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                                    ErrorMessage="* character limit is 1000" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                            </label>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlDetails" runat="server">
                        <p>
                            <label>
                                Details:</label>
                            <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <script>

                                // Replace the <textarea id="editor1"> with a CKEditor
                                // instance, using default configuration.

                                CKEDITOR.replace('<%=txtDetails.ClientID %>',
                                    {
                                        filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                        "extraPlugins": "imagebrowser",
                                        "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                                    }
                        );



                            </script>
                        </p>
                    </asp:Panel>



                    <asp:Panel ID="pnlTripAdvisor" runat="server">
                        <p>
                            <label>
                                Trip Advisor:</label>
                            <asp:TextBox ID="txtTripAdvisor" runat="server" Text="" TextMode="MultiLine" CssClass="input-xlarge" MaxLength="400" />
                        </p>
                    </asp:Panel>

                    <asp:Panel ID="pnlBooking" runat="server">
                        <p>
                            <label>
                                Booking Code:</label>
                            <asp:TextBox ID="txtBookingCode" runat="server" Text="" TextMode="MultiLine" CssClass="input-xlarge" MaxLength="400" />
                        </p>
                    </asp:Panel>


                    <asp:Panel ID="pnlLink" runat="server">
                        <p>
                            <label>
                                BG class:</label>
                            <%--  <asp:TextBox ID="txtLinkTextBox" runat="server" Text="" CssClass="input-xlarge" MaxLength="400" />--%>
                            <asp:DropDownList ID="txtLinkTextBox" CssClass="input-xlarge" MaxLength="400" runat="server">
                                <asp:ListItem>
                             bluebg
                                </asp:ListItem>
                                <asp:ListItem>
                             greenbg
                                </asp:ListItem>
                                <asp:ListItem>
                             purplebg
                                </asp:ListItem>
                            </asp:DropDownList>
                        </p>
                    </asp:Panel>

                    <asp:Panel ID="pnlFeatured" runat="server">
                        <p>
                            <asp:CheckBox ID="chkFeatured" runat="server" Text="Featured " TextAlign="Left" />
                            <label class="red">
                            </label>
                        </p>
                        <p>
                            <label>
                                Sort Order:
                            </label>
                            <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                            <label class="red">
                                <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                                    MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"
                                    ValidationGroup="form"></asp:RangeValidator>
                            </label>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlPGallery" Visible="false" runat="server">
                        <p>
                            <asp:Image ID="imgLogoSmall" runat="server" Width="100px" Visible="false" />
                            <asp:HiddenField ID="hdnLogoSmall" runat="server" />
                        </p>
                        <p>
                            <label>
                                Upload Photo Album Image  : (Width=<%= smallLogoImageWidth%>; Height=<%= smallLogoImageHeight%>)</label>
                            <asp:FileUpload ID="fuLogoSmall" runat="server" CssClass="input-xlarge" />
                            <label class="red">
                            </label>
                            <asp:CheckBox ID="chkSmallLogoImageDelete" Text="Delete Small Logo Image" runat="server" />
                        </p>
                        <p>
                            <label>
                                Select Photo Gallery 
                            </label>
                            <asp:DropDownList ID="ddlPGallery" runat="server" DataSourceID="sdsPGallery" AppendDataBoundItems="true" CssClass="input-xlarge"
                                DataTextField="Title" DataValueField="GalleryID">
                                <asp:ListItem Value="">Select Album</asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="sdsPGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                SelectCommand="SELECT DISTINCT [GalleryID], [Title] FROM [Gallery] WHERE (([Status] = @Status) AND ([Lang] = @Lang)) order by Title ASC">
                                <SelectParameters>
                                    <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                    <asp:Parameter DefaultValue="en" Name="Lang" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </p>



                    </asp:Panel>
                    <asp:Panel ID="pnlVGallery" Visible="false" runat="server">
                        <p>
                            <asp:Image ID="imgSmallImage" runat="server" Width="100px" Visible="false" />
                            <asp:HiddenField ID="hdnSmallImage" runat="server" />
                        </p>
                        <p>
                            <label>
                                Upload Video Album Image   : (Width=<%= videoWidth%>; Height=<%= videoHeight%>)</label>
                            <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                            <label class="red">
                            </label>
                            <asp:CheckBox ID="chkBigLogoImageDelete" Text="Delete Video Gallery Image" runat="server" />
                        </p>
                        <p>
                            <label>
                                Select Video Gallery 
                            </label>
                            <asp:DropDownList ID="ddlVGallery" runat="server" DataSourceID="sdsVGallery" AppendDataBoundItems="true" CssClass="input-xlarge"
                                DataTextField="Title" DataValueField="GalleryID">
                                <asp:ListItem Value="">Select Album</asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="sdsVGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                SelectCommand="SELECT DISTINCT [GalleryID], [Title] FROM [Video] WHERE (([Status] = @Status) AND ([Lang] = @Lang)) order by Title ASC">
                                <SelectParameters>
                                    <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                    <asp:Parameter DefaultValue="en" Name="Lang" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </p>
                    </asp:Panel>
                    <p>
                        <asp:CheckBox ID="chkStatus" Checked="true" runat="server" TextAlign="Left" Text="Status " />
                        <label class="red">
                        </label>
                    </p>
                    <p>
                        <label>
                            Language</label>
                        <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                            DataTextField="LangFullName" DataValueField="Lang">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
                    </p>
                    <asp:HiddenField ID="hdnMasterID" runat="server" />
                    <asp:HiddenField ID="hdnUpdateDate" runat="server" />
                </div>
                <div class="btn-toolbar">
                    <%--<asp:Button ID="btnSubmit" runat="server" Text="<i class='icon-save'></i> Add New" validationgroup="form" class="btn btn-primary" />--%>
                    <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>Add New</button>
                    <div class="btn-group">
                    </div>
                </div>

                <asp:SqlDataSource ID="sdsList" runat="server"
                    ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    DeleteCommand="DELETE FROM [List_Restaurant] WHERE [RestaurantID] = @RestaurantID"
                    InsertCommand="INSERT INTO [List_Restaurant] ( [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [Link], [Featured], [SortIndex], [Status], [LastUpdated],ImageAltText,MasterID, Lang,OpeningTime,TripAdvisor,Booking,LogoSmall, GalleryID, VGalleryID) VALUES ( @Title, @SmallDetails, @BigDetails, @SmallImage, @BigImage, @Link, @Featured, @SortIndex, @Status, @LastUpdated,@ImageAltText,@MasterID, @Lang,@OpeningTime,@TripAdvisor,@Booking,@LogoSmall, @GalleryID, @VGalleryID)"
                    SelectCommand="SELECT * FROM [List_Restaurant]"
                    UpdateCommand="UPDATE [List_Restaurant] SET  [Title] = @Title, [SmallDetails] = @SmallDetails, [BigDetails] = @BigDetails, [SmallImage] = @SmallImage, [BigImage] = @BigImage, [Link] = @Link, [Featured] = @Featured, [SortIndex] = @SortIndex, [Status] = @Status, [LastUpdated] = @LastUpdated, ImageAltText=@ImageAltText, MasterID=@MasterID, Lang=@Lang,OpeningTime=@OpeningTime,TripAdvisor=@TripAdvisor,Booking=@Booking,LogoSmall=@LogoSmall, GalleryID=@GalleryID, VGalleryID=@VGalleryID  WHERE [RestaurantID] = @RestaurantID">
                    <DeleteParameters>
                        <asp:Parameter Name="RestaurantID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails"
                            PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtDetails" Name="BigDetails"
                            PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage"
                            PropertyName="Value" Type="String" />
                        <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage"
                            PropertyName="Value" Type="String" />
                        <asp:ControlParameter ControlID="hdnLogoSmall" Name="LogoSmall"
                            PropertyName="Value" Type="String" />
                        <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtLinkTextBox" Name="Link"
                            PropertyName="SelectedValue" Type="String" />
                        <asp:ControlParameter ControlID="chkFeatured" Name="Featured"
                            PropertyName="Checked" Type="Boolean" />
                        <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex"
                            PropertyName="Text" Type="Int32" />
                        <asp:ControlParameter ControlID="chkStatus" Name="Status"
                            PropertyName="Checked" Type="Boolean" />
                        <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated"
                            PropertyName="Value" Type="DateTime" />
                        <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                        <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="txtTripAdvisor" Name="TripAdvisor" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtBookingCode" Name="Booking" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtOpeningTime" Name="OpeningTime" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="ddlPGallery" Name="GalleryID" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="ddlVGallery" Name="VGalleryID" PropertyName="SelectedValue" />

                    </InsertParameters>
                    <UpdateParameters>
                        <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails"
                            PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtDetails" Name="BigDetails"
                            PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage"
                            PropertyName="Value" Type="String" />
                        <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage"
                            PropertyName="Value" Type="String" />
                        <asp:ControlParameter ControlID="hdnLogoSmall" Name="LogoSmall"
                            PropertyName="Value" Type="String" />
                        <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtLinkTextBox" Name="Link"
                            PropertyName="SelectedValue" Type="String" />
                        <asp:ControlParameter ControlID="chkFeatured" Name="Featured"
                            PropertyName="Checked" Type="Boolean" />
                        <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex"
                            PropertyName="Text" Type="Int32" />
                        <asp:ControlParameter ControlID="chkStatus" Name="Status"
                            PropertyName="Checked" Type="Boolean" />
                        <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated"
                            PropertyName="Value" Type="DateTime" />
                        <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                        <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
                        <asp:QueryStringParameter Name="RestaurantID" QueryStringField="lid"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="txtTripAdvisor" Name="TripAdvisor" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtBookingCode" Name="Booking" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtOpeningTime" Name="OpeningTime" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="ddlPGallery" Name="GalleryID" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="ddlVGallery" Name="VGalleryID" PropertyName="SelectedValue" />

                    </UpdateParameters>
                </asp:SqlDataSource>

            </div>
        </div>
    </div>


</asp:Content>
