﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="GA.aspx.vb" Inherits="Admin_A_Home_GA" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

        <script src='../javascripts/oocharts.js'></script>

        
           <script type="text/javascript">

               window.onload = function () {

                   oo.setAPIKey('<%= apikey %>');

                   oo.load(function () {

                       var table = new oo.Table('<%= profileid %>', '<%= timeline %>');

                       table.addMetric("ga:pageviews", "Visits");

                       table.addDimension("ga:pageTitle", "Page");

                       table.draw('visitortable');





                       var pie = new oo.Pie('<%= profileid %>', '<%= timeline %>');

                       pie.setMetric("ga:visits", "Visits");
                       pie.setDimension("ga:browser");

                       pie.draw('visitorpie');




                       var pie2 = new oo.Pie('<%= profileid %>', '<%= timeline %>');

                       pie2.setMetric("ga:organicSearches", "Search");
                       pie2.setDimension("ga:continent");

                       pie2.draw('visitorpie2');













                       var bar = new oo.Bar('<%= profileid %>', '<%= timeline %>');

                       bar.addMetric("ga:visits", "Visits");

                       bar.addMetric("ga:newVisits", "New Visits");

                       bar.setDimension("ga:continent");

                       bar.draw('visitorbar');


                       var timeline = new oo.Timeline('<%= profileid %>', '<%= timeline %>');

                       timeline.addMetric("ga:visits", "Visits");

                       timeline.addMetric("ga:newVisits", "New Visits");

                       timeline.draw('visitortimeline');



                       var Column1 = new oo.Column('<%= profileid %>', '<%= timeline %>');

//                       Column1.addMetric("ga:organicSearches", "Visits");

                       //                       Column1.addMetric("ga:newVisits", "New Visits");

                       Column1.addMetric("ga:visits", "Visits");

                       Column1.addMetric("ga:newVisits", "New Visits");

                       Column1.setDimension("ga:deviceCategory");

                       Column1.draw('visitorcolumn');



                   });
               };

        </script>


</head>
<body>
    <form id="form1" runat="server">
     <div id='visitortimeline'></div>   

     <div id='visitorcolumn'></div> 

     <div id='visitorbar'></div>
    
     <div id='visitorpie'></div>

     <div id='visitorpie2'></div>

     <div id='visitortable'></div>





    </form>
</body>
</html>
