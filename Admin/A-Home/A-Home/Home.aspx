﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="Home.aspx.vb" Inherits="Admin_A_Home_Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("visualization", "1", { packages: ["corechart"] });
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
          ['Date', 'Contacts', 'Newsletter Subscription'],
          <%= Contact_SubscriptionData() %>
            ]);

            var options = {
                title: ''
            };

            var chart = new google.visualization.LineChart(document.getElementById('chart_ContactAndNewsletter'));
            chart.draw(data, options);
        }
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script>
        (function (w, d, s, g, js, fs) {
            g = w.gapi || (w.gapi = {}); g.analytics = { q: [], ready: function (f) { this.q.push(f); } };
            js = d.createElement(s); fs = d.getElementsByTagName(s)[0];
            js.src = 'https://apis.google.com/js/platform.js';
            fs.parentNode.insertBefore(js, fs); js.onload = function () { g.load('analytics'); };
        }(window, document, 'script'));
    </script>



    <div class="stats">
        <p class="stat">
            <span class="number">
                <asp:Literal ID="ltTotlaContact" runat="server"></asp:Literal></span>Contact
        </p>

        <p class="stat">
            <span class="number">
                <asp:Literal ID="ltTotalNewsLetter" runat="server"></asp:Literal>
            </span>Newsletter
        </p>
    </div>
    <h1 class="page-title">Dashboard</h1>

    <div id="embed-api-auth-container"></div>

    <div class="row-fluid">
        <div class="block">
            <p class="block-heading" data-toggle="collapse" data-target="#chart-container">Visitor Vs Timeline</p>
            <div id="chart-container" class="block-body collapse in">


                <div id="linechart-container"></div>
                <div id="view-selector-container"></div>


                <div id='visitortimeline'></div>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="block span6">
            <div class="block-heading" data-toggle="collapse" data-target="#widgetVisitorColumn">Visitor Vs Continent</div>
            <div id="widgetVisitorColumn" class="block-body collapse in">

                <div id="chart-1-container"></div>
                <div id="view-selector-1-container"></div>


            </div>
        </div>
        <div class="block span6">
            <div class="block-heading" data-toggle="collapse" data-target="#widgetVisitorBar">Visitor Vs Device & Browser</div>
            <div id="widgetVisitorBar" class="block-body collapse in">


                <div id="main-chart-container"></div>
                <div id="view-selector-container1"></div>
                <div id="breakdown-chart-container"></div>


            </div>
        </div>

    </div>



    <div class="row-fluid">
        <div class="block span6">
            <div class="block-heading" data-toggle="collapse" data-target="#widget2container">Contact us<span class="label label-warning">+<asp:Literal ID="ltCountContact" runat="server"></asp:Literal></span></div>
            <div id="widget2container" class="block-body collapse in">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Message</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Literal ID="ltTopContact" runat="server"></asp:Literal>
                    </tbody>
                </table>
                <p><a href="../A-Contact/AllContact.aspx" class="btn btn-primary btn-large">More &raquo;</a></p>
            </div>
        </div>
        <div class="block span6">
            <div class="block-heading" data-toggle="collapse" data-target="#widget3container">Registration<span class="label label-warning">+<asp:Literal ID="ltCountReg" runat="server"></asp:Literal></span></div>

            <div id="widget3container" class="block-body collapse in">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Comments</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Literal ID="ltTopReg" runat="server"></asp:Literal>
                    </tbody>
                </table>
                <p><a class="btn btn-primary btn-large" href="../A-RegistrationForm/AllRegistrationForm.aspx">More &raquo;</a></p>
            </div>
        </div>
        <div class="block span6">
            <div class="block-heading" data-toggle="collapse" data-target="#tablewidget">Quick Links</div>
            <div id="tablewidget" class="block-body collapse in">
                <ol>
                    <%--<li><a href="../A-SirBaniYasIslandHTML/AllHTML.aspx" title="">
                            <img alt="" src="../images/user.png">
                           Sir Bani yas & Desert Island Texts</a></li>
                        <li><a href="../A-Banner/AllTopBanner.aspx" title="">
                            <img alt="" src="../images/user.png">
                            Top Banners</a></li>
                        <li><a href="../A-WildLifeAnimal/AllList1.aspx" title="">
                            <img alt="" src="../images/user.png">
                            Animals</a></li>--%>
                </ol>
            </div>
        </div>
    </div>

    <script>

        gapi.analytics.ready(function () {

            /**
             * Authorize the user immediately if the user has already granted access.
             * If no access has been created, render an authorize button inside the
             * element with the ID "embed-api-auth-container".
             */
            gapi.analytics.auth.authorize({
                container: 'embed-api-auth-container',
                clientid: '807000372883-3pkht931rajm1m5hsk4u16pibiq68htm.apps.googleusercontent.com'
            });


            /**
             * Create a new ViewSelector instance to be rendered inside of an
             * element with the id "view-selector-container".
             */
            var viewSelector = new gapi.analytics.ViewSelector({
                container: 'view-selector-container'
            });

            // Render the view selector to the page.
            viewSelector.execute();


            /**
             * Create a new DataChart instance with the given query parameters
             * and Google chart options. It will be rendered inside an element
             * with the id "chart-container".
             */
            var dataChart = new gapi.analytics.googleCharts.DataChart({
                query: {
                    metrics: 'ga:sessions',
                    dimensions: 'ga:date',
                    'start-date': '30daysAgo',
                    'end-date': 'yesterday'
                },
                chart: {
                    container: 'linechart-container',
                    type: 'LINE',
                    options: {
                        width: '100%'
                    }
                }
            });


            /**
             * Render the dataChart on the page whenever a new view is selected.
             */
            viewSelector.on('change', function (ids) {
                dataChart.set({ query: { ids: ids } }).execute();
            });



            //----------------------------------------------------------------------------------------



            /**
       * Create a ViewSelector for the first view to be rendered inside of an
       * element with the id "view-selector-1-container".
       */
            var viewSelector1 = new gapi.analytics.ViewSelector({
                container: 'view-selector-1-container'
            });


            // Render both view selectors to the page.
            viewSelector1.execute();



            /**
             * Create the first DataChart for top countries over the past 30 days.
             * It will be rendered inside an element with the id "chart-1-container".
             */
            var dataChart1 = new gapi.analytics.googleCharts.DataChart({
                query: {
                    metrics: 'ga:sessions',
                    dimensions: 'ga:country',
                    'start-date': '30daysAgo',
                    'end-date': 'yesterday',
                    'max-results': 6,
                    sort: '-ga:sessions'
                },
                chart: {
                    container: 'chart-1-container',
                    type: 'PIE',
                    options: {
                        width: '100%'
                    }
                }
            });



            /**
             * Update the first dataChart when the first view selecter is changed.
             */
            viewSelector1.on('change', function (ids) {
                dataChart1.set({ query: { ids: ids } }).execute();
            });

            //------------------------------------------------------------------------------------


            /**
   * Create a new ViewSelector instance to be rendered inside of an
   * element with the id "view-selector-container".
   */
            var viewSelector = new gapi.analytics.ViewSelector({
                container: 'view-selector-container1'
            });

            // Render the view selector to the page.
            viewSelector.execute();

            /**
             * Create a table chart showing top browsers for users to interact with.
             * Clicking on a row in the table will update a second timeline chart with
             * data from the selected browser.
             */
            var mainChart = new gapi.analytics.googleCharts.DataChart({
                query: {
                    'dimensions': 'ga:browser',
                    'metrics': 'ga:sessions',
                    'sort': '-ga:sessions',
                    'max-results': '6'
                },
                chart: {
                    type: 'TABLE',
                    container: 'main-chart-container',
                    options: {
                        width: '100%'
                    }
                }
            });


            /**
             * Create a timeline chart showing sessions over time for the browser the
             * user selected in the main chart.
             */
            var breakdownChart = new gapi.analytics.googleCharts.DataChart({
                query: {
                    'dimensions': 'ga:date',
                    'metrics': 'ga:sessions',
                    'start-date': '7daysAgo',
                    'end-date': 'yesterday'
                },
                chart: {
                    type: 'LINE',
                    container: 'breakdown-chart-container',
                    options: {
                        width: '100%'
                    }
                }
            });


            /**
             * Store a refernce to the row click listener variable so it can be
             * removed later to prevent leaking memory when the chart instance is
             * replaced.
             */
            var mainChartRowClickListener;


            /**
             * Update both charts whenever the selected view changes.
             */
            viewSelector.on('change', function (ids) {
                var options = { query: { ids: ids } };

                // Clean up any event listeners registered on the main chart before
                // rendering a new one.
                if (mainChartRowClickListener) {
                    google.visualization.events.removeListener(mainChartRowClickListener);
                }

                mainChart.set(options).execute();
                breakdownChart.set(options);

                // Only render the breakdown chart if a browser filter has been set.
                if (breakdownChart.get().query.filters) breakdownChart.execute();
            });


            /**
             * Each time the main chart is rendered, add an event listener to it so
             * that when the user clicks on a row, the line chart is updated with
             * the data from the browser in the clicked row.
             */
            mainChart.on('success', function (response) {

                var chart = response.chart;
                var dataTable = response.dataTable;

                // Store a reference to this listener so it can be cleaned up later.
                mainChartRowClickListener = google.visualization.events
                    .addListener(chart, 'select', function (event) {

                        // When you unselect a row, the "select" event still fires
                        // but the selection is empty. Ignore that case.
                        if (!chart.getSelection().length) return;

                        var row = chart.getSelection()[0].row;
                        var browser = dataTable.getValue(row, 0);
                        var options = {
                            query: {
                                filters: 'ga:browser==' + browser
                            },
                            chart: {
                                options: {
                                    title: browser
                                }
                            }
                        };

                        breakdownChart.set(options).execute();
                    });
            });



        });
    </script>

</asp:Content>

