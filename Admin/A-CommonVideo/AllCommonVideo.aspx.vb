﻿
Partial Class Admin_A_CommonGallery_AllCommonGallery
    Inherits System.Web.UI.Page
    Protected Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.ServerClick
        If Request.Cookies("backurlAdmin") IsNot Nothing Then
            Response.Redirect(Request.Cookies("backurlAdmin").Value)
        End If
        ' Response.Redirect("AllList1.aspx")
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.Cookies("TableName") Is Nothing Then
            Response.Cookies.Add(New HttpCookie("TableName", Request.QueryString("TableName")))
        Else
            Response.Cookies("TableName").Value = Request.QueryString("TableName")

        End If
        If Request.Cookies("TableMasterID") Is Nothing Then
            Response.Cookies.Add(New HttpCookie("TableMasterID", Request.QueryString("TableMasterID")))
        Else
            Response.Cookies("TableMasterID").Value = Request.QueryString("TableMasterID")
        End If
    End Sub

    Protected Sub btnAddNew_ServerClick(sender As Object, e As EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("CommonVideo.aspx?smallImageWidth=478&smallImageHeight=233&TableName=" & Request.QueryString("TableName") & "&TableMasterID=" & Request.QueryString("TableMasterID") & "&t=" & Request.QueryString("t"))
    End Sub
End Class
