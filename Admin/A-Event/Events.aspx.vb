﻿Imports System.Data.OleDb
Imports System.Drawing.Imaging
Imports System.Data.SqlClient

Partial Class Events
    Inherits System.Web.UI.Page
    Protected smallImageWidth As String = "", smallImageHeight As String = "", otherImageWidth As String = "128", otherImageHeight As String = "214", bigImageWidth As String = "360", bigImageHeight As String = "268"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Utility.GetDimentionSetting("Event", "", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, "", "")

        If IsPostBack = False Then
            If ddlCategory.SelectedValue = "Plan-Your-Event" Then
                smallImageWidth = "263"
                smallImageHeight = "184"
            Else
                smallImageWidth = "263"
                smallImageHeight = "215"
            End If
            If Request.QueryString("eventId") <> "" Then
                Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                Dim sqlConn As SqlConnection = New SqlConnection(strConn)
                sqlConn.Open()
                Dim sqlcomm As SqlCommand = New SqlCommand()
                sqlcomm.CommandText = "SELECT * FROM List_Event where EventID=" & Request.QueryString("eventId")
                sqlcomm.Connection = sqlConn
                sqlcomm.CommandType = Data.CommandType.Text
                Dim reader As SqlDataReader = sqlcomm.ExecuteReader()
                reader.Read()

                lblFileName.Text = reader("FileUploaded").ToString()
                hdnFile.Value = reader("FileUploaded").ToString()
                txtTitle.Text = reader("Title").ToString()
                ddlCategory.SelectedValue = reader("Category")
                txtSmallDetails.Text = reader("SmallDetails").ToString()
                txtDetails.Text = reader("BigDetails").ToString()
                imgSmallImage.ImageUrl = "~/Admin/" + reader("SmallImage").ToString()
                hdnSmallImage.Value = reader("SmallImage").ToString()
                imgBigImage.ImageUrl = "~/Admin/" + reader("BigImage").ToString()
                hdnBigImage.Value = reader("BigImage").ToString()
                chkFeatured.Checked = reader("Featured").ToString()
                txtSortIndex.Text = reader("SortIndex").ToString()
                chkStatus.Checked = reader("Status").ToString()
                lblLastUpdated.Text = reader("LastUpdated").ToString()
                txtImgAlt.Text = reader("ImageAltText") & ""
                hdnMasterID.Value = reader("MasterID") & ""
                ddlLang.SelectedValue = reader("Lang") & ""
                txtOtherTitle.Text = reader("OtherTitle").ToString()
                txtOtherText.Text = reader("OtherText").ToString()
                ImgOther.ImageUrl = "~/Admin/" + reader("OtherImage").ToString()
                hdnOtherfile.Value = reader("OtherImage").ToString()
                txtTiming.Text = reader("Timings").ToString()
                imgSmallImage.Visible = True
                imgBigImage.Visible = True
                ImgOther.Visible = True
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
                txtEndDate.Text = reader("EndDate").ToString()
                txtStartDate.Text = reader("StartDate").ToString()
                txtLocation.Text = reader("Location").ToString()
                txtLink.Text = reader("Link").ToString()
                ImgPGal.ImageUrl = "~/Admin/" + reader("PhotoGalleryImage").ToString()
                hdnPGal.Value = reader("PhotoGalleryImage").ToString()
                ImgVGal.ImageUrl = "~/Admin/" + reader("VideoGalleryImage").ToString()
                hdnVGal.Value = reader("VideoGalleryImage").ToString()
                If IsDBNull(reader("GalleryID")) = False Then
                    ddlPGallery.SelectedValue = reader("GalleryID").ToString()
                End If
                If IsDBNull(reader("VGalleryID")) = False Then
                    ddlVGallery.SelectedValue = reader("VGalleryID").ToString
                End If
                sqlConn.Close()
            End If



        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.ServerClick
        '   Try
        lblLastUpdated.Text = DateTime.Now()
        If fuSmallImage.FileName <> "" Then
            hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
            imgSmallImage.ImageUrl = "~/Admin/" + hdnSmallImage.Value
            imgSmallImage.Visible = True
        End If
        If fuBigImage.FileName <> "" Then
            hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
            imgBigImage.ImageUrl = "~/Admin/" + hdnBigImage.Value
            imgBigImage.Visible = True
        End If

        If fuOtherImage.FileName <> "" Then
            hdnOtherfile.Value = Utility.AddImage(fuOtherImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Other", Utility.EncodeTitle(txtTitle.Text, "-") & "-Other"), Server)
            ImgOther.ImageUrl = "~/Admin/" + hdnOtherfile.Value
            ImgOther.Visible = True
        End If
        If fuPGal.FileName <> "" Then
            hdnPGal.Value = Utility.AddImage(fuPGal, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Other", Utility.EncodeTitle(txtTitle.Text, "-") & "-Other"), Server)
            ImgPGal.ImageUrl = "~/Admin/" + hdnPGal.Value
            ImgPGal.Visible = True
        End If
        If fuVGallery.FileName <> "" Then
            hdnVGal.Value = Utility.AddImage(fuVGallery, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Other", Utility.EncodeTitle(txtTitle.Text, "-") & "-Other"), Server)
            ImgVGal.ImageUrl = "~/Admin/" + hdnVGal.Value
            ImgVGal.Visible = True
        End If
        If fufile.FileName <> "" Then
            hdnFile.Value = Utility.UploadFile(fufile, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-File", Utility.EncodeTitle(txtTitle.Text, "-") & "-File"), Server)
        End If
        Dim a As Integer
        If String.IsNullOrEmpty(Request.QueryString("eventId")) Or Request.QueryString("new") = 1 Then
            If Request.QueryString("new") <> "1" Then
                hdnMasterID.Value = GetMasterID()
            End If

            If SqlDatasourceEvent.Insert() > 0 Then

                hdnNewID.Value = getNewID()
                a = SqlDataSourceSEO.Insert()
                Response.Redirect("AllEvent.aspx")
            Else
                divSuccess.Visible = False
                divError.Visible = True
                lblErrMessage.Text = "Failed"
            End If


        Else

            If SqlDatasourceEvent.Update() > 0 Then
                a = 1
            Else
                divSuccess.Visible = False
                divError.Visible = True
                lblErrMessage.Text = "Failed"
                Return
            End If

        End If

        If a > 0 Then
            divSuccess.Visible = True
            divError.Visible = False
        Else
            divSuccess.Visible = False
            divError.Visible = True
        End If
        'Catch ex As Exception
        '    divSuccess.Visible = False
        '    divError.Visible = True
        'End Try
    End Sub

    Protected Function GetMasterID() As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT (Max( MasterID)+1) as MaxMasterID  FROM   List_Event "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)


        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = If(reader("MaxMasterID") & "" = "", "1", reader("MaxMasterID") & "")
        End If
        conn.Close()

        Return retVal
    End Function


    Private Function getNewID() As String
        Dim newID As String = ""
        Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(strConn)
        sqlConn.Open()
        Dim sqlcomm As SqlCommand = New SqlCommand()
        sqlcomm.CommandText = "SELECT Max(EventID) as MaxID FROM List_Event"
        sqlcomm.Connection = sqlConn
        sqlcomm.CommandType = Data.CommandType.Text
        Dim reader As SqlDataReader = sqlcomm.ExecuteReader()
        reader.Read()
        newID = CInt(reader("MaxID").ToString())
        sqlConn.Close()
        Return newID
    End Function

    

   
  
End Class
