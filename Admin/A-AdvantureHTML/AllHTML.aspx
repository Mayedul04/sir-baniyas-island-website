﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="AllHTML.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">
        Fixed Text</h1>
    <div class="btn-toolbar" style="display:none" >
        <button runat="server" id="btnAddNew" class="btn btn-primary">
            <i class="icon-save"></i> Add New</button>
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="All Texts"></asp:Label></h2>
    <div>
        <p >
            <label>
                Language</label>
            <asp:DropDownList ID="ddlLang" runat="server" CssClass="input-xlarge" AutoPostBack="true"
                DataSourceID="sdsLang" DataTextField="LangFullName" DataValueField="Lang">
            </asp:DropDownList>
            <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
            </asp:SqlDataSource>
        </p>
        <div class="well">
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            HTML ID
                        </th>
                        <th>
                            Title
                        </th>
                        <th>
                            Details
                        </th>
                        <th>
                            Master ID
                        </th>
                       <%-- <th>
                            Lang
                        </th>--%>
                       <%-- <th>Images</th>--%>
                        <th style="width: 60px;">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="HtmlID" DataSourceID="sdsHTML">
                        <EmptyDataTemplate>
                            <table runat="server" style="">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                <td>
                                    <asp:Label ID="HTMLID" runat="server" Text='<%# Eval("HTMLID") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="SmallDetailsLabel" runat="server" Text='<%# Eval("SmallDetails") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblMasterID" runat="server" Text='<%# Eval("MasterID") %>' />
                                </td>
                                <%--<td>
                                    <asp:Label ID="LangLabel" runat="server" Text='<%# Eval("Lang") %>' />
                                </td>--%>
                               <%-- <td>
                                    <a href="../A-CommonGallery/AllCommonGallery.aspx?TableName=HTML&TableMasterID=<%# Eval("MasterID") %>&BigImageWidth=205&BigImageHeight=126&t=<%# Eval("Title") %>">Add/Edit Images</a>
                                </td>--%>
                                <td>
                                    <a href='<%# "HTML.aspx?hid=" & Eval("HTMLID") %>' title="Edit"><i class="icon-pencil"></i></a>
                                    <%--&nbsp 
                                    <a href='<%# "#" & Eval("HtmlID") %>' data-toggle="modal"><i class="icon-remove">
                                        </i></a>--%>
                                    <div class="modal small hide fade" id='<%# Eval("HtmlID") %>' tabindex="-1" role="dialog"
                                        aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                ×</button>
                                            <h3 id="myModalLabel">
                                                Delete Confirmation</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p class="error-text">
                                                <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">
                                                Cancel</button>
                                            <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                                Text="Delete" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <div style="" class="paginationNew pull-right ">
                                <asp:DataPager ID="DataPager1" PageSize="50" runat="server">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                            ShowPreviousPageButton="True" />
                                        <asp:NumericPagerField />
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                            ShowPreviousPageButton="False" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </LayoutTemplate>
                    </asp:ListView>
                </tbody>
            </table>
        </div>
        <asp:SqlDataSource ID="sdsHTML" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="SELECT HTML.* FROM [HTML] inner join Languages on HTML.Lang=Languages.Lang  where HTML.Lang = @Lang and HTML.MasterID in (6) order by MasterID, Languages.SortIndex"
            DeleteCommand="delete from HTML where HtmlID=@HtmlID">
            <DeleteParameters>
                <asp:Parameter Name="HtmlID" />
            </DeleteParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue"
                    DefaultValue="en" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    <!-- Eof content -->
</asp:Content>
