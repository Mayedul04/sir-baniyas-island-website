﻿
Partial Class Admin_MasterPage_Main
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            If Not Utility.isLoggedIn(Request) Then
                Response.Cookies("userName").Value = ""
                Response.Cookies("userName").Expires = Date.Now.AddDays(-2)
                Response.Cookies("userFullName").Value = ""
                Response.Cookies("userFullName").Expires = Date.Now.AddDays(-2)
                Response.Cookies("userpass").Value = ""
                Response.Cookies("userpass").Expires = Date.Now.AddDays(-2)

                If Request.Url.AbsolutePath.ToLower().Contains("default.aspx") Then
                    Response.Redirect("A-Login/login.aspx")
                End If
                Response.Redirect("../A-Login/login.aspx")
            End If
            ltUser.Text = Request.Cookies("userFullName").Value
            ' GetToggleClass()
        End If
    End Sub



    Protected Function GetCurrentClass(pageName As String) As String
        Dim virtualPath As String = Page.AppRelativeVirtualPath.Replace("~/", "").ToLower()
        If virtualPath.Contains(pageName.ToLower()) Then
            Return "active"

        Else
            Return ""
        End If

    End Function

    'Protected Sub GetToggleClass()
    '    Dim virtualPath As String = Request.Url.AbsoluteUri
    '    If virtualPath.Contains("/A-WildLifeHTML/") Or virtualPath.Contains("/A-WildLifeAnimal/") Then
    '        WildLife.Attributes.Remove("class")
    '        WildLife.Attributes.Add("class", "nav nav-list collapse in")
    '    ElseIf virtualPath.Contains("/A-Home/") Or virtualPath.Contains("/A-HTML/") Then
    '        dashboard.Attributes.Remove("class")
    '        dashboard.Attributes.Add("class", "nav nav-list collapse in")
    '    End If

    'End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                If Request.Cookies("backurlAdmin") Is Nothing Then
                    Dim backurlAdmin = New HttpCookie("backurlAdmin", Request.UrlReferrer.AbsoluteUri)
                    Response.Cookies.Add(backurlAdmin)
                Else



                    Response.Cookies("backurlAdmin").Value = Request.UrlReferrer.AbsoluteUri


                End If
            End If

        Catch ex As Exception
            Response.Cookies("backurlAdmin").Value = Request.Url.AbsoluteUri
        End Try
       

    End Sub
End Class

