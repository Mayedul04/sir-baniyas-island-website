﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="MediaFile.aspx.vb" Inherits="Admin_A_Media_Files_MediaFile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <h1 class="page-title">Media File</h1>
    <div class="btn-toolbar">

        <a href="../A-Media-Files/AllMediaFiles.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add News"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <p>
                <label>
                    Title:</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <p>
                <label>Section:</label>
                <asp:DropDownList ID="ddlCat" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Value="">Select</asp:ListItem>
                    <asp:ListItem>Photo</asp:ListItem>
                    <asp:ListItem>Video</asp:ListItem>
                    <asp:ListItem>Brochure</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlCat" ValidationGroup="form" ErrorMessage="*"></asp:RequiredFieldValidator>
            </p>
            <asp:Panel ID="pnlSmallImage" runat="server">
                <p>
                    <asp:Image ID="imgSmallImage" runat="server" Width="100px" Visible="false" />
                    <asp:HiddenField ID="hdnSmallImage" runat="server" />
                </p>
                <p>
                    <label>
                        Upload Small Image : (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                    <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>

            
            <p>
                <label>
                    File Name(Path): 
                </label>
                <asp:TextBox ID="txtFileName" runat="server" Text="" CssClass="input-xlarge" MaxLength="400" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFileName" ValidationGroup="form" ErrorMessage="*"></asp:RequiredFieldValidator>
            </p>
            <p>
                <label>
                    Sort Order:
                </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                        MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"
                        ValidationGroup="form"></asp:RangeValidator>
                </label>
            </p>

            <p>
                <asp:CheckBox ID="chkStatus" Checked="true" runat="server" TextAlign="Left" Text="Status " />
                <label class="red">
                </label>
            </p>

            <p>
                <label>
                    Language</label>
                <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                    DataTextField="LangFullName" DataValueField="Lang">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
            </p>
            <asp:HiddenField ID="hdnMasterID" runat="server" />
            <asp:HiddenField ID="hdnUpdateDate" runat="server" />
        </div>

        <div class="btn-toolbar">
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">
                <i class="icon-save"></i>Add New</button>

            <div class="btn-group">
            </div>
        </div>
    </div>
    <asp:SqlDataSource ID="sdsFiles" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [MediaFiles] WHERE [AlbumID] = @AlbumID" 
        InsertCommand="INSERT INTO [MediaFiles] ([Title], [Section], [CoverImage], [FileName], [LastUpdated], [MasterID], [Lang], [Status], [SortIndex]) VALUES (@Title, @Section, @CoverImage, @FileName, @LastUpdated, @MasterID, @Lang, @Status, @SortIndex)" 
        SelectCommand="SELECT * FROM [MediaFiles]" UpdateCommand="UPDATE [MediaFiles] SET [Title] = @Title, [Section] = @Section, [CoverImage] = @CoverImage, [FileName] = @FileName, [LastUpdated] = @LastUpdated, [MasterID] = @MasterID, [Lang] = @Lang, [Status] = @Status, [SortIndex] = @SortIndex WHERE [AlbumID] = @AlbumID">
        <DeleteParameters>
            <asp:Parameter Name="AlbumID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="ddlCat" Name="Section" PropertyName="SelectedValue" Type="String" />
            <asp:ControlParameter ControlID="hdnSmallImage" Name="CoverImage" PropertyName="Value" Type="String" />
            <asp:ControlParameter ControlID="txtFileName" Name="FileName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" PropertyName="Value" Type="DateTime" />
            <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" Type="String" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="ddlCat" Name="Section" PropertyName="SelectedValue" Type="String" />
            <asp:ControlParameter ControlID="hdnSmallImage" Name="CoverImage" PropertyName="Value" Type="String" />
            <asp:ControlParameter ControlID="txtFileName" Name="FileName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" PropertyName="Value" Type="DateTime" />
            <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" Type="String" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text" Type="Int32" />
            <asp:QueryStringParameter Name="AlbumID" QueryStringField="fid" Type="Int32" />
        </UpdateParameters>
     </asp:SqlDataSource>
    <!-- Eof content -->
</asp:Content>

