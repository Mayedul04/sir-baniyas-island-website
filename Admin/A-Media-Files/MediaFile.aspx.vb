﻿
Partial Class Admin_A_Media_Files_MediaFile
    Inherits System.Web.UI.Page
    Protected smallImageWidth As String = "370", smallImageHeight As String = "213", bigImageWidth As String = "555", bigImageHeight As String = "310", videoWidth As String = "", videoHeight As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("fid")) Then
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
                lblTabTitle.Text = "Update File"
                LoadContent(Request.QueryString("fid"))
            Else

            End If
           
        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnUpdateDate.Value = DateTime.Now


        If fuSmallImage.FileName <> "" Then
            hdnSmallImage.Value = Utility.AddImage(fuSmallImage, Utility.EncodeTitle(txtTitle.Text, "-") & "-Cover", Server)
        End If
       
        If hdnSmallImage.Value <> "" Then
            imgSmallImage.Visible = True
            imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
        End If
        


        'hdnDetails.Value = FCKeditor1.Value

        If String.IsNullOrEmpty(Request.QueryString("fid")) Or Request.QueryString("new") = 1 Then
            If Request.QueryString("new") <> "1" Then
                hdnMasterID.Value = GetMasterID()
            End If

            If sdsFiles.Insert() > 0 Then

                Response.Redirect("AllMediaFiles.aspx")
            Else
                divError.Visible = True
            End If
        Else
            If sdsFiles.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = False
            End If
        End If

    End Sub

    Protected Function GetMasterID() As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT (Max( MasterID)+1) as MaxMasterID  FROM   MediaFiles "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)


        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = If(reader("MaxMasterID") & "" = "", "1", reader("MaxMasterID") & "")
        End If
        conn.Close()

        Return retVal
    End Function

    


    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT      *   FROM   MediaFiles where AlbumID=@AlbumID  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("AlbumID", Data.SqlDbType.Int)
        cmd.Parameters("AlbumID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            ddlCat.SelectedValue = reader("Section").ToString()
            txtTitle.Text = reader("Title") & ""
            hdnSmallImage.Value = reader("CoverImage") & ""
          
            If hdnSmallImage.Value <> "" Then
                imgSmallImage.Visible = True
                imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
            End If
           
           
            txtSortIndex.Text = reader("SortIndex") & ""
            Boolean.TryParse(reader("Status") & "", chkStatus.Checked)
            hdnUpdateDate.Value = reader("LastUpdated") & ""

            hdnMasterID.Value = reader("MasterID") & ""
            ddlLang.SelectedValue = reader("Lang") & ""
            txtFileName.Text = reader("FileName").ToString()
        End If
        conn.Close()
    End Sub
End Class
