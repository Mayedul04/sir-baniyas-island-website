﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="GalleryItem.aspx.vb" Inherits="GalleryItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">
        Viseo Gallery Item</h1>
    <div class="btn-toolbar">
        
        <a href='<%= "AllGalleryItem.aspx?galleryId=" & Request.QueryString("galleryId")  %>'
            data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add Video Gallery Item"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
            </span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <p>
                <label>
                    Title</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge"></asp:TextBox>
            </p>
            <p>
                <label>
                    Small Details</label>
                <asp:TextBox ID="txtSmallDetails" runat="server" Height="100" Width="550" TextMode="MultiLine"
                    CssClass="input-xlarge"></asp:TextBox>
            </p>

            
            <p>
                 <asp:Image ID="imgVideoImageURL" runat="server" Width="100px"/>
                <label>
                   Video:(Vimeo/Youtube)(Width=<%= videoWidth%>; Height=<%= videoHeight%>)
                </label>
                 <asp:TextBox ID="txtVideoOriginalURL" runat="server" CssClass="txt"></asp:TextBox>
                    
                    <asp:HiddenField ID="hdnVideoEmbedCode" runat="server" />
                    <asp:HiddenField ID="hdnVideoImageURL" runat="server" />
                 <label class="red">
                    <asp:RegularExpressionValidator Display="Dynamic"  ID="RegularExpressionValidator2" 
                        ValidationGroup="form" ControlToValidate="txtVideoOriginalURL" 
                        SetFocusOnError="true" runat="server" ErrorMessage="* Invalid URL" 
                        ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"></asp:RegularExpressionValidator>
                </label>
    
            </p>






            <p style="display:Block  ">
                <label>
                    Small Image (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                <asp:Image ID="imgSmallImage" runat="server" />
            </p>
            <p style="display:Block  ">
                <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                <label>
                    <%--<asp:RequiredFieldValidator ID="rfvSmallImage" runat="server" Display="Dynamic" ControlToValidate="fuSmallImage"
                        ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                </label>
                <asp:HiddenField ID="hdnSmallImage" runat="server" />
            </p>
            <p style="display:none  ">
                <label>
                    Big Image (Width=<%= bigImageWidth%>; Height=<%= bigImageHeight%>)</label>
                <asp:Image ID="imgBigImage" Visible="false" runat="server" />
            </p>
            <p style="display:none  ">
                <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                <%--<label>
                    <asp:RequiredFieldValidator ID="rfvBigImage" runat="server" Display="Dynamic" ControlToValidate="fuBigImage"
                        ErrorMessage="* Required"></asp:RequiredFieldValidator>
                </label>--%>
                <asp:HiddenField ID="hdnBigImage" runat="server" />
            </p>
            <p style="display:block  ">
                <label>
                    Image CSS Class(Optiomal)</label>
                <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
            </p>
            <p>
                <label>
                    Sort Order:
                </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" Display="Dynamic"
                        SetFocusOnError="true" MinimumValue="1" MaximumValue="999999" runat="server"
                        ErrorMessage="* range from 1 to 999999"></asp:RangeValidator>
                </label>
            </p>
            <p>
                <label>
                    Status:</label>
                <asp:CheckBox ID="chkStatus" CssClass="chkbox" runat="server" Checked="true" />
            </p>
            <p>
                <label>
                    Last Updated :</label>
                <asp:Label ID="lbLastUpdated" runat="server"></asp:Label>
            </p>
        </div>

        <div class="btn-toolbar">
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">
                <i class="icon-save"></i> Add New</button>
            
            <div class="btn-group">
            </div>
        </div>
    </div>
    <!-- Eof content -->
    <asp:SqlDataSource ID="SqlDataSourceGalleryItem" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [VideoItem] WHERE [GalleryItemID] = @GalleryItemID"
        InsertCommand="INSERT INTO [VideoItem] ([GalleryID], [Title], [SmallDetails], [SmallImage], [BigImage], [ImageAltText], [SortIndex], [Status],  [LastUpdated],[VideoOriginalURL], [VideoImageURL], [VideoEmbedCode]) VALUES (@GalleryID, @Title, @SmallDetails, @SmallImage, @BigImage, @ImageAltText, @SortIndex, @Status, @LastUpdated,@VideoOriginalURL, @VideoImageURL, @VideoEmbedCode)"
        SelectCommand="SELECT * FROM [VideoItem]" UpdateCommand="UPDATE [VideoItem] SET [Title] = @Title, [SmallDetails] = @SmallDetails, [SmallImage] = @SmallImage, [BigImage] = @BigImage, [ImageAltText]=@ImageAltText, [SortIndex] = @SortIndex, [Status] = @Status,  [LastUpdated] = @LastUpdated,[VideoOriginalURL]=@VideoOriginalURL, [VideoImageURL]=@VideoImageURL, [VideoEmbedCode]=@VideoEmbedCode WHERE [GalleryItemID] = @GalleryItemID">
        <DeleteParameters>
            <asp:Parameter Name="GalleryItemID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:QueryStringParameter Name="GalleryID" QueryStringField="galleryId" Type="Int32" />
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                Type="Int32" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                Type="Boolean" />
            <asp:ControlParameter ControlID="lbLastUpdated" Name="LastUpdated" PropertyName="Text"
                Type="DateTime" />

             <asp:ControlParameter ControlID="txtVideoOriginalURL" Name="VideoOriginalURL" 
                                PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="hdnVideoImageURL" Name="VideoImageURL" 
                                PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnVideoEmbedCode" Name="VideoEmbedCode" 
                                PropertyName="Value" Type="String" />

        </InsertParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                Type="Int32" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                Type="Boolean" />
            <asp:ControlParameter ControlID="lbLastUpdated" Name="LastUpdated" PropertyName="Text"
                Type="DateTime" />
             <asp:ControlParameter ControlID="txtVideoOriginalURL" Name="VideoOriginalURL" 
                                PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="hdnVideoImageURL" Name="VideoImageURL" 
                                PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnVideoEmbedCode" Name="VideoEmbedCode" 
                                PropertyName="Value" Type="String" />
            <asp:QueryStringParameter Name="GalleryItemID" QueryStringField="galleryItemId" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
