﻿<%@ Page Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master" ValidateRequest="false"
    AutoEventWireup="false" ViewStateEncryptionMode="Never" EnableViewStateMac="false"
    CodeFile="GalleryEdit.aspx.vb" Inherits="GalleryEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="block">
        <div class="block-heading">
            Video Gallery</div>
        <div class="block-body">
            <h1 class="page-title">
               Video  Gallery</h1>
            
            <h2>
                <asp:Label ID="lblTabTitle" runat="server" Text="Add Gallery"></asp:Label></h2>
            <div class="success-details" visible="false" id="divSuccess" runat="server">
                <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
                <div class="corners">
                    <span class="success-left-top"></span><span class="success-right-top"></span><span
                        class="success-left-bot"></span><span class="success-right-bot"></span>
                </div>
            </div>
            <div class="error-details" id="divError" visible="false" runat="server">
                <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
                <div class="corners">
                    <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
                    </span><span class="error-right-bot"></span>
                </div>
            </div>
            <!-- content -->
            <div class="well">
                <div id="myTabContent" class="tab-content">
                    <p>
                        <label>
                            Category</label>
                        <asp:DropDownList ID="ddCategory" runat="server" Enabled ="false"  AutoPostBack="True">
                            <asp:ListItem Text="All" Value="0" Selected="True"></asp:ListItem>
                 
                        </asp:DropDownList>
                    </p>
                    <p>
                        <label>
                            Title</label>
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge"></asp:TextBox>
                        <label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                ControlToValidate="txtTitle" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                        </label>
                    </p>
                    <p>
                        <label>
                            Small Image (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                        <asp:Image ID="imgSmallImage" runat="server" />
                    </p>
                    <p>
                        <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                        <label>
                            <asp:RequiredFieldValidator ID="rfvSmallImage" runat="server" Display="Dynamic" ControlToValidate="fuSmallImage"
                                ErrorMessage="* Required"></asp:RequiredFieldValidator>
                        </label>
                        <asp:HiddenField ID="hdnSmallImage" runat="server" />
                    </p>
                    <p>
                        <label>
                            Image Alt Text</label>
                        <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
                    </p>
                    <p>
                        <label>
                            Sort Order:
                        </label>
                        <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                        <label class="red">
                            <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" Display="Dynamic"
                                SetFocusOnError="true" MinimumValue="1" MaximumValue="999999" runat="server"
                                ErrorMessage="* range from 1 to 999999"></asp:RangeValidator>
                        </label>
                    </p>
                    <p>
                        <label>
                            Status:</label>
                        <asp:CheckBox ID="chkStatus" CssClass="input-xlarge" runat="server" Checked="true" />
                    </p>
                    <p>
                        <label>
                            Language</label>
                        <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                            DataTextField="LangFullName" DataValueField="Lang">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
                        </asp:SqlDataSource>
                    </p>
                    <asp:HiddenField ID="hdnMasterID" runat="server" />
                    <p>
                        <label>
                            Last Updated :</label>
                        <asp:Label ID="lbLastUpdated" runat="server"></asp:Label>
                    </p>
                </div>

                <div class="btn-toolbar">
                    <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">
                        <i class="icon-save"></i> Add New</button>
            
                    <div class="btn-group">
                    </div>
                </div>
            </div>
            <!-- Eof content -->
            <asp:SqlDataSource ID="SqlDataSourceGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                DeleteCommand="DELETE FROM [Video] WHERE [GalleryID] = @GalleryID" InsertCommand="INSERT INTO [Video] ([CategoryID], [Title], [SmallImage], [ImageAltText], [SortIndex], [Status], [LastUpdated],MasterID, Lang) VALUES (@CategoryID, @Title, @SmallImage, @ImageAltText, @SortIndex, @Status, @LastUpdated,@MasterID, @Lang)"
                SelectCommand="SELECT * FROM [Video]" UpdateCommand="UPDATE [Video] SET [CategoryID] = @CategoryID, [Title] = @Title, [SmallImage] = @SmallImage, [ImageAltText] = @ImageAltText, [SortIndex] = @SortIndex, [Status] = @Status, [LastUpdated] = @LastUpdated,MasterID=@MasterID, Lang=@Lang  WHERE [GalleryID] = @GalleryID">
                <DeleteParameters>
                    <asp:Parameter Name="GalleryID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="ddCategory" Name="CategoryID" Type="Int32" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                        Type="Boolean" />
                    <asp:ControlParameter ControlID="lbLastUpdated" Name="LastUpdated" PropertyName="Text"
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:ControlParameter ControlID="ddCategory" Name="CategoryID" Type="Int32" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                        Type="Boolean" />
                    <asp:ControlParameter ControlID="lbLastUpdated" Name="LastUpdated" PropertyName="Text"
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
                    <asp:QueryStringParameter Name="GalleryID" QueryStringField="galleryId" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <asp:HiddenField ID="hdnNewID" runat="server" />
            <asp:SqlDataSource ID="SqlDataSourceSEO" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                InsertCommand="INSERT INTO [SEO] ([PageID], [PageType], [SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot],Lang) VALUES (@PageID, @PageType, @SEOTitle, @SEODescription, @SEOKeyWord, @SEORobot,@Lang)">
                <InsertParameters>
                    <asp:ControlParameter ControlID="hdnNewID" DefaultValue="" Name="PageID" PropertyName="Value"
                        Type="Int32" />
                    <asp:Parameter Name="PageType" Type="String" DefaultValue="Video" />
                    <asp:ControlParameter ControlID="txtTitle" Name="SEOTitle" PropertyName="Text" Type="String" />
                    <asp:Parameter Name="SEODescription" Type="String" DefaultValue="" />
                    <asp:Parameter Name="SEOKeyWord" Type="String" DefaultValue="" />
                    <asp:Parameter Name="SEORobot" Type="Boolean" DefaultValue="true" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
                </InsertParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
