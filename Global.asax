﻿<%@ Application Language="VB" %>
<%@ Import Namespace="System.Globalization" %>
<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
        RegisterRoute(Routing.RouteTable.Routes)
        RegisterRouteAr(Routing.RouteTable.Routes)
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
    
    Sub RegisterRoute(ByVal routes As Routing.RouteCollection)
       
        routes.MapPageRoute("Default-Home", "", "~/Default.aspx")
        routes.MapPageRoute("ContactUs", "Contact-us", "~/ContactUs.aspx")
        routes.MapPageRoute("AboutUs", "AboutUs", "~/AboutUs.aspx")
        routes.MapPageRoute("Dine1", "Dine", "~/Dine.aspx")
        routes.MapPageRoute("Relax", "Relax", "~/Relax.aspx")
        routes.MapPageRoute("Spa", "Spa", "~/RelaxSpa.aspx")
        routes.MapPageRoute("Pool", "Beaches", "~/RelaxPools.aspx")
        routes.MapPageRoute("Enquiry", "Enquiry/{section}", "~/Enquiry.aspx")
        routes.MapPageRoute("Travel", "Travel", "~/Travel.aspx")
        routes.MapPageRoute("Travel1", "Travel-Air", "~/TravelAir.aspx")
        routes.MapPageRoute("Travel2", "Travel-Road", "~/TravelRoad.aspx")
        routes.MapPageRoute("Travel3", "Travel-Sea", "~/TravelSea.aspx")
        routes.MapPageRoute("Gallery", "Gallery", "~/Gallery.aspx")
        routes.MapPageRoute("PhotoGallery", "PhotoGallery", "~/PhotoGallery.aspx")
        routes.MapPageRoute("PhotoGallery1", "PhotoGallery/{page}", "~/PhotoGallery.aspx")
        routes.MapPageRoute("PhotoGalleryInner", "InnerPhotoGallery/{id}/{title}", "~/InnerPhotoGallery.aspx")
        routes.MapPageRoute("PhotoGalleryInner1", "InnerPhotoGallery/{id}/{title}/{page}", "~/InnerPhotoGallery.aspx")
        routes.MapPageRoute("Photos", "Photos/{section}/{id}/{title}", "~/Photos.aspx")
        routes.MapPageRoute("Photos1", "Photos/{section}/{id}/{title}/{page}", "~/Photos.aspx")
        routes.MapPageRoute("PhotoPop", "PhotoPop/{section}/{id}", "~/PhotoPop.aspx")
        routes.MapPageRoute("Exp", "Experiences", "~/Experiences.aspx")
        
        
        routes.MapPageRoute("VideoGallery", "VideoGallery", "~/VideoGallery.aspx")
        routes.MapPageRoute("VideoGalleryInner", "InnerVideoGallery/{id}/{title}", "~/InnerVideoGallery.aspx")
        routes.MapPageRoute("VideoGalleryInner1", "InnerVideoGalleryInner/{id}/{title}/{page}", "~/InnerVideoGallery.aspx")
        routes.MapPageRoute("VideoGallery1", "VideoGallery/{page}", "~/VideoGallery.aspx")
        routes.MapPageRoute("Videos", "Videos/{section}/{id}/{title}", "~/Videos.aspx")
        routes.MapPageRoute("Videos1", "Videos/{section}/{id}/{title}/{page}", "~/Videos.aspx")
        routes.MapPageRoute("VideoPop", "VideoPop/{section}/{id}", "~/VideoPop.aspx")
        routes.MapPageRoute("MediaCentre", "Media-Centre", "~/MediaCentre.aspx")
        routes.MapPageRoute("MediaLibrary", "Media-Library-Request", "~/MediaLibrary.aspx")
        routes.MapPageRoute("Media", "Media-Library-Loggedin/{section}", "~/MediaLogged.aspx")
        
        routes.MapPageRoute("News", "Island-News", "~/News.aspx")
        routes.MapPageRoute("Search", "Search", "~/search.aspx")
        routes.MapPageRoute("Search1", "Search/{q}", "~/search.aspx")
        routes.MapPageRoute("Search2", "Search/{q}/{page}", "~/search.aspx")
       
        routes.MapPageRoute("News2", "Featured-News/{id}/{title}", "~/FeaturedNews.aspx")
        routes.MapPageRoute("News4", "Featured-News", "~/FeaturedNews.aspx")
        routes.MapPageRoute("News3", "Press-Release-News", "~/PressNews.aspx")
        routes.MapPageRoute("New", "WhatsNew", "~/WhatsNew.aspx")
        routes.MapPageRoute("New1", "WhatsNew/{page}", "~/WhatsNew.aspx")
        routes.MapPageRoute("New2", "Whats-New-Details/{id}/{title}", "~/WhatsNewDetails.aspx")
       
        routes.MapPageRoute("Event", "Events", "~/Events.aspx")
        routes.MapPageRoute("Event1", "Event/{id}/{title}", "~/EventDetails.aspx")
        routes.MapPageRoute("Event2", "Events-Summary", "~/EventSummary.aspx")
        
        
        routes.MapPageRoute("Facts", "Interesting-Facts", "~/InterestingFacts.aspx")
        routes.MapPageRoute("Facts1", "Interesting-Facts/{page}", "~/InterestingFacts.aspx")
        
        routes.MapPageRoute("Thanks3", "Thanks-news", "~/Thankyou-Newsletter.aspx")
        routes.MapPageRoute("Thanks4", "Thank-you/{section}", "~/thankyou.aspx")
        routes.MapPageRoute("Thanks", "Thank-you", "~/thankyou.aspx")
        routes.MapPageRoute("Thanks1", "Thanks/{st}", "~/Thanks.aspx")
        routes.MapPageRoute("Thanks2", "Thanks/{section}", "~/Thanks.aspx")
        routes.MapPageRoute("Archaelogical1", "Archaelogical-Details", "~/Archaelogical-detail.aspx")
        
        routes.MapPageRoute("WildLife", "Wildlife", "~/Wildlife.aspx")
        routes.MapPageRoute("Arabian-WildLife", "Arabian-Wildlife", "~/Arabian-Wildlife.aspx")
        routes.MapPageRoute("Animal", "Animal", "~/Animal.aspx")
        routes.MapPageRoute("Animal-list", "MeetAnimal/{id}/{t}", "~/MeetAnimal.aspx")
        routes.MapPageRoute("Breeding", "Breeding", "~/Breeding.aspx")
        routes.MapPageRoute("Adventure", "Adventure", "~/Advanture.aspx")
        routes.MapPageRoute("Adventure1", "Adventure-Activity-Land", "~/ActivityLand.aspx")
        routes.MapPageRoute("Adventure2", "Adventure-Activity-Water", "~/ActivityWater.aspx")
         routes.MapPageRoute("Adventure3", "Adventure-Activity-Day-Trip", "~/Daytrips.aspx")
        routes.MapPageRoute("Adventure-Activity", "Adventure-Activity/{id}/{t}", "~/Advanture-Activity.aspx")
        routes.MapPageRoute("Stay", "Stay", "~/Stay.aspx")
        routes.MapPageRoute("Stay-Details", "Stay-Details/{id}/{t}", "~/Stay-Details.aspx")
        routes.MapPageRoute("DesertIsland", "Desert-Islands", "~/Desert-Island.aspx")
        routes.MapPageRoute("SirBaniYasIsland", "Sir-Bani-Yas-Island", "~/Sir-Bani-Yas-Island.aspx")
        routes.MapPageRoute("DelmaIsland", "Dalma-Island", "~/Delma-Island.aspx")
        routes.MapPageRoute("Conservation", "Conservation", "~/Conservation.aspx")
        routes.MapPageRoute("Disclaimer", "Disclaimer", "~/Disclaimer.aspx")
        routes.MapPageRoute("DiscoveryIsland", "Discovery-Islands", "~/Discovery-Island.aspx")
        routes.MapPageRoute("Flora", "Flora", "~/Flora.aspx")
        routes.MapPageRoute("GoogleSearch", "Google-Search/{t}", "~/Google-Search.aspx")
        routes.MapPageRoute("HistoricalTimeline", "Historical-Timeline", "~/Historical-Timeline.aspx")
        routes.MapPageRoute("History", "History", "~/History.aspx")
        routes.MapPageRoute("MapPop", "Map-Pop", "~/Map-Pop.aspx")
        routes.MapPageRoute("IslandMapPop", "IslandMap-Pop", "~/IslandMap-Pop.aspx")
        routes.MapPageRoute("Mangrove", "Mangrove", "~/Mangrove.aspx")
        routes.MapPageRoute("Play-Your-Trip", "Play-Your-Trip/{t}", "~/Play-Your-Trip.aspx")
        routes.MapPageRoute("PrivacyPolicy", "Privacy-Policy", "~/PrivacyPolicy.aspx")
        routes.MapPageRoute("SaltDome", "Salt-Domes", "~/Salt-Dome.aspx")
        routes.MapPageRoute("SirBaniYasHistory", "Sir-Bani-Yas-History", "~/Sir-Bani-Yas-History.aspx")
        routes.MapPageRoute("Sitemap", "Sitemap", "~/Sitemap.aspx")
        routes.MapPageRoute("SuccessStory", "Success-Story/{id}/{t}", "~/Success-Story.aspx")
        routes.MapPageRoute("TermsCondition", "Terms-Condition", "~/TermsCondition.aspx")
        routes.MapPageRoute("ThankyouNewsletter", "Thankyou-Newsletter", "~/Thankyou-Newsletter.aspx")
        routes.MapPageRoute("VetConservationTeam", "Vet-Conservation-Team", "~/Vet-Conservation-Team.aspx")
        routes.MapPageRoute("VetConservationTeam1", "Vet-Conservation-Team/{page}", "~/Vet-Conservation-Team.aspx")
        
        routes.MapPageRoute("VetConservationDetails", "Vet-Conservation-Details/{id}/{t}", "~/Vet-Conservation-Details.aspx")
        
        routes.MapPageRoute("BookNOW", "BookNow-Pop", "~/BookNow-Pop.aspx")
        routes.MapPageRoute("PopAnimal", "PhotoPop-Animal/{section}/{id}", "~/PhotoPop-Animal.aspx")
        routes.MapPageRoute("Flights", "Flights", "~/Flights.aspx")
        routes.MapPageRoute("philosophy", "Philosophy", "~/philosophy.aspx")
        routes.MapPageRoute("AboutTDIC", "About-TDIC", "~/About-TDIC.aspx")
        routes.MapPageRoute("PrivateCharter", "Private-Charter-Flights", "~/PrivateCharterFlight.aspx")
        
        
        
        
    End Sub
       
    Sub RegisterRouteAr(ByVal routes As Routing.RouteCollection)
       
        routes.MapPageRoute("Default-Home-ar", "", "~/Ar/Default.aspx")
        routes.MapPageRoute("ContactUs-ar", "Ar/Contact-us", "~/Ar/ContactUs.aspx")
        routes.MapPageRoute("AboutUs-ar", "Ar/AboutUs", "~/Ar/AboutUs.aspx")
        routes.MapPageRoute("Dine1-ar", "Ar/Dine", "~/Ar/Dine.aspx")
        routes.MapPageRoute("Relax-ar", "Ar/Relax", "~/Ar/Relax.aspx")
        routes.MapPageRoute("Spa-ar", "Ar/Spa", "~/Ar/RelaxSpa.aspx")
        routes.MapPageRoute("Pool-ar", "Ar/Beaches", "~/Ar/RelaxPools.aspx")
        routes.MapPageRoute("Enquiry-ar", "Ar/Enquiry/{section}", "~/Ar/Enquiry.aspx")
        routes.MapPageRoute("Travel-ar", "Ar/Travel", "~/Ar/Travel.aspx")
        routes.MapPageRoute("Travel1-ar", "Ar/Travel-Air", "~/Ar/TravelAir.aspx")
        routes.MapPageRoute("Travel2-ar", "Ar/Travel-Road", "~/Ar/TravelRoad.aspx")
        routes.MapPageRoute("Travel3-ar", "Ar/Travel-Sea", "~/Ar/TravelSea.aspx")
        routes.MapPageRoute("Gallery-ar", "Ar/Gallery", "~/Ar/Gallery.aspx")
        routes.MapPageRoute("PhotoGallery-ar", "Ar/PhotoGallery", "~/Ar/PhotoGallery.aspx")
        routes.MapPageRoute("PhotoGallery1-ar", "Ar/PhotoGallery/{page}", "~/Ar/PhotoGallery.aspx")
        routes.MapPageRoute("PhotoGalleryInner-ar", "Ar/InnerPhotoGallery/{id}/{title}", "~/Ar/InnerPhotoGallery.aspx")
        routes.MapPageRoute("PhotoGalleryInner1-ar", "Ar/InnerPhotoGallery/{id}/{title}/{page}", "~/Ar/InnerPhotoGallery.aspx")
        routes.MapPageRoute("Photos-ar", "Ar/Photos/{section}/{id}/{title}", "~/Ar/Photos.aspx")
        routes.MapPageRoute("Photos1-ar", "Ar/Photos/{section}/{id}/{title}/{page}", "~/Ar/Photos.aspx")
        routes.MapPageRoute("PhotoPop-ar", "Ar/PhotoPop/{section}/{id}", "~/Ar/PhotoPop.aspx")
        routes.MapPageRoute("Exp-ar", "Ar/Experiences", "~/Ar/Experiences.aspx")
        
        
        routes.MapPageRoute("VideoGallery-ar", "Ar/VideoGallery", "~/Ar/VideoGallery.aspx")
        routes.MapPageRoute("VideoGallery1-ar", "Ar/VideoGallery/{page}", "~/Ar/VideoGallery.aspx")
        routes.MapPageRoute("VideoGalleryInner-ar", "Ar/InnerVideoGallery/{id}/{title}", "~/Ar/InnerVideoGallery.aspx")
        routes.MapPageRoute("VideoGalleryInner1-ar", "Ar/InnerVideoGalleryInner/{id}/{title}/{page}", "~/Ar/InnerVideoGallery.aspx")
        routes.MapPageRoute("Videos-ar", "Ar/Videos/{section}/{id}/{title}", "~/Ar/Videos.aspx")
        routes.MapPageRoute("Videos1-ar", "Ar/Videos/{section}/{id}/{title}/{page}", "~/Ar/Videos.aspx")
        routes.MapPageRoute("VideoPop-ar", "Ar/VideoPop/{section}/{id}", "~/Ar/VideoPop.aspx")
        routes.MapPageRoute("MediaCentre-ar", "Ar/Media-Centre", "~/Ar/MediaCentre.aspx")
        routes.MapPageRoute("MediaLibrary-ar", "Ar/Media-Library-Request", "~/Ar/MediaLibrary.aspx")
        routes.MapPageRoute("Media-ar", "Ar/Media-Library-Loggedin/{section}", "~/Ar/MediaLogged.aspx")
        
        routes.MapPageRoute("News-ar", "Ar/Island-News", "~/Ar/News.aspx")
        routes.MapPageRoute("Search-ar", "Ar/Search", "~/Ar/search.aspx")
        routes.MapPageRoute("Search1-ar", "Ar/Search/{q}", "~/Ar/search.aspx")
        routes.MapPageRoute("Search2-ar", "Ar/Search/{q}/{page}", "~/Ar/search.aspx")
       
        routes.MapPageRoute("News2-ar", "Ar/Featured-News/{id}/{title}", "~/Ar/FeaturedNews.aspx")
        routes.MapPageRoute("News4-ar", "Ar/Featured-News", "~/Ar/FeaturedNews.aspx")
        routes.MapPageRoute("News3-ar", "Ar/Press-Release-News", "~/Ar/PressNews.aspx")
        routes.MapPageRoute("New-ar", "Ar/WhatsNew", "~/Ar/WhatsNew.aspx")
        routes.MapPageRoute("New1-ar", "Ar/WhatsNew/{page}", "~/Ar/WhatsNew.aspx")
        routes.MapPageRoute("New2-ar", "Ar/Whats-New-Details/{id}/{title}", "~/Ar/WhatsNewDetails.aspx")
       
        routes.MapPageRoute("Event-ar", "Ar/Events", "~/Ar/Events.aspx")
        routes.MapPageRoute("Event1-ar", "Ar/Event/{id}/{title}", "~/Ar/EventDetails.aspx")
        routes.MapPageRoute("Event2-ar", "Ar/Events-Summary", "~/Ar/EventSummary.aspx")
        
        
        routes.MapPageRoute("Facts-ar", "Ar/Interesting-Facts", "~/Ar/InterestingFacts.aspx")
        routes.MapPageRoute("Facts1-ar", "Ar/Interesting-Facts/{page}", "~/Ar/InterestingFacts.aspx")
        
        routes.MapPageRoute("Thanks3-ar", "Ar/Thanks-news", "~/Ar/Thankyou-Newsletter.aspx")
        routes.MapPageRoute("Thanks-ar", "Ar/Thank-you", "~/Ar/thankyou.aspx")
        routes.MapPageRoute("Thanks1-ar", "Ar/Thanks/{st}", "~/Ar/Thanks.aspx")
        routes.MapPageRoute("Thanks2-ar", "Ar/Thanks/{section}", "~/Ar/Thanks.aspx")
        routes.MapPageRoute("Archaelogical1-ar", "Ar/Archaelogical-Details", "~/Ar/Archaelogical-detail.aspx")
        
        routes.MapPageRoute("WildLife-ar", "Ar/Wildlife", "~/Ar/Wildlife.aspx")
        routes.MapPageRoute("Arabian-WildLife-ar", "Ar/Arabian-Wildlife", "~/Ar/Arabian-Wildlife.aspx")
        routes.MapPageRoute("Animal-ar", "Ar/Animal", "~/Ar/Animal.aspx")
        routes.MapPageRoute("Animal-list-ar", "Ar/MeetAnimal/{id}/{t}", "~/Ar/MeetAnimal.aspx")
        routes.MapPageRoute("Breeding-ar", "Ar/Breeding", "~/Ar/Breeding.aspx")
        routes.MapPageRoute("Adventure-ar", "Ar/Adventure", "~/Ar/Advanture.aspx")
        routes.MapPageRoute("Adventure1-ar", "Ar/Adventure-Activity-Land", "~/Ar/ActivityLand.aspx")
        routes.MapPageRoute("Adventure2-ar", "Ar/Adventure-Activity-Water", "~/Ar/ActivityWater.aspx")
        routes.MapPageRoute("Adventure3-ar", "Ar/Adventure-Activity-Day-Trip", "~/Ar/Daytrips.aspx")
        routes.MapPageRoute("Adventure-Activity-ar", "Ar/Adventure-Activity/{id}/{t}", "~/Ar/Advanture-Activity.aspx")
        routes.MapPageRoute("Stay-ar", "Ar/Stay", "~/Ar/Stay.aspx")
        routes.MapPageRoute("Stay-Details-ar", "Ar/Stay-Details/{id}/{t}", "~/Ar/Stay-Details.aspx")
        routes.MapPageRoute("DesertIsland-ar", "Ar/Desert-Islands", "~/Ar/Desert-Island.aspx")
        routes.MapPageRoute("SirBaniYasIsland-ar", "Ar/Sir-Bani-Yas-Island", "~/Ar/Sir-Bani-Yas-Island.aspx")
        routes.MapPageRoute("DelmaIsland-ar", "Ar/Dalma-Island", "~/Ar/Delma-Island.aspx")
        routes.MapPageRoute("Conservation-ar", "Ar/Conservation", "~/Ar/Conservation.aspx")
        routes.MapPageRoute("Disclaimer-ar", "Ar/Disclaimer", "~/Ar/Disclaimer.aspx")
        routes.MapPageRoute("DiscoveryIsland-ar", "Ar/Discovery-Islands", "~/Ar/Discovery-Island.aspx")
        routes.MapPageRoute("Flora-ar", "Ar/Flora", "~/Ar/Flora.aspx")
        routes.MapPageRoute("GoogleSearch-ar", "Ar/Google-Search/{t}", "~/Ar/Google-Search.aspx")
        routes.MapPageRoute("HistoricalTimeline-ar", "Ar/Historical-Timeline", "~/Ar/Historical-Timeline.aspx")
        routes.MapPageRoute("History-ar", "Ar/History", "~/Ar/History.aspx")
        routes.MapPageRoute("MapPop-ar", "Ar/Map-Pop", "~/Ar/Map-Pop.aspx")
        routes.MapPageRoute("IslandMapPop-ar", "Ar/IslandMap-Pop", "~/Ar/IslandMap-Pop.aspx")
        routes.MapPageRoute("Mangrove-ar", "Ar/Mangrove", "~/Ar/Mangrove.aspx")
        routes.MapPageRoute("Play-Your-Trip-ar", "Ar/Play-Your-Trip/{t}", "~/Ar/Play-Your-Trip.aspx")
        routes.MapPageRoute("PrivacyPolicy-ar", "Ar/Privacy-Policy", "~/Ar/PrivacyPolicy.aspx")
        routes.MapPageRoute("SaltDome-ar", "Ar/Salt-Domes", "~/Ar/Salt-Dome.aspx")
        routes.MapPageRoute("SirBaniYasHistory-ar", "Ar/Sir-Bani-Yas-History", "~/Ar/Sir-Bani-Yas-History.aspx")
        routes.MapPageRoute("Sitemap-ar", "Ar/Sitemap", "~/Ar/Sitemap.aspx")
        routes.MapPageRoute("SuccessStory-ar", "Ar/Success-Story/{id}/{t}", "~/Ar/Success-Story.aspx")
        routes.MapPageRoute("TermsCondition-ar", "Ar/Terms-Condition", "~/Ar/TermsCondition.aspx")
        routes.MapPageRoute("ThankyouNewsletter-ar", "Ar/Thankyou-Newsletter", "~/Ar/Thankyou-Newsletter.aspx")
        routes.MapPageRoute("VetConservationTeam-ar", "Ar/Vet-Conservation-Team", "~/Ar/Vet-Conservation-Team.aspx")
        routes.MapPageRoute("VetConservationTeam1-ar", "Ar/Vet-Conservation-Team/{page}", "~/Ar/Vet-Conservation-Team.aspx")
        
        routes.MapPageRoute("VetConservationDetails-ar", "Ar/Vet-Conservation-Details/{id}/{t}", "~/Ar/Vet-Conservation-Details.aspx")
        
        routes.MapPageRoute("BookNOW-ar", "Ar/BookNow-Pop", "~/Ar/BookNow-Pop.aspx")
        routes.MapPageRoute("PopAnimal-ar", "Ar/PhotoPop-Animal/{section}/{id}", "~/Ar/PhotoPop-Animal.aspx")
        routes.MapPageRoute("Flights-ar", "Ar/Flights", "~/Ar/Flights.aspx")
        routes.MapPageRoute("philosophy-ar", "Ar/Philosophy", "~/Ar/philosophy.aspx")
        routes.MapPageRoute("AboutTDIC-ar", "Ar/About-TDIC", "~/Ar/About-TDIC.aspx")
        routes.MapPageRoute("PrivateCharter-ar", "Ar/Private-Charter-Flights", "~/Ar/PrivateCharterFlight.aspx")
        
           
        
    End Sub
    
    Protected Sub Application_BeginRequest(sender As [Object], e As EventArgs)
      
        Dim domainName As String
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
        Dim newCulture As CultureInfo = DirectCast(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
        newCulture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy"
        newCulture.DateTimeFormat.DateSeparator = "/"
        Threading.Thread.CurrentThread.CurrentCulture = newCulture
         
        
    End Sub
    
    
    
</script>