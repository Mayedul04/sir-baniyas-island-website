﻿Imports System.Data.SqlClient

Partial Class MediaLibrary
    Inherits System.Web.UI.Page
    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Protected Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        If MyCaptcha.IsValid Then
            hdnDate.Value = Date.Now
            hdnPass.Value = Mid(System.Guid.NewGuid.ToString(), 1, 6)
            If IsExist() = False Then
                If sdsRegister.Insert() > 0 Then
                    Dim msg As String = "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
                    msg += "<tr><td style=""border:1px solid #ccc;""><table width=""740"" border=""0"" cellspacing=""0"" cellpadding=""0"">From the following Person, we got a media library request.</td>"
                    msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;""> Name</td>"
                    msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtFName.Text & " " & txtLName.Text & "</td></tr>"
                    msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Email Address</td>"
                    msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtEmail.Text & "</td></tr>"
                   
                    msg += "</table></td></tr></table>"


                    Dim emailtemplate As String = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
                    emailtemplate += "<html xmlns=""http://www.w3.org/1999/xhtml"">"
                    emailtemplate += "<head><meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />"
                    emailtemplate += "<title>Sir Bani Yas</title></head>"
                    emailtemplate += "<body style=""margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; font-size: 12px; background-color: #f1f1f1;"">"
                    emailtemplate += "<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
                    emailtemplate += "<tr><td style=""background-color: #f1f1f1;"">"
                    emailtemplate += "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
                    emailtemplate += "<tr><td>&nbsp;</td></tr>"
                    emailtemplate += "<tr><td><a href=""http://www.sirbaniyasisland.com/"" target=""_blank""><img src=""" & domainName & "images/banner-img.jpg"" width=""800"" /></a>"
                    emailtemplate += "</td></tr><tr><td style=""background-color: #fff;"">"
                    emailtemplate += "<table width=""712"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
                    emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
                    emailtemplate += "<tr><td><h1 style=""color: #f58023; text-align: center; text-transform: uppercase; font-weight: normal"">THANK YOU FOR REQUESTING ACCESS TO THE  MEDIA LIBRARY</h1>"
                    emailtemplate += "<h2 style=""color: #333; text-align: center;"">An email will be sent to you with a password shortly</h2></td>"
                    emailtemplate += "</tr><tr><td></td></tr>"
                    emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
                    emailtemplate += "<tr><td style=""height: 52px; text-align: center;""><a href=""mailto:info@tdic.ae"" style=""color: #f58023"">info@tdic.ae</a> or call us"
                    emailtemplate += "800-TDIC (8342)<td></tr></table>"
                    emailtemplate += "</td></tr><tr><td>&nbsp;</td></tr></table></td></tr><tr>"
                    emailtemplate += "<td style="""">&nbsp;</td></tr></table></body></html>"

                    Utility.SendMail(txtFName.Text & " " & txtLName.Text, txtEmail.Text, "lstagg@tdic.ae", "rsanchez@tdic.ae", "mayedul@digitalnexa.com", "Media Library Request", msg)
                    Utility.SendMail("Admin", "noreply@sirbaniyasisland.com", txtEmail.Text, "lstagg@tdic.ae;rsanchez@tdic.ae", "jatin@digitalnexa.com", "Media Library Request", emailtemplate)
                    Response.Redirect(domainName & "Thank-you/media")
                End If
            Else
                lbCaptchaError.Text = "You already have an account. Please login."
            End If

        End If
    End Sub
    Public Function IsExist() As Boolean
        Dim sConn As String
        Dim selectString1 As String = "Select RegisterUserID from RegisteredUser where Email=@Email"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Email", Data.SqlDbType.NVarChar, 50).Value = txtEmail.Text
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            cn.Close()
            Return True
        Else
            cn.Close()
            Return False
        End If

    End Function

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim sConn As String
        Dim selectString1 As String = "Select RegisterUserID, User1 from RegisteredUser where Email=@Email and Pass1=@Pass1 and Approve=1"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Email", Data.SqlDbType.NVarChar, 50).Value = txtEmail1.Text
        cmd.Parameters.Add("Pass1", Data.SqlDbType.NVarChar, 50).Value = txtPass.Text
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            Response.Cookies("RegUser").Value = reader("RegisterUserID").ToString()
            Response.Cookies("RegUser").Expires = Date.Now.AddDays(+1)
            Response.Redirect(domainName & "Media-Library-Loggedin/Photo")
            cn.Close()
        Else
            cn.Close()

        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Not Request.Cookies("RegUser") Is Nothing Then
                Response.Redirect(domainName & "Media-Library-Loggedin/Photo")
           
            End If
        End If
    End Sub

    Protected Sub btnForgotPass_Click(sender As Object, e As EventArgs) Handles btnForgotPass.Click
        Dim pass As String = ""
        Dim name As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select Pass1, FName, LName from RegisteredUser where Email=@Email and Approve=1"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Email", Data.SqlDbType.NVarChar, 50).Value = txtEmail1.Text
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                name = reader("FName").ToString() & " " & reader("LName").ToString()
                pass = reader("Pass1").ToString()
            End While
        Else
            Response.Redirect(domainName & "Thank-you/wrong-media")
        End If
        cn.Close()

        Dim emailtemplate As String = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
        emailtemplate += "<html xmlns=""http://www.w3.org/1999/xhtml"">"
        emailtemplate += "<head><meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />"
        emailtemplate += "<title>Sir Bani Yas</title></head>"
        emailtemplate += "<body style=""margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; font-size: 12px; background-color: #f1f1f1;"">"
        emailtemplate += "<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
        emailtemplate += "<tr><td style=""background-color: #f1f1f1;"">"
        emailtemplate += "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        emailtemplate += "<tr><td>&nbsp;</td></tr>"
        emailtemplate += "<tr><td><a href=""http://www.sirbaniyasisland.com/"" target=""_blank""><img src=""http://sby.nexadesigns.com/images/banner-img.jpg"" width=""800"" /></a>"
        emailtemplate += "</td></tr><tr><td style=""background-color: #fff;"">"
        emailtemplate += "<table width=""712"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
        emailtemplate += "<tr><td><h1 style=""color: #f58023; text-align: center; text-transform: uppercase; font-weight: normal"">Forgot Password! Here is your Credentials </h1>"
        emailtemplate += "<p style=""color: #333; text-align: center;""><strong>Email :</strong> " & name & "</p>"
        emailtemplate += "<p style=""color: #333; text-align: center;""><strong>Password :</strong>  " & pass & "</p>"
        emailtemplate += "</tr><tr><td></td></tr>"
        emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
        emailtemplate += "<tr><td style=""height: 52px; text-align: center;""><a href=""mailto:info@tdic.ae"" style=""color: #f58023"">info@tdic.ae</a> or call us"
        emailtemplate += "800-TDIC (8342)<td></tr></table>"
        emailtemplate += "</td></tr><tr><td>&nbsp;</td></tr></table></td></tr><tr>"
        emailtemplate += "<td style="""">&nbsp;</td></tr></table></body></html>"

        


        

        Utility.SendMail("Admin", "noreply@sirbaniyasisland.com", txtEmail.Text, "lstagg@tdic.ae;rsanchez@tdic.ae", "", "Media Library Password Retrieval", emailtemplate)
        Response.Redirect(domainName & "Thank-You/media")
    End Sub
End Class
