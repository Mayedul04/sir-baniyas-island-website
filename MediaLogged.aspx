﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="MediaLogged.aspx.vb" Inherits="MediaLogged" %>

<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Media-Centre" %>'>Media Centre</a></li>
            <li class="active">Media Library</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">media library</h1>

            </section>
            <!-- -- welcome-text ends here -- -->
        </div>
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <section class="meet-the-animals">

        <div class="container">
            <ul class="list-unstyled row">
                <li class="col-sm-4 aboutDesert mediaSection">
                    <div class="bluebg border-box imgSection <%= GetClass("photo")%>">
                       <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="112" ImageType="Small" IMGSmallHeight="225" IMGSmallWidth="263" ImageClass="boxthumbnail" ShowEdit="true" />
                        <a href='<%= domainName & "Media-Library-Loggedin/Photo" %>' class="linkbtn">Photo Library <span class="arrow"></span></a>
                    </div>
                </li>

                <li class="col-sm-4 aboutDesert mediaSection">
                    <div class="greenbg border-box imgSection <%= GetClass("video")%>">
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" HTMLID="113" ImageType="Small" IMGSmallHeight="225" IMGSmallWidth="263" ImageClass="boxthumbnail" ShowEdit="true" />
                        <a href='<%= domainName & "Media-Library-Loggedin/Video" %>' class="linkbtn">Video Library<span class="arrow"></span></a>
                    </div>
                </li>
                <%--<li class="col-sm-3 aboutDesert mediaSection">
                    <div class="orangebg border-box imgSection <%= GetClass("news")%>">
                         <uc1:UserHTMLImage runat="server" ID="UserHTMLImage3" HTMLID="307" ImageType="Small" IMGSmallHeight="225" IMGSmallWidth="263" ImageClass="boxthumbnail" ShowEdit="true" />

                        <a href='<%= domainName & "Media-Library-Loggedin/News" %>' class="linkbtn">News<span class="arrow"></span></a>
                    </div>
                </li>--%>
                <li class="col-sm-4 aboutDesert mediaSection">
                    <div class="purplebg border-box imgSection <%= GetClass("brochure")%>">
                         <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="114" ImageType="Small" IMGSmallHeight="225" IMGSmallWidth="263" ImageClass="boxthumbnail" ShowEdit="true" />

                        <a href='<%= domainName & "Media-Library-Loggedin/Brochure" %>' class="linkbtn">Brochures<span class="arrow"></span></a>
                    </div>
                </li>
            </ul>
        </div>
    </section>

    <!-- Download Listings -->
    <section class="downloadList">

        <div class="container">

            <div class="row">

                <ul class="downloadListings">

                  <%= LoadPermittedMedia() %>
                </ul>

            </div>

        </div>

    </section>
    <!-- Download Listings -->

    <section class="container">
        <div class="footer-nav">
            <div class="row">

               <div class="col-sm-4 col-sm-offset-2">
                    <a href='<%= domainName & "Island-News" %>' class="orangebg">
                        <img src='<%= domainName & "ui/media/dist/media-center/island-news-icon.png"%>' alt="" style="margin-top: -5px">
                        Island News</a>
                </div>

                <div class="col-sm-4">
                    <a href='<%= domainName & "Press-Release-News" %>' class="bluebg">
                        <img src='<%= domainName & "ui/media/dist/media-center/pressrelese-icon.png"%>' alt="">
                        Press Release</a>
                </div>

                


            </div>
        </div>
    </section>
</asp:Content>

