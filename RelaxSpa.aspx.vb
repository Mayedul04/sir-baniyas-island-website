﻿Imports System.Data.SqlClient

Partial Class RelaxSpa
    Inherits System.Web.UI.Page
    Public domainName As String
    Public pgalid, vgalid As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Public Function HtmlContentTreatement() As String
        Dim M As String = ""
        Dim sConn As String
        Dim selectString1 As String = "SELECT * FROM [List_InterestingFacts] WHERE ([TableName] = 'Treatments' and TableMasterID=24 and Lang=@Lang )"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                M += "<li>"

                M += "<div class=""double-box greenbg"">"
                M += "<div class=""twopanel"">"
                If IsDBNull(reader("Link")) = False Then
                    M += "<h3 class=""title""><a href=""" & reader("Link").ToString() & """ target=""_blank"">" & reader("Title") & "</a></h3>"
                Else
                    M += "<h3 class=""title"">" & reader("Title") & "</h3>"
                End If


                M += reader("BigDetails")


                If IsDBNull(reader("Link")) = False Then
                    M += "<a class=""readmorebtn"" style=""bottom:0 !important"" href=""" & reader("Link").ToString() & """ target=""_blank"">read more<span class=""arrow""></a></span>"
                End If
                M += "<br/>" & Utility.showEditButton(Request, domainName & "Admin/A-RelaxHTML/CommonFactsEdit.aspx?cgid=" & reader("ID") & "&TableName=Treatments&TableMasterID=24&smallImageWidth=252&smallImageHeight=233&Lang=en")
                M += "</div>"
                M += "<div class=""twopanel imageholder"">"
                M += "<img src=""" & domainName & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """>"

                M += "</div>"

                '  M += "<h5 class=""subtitle""><a href=""" & domainName & "Interesting-Facts"" >More</a></h5>"

                M += "</li>"

            End While
        End If
        cn.Close()
        Return M
    End Function
    Public Sub LoadGallery(ByVal galid As Integer)
        Dim sConn As String
        Dim selectString1 As String = "Select * from GalleryItem where  GalleryID=@GalleryID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                lblPhotoGallery.Text += "<a href=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ class=""linkbtn withthumbnail"" rel=""Spa"">Photo Gallery<span class=""arrow""></span></a>"
            End While
        End If
        cn.Close()
    End Sub
    Public Function HtmlContentPDF(ByVal htmlid As Integer) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select * from HTML where  Lang=@Lang and HtmlID=@HtmlID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        cmd.Parameters.Add("HtmlID", Data.SqlDbType.Int, 32).Value = htmlid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ alt="""" class=""transparent"">"
                retstr += "<h3 class=""title"">" & reader("Title").ToString() & "</h3>"
                retstr += "<p>" & reader("SmallDetails").ToString() & "</p>"
                retstr += "<a target=""_blank"" href=""" & domainName & "Admin/" & reader("FileUploaded").ToString() & """ class=""readmorebtn"">Read menu</a>"
                ''  retstr += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")
            End While
        End If
        cn.Close()
        Return retstr
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim sConn As String
            Dim selectString1 As String = "Select GalleryID, VGalleryID from HTML where  HTMLID=@HTMLID and Lang='en'"
            sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
            Dim cn As SqlConnection = New SqlConnection(sConn)
            cn.Open()
            Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
            cmd.Parameters.Add("HTMLID", Data.SqlDbType.NVarChar, 50).Value = 203
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            If reader.HasRows Then
                While reader.Read()
                    If IsDBNull(reader("GalleryID")) = False Then
                        pgalid = reader("GalleryID").ToString()
                    End If
                    If IsDBNull(reader("VGalleryID")) = False Then
                        vgalid = reader("VGalleryID").ToString()
                    End If


                End While
            End If
            cn.Close()
            If pgalid <> 0 Then
                LoadGallery(pgalid)
            End If

        End If
    End Sub
End Class
