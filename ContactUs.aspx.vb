﻿Imports System.Data.SqlClient

Partial Class ContactUs
    Inherits System.Web.UI.Page
    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Protected Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        If MyCaptcha.IsValid Then
            hdnDate.Value = Date.Now()
            If sdsContact.Insert > 0 Then
                Dim emailtemplate As String = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
                emailtemplate += "<html xmlns=""http://www.w3.org/1999/xhtml"">"
                emailtemplate += "<head><meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />"
                emailtemplate += "<title>Arabian Pacific</title></head>"
                emailtemplate += "<body style=""margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; font-size: 12px; background-color: #f1f1f1;"">"
                emailtemplate += "<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
                emailtemplate += "<tr><td style=""background-color: #f1f1f1;"">"
                emailtemplate += "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
                emailtemplate += "<tr><td>&nbsp;</td></tr>"
                emailtemplate += "<tr><td><a href=""http://www.sirbaniyasisland.com/"" target=""_blank""><img src=""" & domainName & "images/banner-img.jpg"" width=""800"" /></a>"
                emailtemplate += "</td></tr><tr><td style=""background-color: #fff;"">"
                emailtemplate += "<table width=""712"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
                emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
                emailtemplate += "<tr><td><h1 style=""color: #f58023; text-align: center; text-transform: uppercase; font-weight: normal"">Thank you for your enquiry</h1>"
                emailtemplate += "<h2 style=""color: #333; text-align: center;"">A representative will be in contact shortly</h2></td>"
                emailtemplate += "</tr><tr><td></td></tr>"
                emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
                emailtemplate += "<tr><td style=""height: 52px; text-align: center;""><a href=""mailto:info@tdic.ae"" style=""color: #f58023"">info@tdic.ae</a> or call us"
                emailtemplate += "800-TDIC (8342)<td></tr></table>"
                emailtemplate += "</td></tr><tr><td>&nbsp;</td></tr></table></td></tr><tr>"
                emailtemplate += "<td style="""">&nbsp;</td></tr></table></body></html>"

                Dim msg As String = "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
                msg += "<tr><td style=""border:1px solid #ccc;""><table width=""740"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
                msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Contact For</td>"
                msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtContactfor.Text & "</td></tr>"
                msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Full Name</td>"
                msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtName.Text & "</td></tr>"
                msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Email Address</td>"
                msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtEmail.Text & "</td></tr>"
                msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Phone</td>"
                msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtPhone.Text & "</td></tr>"
                msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Country</td>"
                msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & ddlCountry.SelectedValue & "</td></tr>"
                msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Message</td>"
                msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtmsg.Text & "</td></tr>"
                msg += "</table></td></tr></table>"


                Utility.SendMail("Admin", "noreply@sirbaniyasisland.com", txtEmail.Text, "info@tdic.ae", "jatin@digitalnexa.com", "Contact Request", emailtemplate)
                Utility.SendMail(txtName.Text, txtEmail.Text, "info@tdic.ae", "", "mayedul.islam@wvss.net", "Contact Us", msg)
                Response.Redirect(domainName & "Thank-you")
            End If


        End If
    End Sub
    Public Function HeadOffice() As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select top 1. * from ContactDetails where Status=1 and Lang=@Lang and HeadOffice=1"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<p>" & reader("Heading").ToString() & "</p><p>"
                retstr += "<span><img src=""" & domainName & "ui/media/dist/contact/fon-icon.jpg"" alt=""""></span>" & reader("Phone").ToString() & "<br>"
                If IsDBNull(reader("Fax")) = False Then
                    retstr += "<span><img src=""" & domainName & "ui/media/dist/contact/fax-icon.jpg"" alt=""""></span>" & reader("Fax").ToString() & "<br>"
                End If
                If IsDBNull(reader("Email")) = False Then
                    retstr += "<span><img src=""" & domainName & "ui/media/dist/contact/mail-icon.jpg"" alt=""""></span><a href=""mailto:" & reader("Email").ToString().ToLower() & """>" & reader("Email").ToString() & "</a><br>"
                End If
                If IsDBNull(reader("WebSite")) = False Then
                    retstr += "<span><img src=""" & domainName & "ui/media/dist/contact/web-icon.jpg"" alt=""""></span><a href=""" & reader("Website").ToString() & """>" & reader("Website").ToString() & "</a></p>"
                End If

                retstr += Utility.showEditButton(Request, domainName & "Admin/A-Contact-Address/LocationEdit.aspx?lid=" & reader("ContactID").ToString())
            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function OtherAddress() As String
        Dim retstr As String = ""
        Dim M As String = ""
        Dim count As Integer = 1
        Dim sConn As String
        Dim selectString1 As String = "Select * from ContactDetails where Status=1 and Lang=@Lang and HeadOffice=0"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<div class=""col-md-4 col-sm-4""><h6>" & reader("Heading").ToString() & " :</h6><p>"
                If IsDBNull(reader("Email")) = False Then
                    retstr += "Email :<a href=""mailto:" & reader("Email").ToString().ToLower() & """>" & reader("Email").ToString() & "</a><br>"
                End If
                If IsDBNull(reader("Phone")) = False Then
                    retstr += "Tel :" & reader("Phone").ToString() & "<br>"
                End If
                If IsDBNull(reader("Fax")) = False Then
                    retstr += "Fax :" & reader("Fax").ToString() & "<br>"
                End If
                retstr += "</p>" & Utility.showEditButton(Request, domainName & "Admin/A-Contact-Address/LocationEdit.aspx?lid=" & reader("ContactID").ToString()) & "</div>"

                If count Mod 3 = 0 Then
                    M += "<div class=""row marginBtm20"">" & retstr & "</div>"
                    retstr = ""
                End If
                count += 1
            End While
            
        End If
        If retstr <> "" Then
            M += "<div class=""row marginBtm20"">" & retstr & "</div>"
            retstr = ""
        End If
        cn.Close()
        Return M
    End Function
    Public Function HtmlContent(ByVal htmlid As Integer) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select * from HTML where  Lang=@Lang and HtmlID=@HtmlID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        cmd.Parameters.Add("HtmlID", Data.SqlDbType.Int, 32).Value = htmlid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<div class=""twopanel""><h3 class=""title""><a href=""" & domainName & "Philosophy" & """>" & reader("Title").ToString() & "</a></h3>"
                'retstr += "<h5 class=""subtitle"">" & reader("SmallDetails").ToString() & "</h5>"
                retstr += "<p>" & reader("SmallDetails").ToString() & "</p><br/><a class=""readmorebtn"" href=""" & domainName & "Philosophy" & """>Read more</a>"
                '& Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0&BigDetails=0") & 
                retstr += "<span class=""arrow""></span></div>"
                retstr += "<div class=""twopanel imageholder""><img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ alt="""" width=""307px""></div>"
            End While
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function HtmlContentMap(ByVal htmlid As Integer) As String
        Dim retstr As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select * from HTML where  Lang=@Lang and HtmlID=@HtmlID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        cmd.Parameters.Add("HtmlID", Data.SqlDbType.Int, 32).Value = htmlid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<div class=""innerMapImgHold""><img src=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ alt="""">"
                retstr += "<div class=""textContent""><h2><a href=""" & domainName & "Map-Pop" & """ class=""mapFancyIframe"">" & reader("Title").ToString() & "</a></h2>"
                'retstr += "<h5 class=""subtitle"">" & reader("SmallDetails").ToString() & "</h5>"
                retstr += "<div class=""content""><p>" & reader("SmallDetails").ToString() & "</p><br><a class=""readmorebtn mapFancyIframe"" href=""" & domainName & "Map-Pop" & """>Click here</a><span class=""arrow""></span></div>"
                '& Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=1&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0&SmallDetails=0") & "</div>"
                retstr += "</div></div>"
            End While
        End If
        cn.Close()
        Return retstr
    End Function
End Class
