﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="TravelSea.aspx.vb" Inherits="TravelSea" %>

<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserHTMLTitle.ascx" TagPrefix="uc1" TagName="UserHTMLTitle" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>
<%@ Register Src="~/CustomControl/UserControlEditButton.ascx" TagPrefix="uc1" TagName="UserControlEditButton" %>






<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->
    <div class="container">
        <ol class="breadcrumb">
            <li><a href='<%= domainName %>'>Home</a></li>
            <li><a href='<%= domainName & "Travel" %>'>Travel</a></li>
            <li class="active">Travel By Sea</li>
        </ol>
    </div>
    <!-- -- Breadcrumb starts here 
    -------------------------------------------- -->

    <!-- Content-area starts here 
    -------------------------------------------- -->
    <section id="Content-area" class="wildlife-section mainsection">
        <div class="container">
            <!-- -- welcome-text starts here -- -->
            <section class="welcome-text">
                <h1 class="capital">Travel By Sea</h1>
                <p>
                    <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt"  HTMLID="63"/>
               
                </p>
                
            </section>
            <!-- -- welcome-text ends here -- -->
            <div class="row">
                <%--<div class="col-sm-8">--%>
                  <div class="col-md-6 col-sm-offset-1">
                    <div class="innerMapImgHold">
                        <asp:Literal ID="ltLoactionMap" runat="server"></asp:Literal>
                    </div>
                </div>
                    <%--<h2 class="bluefont">
                        <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle2" ShowEdit="false" ImageEdit="false"  HTMLID="83" />
                    </h2>
                     <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt3" HTMLID="83" />--%>
                   
                <%--</div>--%>
                <div class="col-sm-4">
                    <div class="row">
                        
                            <div class="orangebg border-box small">
                                 <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" HTMLID="85" ShowEdit="False" ImageType="Small" ImageWidth="390"  ImageClass="boxthumbnail" />
                               
                                <span class="arrow"></span>
                                 <asp:Literal ID="lblPhotoGallery" runat="server"></asp:Literal>
                                    
                                <a href='<%= galurl%>' class="linkbtn withthumbnail" rel="TravelSea">
                                    <uc1:UserHTMLTitle runat="server" ID="UserHTMLTitle4" ShowEdit="False"   HTMLID="85"  />
                                </a>
                            </div>
                            <uc1:UserControlEditButton runat="server" ID="EditPhotoGalleryFooter" HTMLID="85" PGalleryEdit="true"  TitleEdit="true" ImageEdit="true"  ImageType="Small" ImageHeight="225" ImageWidth="405" TextEdit="false" LinkEdit="false" />
                       
                    </div>
                    <%--<div class="imagethumb">
                         <uc1:UserHTMLImage runat="server" ID="UserHTMLImage2" HTMLID="81" ShowEdit="True"  ImageType="Big" />
                        
                    </div>--%>
                </div>
            </div>
             <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="257" />
        </div>
       
    </section>
    <!-- Content-area ends here 
    -------------------------------------------- -->

    <!-- -- section-footer starts here
    -------------------------------------------- -->
    <footer id="section-footer">
        <div class="container">

            <div class="footer-nav">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-2">
                        <a href='<%= domainName & "Travel-Air" %>' class="purplebg">
                            <img src='<%= domainName & "ui/media/dist/icons/air-btn.png"%>' alt="" style="margin-top: -6px">
                            By Air</a>
                    </div>
                    <div class="col-sm-4">
                        <a href='<%= domainName & "Travel-Road" %>' class="orangebg">
                            <img src='<%= domainName & "ui/media/dist/icons/road-btn.png"%>' alt="" style="margin-top: -5px">
                            By Road</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- -- section-footer ends here
    -------------------------------------------- -->
</asp:Content>

